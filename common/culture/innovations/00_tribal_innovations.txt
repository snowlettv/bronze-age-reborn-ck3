﻿#### Local Types ####
@administration_1 = "gfx/interface/icons/culture_innovations/innovation_administration_01.dds"
@administration_2 = "gfx/interface/icons/culture_innovations/innovation_administration_02.dds"
@administration_3 = "gfx/interface/icons/culture_innovations/innovation_administration_03.dds"
@civil_construction_1 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_01.dds"
@civil_construction_2 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_02.dds"
@leadership_1 = "gfx/interface/icons/culture_innovations/innovation_leadership_01.dds"
@leadership_2 = "gfx/interface/icons/culture_innovations/innovation_leadership_02.dds"
@raised_banner = "gfx/interface/icons/culture_innovations/innovation_raised_banner.dds"
@fortifications = "gfx/interface/icons/culture_innovations/innovation_fortifications.dds"
@siege_weapons = "gfx/interface/icons/culture_innovations/innovation_siege_weapons.dds"
@levy_building = "gfx/interface/icons/culture_innovations/innovation_levy_building.dds"
@maa_01 = "gfx/interface/icons/culture_innovations/innovation_maa_01.dds"
@maa_02 = "gfx/interface/icons/culture_innovations/innovation_maa_02.dds"
@weapons_and_armor_01 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_01.dds"
@weapons_and_armor_02 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_02.dds"
@knight = "gfx/interface/icons/culture_innovations/innovation_knight.dds"
@majesty_01 = "gfx/interface/icons/culture_innovations/innovation_majesty_01.dds"
@majesty_02 = "gfx/interface/icons/culture_innovations/innovation_majesty_02.dds"
@majesty_03 = "gfx/interface/icons/culture_innovations/innovation_majesty_03.dds"
@nobility_01 = "gfx/interface/icons/culture_innovations/innovation_nobility_01.dds"
@nobility_02 = "gfx/interface/icons/culture_innovations/innovation_nobility_02.dds"
@nobility_03 = "gfx/interface/icons/culture_innovations/innovation_nobility_03.dds"
@nobility_04 = "gfx/interface/icons/culture_innovations/innovation_nobility_04.dds"
@misc_inventions = "gfx/interface/icons/culture_innovations/innovation_misc_inventions.dds"

@camel = "gfx/interface/icons/culture_innovations/innovation_camel.dds"
@elephant = "gfx/interface/icons/culture_innovations/innovation_elephant.dds"
@special_maa_01 = "gfx/interface/icons/culture_innovations/innovation_special_maa_01.dds"
@special_maa_02 = "gfx/interface/icons/culture_innovations/innovation_special_maa_02.dds"


#culture_group_military
innovation_earthworks = { #Fort 1
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_fortifications.dds"

	custom = unlock_early_bronze_age_fortification_buildings

	flag = global_regular
	flag = tribal_era_regular
}

innovation_barracks = { #Military 1
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_barracks.dds"

	custom = unlock_early_bronze_age_military_buildings

	flag = global_regular
	flag = tribal_era_regular
}

innovation_raiding_parties = { # Misc
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_raiding_expeditions.dds"

	unlock_building = tribe_02

	unlock_casus_belli = raiding_party_cb

	flag = global_regular
	flag = tribal_era_regular
}

innovation_mustering_grounds = { # Maa cap 1
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_mustering_grounds.dds"

	character_modifier = {
		men_at_arms_cap = 1
		men_at_arms_limit = 2
	}

	flag = global_regular
	flag = tribal_era_regular
}

innovation_tribal_vassals = { # Misc 
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_tribal_vassals.dds"

	custom = unlock_march_contract
	
	character_modifier = {
		tribal_government_vassal_opinion = 10
		active_accolades = 1
	}

	flag = global_regular
	flag = tribal_era_regular
}

innovation_bronze_socket_axe = { #Maa 1
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_bronze_socket_axe.dds"

	unlock_maa = ba_axemen

	flag = global_regular
	flag = tribal_era_regular
}

innovation_siege_ladders = { # Maa 1
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_siege_weapons.dds"

	unlock_maa = siege_ladders

	flag = global_regular
	flag = tribal_era_regular
}

#Civic

innovation_city_planning = { # Development 1
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_city_planning.dds"
	
	county_modifier = {
		building_slot_add = 1
	}
	
	custom = reduce_develop_county_penalty_01

	flag = global_regular
	flag = tribal_era_regular
}

innovation_early_palaces = { # Palatial 1
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_early_palaces.dds"

	character_modifier = {
		diplomatic_range_mult = 0.25
	}

	custom = unlocks_first_duchy_buildings
	#custom = less_negative_impact_from_distance_01

	custom = urban_can_become_palatial

	flag = global_regular
	flag = tribal_era_regular
}

innovation_centralized_irrigation = { # Agri 1
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_centralized_irrigation.dds"

	character_modifier = {
		domain_limit = 1
	}
	custom = unlock_early_bronze_age_agriculture_buildings

	flag = global_regular
	flag = tribal_era_regular
}

innovation_potters_wheel = { # Econ 1
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_potter_wheel.dds"
	
	county_modifier = {
		building_slot_add = 1
	}
	
	custom = unlock_early_bronze_age_economic_buildings

	flag = global_regular
	flag = tribal_era_regular
}

innovation_city_states = { # Misc
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_city_states.dds"

	unlock_building = castle_01

	unlock_law = city_confederation_elective_succession_law

	flag = global_regular
	flag = tribal_era_regular
}

innovation_kingship = { # Misc
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_kingship.dds"

	unlock_law = high_partition_succession_law
	
	custom = can_designate_heir

	character_modifier = {
		domain_limit = 1
	}

	flag = global_regular
	flag = tribal_era_regular
}

innovation_kings_justice = { # Law 1
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_king_justice.dds"

	unlock_law = crown_authority_1

	flag = global_regular
	flag = tribal_era_regular
}

innovation_border_stones = { # Misc
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_border_stones.dds"

	character_modifier = {
		domain_limit = 1
	}

	unlock_casus_belli = individual_duchy_de_jure_cb

	custom = fabricate_claim_speed
	custom = cb_discount_prestige_10

	flag = global_regular
	flag = tribal_era_regular
}

#innovation_cults = { # Misc
#	group = culture_group_civic
#	culture_era = culture_era_tribal
#	icon = "gfx/interface/icons/culture_innovations/innovation_cults.dds"
#
#	character_modifier = {
#		monthly_piety_gain_mult = 0.1
#	}
#
#	custom = enables_cults
#
#	flag = global_regular
#	flag = tribal_era_regular
#}
#innovation_tombs = { 
#	group = culture_group_civic
#	culture_era = culture_era_tribal
#	icon = "gfx/interface/icons/culture_innovations/innovation_cults.dds"
#
#	character_modifier = {
#		monthly_dynasty_prestige_mult = 0.05
#	}
#
#	custom = enables_tombs
#
#	flag = global_regular
#	flag = tribal_era_regular
#}
innovation_permanent_settlements = { 
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_cults.dds"

	character_modifier = {
	}

	unlock_building = city_01
	unlock_building = temple_01
	unlock_building = monastic_schools_01
	unlock_building = megalith_01
	unlock_building = market_villages_01

	custom = tribes_can_become_feudal

	flag = global_regular
	flag = tribal_era_regular
}
#Based on spread, non-region
innovation_writing = {
	group = culture_group_civic
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_writing.dds"

	can_progress = {
		custom_description = {
			text = culture_exposed_to_writing
			has_variable = writing_exposure
		}
	}
	character_modifier = {
		character_capital_county_monthly_development_growth_add = 1
	}

	unlock_building = scribal_school_01
	unlock_building = library_01
	unlock_building = scriptorium_01

	custom = enables_writing

	flag = global_regional
	flag = tribal_era_regional
}
innovation_spoked_wheel = {
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_spoked_wheel.dds"

	can_progress = {
		custom_description = {
			text = culture_exposed_to_spoked_wheel
			has_variable = spoked_wheel_exposure
		}
	}
	character_modifier = {
		knight_limit = 4
		knight_effectiveness_mult = 0.2
	}

	unlock_building = horse_breeder_01

	unlock_maa = light_chariot
	
	custom = eligible_accolade_horse_archer_attribute

	#custom = enables_charioteering

	flag = global_regional
	flag = tribal_era_regional
}
innovation_composite_bow = {
	group = culture_group_military
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_composite_bow.dds"

	can_progress = {
		custom_description = {
			text = culture_exposed_to_composite_bows
			has_variable = composite_bow_exposure
		}
	}
	character_modifier = {
		archer_cavalry_damage_mult = 0.1
		archers_damage_mult = 0.1
	}

	flag = global_regional
	flag = tribal_era_regional
}

#culture_group_regional
innovation_aegean_raiders = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	region = world_aegean_raiders
	icon = "gfx/interface/icons/culture_innovations/innovation_aegean_raiders.dds"

	character_modifier = {
		embarkation_cost_mult = -0.75
		naval_movement_speed_mult = 0.25
	}

	custom = unlocks_naval_raiding
	custom = unlocks_sailable_major_rivers

	flag = global_regional
	flag = tribal_era_regional
}
innovation_dilmun_magan_seafarers = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	region = world_persian_gulf
	icon = "gfx/interface/icons/culture_innovations/innovation_dilmunite_seafarers.dds"

	character_modifier = {
		embarkation_cost_mult = -0.75
		naval_movement_speed_mult = 0.25
	}

	custom = unlocks_naval_raiding
	custom = unlocks_sailable_major_rivers

	flag = global_regional
	flag = tribal_era_regional
}
innovation_papyrus = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	region = world_nile_valley
	icon = "gfx/interface/icons/culture_innovations/innovation_papyrus.dds"

	unlock_building = papyrus_farms_01
	unlock_building = papyrus_press_01

	character_modifier = {
		cultural_head_fascination_mult = 0.05
	}

	flag = global_regional
	flag = tribal_era_regional
}
innovation_grain_rations = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	region = world_sumer
	icon = "gfx/interface/icons/culture_innovations/innovation_grain_rations.dds"

	character_modifier = {
		county_opinion_add = 5
		development_growth_factor = 0.05
	}

	flag = global_regional
	flag = tribal_era_regional
}
# Culture-Specific Innovations
innovation_ba_migrators = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_migrators.dds"
	
	potential = {
		has_fp1_dlc_trigger = yes
		OR = {
			has_innovation = innovation_ba_migrators
			this = culture:amoritic
			any_parent_culture_or_above = {
				this = culture:amoritic #Amoritic
			}
			this = culture:aramean
			any_parent_culture_or_above = {
				this = culture:aramean #Aramean/Ahlamu
			}
			this = culture:proto_greek
			any_parent_culture_or_above = {
				this = culture:proto_greek #Proto-Greek
			}
		}
		NOT = { has_cultural_era_or_later = culture_era_early_medieval }
	}
	
	custom = unlock_ba_migration_cb
	
	flag = global_regional
	flag = tribal_era_regional
}