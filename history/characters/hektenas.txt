﻿#Generated
179 = {
	name = "Nanas"
	dynasty = 179
	religion = "aegean_faith"
	culture = "hektenas"
	761.7.14 = {
		birth = yes
	}
	803.8.8 = {
		death = yes
	}
}
214 = {
	name = "Amphiktyon"
	dynasty = 214
	religion = "aegean_faith"
	culture = "hektenas"
	748.8.20 = {
		birth = yes
	}
	817.8.16 = {
		death = yes
	}
}
226 = {
	name = "Nireus"
	dynasty = 226
	religion = "aegean_faith"
	culture = "hektenas"
	750.11.2 = {
		birth = yes
	}
	802.4.26 = {
		death = yes
	}
}
485 = {
	name = "Eurystheus"
	dynasty = 485
	religion = "aegean_faith"
	culture = "hektenas"
	754.3.8 = {
		birth = yes
	}
	823.9.23 = {
		death = yes
	}
}
1372 = {
	name = "Nykteus"
	dynasty = 1372
	religion = "aegean_faith"
	culture = "hektenas"
	1276.9.6 = {
		birth = yes
	}
	1323.1.2 = {
		death = yes
	}
}