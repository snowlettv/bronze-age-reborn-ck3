# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:2halab1
bookmark_mursili_halab={
	type=male
	id=1162
	age=0.370000
	genes={ 		hair_color={ 117 235 120 233 }
 		skin_color={ 150 160 153 158 }
 		eye_color={ 62 248 69 249 }
 		gene_chin_forward={ "chin_forward_pos" 165 "chin_forward_pos" 141 }
 		gene_chin_height={ "chin_height_pos" 155 "chin_height_pos" 131 }
 		gene_chin_width={ "chin_width_neg" 117 "chin_width_neg" 126 }
 		gene_eye_angle={ "eye_angle_pos" 162 "eye_angle_pos" 159 }
 		gene_eye_depth={ "eye_depth_pos" 135 "eye_depth_pos" 132 }
 		gene_eye_height={ "eye_height_pos" 136 "eye_height_neg" 123 }
 		gene_eye_distance={ "eye_distance_neg" 116 "eye_distance_neg" 119 }
 		gene_eye_shut={ "eye_shut_pos" 127 "eye_shut_neg" 105 }
 		gene_forehead_angle={ "forehead_angle_neg" 122 "forehead_angle_pos" 140 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 85 "forehead_brow_height_neg" 127 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 67 "forehead_roundness_neg" 49 }
 		gene_forehead_width={ "forehead_width_pos" 162 "forehead_width_neg" 118 }
 		gene_forehead_height={ "forehead_height_neg" 76 "forehead_height_neg" 44 }
 		gene_head_height={ "head_height_neg" 123 "head_height_pos" 140 }
 		gene_head_width={ "head_width_neg" 98 "head_width_pos" 160 }
 		gene_head_profile={ "head_profile_neg" 114 "head_profile_neg" 67 }
 		gene_head_top_height={ "head_top_height_pos" 131 "head_top_height_pos" 140 }
 		gene_head_top_width={ "head_top_width_neg" 110 "head_top_width_neg" 106 }
 		gene_jaw_angle={ "jaw_angle_neg" 83 "jaw_angle_pos" 127 }
 		gene_jaw_forward={ "jaw_forward_neg" 124 "jaw_forward_pos" 128 }
 		gene_jaw_height={ "jaw_height_neg" 117 "jaw_height_pos" 134 }
 		gene_jaw_width={ "jaw_width_pos" 145 "jaw_width_neg" 102 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_pos" 139 "mouth_corner_depth_pos" 131 }
 		gene_mouth_corner_height={ "mouth_corner_height_neg" 124 "mouth_corner_height_neg" 124 }
 		gene_mouth_forward={ "mouth_forward_pos" 132 "mouth_forward_neg" 119 }
 		gene_mouth_height={ "mouth_height_neg" 121 "mouth_height_neg" 124 }
 		gene_mouth_width={ "mouth_width_neg" 118 "mouth_width_neg" 120 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 132 "mouth_upper_lip_size_pos" 163 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 200 "mouth_lower_lip_size_pos" 135 }
 		gene_mouth_open={ "mouth_open_neg" 16 "mouth_open_neg" 88 }
 		gene_neck_length={ "neck_length_pos" 149 "neck_length_pos" 131 }
 		gene_neck_width={ "neck_width_neg" 112 "neck_width_neg" 116 }
 		gene_bs_cheek_forward={ "cheek_forward_neg" 25 "cheek_forward_pos" 33 }
 		gene_bs_cheek_height={ "cheek_height_neg" 39 "cheek_height_neg" 6 }
 		gene_bs_cheek_width={ "cheek_width_neg" 26 "cheek_width_neg" 25 }
 		gene_bs_ear_angle={ "ear_angle_neg" 18 "ear_angle_neg" 26 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 15 "ear_inner_shape_pos" 90 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 47 "ear_upper_bend_pos" 31 }
 		gene_bs_ear_outward={ "ear_outward_pos" 28 "ear_outward_pos" 0 }
 		gene_bs_ear_size={ "ear_size_neg" 50 "ear_size_neg" 8 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 112 "eye_corner_depth_neg" 72 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 35 "eye_fold_shape_pos" 126 }
 		gene_bs_eye_size={ "eye_size_pos" 90 "eye_size_pos" 9 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_pos" 63 "eye_upper_lid_size_neg" 84 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 102 "forehead_brow_curve_pos" 1 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 18 "forehead_brow_forward_pos" 49 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 60 "forehead_brow_inner_height_neg" 38 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 49 "forehead_brow_outer_height_pos" 23 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 2 "forehead_brow_width_pos" 80 }
 		gene_bs_jaw_def={ "jaw_def_neg" 45 "jaw_def_neg" 29 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 14 "mouth_lower_lip_def_pos" 66 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 79 "mouth_lower_lip_full_pos" 24 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 116 "mouth_lower_lip_pad_pos" 51 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_pos" 101 "mouth_lower_lip_width_pos" 2 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 48 "mouth_philtrum_def_pos" 44 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_pos" 4 "mouth_philtrum_shape_pos" 107 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_pos" 47 "mouth_philtrum_width_pos" 16 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 9 "mouth_upper_lip_def_pos" 6 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_neg" 38 "mouth_upper_lip_full_pos" 123 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_pos" 44 "mouth_upper_lip_profile_pos" 12 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 29 "mouth_upper_lip_width_pos" 33 }
 		gene_bs_nose_forward={ "nose_forward_neg" 1 "nose_forward_neg" 11 }
 		gene_bs_nose_height={ "nose_height_pos" 86 "nose_height_pos" 71 }
 		gene_bs_nose_length={ "nose_length_neg" 56 "nose_length_neg" 46 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_pos" 69 "nose_nostril_height_pos" 3 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_neg" 81 "nose_nostril_width_neg" 190 }
 		gene_bs_nose_profile={ "nose_profile_pos" 23 "nose_profile_pos" 111 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_neg" 17 "nose_ridge_angle_neg" 78 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 30 "nose_ridge_width_pos" 8 }
 		gene_bs_nose_size={ "nose_size_pos" 106 "nose_size_pos" 11 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 24 "nose_tip_angle_neg" 160 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 94 "nose_tip_forward_pos" 12 }
 		gene_bs_nose_tip_width={ "nose_tip_width_pos" 188 "nose_tip_width_neg" 129 }
 		face_detail_cheek_def={ "cheek_def_02" 24 "cheek_def_01" 10 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 91 "cheek_fat_02_pos" 8 }
 		face_detail_chin_cleft={ "chin_cleft" 4 "chin_cleft" 18 }
 		face_detail_chin_def={ "chin_def" 102 "chin_def" 242 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 176 "eye_lower_lid_def" 175 }
 		face_detail_eye_socket={ "eye_socket_02" 95 "eye_socket_03" 251 }
 		face_detail_nasolabial={ "nasolabial_01" 74 "nasolabial_02" 13 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 6 "nose_ridge_def_pos" 217 }
 		face_detail_nose_tip_def={ "nose_tip_def" 229 "nose_tip_def" 75 }
 		face_detail_temple_def={ "temple_def" 24 "temple_def" 216 }
 		expression_brow_wrinkles={ "brow_wrinkles_02" 193 "brow_wrinkles_04" 68 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 255 "eye_wrinkles_01" 80 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_02" 241 "forehead_wrinkles_01" 27 }
 		expression_other={ "cheek_wrinkles_both_01" 0 "cheek_wrinkles_both_01" 0 }
 		complexion={ "complexion_5" 191 "complexion_1" 254 }
 		gene_height={ "normal_height" 128 "normal_height" 117 }
 		gene_bs_body_type={ "body_fat_head_fat_medium" 134 "body_fat_head_fat_medium" 139 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 186 "body_shape_hourglass_full" 0 }
 		gene_bs_bust={ "bust_clothes" 98 "bust_shape_3_half" 143 }
 		gene_age={ "old_4" 69 "old_4" 216 }
 		gene_eyebrows_shape={ "avg_spacing_high_thickness" 241 "close_spacing_low_thickness" 250 }
 		gene_eyebrows_fullness={ "layer_2_high_thickness" 174 "layer_2_high_thickness" 176 }
 		gene_body_hair={ "body_hair_avg" 219 "body_hair_dense" 227 }
 		gene_hair_type={ "hair_straight" 168 "hair_wavy" 121 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 97 "normal_eyes" 97 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "normal_eyelashes" 225 "normal_eyelashes" 225 }
 		pose={ "" 255 "" 0 }
 		beards={ "fp3_iranian_beards_straight" 158 "no_beard" 0 }
 		hairstyles={ "fp3_hairstyles_iranian_straight" 83 "all_hairstyles" 0 }
 		legwear={ "mena_nobility_legwear" 24 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "fp3_iranian_high_nobility_clothes" 111 "most_clothes" 0 }
 		headgear={ "fp3_iranian_royalty" 109 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 2697330347 2697330347 }
	tags={ {
			hash=2125534279
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=2793867254
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 {
			hash=1485265044
			invert=no
		}
 }
}

