﻿e_amurru = {
	1310.1.1 = {
		effect = {
			set_capital_county = title:c_halab
		}
	}
}
k_amurru = {
	1310.1.1 = {
		holder = 1047
	}
	1311.1.1 = {
		holder = 0
	}
}
k_alasiya = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	1310.1.1 = {
		holder = 2alashiya1
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_kouklia = {
	785.1.1 = {
		holder = 141
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1086
	}
	1311.1.1 = {
		holder = 0
	}
}
c_kouklia = {
	785.1.1 = {
		holder = 141
	}
	1310.1.1 = {
		holder = 1086
	}
}
c_bamboula = {
	785.1.1 = {
		holder = 141
	}
	1310.1.1 = {
		holder = 1086
	}
}
####
#d_amorosa
c_amorosa = {
	785.1.1 = {
		holder = 140
	}
	1310.1.1 = {
		liege = "d_kouklia"
		holder = 1652
	}
	1311.1.1 = {
		liege = 0
	}
}
c_pappa = {
	785.1.1 = {
		holder = 140
	}
	1310.1.1 = {
		holder = 1086
	}
}
c_vouni = {
	785.1.1 = {
		holder = 801
	}
	1310.1.1 = {
		liege = "d_kouklia"
		holder = 1653
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_kalabasos
c_kalabasos = {
	785.1.1 = {
		holder = 802
	}
	1310.1.1 = {
		liege = "d_alasiya"
		holder = 1654
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ora = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 803
	}
	1310.1.1 = {
		liege = "d_philia"
		holder = 1655
	}
	1311.1.1 = {
		liege = 0
	}
}
c_akhna = {
	785.1.1 = {
		holder = 804
	}
	1310.1.1 = {
		holder = 2alashiya1
	}
}
b_3149 = {
	1310.1.1 = {
		holder = 2alashiya1
	}
}
####
d_alasiya = {
	1310.1.1 = {
		holder = 2alashiya1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_alasiya = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 138
	}
	1310.1.1 = {
		holder = 2alashiya1
	}
}
c_liopetri = {
	785.1.1 = {
		holder = 138
	}
	1310.1.1 = {
		holder = 2alashiya1
	}
}
####
d_ledra = {
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1085
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_ledra = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 139
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1085
	}
	1311.1.1 = {
		liege = 0
	}
}
c_golgoi = {
	785.1.1 = {
		holder = 139
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1085
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_philia = {
	785.1.1 = {
		holder = 137
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1083
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_philia = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 137
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1083
	}
	1311.1.1 = {
		liege = 0
	}
}
c_morphou = {
	785.1.1 = {
		holder = 137
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1083
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_vounous
c_vounous = {
	785.1.1 = {
		holder = 805
	}
	1310.1.1 = {
		liege = "d_ledra"
		holder = 1656
	}
	1311.1.1 = {
		liege = 0
	}
}
c_laphitos = {
	785.1.1 = {
		holder = 137
	}
	1310.1.1 = {
		liege = "k_alasiya"
		holder = 1083
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_rialonsa
c_rialonsa = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 806
	}
	1310.1.1 = {
		liege = "d_alasiya"
		holder = 1657
	}
	1311.1.1 = {
		liege = 0
	}
}
c_alaas = {
	785.1.1 = {
		holder = 138
	}
	1310.1.1 = {
		holder = 2alashiya1
	}
}
####
k_ugarit = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
}
####
d_ugarit = {
	785.1.1 = {
		holder = 74
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1046
	}
	1311.1.1 = {
		holder = 0
	}
}
c_ugarit = {
	785.1.1 = {
		holder = 74
	}
	1310.1.1 = {
		holder = 1046
	}
}
c_gibala = {
	785.1.1 = {
		holder = 74
	}
	1310.1.1 = {
		holder = 1046
	}
}
c_kesab = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 108
	}
	1310.1.1 = {
		liege = "d_ugarit"
		holder = 1221
	}
	1311.1.1 = {
		liege = 0
	}
}
c_suhurre = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 108
	}
	1310.1.1 = {
		liege = "d_ugarit"
		holder = 1221
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_suksi = {
	1310.1.1 = {
		holder = 1046
	}
	1311.1.1 = {
		holder = 0
	}
}
c_suksi = {
	785.1.1 = {
		liege = "d_ugarit"
		holder = 807
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1046
	}
}
c_bayda = {
	785.1.1 = {
		holder = 75
	}
	1310.1.1 = {
		liege = "d_suksi"
		holder = 1658
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_arwada = {
	785.1.1 = {
		holder = 75
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1047
		de_jure_liege = k_amurru
	}
	1311.1.1 = {
		holder = 0
	}
}
c_sumura = {
	785.1.1 = {
		holder = 75
	}
	1310.1.1 = {
		holder = 1047
	}
}
c_arwada = {
	785.1.1 = { change_development_level = 8 }
	785.1.1 = { change_development_level = 14 }
	1310.1.1 = { change_development_level = 15 }
	1310.1.1 = { change_development_level = 24 }
	785.1.1 = {
		holder = 75
	}
	1310.1.1 = {
		holder = 1047
	}
}
c_akkari = {
	785.1.1 = {
		holder = 80
	}
	1310.1.1 = {
		holder = 1047
	}
}
####
k_qatna = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	1310.1.1 = {
		holder = 2qatna1
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_amatu = {
	1310.1.1 = {
		holder = 2qatna1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_amatu = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_qatna"
		holder = 72
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2qatna1
	}
}
c_sinzar = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_qatna"
		holder = 72
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2qatna1
	}
}
c_aweed = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 808
	}
	1310.1.1 = {
		liege = "d_amatu"
		holder = 1659
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_qatna = {
	785.1.1 = {
		holder = 71
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2qatna1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_qatna = {
	785.1.1 = { change_development_level = 13 }
	1310.1.1 = { change_development_level = 23 }
	785.1.1 = {
		holder = 71
	}
	1310.1.1 = {
		holder = 2qatna1
	}
}
c_dabaa = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		liege = "d_qatna"
		holder = 809
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_qatna"
		holder = 1660
	}
	1311.1.1 = {
		liege = 0
	}
}
c_tunanapa = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 71
	}
	1310.1.1 = {
		holder = 2qatna1
	}
}
####
d_qadesh = {
	785.1.1 = {
		holder = 70
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 979
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_qadesh = {
	785.1.1 = { change_development_level = 13 }
	1310.1.1 = { change_development_level = 23 }
	785.1.1 = {
		holder = 70
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 979
	}
	1311.1.1 = {
		liege = 0
	}
}
c_mansuate = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 810
	}
	1310.1.1 = {
		liege = "d_qatna"
		holder = 1661
	}
	1311.1.1 = {
		liege = 0
	}
}
c_khanaser = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		liege = "d_qadesh"
		holder = 811
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 979
	}
	1311.1.1 = {
		liege = 0
	}
}
c_shairat = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		liege = "d_qadesh"
		holder = 812
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 979
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_ebla = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = ebla1
	}
	786.1.1 = {
		holder = 0
	}
}
####
d_jawbas = {
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1223
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_jawbas = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 813
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1223
	}
	1311.1.1 = {
		liege = 0
	}
}
c_saraqib = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 814
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1223
	}
	1311.1.1 = {
		liege = 0
	}
}
c_gharbiya = {
	785.1.1 = {
		liege = "d_ebla"
		holder = 815
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_jawbas"
		holder = 1662
	}
	1311.1.1 = {
		liege = 0
	}
}
c_atil = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 816
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1223
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_qatan = {
	1310.1.1 = {
		holder = 1210
		de_jure_liege = k_halab
	}
	1311.1.1 = {
		holder = 0
	}
}
c_qatan = {
	785.1.1 = {
		holder = 89
	}
	1310.1.1 = {
		holder = 1210
	}
}
c_tunip = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 89
	}
	1310.1.1 = {
		holder = 2qatna1
	}
}
c_arzigana = {
	785.1.1 = {
		holder = 88
	}
	1310.1.1 = {
		holder = 1210
	}
}
####
d_ebla = {
	785.1.1 = {
		holder = ebla1
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1222
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_ebla = {
	785.1.1 = { change_development_level = 18 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		holder = ebla1
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1222
	}
	1311.1.1 = {
		liege = 0
	}
}
c_arahti = {
	785.1.1 = { change_development_level = 14 }
	1310.1.1 = { change_development_level = 24 }
	785.1.1 = {
		holder = ebla1
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1222
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hassu = {
	785.1.1 = { change_development_level = 14 }
	1310.1.1 = { change_development_level = 24 }
	785.1.1 = {
		holder = ebla1
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1222
	}
	1311.1.1 = {
		liege = 0
	}
}
c_zulabi = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 817
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1222
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hanaya = {
	785.1.1 = {
		holder = ebla1
	}
	1310.1.1 = {
		liege = "d_ebla"
		holder = 1663
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_tavneel = {
	1310.1.1 = {
		liege = "k_qatna"
		de_jure_liege = k_qatna
		holder = 980
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_tavneel = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 104
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 980
	}
	1311.1.1 = {
		liege = 0
	}
}
c_soha = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 104
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 980
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_dinanu = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 87
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1209
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_urume = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 87
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1209
	}
	1311.1.1 = {
		liege = 0
	}
}
c_idiba = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 87
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1209
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_halab = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	1090.1.1 = {
		holder = 2halab12
	}
	1120.1.1 = {
		holder = 2halab11
	}
	1136.1.1 = {
		holder = 2halab10
	}
	1150.1.1 = {
		holder = 2halab9
	}
	1180.1.1 = {
		holder = 2halab8
	}
	1200.1.1 = {
		holder = 2halab7
	}
	1225.1.1 = {
		holder = 2halab6
	}
	1229.1.1 = {
		holder = 2halab5
	}
	1238.1.1 = {
		holder = 2halab4
	}
	1275.1.1 = {
		holder = 2halab3
	}
	1300.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_hazazu = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 98
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1215
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_hazazu = {
	785.1.1 = { change_development_level = 7 }
	1310.1.1 = { change_development_level = 14 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 98
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1215
	}
	1311.1.1 = {
		liege = 0
	}
}
c_sarmin = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_hazazu"
		holder = 818
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hazazu"
		holder = 1664
	}
	1311.1.1 = {
		liege = 0
	}
}
c_anadan = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_hazazu"
		holder = 819
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1215
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_halab = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 73
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_halab = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 73
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
}
c_arpadda = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 73
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
}
c_arada = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 73
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
}
c_arne = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		liege = "d_halab"
		holder = 86
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
}
c_arsoun = {
	785.1.1 = {
		liege = "d_halab"
		holder = 86
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2halab1
	}
}
c_amim = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_halab"
		holder = 820
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_halab"
		holder = 1665
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_qatuma = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 97
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1214
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_qatuma = {
	785.1.1 = { change_development_level = 7 }
	1310.1.1 = { change_development_level = 14 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 97
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1214
	}
	1311.1.1 = {
		liege = 0
	}
}
c_pittina = {
	785.1.1 = { change_development_level = 7 }
	1310.1.1 = { change_development_level = 14 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 97
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1214
	}
	1311.1.1 = {
		liege = 0
	}
}
c_eltiten = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_qatuma"
		holder = 821
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_qatuma"
		holder = 1666
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_mukis = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	1165.1.1 = {
		holder = 2alalakh4
	}
	1221.1.1 = {
		holder = 2alalakh3
	}
	1265.1.1 = {
		holder = 2alalakh2
	}
	1305.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_alalakh = {
	785.1.1 = {
		holder = 90
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_alalakh = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		holder = 90
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
}
c_sawran = {
	785.1.1 = {
		holder = 90
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
}
c_barga = {
	785.1.1 = {
		holder = 88
	}
	1310.1.1 = {
		liege = "d_alalakh"
		holder = 1667
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_meroe = {
	785.1.1 = {
		holder = 91
	}
	786.1.1 = {
		holder = 0
	}
}
c_meroe = {
	785.1.1 = {
		holder = 91
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
}
c_mouth_of_orontes = {
	785.1.1 = {
		holder = 91
	}
	1310.1.1 = {
		holder = 2alalakh1
	}
}
####
#d_mukis
c_mukis = {
	785.1.1 = {
		liege = "d_alalakh"
		holder = 822
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_alalakh"
		holder = 1668
	}
	1311.1.1 = {
		liege = 0
	}
}
c_marea = {
	785.1.1 = {
		liege = "d_alalakh"
		holder = 823
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_alalakh"
		holder = 1669
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ikinkalis = {
	785.1.1 = {
		liege = "d_zalwar"
		holder = 824
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_alalakh"
		holder = 1670
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_zalwar = {
	785.1.1 = { change_development_level = 7 }
	1310.1.1 = { change_development_level = 14 }
}
####
d_zalwar = {
	785.1.1 = {
		holder = 92
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_mukis"
		holder = 1211
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_zalwar = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 92
	}
	1310.1.1 = {
		liege = "k_mukis"
		holder = 1211
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ziadiyah = {
	785.1.1 = {
		holder = 92
	}
	1310.1.1 = {
		liege = "k_mukis"
		holder = 1211
	}
	1311.1.1 = {
		liege = 0
	}
}
c_tsurgi = {
	785.1.1 = {
		holder = 825
	}
	1310.1.1 = {
		liege = "d_zalwar"
		holder = 1671
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_samalla = {
	785.1.1 = {
		holder = 93
	}
	786.1.1 = {
		holder = 0
	}
}
c_samalla = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 93
	}
	1310.1.1 = {
		liege = "d_marqasa"
		holder = 1672
	}
	1311.1.1 = {
		liege = 0
	}
}
c_herbel = {
	785.1.1 = {
		liege = "d_samalla"
		holder = 826
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hazazu"
		holder = 1673
	}
	1311.1.1 = {
		liege = 0
	}
}
c_pabniri = {
	785.1.1 = {
		liege = "d_samalla"
		holder = 827
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hazazu"
		holder = 1674
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_marqasa = {
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1212
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_marqasa = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 94
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1212
	}
	1311.1.1 = {
		liege = 0
	}
}
c_suamani = {
	785.1.1 = {
		holder = 94
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1212
	}
	1311.1.1 = {
		liege = 0
	}
}
c_eraditae = {
	785.1.1 = {
		holder = 93
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1212
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_bulbulhum = {
	785.1.1 = {
		holder = 95
	}
	786.1.1 = {
		holder = 0
	}
}
c_bulbulhum = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 95
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1212
	}
	1311.1.1 = {
		liege = 0
	}
}
c_shutime = {
	785.1.1 = {
		liege = "d_bulbulhum"
		holder = 828
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_halpi"
		holder = 1675
	}
	1311.1.1 = {
		liege = 0
	}
}
c_punyiti = {
	785.1.1 = {
		liege = "d_bulbulhum"
		holder = 829
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_halpi"
		holder = 1676
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_karkamisha = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
}
####
#d_sarugu
c_sarugu = {
	785.1.1 = {
		holder = 232
	}
	1310.1.1 = {
		liege = "d_upperpurattu"
		holder = 1677
	}
	1311.1.1 = {
		liege = 0
	}
}
c_shurqu = {
	785.1.1 = {
		holder = 232
	}
	1310.1.1 = {
		liege = "d_karkamisa"
		holder = 1678
	}
	1311.1.1 = {
		liege = 0
	}
}
c_parrisu = {
	785.1.1 = {
		liege = "d_karkamisa"
		holder = 830
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_karkamisa"
		holder = 1679
	}
	1311.1.1 = {
		liege = 0
	}
}
c_irridu = {
	785.1.1 = {
		holder = 236
	}
	1310.1.1 = {
		liege = "d_harran"
		holder = 1680
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_karkamisa = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		holder = 100
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1217
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_karkamisa = {
	785.1.1 = {
		holder = 100
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1217
	}
	1311.1.1 = {
		liege = 0
	}
}
c_tobeh = {
	785.1.1 = {
		liege = "d_urshu"
		holder = 831
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_karkamisa"
		holder = 1681
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hassuwa = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		liege = "d_urshu"
		holder = 832
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1216
	}
	1311.1.1 = {
		liege = 0
	}
}
c_tuba = {
	785.1.1 = {
		holder = 100
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1217
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_urshu = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 99
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1216
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_urshu = {
	785.1.1 = { change_development_level = 10 }
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 99
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1216
	}
	1311.1.1 = {
		liege = 0
	}
}
c_urumu = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 99
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1216
	}
	1311.1.1 = {
		liege = 0
	}
}
c_awtiye = {
	785.1.1 = {
		liege = "d_urshu"
		holder = 833
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_urshu"
		holder = 1682
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_emar = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
}
####
d_emar = {
	785.1.1 = {
		holder = 102
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_emar = {
	785.1.1 = { change_development_level = 14 }
	1310.1.1 = { change_development_level = 24 }
	785.1.1 = {
		holder = 102
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
	}
}
c_yaharissa = {
	785.1.1 = {
		holder = 102
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
	}
}
c_rawdah = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		liege = "d_emar"
		holder = 834
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_emar"
		holder = 1683
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ekalte = {
	785.1.1 = {
		holder = 102
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_hermesh = {
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1220
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_hermesh = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 106
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1220
	}
	1311.1.1 = {
		liege = 0
	}
}
c_arraba = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_ebla"
		holder = 106
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1220
	}
	1311.1.1 = {
		liege = 0
	}
}
c_malal = {
	785.1.1 = {
		holder = 835
	}
	1310.1.1 = {
		liege = "d_hermesh"
		holder = 1684
	}
	1311.1.1 = {
		liege = 0
	}
}
c_zoran = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_dub"
		holder = 836
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hermesh"
		holder = 1685
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_dub = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		liege = "k_ebla"
		holder = 101
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1218
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_dub = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 101
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1218
	}
	1311.1.1 = {
		liege = 0
	}
}
c_houz = {
	785.1.1 = {
		liege = "d_dub"
		holder = 837
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_dub"
		holder = 1686
	}
	1311.1.1 = {
		liege = 0
	}
}
c_eliyahu = {
	785.1.1 = {
		liege = "k_ebla"
		holder = 101
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1218
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_azu = {
	785.1.1 = {
		holder = 103
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_halab"
		de_jure_liege = k_halab
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_azu = {
	785.1.1 = {
		holder = 103
	}
	1310.1.1 = {
		liege = "k_halab"
		holder = 1219
	}
	1311.1.1 = {
		liege = 0
	}
}
c_basiru = {
	785.1.1 = {
		holder = 103
	}
	1310.1.1 = {
		liege = "d_emar"
		holder = 1687
	}
	1311.1.1 = {
		liege = 0
	}
}
c_pituru = {
	785.1.1 = {
		liege = "d_azu"
		holder = 838
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_karkamisa"
		holder = 1688
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_tuttul = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
}
####
d_bitadini = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	1310.1.1 = {
		holder = 1260
	}
	1311.1.1 = {
		holder = 0
	}
}
c_musharu = {
	785.1.1 = {
		holder = 245
	}
	1310.1.1 = {
		holder = 1260
	}
}
c_shinnu = {
	785.1.1 = {
		holder = 839
	}
	1310.1.1 = {
		holder = 1260
	}
}
c_ditillu = {
	785.1.1 = {
		holder = 245
	}
	1310.1.1 = {
		holder = 1260
	}
}
####
d_halabit = {
	785.1.1 = {
		holder = 238
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1267
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_lasqum = {
	785.1.1 = {
		holder = 238
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1267
	}
	1311.1.1 = {
		liege = 0
	}
}
c_huppudu = {
	785.1.1 = {
		liege = "d_tuttul"
		holder = 840
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_tuttul"
		holder = 1689
	}
	1311.1.1 = {
		liege = 0
	}
}
c_nuru = {
	785.1.1 = {
		holder = 238
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1267
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_abattum = {
	785.1.1 = {
		holder = 233
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1259
	}
	1311.1.1 = {
		holder = 0
	}
}
c_abattum = {
	785.1.1 = {
		holder = 233
	}
	1310.1.1 = {
		holder = 1259
	}
}
c_suru = {
	785.1.1 = {
		holder = 233
	}
	1310.1.1 = {
		holder = 1259
	}
}
####
d_tuttul = {
	785.1.1 = {
		holder = 234
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1259
	}
	1311.1.1 = {
		holder = 0
	}
}
c_tuttul = {
	785.1.1 = {
		holder = 234
	}
	1310.1.1 = {
		holder = 1259
	}
}
c_azmu = {
	785.1.1 = {
		holder = 234
	}
	1310.1.1 = {
		holder = 1259
	}
}
c_natalu = {
	785.1.1 = {
		holder = 234
	}
	1310.1.1 = {
		liege = "d_tuttul"
		holder = 1690
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#k_tadmor
####
d_tadmor_oasis = {
	785.1.1 = {
		holder = 96
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1213
	}
	1311.1.1 = {
		holder = 0
	}
}
c_tadmor_oasis = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 96
	}
	1310.1.1 = {
		holder = 1213
	}
}
c_kamra = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 96
	}
	1310.1.1 = {
		holder = 1213
	}
}
c_sushkallu = {
	1310.1.1 = { change_development_level = 3 }
	785.1.1 = {
		holder = 841
	}
	1310.1.1 = {
		liege = "d_tadmor_oasis"
		holder = 1691
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_amorite_homeland = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 244
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1261
	}
	1311.1.1 = {
		holder = 0
	}
}
c_ekkemu = {
	785.1.1 = {
		holder = 244
	}
	1310.1.1 = {
		holder = 1261
	}
}
c_mashiru = {
	785.1.1 = {
		holder = 244
	}
	1310.1.1 = {
		holder = 1261
	}
}
c_sakkuku = {
	785.1.1 = {
		liege = "d_amorite_homeland"
		holder = 842
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_amorite_homeland"
		holder = 1692
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_nazala = {
	1310.1.1 = {
		holder = 981
		de_jure_liege = k_qatna
	}
	1311.1.1 = {
		holder = 0
	}
}
c_nazala = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 107
	}
	1310.1.1 = {
		holder = 981
	}
}
c_sadad = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		holder = 107
	}
	1310.1.1 = {
		holder = 981
	}
}
c_elelbawi = {
	1310.1.1 = { change_development_level = 3 }
	785.1.1 = {
		holder = 107
	}
	1310.1.1 = {
		liege = "d_tadmor_oasis"
		holder = 1693
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_akash = {
	1310.1.1 = {
		de_jure_liege = k_qatna
	}
}
c_akash = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		holder = 105
	}
	1310.1.1 = {
		liege = "k_qatna"
		holder = 980
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hilal = {
	1310.1.1 = { change_development_level = 3 }
	785.1.1 = {
		holder = 105
	}
	1310.1.1 = {
		liege = "d_tadmor_oasis"
		holder = 1694
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_mari = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		holder = mari1
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2khana1
		name = KHANA_NAME
		succession_laws = { city_confederation_elective_succession_law }
		effect = {
			set_capital_county = title:c_terqa
		}
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_mari = {
	785.1.1 = {
		holder = mari1
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1266
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_mari = {
	785.1.1 = { change_development_level = 16 }
	1310.1.1 = { change_development_level = 27 }
	785.1.1 = {
		holder = mari1
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1266
	}
	1311.1.1 = {
		liege = 0
	}
}
c_arbatu = {
	785.1.1 = {
		holder = mari1
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1266
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_terqa = {
	785.1.1 = {
		holder = mari1
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2khana1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_terqa = {
	785.1.1 = { change_development_level = 12 }
	1310.1.1 = { change_development_level = 21 }
	785.1.1 = {
		holder = mari1
	}
	1310.1.1 = {
		holder = 2khana1
	}
}
c_yalihum = {
	785.1.1 = {
		liege = "d_terqa"
		holder = 843
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2khana1
	}
}
####
d_haradu = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		liege = "k_mari"
		holder = 242
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
}
####
c_haradu = {
	785.1.1 = {
		liege = "k_mari"
		holder = 242
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1283
	}
}
c_elelu = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		liege = "k_mari"
		holder = 242
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_mari"
		holder = 1695
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_usala = {
	785.1.1 = {
		liege = "k_mari"
		holder = 241
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1268
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_usala = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "k_mari"
		holder = 241
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1268
	}
	1311.1.1 = {
		liege = 0
	}
}
c_padan = {
	785.1.1 = {
		liege = "k_mari"
		holder = 241
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_mari"
		holder = 1268
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_kipina = {
	785.1.1 = {
		liege = "k_mari"
		holder = 239
	}
	786.1.1 = {
		liege = 0
		holder = 0
	}
	1310.1.1 = {
		holder = 2khana1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_saggaratum = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "k_mari"
		holder = 239
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2khana1
	}
}
c_bararu = {
	785.1.1 = {
		holder = 238
	}
	1310.1.1 = {
		liege = "d_kipina"
		holder = 1696
	}
	1311.1.1 = {
		liege = 0
	}
}
c_duryahdun = {
	785.1.1 = {
		liege = "k_mari"
		holder = 239
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2khana1
	}
}
####