﻿#k_egyptian_oases
##d_kharga_oasis ###################################
###c_kharga_oasis
4800 = { #Kenmet
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
	
	1310.1.1 = {
		holding = castle_holding
	}
}
4801 = { #Maqet
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
4802 = { #Aber
	holding = none
}
4803 = { #Sekhtu
	holding = none
}
###c_hebut
4762 = { #Hebut
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
	
	785.1.1 = {
		special_building_slot = RICE_kharga_temple_of_hibis
	}
	
	1310.1.1 = {
		holding = castle_holding
	}
}
4797 = { #Akhsesef
	holding = none
}
4798 = { #Nabtu
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
4799 = { #Fekh
	holding = none
}
###c_harnata
4767 = { #Harnata
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
}
4766 = { #Hainu
	holding = none
}
4768 = { #Putchu
	holding = none
}
###c_khepera
4770 = { #Khepera
	culture = upper_egyptian
	religion = egyptian_faith
	holding = tribal_holding
}
4769 = { #Putukhipa
	holding = none
}
4773 = { #Sekhem
	holding = none
}
##d_dakhla_oasis
###c_dakhla_oasis
4794 = { #Whet-rasw
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
}
4795 = { #Seqai
	holding = none
}
4792 = { #Qenbet
	holding = none
}
4763 = { #Nut
	holding = none
}
4796 = { #Nubti
	holding = none
}
4793 = { #Par-Mewat
	holding = none
}
###c_hetgat
4779 = { #Hetgat
	culture = temehu
	religion = berber_faith
	holding = tribal_holding
}
4777 = { #Nuttiu
	holding = none
}
###c_babai
4776 = { #Babai
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
}
4772 = { #Ermen
	holding = none
}
4778 = { #Hettiu
	holding = none
}
##d_bahariya_oasis
###c_bahariya_oasis
4765 = { #Psôbthis
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
	
	1310.1.1 = {
		holding = castle_holding
	}
}
4810 = { #Nubau
	holding = none
}
4809 = { #Qerqer
	holding = none
}
4813 = { #Maati
	holding = none
}
4812 = { #Nutiu
	holding = none
}
4811 = { #Seshem
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
###c_bathit
4786 = { #Bathit
	culture = temehu
	religion = egyptian_faith
	holding = tribal_holding
}
4785 = { #Khemiu
	holding = none
}
4774 = { #Amset
	holding = none
}
##d_farafra_oasis
###c_farafra_oasis
4806 = { #Te-jahew
	culture = temehu
	religion = berber_faith
	holding = tribal_holding
}
4764 = { #Feqat
	holding = none
}
4805 = { #Qernet
	holding = none
}
4807 = { #Ateptu
	holding = none
}
4808 = { #Ahehi
	holding = none
}
###c_nekhti
4783 = { #Nekhti
	culture = temehu
	religion = berber_faith
	holding = tribal_holding
}
4775 = { #Amtcher
	holding = none
}
4784 = { #Batchar
	holding = none
}
###c_emkhet
4781 = { #Emkhet
	culture = temehu
	religion = berber_faith
	holding = tribal_holding
}
4780 = { #Khebkheb
	holding = none
}
4782 = { #Anith
	holding = none
}