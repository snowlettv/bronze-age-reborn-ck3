﻿ur = {
	county = c_ur
	barony = b_6182
	
	character_modifier = {
		name = holy_site_ur_effect_name
		development_growth_factor = 0.1
		stewardship = 1
	}
}

uruk = {
	county = c_uruk
	barony = b_6022
	
	character_modifier = {
		name = holy_site_uruk_effect_name
		cultural_head_fascination_mult = 0.05
		monthly_county_control_growth_factor = 0.05
	}
}

nippur = {
	county = c_nippur
	barony = b_6000
	
	character_modifier = {
		name = holy_site_nippur_effect_name
		religious_vassal_opinion = 5
		sway_scheme_phase_duration_add = -10
	}
}

dilmun = {
	county = c_dilmun
	barony = b_5666
	
	character_modifier = {
		name = holy_site_dilmun_effect_name
		monthly_piety_gain_mult = 0.1
	}
}

lagash = {
	county = c_lagash
	barony = b_5969
	
	character_modifier = {
		name = holy_site_lagash_effect_name
		learning_per_piety_level = 2
	}
}

liyan = {
	county = c_liyan
	barony = b_2976
	
	character_modifier = {
		name = holy_site_liyan_effect_name
		diplomacy_per_piety_level = 1
	}
}

anshan = {
	county = c_anshan
	barony = b_6552
	
	character_modifier = {
		name = holy_site_anshan_effect_name
		stewardship = 1
	}
}

susan = {
	county = c_susa
	barony = b_6374
	
	character_modifier = {
		name = holy_site_susan_effect_name
		martial_per_piety_level = 1
		knight_effectiveness_mult = 0.1
	}
}

huhnuri = {
	county = c_huhnuri
	barony = b_6479
	
	character_modifier = {
		name = holy_site_huhnuri_effect_name
		monthly_piety_gain_mult = 0.05
		learning = 1
	}
}

qirbit = {
	county = c_qirbit
	barony = b_6275
	
	character_modifier = {
		name = holy_site_qirbit_effect_name
		councillor_opinion = 5
	}
}

nagar = {
	county = c_nagar
	barony = b_4998
	
	character_modifier = {
		name = holy_site_nagar_effect_name
		development_growth_factor = 0.1
		castle_holding_build_gold_cost = -0.05
	}
}

urkesh = {
	county = c_urkesh
	barony = b_5194
	
	character_modifier = {
		name = holy_site_urkesh_effect_name
		monthly_piety_gain_mult = 0.05
		learning_per_piety_level = 1
	}
}

karkamisa = {
	county = c_karkamisa
	barony = b_4362
	
	character_modifier = {
		name = holy_site_karkamisa_effect_name
		diplomacy_per_piety_level = 1
	}
}

harran = {
	county = c_harran
	barony = b_455
	
	character_modifier = {
		name = holy_site_harran_effect_name
		diplomacy = 1
	}
}

lawazantiya = {
	county = c_lawazantiya
	barony = b_3466
	
	character_modifier = {
		name = holy_site_lawazantiya_effect_name
		county_opinion_add = 10
	}
}

shusharra = {
	county = c_susarra
	barony = b_5627
	
	character_modifier = {
		name = holy_site_shusharra_effect_name
		prowess_per_piety_level = 1
		max_combat_roll = 4
	}
}

simurrum = {
	county = c_simurrum
	barony = b_5573
	
	character_modifier = {
		name = holy_site_simurrum_effect_name
		monthly_piety_gain_per_knight_mult = 0.02
		knight_effectiveness_mult = 0.1
	}
}

arrapha = {
	county = c_arrapha
	barony = b_5478
	
	character_modifier = {
		name = holy_site_arrapha_effect_name
		diplomacy_per_piety_level = 1
	}
}

gasur = {
	county = c_gasur
	barony = b_5450
	
	character_modifier = {
		name = holy_site_gasur_effect_name
		diplomacy_per_piety_level = 1
	}
}

henennesut = {
	county = c_henennesut
	barony = b_2571
	
	character_modifier = {
		name = holy_site_henennesut_effect_name
		cultural_head_fascination_mult = 0.1
	}
}

khmun = {
	county = c_khmun
	barony = b_1399
	
	character_modifier = {
		name = holy_site_khmun_effect_name
		levy_size = 0.05
		monthly_county_control_growth_factor = 0.1
	}
}

mennefer = {
	county = c_mennefer
	barony = b_2261
	
	character_modifier = {
		name = holy_site_mennefer_effect_name
		intrigue_per_piety_level = 1
	}
}

perwadjet = {
	county = c_perwadjet
	barony = b_4472
	
	character_modifier = {
		name = holy_site_perwadjet_effect_name
		learning = 2
	}
}

waset = {
	county = c_waset
	barony = b_1331
	
	character_modifier = {
		name = holy_site_waset_effect_name
		prowess = 2
		knight_effectiveness_mult = 0.2
	}
}

kerma = {
	county = c_kerma
	barony = b_1288
	
	character_modifier = {
		name = holy_site_kerma_effect_name
		stewardship_per_piety_level = 1
	}
}

sye = {
	county = c_sye
	barony = b_2271
	
	character_modifier = {
		name = holy_site_sye_effect_name
		diplomacy = 1
		stewardship = 1
	}
}

semna = {
	county = c_semna
	barony = b_2255
	
	character_modifier = {
		name = holy_site_semna_effect_name
		stewardship = 1
	}
}

sinisri = {
	county = c_sinisri
	barony = b_2456
	
	character_modifier = {
		name = holy_site_sinisri_effect_name
		diplomacy_per_piety_level = 1
	}
}

nubt = {
	county = c_nubt
	barony = b_1232
	
	character_modifier = {
		name = holy_site_nubt_effect_name
		stewardship_per_piety_level = 1
	}
}

siwa = {
	county = c_siwa
	barony = b_4902
	
	character_modifier = {
		name = holy_site_siwa_effect_name
		development_growth_factor = 0.1
		stewardship = 1
	}
}

timsahe = {
	county = c_timsahe
	barony = b_4904
	
	character_modifier = {
		name = holy_site_timsahe_effect_name
		martial_per_piety_level = 1
	}
}

tablate = {
	county = c_tablate
	barony = b_4915
	
	character_modifier = {
		name = holy_site_tablate_effect_name
		stress_loss_mult = 0.2
	}
}

farafra = {
	county = c_farafra_oasis
	barony = b_4806
	
	character_modifier = {
		name = holy_site_farafra_effect_name
		learning_per_piety_level = 1
	}
}

bahariya = {
	county = c_bahariya_oasis
	barony = b_4765
	
	character_modifier = {
		name = holy_site_bahariya_effect_name
		learning_per_piety_level = 1
		monthly_prestige_gain_mult = 0.2
	}
}

philia = {
	county = c_philia
	barony = b_3110
	
	character_modifier = {
		name = holy_site_philia_effect_name
		development_growth_factor = 0.1
		stewardship = 1
	}
}

ledra = {
	county = c_ledra
	barony = b_3127
	
	character_modifier = {
		name = holy_site_ledra_effect_name
		same_faith_opinion = 5
		domain_tax_same_faith_mult = 0.1
	}
}

alasiya = {
	county = c_alasiya
	barony = b_3135
	
	character_modifier = {
		name = holy_site_alasiya_effect_name
		defender_advantage = 5
		diplomacy = 1
	}
}

pappa = {
	county = c_pappa
	barony = b_3170
	
	character_modifier = {
		name = holy_site_pappa_effect_name
		same_faith_opinion = 10
	}
}

kalabasos = {
	county = c_kalabasos
	barony = b_3153
	
	character_modifier = {
		name = holy_site_kalabasos_effect_name
		learning_per_piety_level = 1
		monthly_prestige_gain_mult = 0.2
	}
}

yerushalem = {
	county = c_jerusalem
	barony = b_4242
	
	character_modifier = {
		name = holy_site_yerushalem_effect_name
		clergy_opinion = 5
		personal_scheme_phase_duration_add = -10
	}
}

gaza = {
	county = c_gaza
	barony = b_4218
	
	character_modifier = {
		name = holy_site_gaza_effect_name
		stewardship_per_piety_level = 1
	}
}

rabbatammon = {
	county = c_rabbatammon
	barony = b_4160
	
	character_modifier = {
		name = holy_site_rabbatammon_effect_name
		learning_per_piety_level = 1
	}
}

samaria = {
	county = c_samaria
	barony = b_4244
	
	character_modifier = {
		name = holy_site_samaria_effect_name
		learning = 1
	}
}

hazor = {
	county = c_hazor
	barony = b_3975
	
	character_modifier = {
		name = holy_site_hazor_effect_name
		stewardship_per_piety_level = 1
	}
}

acre = {
	county = c_acre
	barony = b_3997
	
	character_modifier = {
		name = holy_site_acre_effect_name
		stress_loss_mult = 0.2
	}
}

tyre = {
	county = c_tyre
	barony = b_3982
	
	character_modifier = {
		name = holy_site_tyre_effect_name
		fertility = 0.10
		monthly_lifestyle_xp_gain_mult = 0.15
	}
}

sidon = {
	county = c_sidon
	barony = b_3905
	
	character_modifier = {
		name = holy_site_sidon_effect_name
		learning = 2
	}
}

byblos = {
	county = c_gubla
	barony = b_3887
	
	character_modifier = {
		name = holy_site_byblos_effect_name
		diplomacy_per_piety_level = 1
	}
}

arwad = {
	county = c_arwada
	barony = b_3553
	
	character_modifier = {
		name = holy_site_arwad_effect_name
		negate_learning_penalty_add = 2
		learning_per_piety_level = 1
	}
}

ebla = {
	county = c_ebla
	barony = b_3701
	
	character_modifier = {
		name = holy_site_ebla_effect_name
		diplomacy = 1
		learning = 1
	}
}

qadesh = {
	county = c_qadesh
	barony = b_3700
	
	character_modifier = {
		name = holy_site_qadesh_effect_name
		martial_per_piety_level = 1
	}
}

qatna = {
	county = c_qatna
	barony = b_3842
	
	character_modifier = {
		name = holy_site_qatna_effect_name
		stewardship = 1
		monthly_prestige_gain_mult = 0.05
	}
}

halab = {
	county = c_halab
	barony = b_3651
	
	character_modifier = {
		name = holy_site_halab_effect_name
		domain_tax_mult = 0.02
	}
}

tadmor = {
	county = c_tadmor_oasis
	barony = b_4008
	
	character_modifier = {
		name = holy_site_tadmor_effect_name
		monthly_county_control_growth_factor = 0.1
	}
}

sakkuku = { #Fictional Amorite homeland site
	county = c_sakkuku
	barony = b_444
	
	character_modifier = {
		name = holy_site_sakkuku_effect_name
		learning = 2
		clergy_opinion = 5
	}
}

mari = {
	county = c_mari
	barony = b_5924
	
	character_modifier = {
		name = holy_site_mari_effect_name
		monthly_piety_gain_per_knight_mult = 0.01
		knight_effectiveness_mult = 0.1
	}
}

emar = {
	county = c_emar
	barony = b_3730
	
	character_modifier = {
		name = holy_site_emar_effect_name
		diplomacy = 1
		powerful_vassal_opinion = 5
	}
}

ugarit = {
	county = c_ugarit
	barony = b_3524
	
	character_modifier = {
		name = holy_site_ugarit_effect_name
		health = 0.1
	}
}

dodona = {
	county = c_dodona
	barony = b_1721
	
	character_modifier = {
		name = holy_site_dodona_effect_name
		fertility = 0.15
	}
}

olympos = {
	county = c_dion
	barony = b_1798
	
	character_modifier = {
		name = holy_site_olympos_effect_name
		stewardship = 1
		courtier_and_guest_opinion = 5
	}
}

delphi = {
	county = c_delphi
	barony = b_392
	
	character_modifier = {
		name = holy_site_delphi_effect_name
		martial = 1
		hard_casualty_modifier = -0.1
	}
}

olympia = {
	county = c_lepreo
	barony = b_166
	
	character_modifier = {
		name = holy_site_olympia_effect_name
		monthly_piety_gain_mult = 0.1
	}
}

delos = {
	county = c_mykonos
	barony = b_1012
	
	character_modifier = {
		name = holy_site_delos_effect_name
		stress_loss_mult = 0.2
		life_expectancy = 1
	}
}

pylos = {
	county = c_pylos
	barony = b_108
	
	character_modifier = {
		name = holy_site_pylos_effect_name
		stewardship = 1
		sway_scheme_phase_duration_add = -20
	}
}

knossos = {
	county = c_knossos
	barony = b_1090
	
	character_modifier = {
		name = holy_site_knossos_effect_name
		county_opinion_add = 5
		different_culture_opinion = 5
	}
}

thera = {
	county = c_thera
	barony = b_1036
	
	character_modifier = {
		name = holy_site_thera_effect_name
		direct_vassal_opinion = 5
	}
}

apasa = {
	county = c_apasa
	barony = b_1385
	
	character_modifier = {
		name = holy_site_apasa_effect_name
		monthly_prestige_gain_mult = 0.1
	}
}

lemnos = {
	county = c_poliochne
	barony = b_2234
	
	character_modifier = {
		name = holy_site_lemnos_effect_name
		learning = 1
		tyranny_gain_mult = -0.2
	}
}

tarhuntassa = {
	county = c_tarhuntassa
	barony = b_7048
	
	character_modifier = {
		name = holy_site_tarhuntassa_effect_name
		diplomacy_per_prestige_level = 1
	}
}

tarsa = {
	county = c_tarsa
	barony = b_3359
	
	character_modifier = {
		name = holy_site_tarsa_effect_name
		hills_advantage = 5
	}
}

wiyanawanda = {
	county = c_wiyanawanda
	barony = b_3320
	
	character_modifier = {
		name = holy_site_wiyanawanda_effect_name
		county_opinion_add = 5
		different_faith_opinion = 5
	}
}

millawanda = {
	county = c_millawanda
	barony = b_1470
	
	character_modifier = {
		name = holy_site_millawanda_effect_name
		monthly_piety_gain_per_knight_mult = 0.01
	}
}

wilion = {
	county = c_wilion
	barony = b_1477
	
	character_modifier = {
		name = holy_site_wilion_effect_name
		dynasty_house_opinion = 5
	}
}

hattusha = {
	county = c_hattusha
	barony = b_3103
	
	character_modifier = {
		name = holy_site_hattusha_effect_name
		diplomacy_per_piety_level = 1
	}
}

kussara = {
	county = c_kussara
	barony = b_1230
	
	character_modifier = {
		name = holy_site_kussara_effect_name
		learning_per_piety_level = 1
	}
}

ankuwa = {
	county = c_ankuwa
	barony = b_3177
	
	character_modifier = {
		name = holy_site_ankuwa_effect_name
		build_speed = -0.15
	}
}

shappiduwa = {
	county = c_shappiduwa
	barony = b_6911
	
	character_modifier = {
		name = holy_site_shappiduwa_effect_name
		same_culture_opinion = 5
	}
}

ulma = {
	county = c_ulma
	barony = b_1386
	
	character_modifier = {
		name = holy_site_ulma_effect_name
		build_gold_cost = -0.05
	}
}

kanesh = {
	county = c_kanesh
	barony = b_6958
	
	character_modifier = {
		name = holy_site_kanesh_effect_name
		stewardship = 1
	}
}

tuwanuwa = {
	county = c_tuwanuwa
	barony = b_7015
	
	character_modifier = {
		name = holy_site_tuwanuwa_effect_name
		same_faith_opinion = 5
	}
}

phaistos = {
	county = c_phaistos
	barony = b_1138
	
	character_modifier = {
		name = holy_site_phaistos_effect_name
		build_gold_cost = -0.1
		stewardship = 1
	}
}

zakros = {
	county = c_zakros
	barony = b_1118
	
	character_modifier = {
		name = holy_site_zakros_effect_name
		diplomacy = 1
		clergy_opinion = 5
	}
}

kudoni = {
	county = c_kydonia
	barony = b_1088
	
	character_modifier = {
		name = holy_site_kudoni_effect_name
		monthly_piety_gain_mult = 0.1
	}
}

akkad = {
	county = c_akkad
	barony = b_6126
	
	character_modifier = {
		name = holy_site_akkad_effect_name
		learning = 1
		county_opinion_add = 5
	}
}

eshnunna = {
	county = c_eshnunna
	barony = b_5753
	
	character_modifier = {
		name = holy_site_eshnunna_effect_name
		monthly_piety_gain_per_knight_mult = 0.02
		heavy_infantry_damage_mult = 0.05
	}
}

assur = {
	county = c_ashur
	barony = b_5303
	
	character_modifier = {
		name = holy_site_assur_effect_name
		knight_effectiveness_mult = 0.2
		prowess_per_piety_level = 1
	}
}

kish = {
	county = c_kish
	barony = b_6046
	
	character_modifier = {
		name = holy_site_kish_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}
hutwaret = {
	county = c_hutwaret
	barony = b_4563
	
	character_modifier = {
		name = holy_site_hutwaret_effect_name
		prowess_per_piety_level = 1
		supply_duration = 0.2
	}
}
akkuddu = {
	county = c_akkuddu
	barony = b_6596
	
	character_modifier = {
		name = holy_site_akkuddu_effect_name
		monthly_piety_gain_mult = 0.1
	}
}
marubishtu = {
	county = c_marubishtu
	barony = b_6640
	
	character_modifier = {
		name = holy_site_marubishtu_effect_name
		mountains_advantage = 5
		learning = 1
	}
}
halman = {
	county = c_halman
	barony = b_5658
	
	character_modifier = {
		name = holy_site_halman_effect_name
		stewardship = 1
	}
}
babylon_ba = {
	county = c_babylon
	barony = b_6087
	
	character_modifier = {
		name = holy_site_babylon_effect_name
		direct_vassal_opinion = 5
	}
}
isin = {
	county = c_isin
	barony = b_6029
	
	character_modifier = {
		name = holy_site_isin_effect_name
		diplomacy_per_piety_level = 1
	}
}
luluban = {
	county = c_luluban
	barony = b_6201
	
	character_modifier = {
		name = holy_site_luluban_effect_name
		intrigue_per_piety_level = 1
	}
}
kanara = {
	county = c_kanara
	barony = b_5638
	
	character_modifier = {
		name = holy_site_kanara_effect_name
		martial_per_piety_level = 1
		knight_effectiveness_mult = 0.1
	}
}

#EVERYTHING BELOW WIP

#Mesopotamian

failaka = {
	county = c_failaka
	barony = b_1995
	
	character_modifier = {
		name = holy_site_failaka_effect_name
		stewardship = 1
		county_opinion_add = 5
	}
}

hili = {
	county = c_hili
	barony = b_6879
	
	character_modifier = {
		name = holy_site_hili_effect_name
		stewardship_per_piety_level = 1
	}
}

nineveh = {
	county = c_nineveh
	barony = b_5288
	
	character_modifier = {
		name = holy_site_nineveh_effect_name
		monthly_piety_gain_per_knight_mult = 0.02
		heavy_infantry_damage_mult = 0.05
	}
}

urbilum = {
	county = c_urbilum
	barony = b_5407
	
	character_modifier = {
		name = holy_site_urblium_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}

sippar = {
	county = c_sippar
	barony = b_6059
	
	character_modifier = {
		name = holy_site_sippar_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}

eridu = {
	county = c_eridu
	barony = b_5998
	
	character_modifier = {
		name = holy_site_eridu_effect_name
		learning = 1
		county_opinion_add = 5
	}
}

barsipa = {
	county = c_barsipa
	barony = b_6084
	
	character_modifier = {
		name = holy_site_barsipa_effect_name
		learning_per_piety_level = 1
	}
}

larsa = {
	county = c_larsa
	barony = b_6183
	
	character_modifier = {
		name = holy_site_larsa_effect_name
		stewardship_per_piety_level = 1
		fertility = 0.1
	}
}

#Egyptian
iunu = {
	county = c_iunu
	barony = b_4575
	
	character_modifier = {
		name = holy_site_iunu_effect_name
		learning = 2
	}
}
akhetaten = {
	county = c_herrut
	barony = b_1636
	
	character_modifier = {
		name = holy_site_akhetaten_effect_name
		prowess_per_piety_level = 1
		max_combat_roll = 4
	}
}
behdet = {
	county = c_behdet
	barony = b_2376
	
	character_modifier = {
		name = holy_site_behdet_effect_name
		cultural_head_fascination_mult = 0.1
	}
}

#Levantine
damascus_ba = {
	county = c_damascus
	barony = b_4030

	character_modifier = {
		name = holy_site_damascus_ba_effect_name
		levy_size = 0.05
		monthly_county_control_growth_factor = 0.1
	}
}

#Jewish
bethel = {
	county = c_siloh
	barony = b_4260

	character_modifier = {
		name = holy_site_bethel_effect_name
		defender_advantage = 5
		diplomacy = 1
	}
}

seir = {
	county = c_petra
	barony = b_4432

	character_modifier = {
		name = holy_site_seir_effect_name
		same_faith_opinion = 10
	}
}

sinai_ba = {
	county = c_makhait
	barony = b_1523

	character_modifier = {
		name = holy_site_sinai_ba_effect_name
		desert_mountains_advantage = 5
		learning = 1
	}
}

gezirim_ba = {
	county = c_samaria
	barony = b_4244

	character_modifier = {
		name = holy_site_gerizim_ba_effect_name
		learning_per_piety_level = 1
		monthly_prestige_gain_mult = 0.2
	}
}

#Hurrian
wassukanni = {
	county = c_wassukanni
	barony = b_4961
	
	character_modifier = {
		name = holy_site_wassukanni_effect_name
		cultural_head_fascination_mult = 0.1
	}
}

#Thracian
pangaion = {
	county = c_oisyme
	barony = b_2120
	character_modifier = {
		name = holy_site_pangaion_effect_name
		development_growth_factor = 0.1
		stewardship = 1
	}
}

samothrace = {
	county = c_samothrake
	barony = b_2227
	character_modifier = {
		name = holy_site_samothrace_effect_name
 		intrigue_per_piety_level = 1
	}
}

