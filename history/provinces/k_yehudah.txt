﻿#k_yehudah
##d_gaza ###################################
###c_gaza
4218 = { #Gaza
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_hauron_01 #Temple of Hauron
	}
}
4219 = { #Deir el-Balah
	holding = city_holding
}
4300 = { #Yurza
	holding = none
}
4298 = { #Ma'agalim
	holding = none
}
4301 = { #Garâr
	holding = none
}
4291 = { #S'dêrot
	holding = none
}
###c_rapihu
4220 = { #Rapihu
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4309 = { #Yevul
	holding = none
}
4310 = { #Sârûhen
	holding = none
}
4307 = { #Rafah
	holding = city_holding
}
4303 = { #Yesha
	holding = none
}
###c_nerbut
1420 = { #Nerbut
	culture = canaanite
	religion = canaanite_faith
	holding = tribal_holding
}
3920 = { #Tannuru
	holding = none
}
4308 = { #Laban
	holding = none
}
##d_ashkalon
###c_ashkalon
4216 = { #'Asqalôn
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4217 = { #Zikim
	holding = city_holding
}
4292 = { #Netivot
	holding = none
}
4290 = { #Or Ha'Nêr
	holding = none
}
4271 = { #Gê'a
	holding = none
}
###c_ashdod
4214 = { #'Asdôd
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4270 = { #Emunim
	holding = none
}
4215 = { #Nitzan
	holding = city_holding
}
4273 = { #'Eqrôn
	holding = none
}
4269 = { #Ba'âlât
	holding = none
}
##d_jerusalem
###c_jerusalem
4242 = { #Yeru-Shalem
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
	#holy_site = omen_shalem
	
	785.1.1 = {
		special_building_slot = temple_of_shalem_01 #Temple of Shalem, later Yahweh
	}
	1310.1.1 = {
		special_building = temple_of_shalem_01
	}
}
4263 = { #Kepirâh
	holding = none
}
4258 = { #Râmâh
	holding = none
}
4259 = { #Gib'ôn
	holding = none
}
4277 = { #Ye'ârim
	holding = church_holding
}
4278 = { #'Eytâm
	holding = none
}
###c_western_dead_sea
4239 = { #Eyn-Gedi
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding

	785.1.1 = {
		special_building_slot = bitumen_mine_01
		special_building = bitumen_mine_01
	}
}
4238 = { #Binyamin
	holding = none
}
4281 = { #Ma'ale Amos
	holding = none
}
4240 = { #Masada
	holding = city_holding
}
4280 = { #Taqô'a
	holding = none
}
4279 = { #Natôpâh
	holding = none
}
###c_gezer
4267 = { #Gezer
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4266 = { #Gimzô
	holding = city_holding
}
4265 = { #Lôd
	holding = none
}
4274 = { #Timnâh
	holding = none
}
4268 = { #'Elteqêh
	holding = none
}
##d_lakish
###c_lakish
4289 = { #Lakis
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4276 = { #Sor'âh
	holding = city_holding
}
4288 = { #'Âdôrayim
	holding = church_holding
}
4275 = { #Gat
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
4272 = { #Beror Hayil
	holding = castle_holding
}
###c_hebron
4284 = { #Hebron
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4286 = { #Zip
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
4283 = { #Bêyt-Sûr
	holding = none
}
4285 = { #Mâ'on
	holding = none
}
4295 = { #Estamô'a
	holding = none
}
###c_tidhar
4297 = { #Tidhar
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4293 = { #Ruhama
	holding = city_holding
}
4302 = { #Maslul
	holding = church_holding
}
4296 = { #Rahat
	holding = castle_holding
}
4305 = { #'Eshel Ha'nasi
	holding = city_holding
}
4294 = { #Dabir
	holding = castle_holding
}
##d_simon
###c_simon
4313 = { #Be'êr-Sâba'
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4320 = { #Hatserim
	holding = city_holding
}
4304 = { #Siqlâg
	holding = church_holding
}
4314 = { #Gilat
	holding = city_holding
}
###c_ramotnegeb
4315 = { #Râmôt-Negeb
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4321 = { #Hura
	holding = none
}
4316 = { #Mahane Yatir
	holding = none
}
4306 = { #Yattir
	holding = city_holding
}
###c_amaleq
4319 = { #el-Far'ah
	culture = canaanite
	religion = canaanite_faith
	holding = tribal_holding
}
4408 = { #Shivta
	holding = none
}
3930 = { #Lukkulu
	holding = none
}
4312 = { #Naveh
	holding = none
}
4311 = { #Jemmeh
	holding = none
}
###c_arad
4299 = { #Arad
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4317 = { #Hura
	holding = none
}
4282 = { #Ka'abne
	holding = none
}
4287 = { #Ma'ale Hever
	holding = city_holding
}