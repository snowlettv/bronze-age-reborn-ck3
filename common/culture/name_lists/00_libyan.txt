﻿name_list_libyan = {

	cadet_dynasty_names = {
		"dynn_Ahmid"
		"dynn_Galesta"
		"dynn_Kenamid"
		"dynn_Narava"
		"dynn_Penamid"
		"dynn_Senua"
		"dynn_Setna"
		"dynn_Adherba"
		"dynn_Masinna"
		"dynn_Tacfarina"
		"dynn_Syphid"
		"dynn_Vermina"
		"dynn_Galid"
		"dynn_Jugurthid"
	}
	dynasty_names = {
		"dynn_Ahmid"
		"dynn_Galesta"
		"dynn_Kenamid"
		"dynn_Narava"
		"dynn_Penamid"
		"dynn_Senua"
		"dynn_Setna"
		"dynn_Adherba"
		"dynn_Masinna"
		"dynn_Tacfarina"
		"dynn_Syphid"
		"dynn_Vermina"
		"dynn_Galid"
		"dynn_Jugurthid"
	}
	male_names = {
		Adherbal Bomilcar Ezena Gauda Gulussa Hiempsal Iampsas Jugurtha Masinissa Massiva Mastanabal Micipsa Nabdalsa Naravas Oxyntas Sypax Syphax Tacfarinas Tychaeus Vermina Zeteres Gala Amastan Mastan 
	}
	female_names = {
		Anno Salammbo Sophonisba Panisba Ena Palema Synno Narva Enno Adhierno Ballanba 
	}
}