﻿#Defined
kubaba1 = {
	name = "Kubaba" #Kubaba was a legendary Mesopotamian queen who according to the Sumerian King List ruled over Kish for a hundred years before the rise of the dynasty of Akshak. It is typically assumed that she was not a historical figure.
	dynasty = dynn_kubaba1
	religion = "akkadian_faith"
	culture = "akkadian"
	martial = 5
	diplomacy = 25
	stewardship = 27
	intrigue = 18
	learning = 12
	female = yes
	trait = education_diplomacy_4
	trait = family_first
	trait = saint
	400.7.1 = {
		birth = yes
	}
	500.6.1 = {
		death = yes
	}
}
sargon1 = {
	name = "Sargon" #Sargon of Akkad, also known as Sargon the Great, was the first ruler of the Akkadian Empire, known for his conquests of the Sumerian city-states in the 24th to 23rd centuries BC. He is sometimes identified as the first person in recorded history to rule over an empire.
	dynasty = dynn_sargon1
	religion = "akkadian_faith"
	culture = "akkadian"
	martial = 25
	diplomacy = 16
	stewardship = 18
	intrigue = 18
	learning = 15
	disallow_random_traits = yes
	trait = education_martial_4
	trait = brave
	trait = arrogant
	trait = ambitious
	trait = callous
	trait = august
	trait = overseer
	trait = saint
	trait = lifestyle_blademaster
	566.7.23 = {
		birth = yes
		effect = {
			add_trait_xp = {
				trait = lifestyle_blademaster
				value = {
					integer_range = {
						min = small_lifestyle_random_xp_low
						max = small_lifestyle_random_xp_high
					}
				}
			}
		}
	}
	616.6.12 = {
		death = yes
	}
}
malgum1 = {
	name = "Ashur-bel-kala"
	dynasty = dynn_malgum1
	religion = "akkadian_faith"
	culture = "akkadian"
	760.5.8 = {
		birth = yes
	}
	828.8.10 = {
		death = yes
	}
}
kish1 = {
	name = "Imshe-Dagan"
	dynasty = dynn_kish1
	religion = "akkadian_faith"
	culture = "akkadian"
	747.1.27 = {
		birth = yes
	}
	792.7.17 = {
		death = yes
	}
}
kutha1 = {
	name = "Abazu"
	dynasty = dynn_kutha1
	religion = "akkadian_faith"
	culture = "akkadian"
	763.1.4 = {
		birth = yes
	}
	812.11.17 = {
		death = yes
	}
}
push1 = {
	name = "Anba"
	dynasty = dynn_push1
	religion = "akkadian_faith"
	culture = "akkadian"
	749.11.25 = {
		birth = yes
	}
	815.8.17 = {
		death = yes
	}
}
rapiqu1 = {
	name = "Sargon"
	dynasty = dynn_rapiqu1
	religion = "akkadian_faith"
	culture = "akkadian"
	745.8.14 = {
		birth = yes
	}
	801.3.21 = {
		death = yes
	}
}
sippar1 = {
	name = "Shamshi-Adad"
	dynasty = dynn_sippar1
	religion = "akkadian_faith"
	culture = "akkadian"
	766.7.18 = {
		birth = yes
	}
	832.3.10 = {
		death = yes
	}
}
babylon1 = {
	name = "Ilu-Mer"
	dynasty = dynn_babylon1
	religion = "akkadian_faith"
	culture = "akkadian"
	757.8.22 = {
		birth = yes
	}
	800.2.23 = {
		death = yes
	}
}
aplak1 = {
	name = "Ashur-rabi"
	dynasty = dynn_aplak1
	religion = "akkadian_faith"
	culture = "akkadian"
	757.6.25 = {
		birth = yes
	}
	823.4.17 = {
		death = yes
	}
}
apak1 = {
	name = "Libaya"
	dynasty = dynn_apak1
	religion = "akkadian_faith"
	culture = "akkadian"
	751.5.24 = {
		birth = yes
	}
	810.4.11 = {
		death = yes
	}
}
marad1 = {
	name = "Zuqaqip"
	dynasty = dynn_marad1
	religion = "akkadian_faith"
	culture = "akkadian"
	769.2.13 = {
		birth = yes
	}
	818.9.25 = {
		death = yes
	}
}
#Generated
246 = {
	name = "Ilu-Mer"
	dynasty = 246
	religion = "akkadian_faith"
	culture = "akkadian"
	757.10.26 = {
		birth = yes
	}
	810.5.23 = {
		death = yes
	}
}
266 = {
	name = "Ashur-shaduni"
	dynasty = 266
	religion = "akkadian_faith"
	culture = "akkadian"
	758.8.2 = {
		birth = yes
	}
	823.10.8 = {
		death = yes
	}
}
313 = {
	name = "Mutakkil-nusku"
	dynasty = 313
	religion = "akkadian_faith"
	culture = "akkadian"
	768.7.2 = {
		birth = yes
	}
	831.10.12 = {
		death = yes
	}
}
314 = {
	name = "Kullassina-bel"
	dynasty = 314
	religion = "akkadian_faith"
	culture = "akkadian"
	755.2.22 = {
		birth = yes
	}
	808.4.11 = {
		death = yes
	}
}
390 = {
	name = "Ashur-dugul"
	dynasty = 390
	religion = "akkadian_faith"
	culture = "akkadian"
	746.7.6 = {
		birth = yes
	}
	813.7.24 = {
		death = yes
	}
}
391 = {
	name = "Yangi"
	dynasty = 391
	religion = "akkadian_faith"
	culture = "akkadian"
	755.7.8 = {
		birth = yes
	}
	805.11.19 = {
		death = yes
	}
}
392 = {
	name = "Ipqi-Ishtar"
	dynasty = 392
	religion = "akkadian_faith"
	culture = "akkadian"
	765.7.17 = {
		birth = yes
	}
	823.7.4 = {
		death = yes
	}
}
405 = {
	name = "Ninurta-tukulti-Ashur"
	dynasty = 405
	religion = "akkadian_faith"
	culture = "akkadian"
	752.7.23 = {
		birth = yes
	}
	808.1.4 = {
		death = yes
	}
}
736 = {
	name = "Mamagal"
	dynasty = 736
	religion = "akkadian_faith"
	culture = "akkadian"
	766.4.4 = {
		birth = yes
	}
	815.1.26 = {
		death = yes
	}
}
748 = {
	name = "Anba"
	dynasty = 748
	religion = "akkadian_faith"
	culture = "akkadian"
	755.11.23 = {
		birth = yes
	}
	820.8.18 = {
		death = yes
	}
}
749 = {
	name = "Rimush"
	dynasty = 749
	religion = "akkadian_faith"
	culture = "akkadian"
	758.5.26 = {
		birth = yes
	}
	821.9.23 = {
		death = yes
	}
}
750 = {
	name = "Ashur-nadin-apli"
	dynasty = 750
	religion = "akkadian_faith"
	culture = "akkadian"
	748.5.21 = {
		birth = yes
	}
	792.8.17 = {
		death = yes
	}
}
751 = {
	name = "Dadasig"
	dynasty = 751
	religion = "akkadian_faith"
	culture = "akkadian"
	751.5.19 = {
		birth = yes
	}
	803.9.7 = {
		death = yes
	}
}
753 = {
	name = "Enbi-Ishtar"
	dynasty = 753
	religion = "akkadian_faith"
	culture = "akkadian"
	757.2.6 = {
		birth = yes
	}
	804.2.7 = {
		death = yes
	}
}
758 = {
	name = "Zamug"
	dynasty = 758
	religion = "akkadian_faith"
	culture = "akkadian"
	762.2.2 = {
		birth = yes
	}
	823.3.18 = {
		death = yes
	}
}
759 = {
	name = "Sennacherib"
	dynasty = 759
	religion = "akkadian_faith"
	culture = "akkadian"
	761.11.9 = {
		birth = yes
	}
	816.9.21 = {
		death = yes
	}
}