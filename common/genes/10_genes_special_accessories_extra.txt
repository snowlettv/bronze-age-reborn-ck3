﻿
special_genes = {

	accessory_genes = {

	#####################################
	#									#
	# 			  HEADGEAR 			    #
	#									#
	#####################################

		headgear_2 = {
			egyptian_pharaoh = {
				index = 0
				male = {
					10 = male_headgear_egyptian_false_beard
				}
				female = {
					30 = empty
				}
				boy = male
				girl = female
			}
		}
		
		headgear_3 = {
			egyptian_pharaoh = {
				index = 0
				male = {
					10 = male_headgear_egyptian_uraeus
				}
				female = {
					30 = empty
				}
				boy = male
				girl = female
			}
		}
		
		misc = {
			egyptian_pharaoh = {
				index = 0
				male = {
					10 = egyptian_bracelets
				}
				female = {
					30 = empty
				}
				boy = male
				girl = female
			}
		}
	}

	morph_genes = {
		
	#####################################
	#									#
	# 			  MAKEUP 			    #
	#									#
	#####################################
		cosmetics_eyeliner = {

			egyptian_1 = {
				index = 0
				male = {
					decal = {											
						body_part = head
						textures = {
							diffuse = "gfx/models/portraits/decals/cosmetics/eyeliner_egyptian_1_diffuse.dds"
							properties = "gfx/models/portraits/decals/cosmetics/eyeliner_egyptian_1_properties.dds"
							
						}
						blend_modes = {				 #overlay/replace/hard_light/multiply
							diffuse = replace
							# normal = overlay
							properties = replace
						}
						priority = 2
					}
				}
				female = male
				boy = male
				girl = male
			}

		}
		
		cosmetics_eyeshadow = {

			egyptian_1 = {
				index = 0
				male = {
					decal = {											
						body_part = head
						textures = {
							diffuse = "gfx/models/portraits/decals/cosmetics/eyeshadow_egyptian_1_diffuse.dds"
							properties = "gfx/models/portraits/decals/cosmetics/eyeshadow_egyptian_1_properties.dds"
							
						}
						blend_modes = {				 #overlay/replace/hard_light/multiply
							diffuse = replace
							# normal = overlay
							properties = replace
						}
						priority = 3
					}
				}
				female = male
				boy = male
				girl = male
			}

		}
		
		cosmetics_lipcolor = {

			egyptian_1 = {
				index = 0
				male = {
					decal = {											
						body_part = head
						textures = {
							diffuse = "gfx/models/portraits/decals/cosmetics/lipcolor_egyptian_1_diffuse.dds"
							properties = "gfx/models/portraits/decals/cosmetics/lipcolor_egyptian_1_properties.dds"
							
						}
						blend_modes = {				 #overlay/replace/hard_light/multiply
							diffuse = replace
							# normal = overlay
							properties = replace
						}
						priority = 4
					}
				}
				female = male
				boy = male
				girl = male
			}

		}

	}

}