﻿#Generated
190 = {
	name = "Apas"
	dynasty = 190
	religion = "thracian_faith"
	culture = "bryges"
	769.6.5 = {
		birth = yes
	}
	816.6.10 = {
		death = yes
	}
}
198 = {
	name = "Attas"
	dynasty = 198
	religion = "thracian_faith"
	culture = "bryges"
	765.2.27 = {
		birth = yes
	}
	806.3.17 = {
		death = yes
	}
}
199 = {
	name = "Dades"
	dynasty = 199
	religion = "thracian_faith"
	culture = "bryges"
	754.3.14 = {
		birth = yes
	}
	811.10.8 = {
		death = yes
	}
}
200 = {
	name = "Delas"
	dynasty = 200
	religion = "thracian_faith"
	culture = "bryges"
	752.1.24 = {
		birth = yes
	}
	820.11.18 = {
		death = yes
	}
}
202 = {
	name = "Kokhos"
	dynasty = 202
	religion = "thracian_faith"
	culture = "bryges"
	747.3.15 = {
		birth = yes
	}
	802.5.23 = {
		death = yes
	}
}
203 = {
	name = "Manes"
	dynasty = 203
	religion = "thracian_faith"
	culture = "bryges"
	760.5.1 = {
		birth = yes
	}
	828.7.19 = {
		death = yes
	}
}
204 = {
	name = "Nanas"
	dynasty = 204
	religion = "thracian_faith"
	culture = "bryges"
	751.6.21 = {
		birth = yes
	}
	812.6.24 = {
		death = yes
	}
}
206 = {
	name = "Papas"
	dynasty = 206
	religion = "thracian_faith"
	culture = "bryges"
	759.5.5 = {
		birth = yes
	}
	810.4.15 = {
		death = yes
	}
}
207 = {
	name = "Papias"
	dynasty = 207
	religion = "thracian_faith"
	culture = "bryges"
	765.4.25 = {
		birth = yes
	}
	820.11.27 = {
		death = yes
	}
}
223 = {
	name = "Tatas"
	dynasty = 223
	religion = "thracian_faith"
	culture = "bryges"
	761.5.19 = {
		birth = yes
	}
	804.8.24 = {
		death = yes
	}
}
224 = {
	name = "Alenpates"
	dynasty = 224
	religion = "thracian_faith"
	culture = "bryges"
	753.8.26 = {
		birth = yes
	}
	817.8.10 = {
		death = yes
	}
}
227 = {
	name = "Alpos"
	dynasty = 227
	religion = "thracian_faith"
	culture = "bryges"
	765.6.17 = {
		birth = yes
	}
	818.8.1 = {
		death = yes
	}
}
559 = {
	name = "Amias"
	dynasty = 559
	religion = "thracian_faith"
	culture = "bryges"
	762.1.17 = {
		birth = yes
	}
	831.1.10 = {
		death = yes
	}
}
560 = {
	name = "Ammios"
	dynasty = 560
	religion = "thracian_faith"
	culture = "bryges"
	758.6.20 = {
		birth = yes
	}
	815.10.25 = {
		death = yes
	}
}
561 = {
	name = "Ammalios"
	dynasty = 561
	religion = "thracian_faith"
	culture = "bryges"
	746.5.12 = {
		birth = yes
	}
	803.8.6 = {
		death = yes
	}
}
562 = {
	name = "Aphiamos"
	dynasty = 562
	religion = "thracian_faith"
	culture = "bryges"
	751.9.5 = {
		birth = yes
	}
	792.3.25 = {
		death = yes
	}
}
563 = {
	name = "Arotaos"
	dynasty = 563
	religion = "thracian_faith"
	culture = "bryges"
	766.8.12 = {
		birth = yes
	}
	817.3.2 = {
		death = yes
	}
}
564 = {
	name = "Artimmes"
	dynasty = 564
	religion = "thracian_faith"
	culture = "bryges"
	765.7.8 = {
		birth = yes
	}
	809.11.9 = {
		death = yes
	}
}
565 = {
	name = "Attapinis"
	dynasty = 565
	religion = "thracian_faith"
	culture = "bryges"
	747.3.14 = {
		birth = yes
	}
	793.5.17 = {
		death = yes
	}
}
566 = {
	name = "Babys"
	dynasty = 566
	religion = "thracian_faith"
	culture = "bryges"
	767.9.3 = {
		birth = yes
	}
	816.10.27 = {
		death = yes
	}
}
567 = {
	name = "Barthas"
	dynasty = 567
	religion = "thracian_faith"
	culture = "bryges"
	761.7.15 = {
		birth = yes
	}
	807.2.9 = {
		death = yes
	}
}
568 = {
	name = "Bestes"
	dynasty = 568
	religion = "thracian_faith"
	culture = "bryges"
	768.8.24 = {
		birth = yes
	}
	829.2.10 = {
		death = yes
	}
}
572 = {
	name = "Boas"
	dynasty = 572
	religion = "thracian_faith"
	culture = "bryges"
	765.7.23 = {
		birth = yes
	}
	818.8.18 = {
		death = yes
	}
}
573 = {
	name = "Gokhas"
	dynasty = 573
	religion = "thracian_faith"
	culture = "bryges"
	757.7.12 = {
		birth = yes
	}
	826.2.9 = {
		death = yes
	}
}
574 = {
	name = "Dadas"
	dynasty = 574
	religion = "thracian_faith"
	culture = "bryges"
	755.5.3 = {
		birth = yes
	}
	800.4.6 = {
		death = yes
	}
}
575 = {
	name = "Dadon"
	dynasty = 575
	religion = "thracian_faith"
	culture = "bryges"
	752.3.1 = {
		birth = yes
	}
	814.8.24 = {
		death = yes
	}
}
1294 = {
	name = "Deoueias"
	dynasty = 1294
	religion = "thracian_faith"
	culture = "bryges"
	1275.2.1 = {
		birth = yes
	}
	1318.4.27 = {
		death = yes
	}
}
1301 = {
	name = "Dormagles"
	dynasty = 1301
	religion = "thracian_faith"
	culture = "bryges"
	1286.9.23 = {
		birth = yes
	}
	1349.4.17 = {
		death = yes
	}
}
1302 = {
	name = "Dorykhanos"
	dynasty = 1302
	religion = "thracian_faith"
	culture = "bryges"
	1272.1.6 = {
		birth = yes
	}
	1320.4.16 = {
		death = yes
	}
}
1303 = {
	name = "Doudas"
	dynasty = 1303
	religion = "thracian_faith"
	culture = "bryges"
	1275.7.20 = {
		birth = yes
	}
	1320.1.5 = {
		death = yes
	}
}
1305 = {
	name = "Doumetaos"
	dynasty = 1305
	religion = "thracian_faith"
	culture = "bryges"
	1271.9.4 = {
		birth = yes
	}
	1313.10.24 = {
		death = yes
	}
}
1306 = {
	name = "Zourzas"
	dynasty = 1306
	religion = "thracian_faith"
	culture = "bryges"
	1273.5.3 = {
		birth = yes
	}
	1331.11.17 = {
		death = yes
	}
}
1307 = {
	name = "Thakheas"
	dynasty = 1307
	religion = "thracian_faith"
	culture = "bryges"
	1271.4.21 = {
		birth = yes
	}
	1329.4.14 = {
		death = yes
	}
}
1309 = {
	name = "Thiarallos"
	dynasty = 1309
	religion = "thracian_faith"
	culture = "bryges"
	1292.7.23 = {
		birth = yes
	}
	1356.3.20 = {
		death = yes
	}
}
1310 = {
	name = "Iassos"
	dynasty = 1310
	religion = "thracian_faith"
	culture = "bryges"
	1280.10.7 = {
		birth = yes
	}
	1346.8.15 = {
		death = yes
	}
}
1326 = {
	name = "Iman"
	dynasty = 1326
	religion = "thracian_faith"
	culture = "bryges"
	1279.6.3 = {
		birth = yes
	}
	1324.4.15 = {
		death = yes
	}
}
1327 = {
	name = "Imas"
	dynasty = 1327
	religion = "thracian_faith"
	culture = "bryges"
	1283.6.3 = {
		birth = yes
	}
	1339.1.7 = {
		death = yes
	}
}
1329 = {
	name = "Eimas"
	dynasty = 1329
	religion = "thracian_faith"
	culture = "bryges"
	1286.2.9 = {
		birth = yes
	}
	1341.2.10 = {
		death = yes
	}
}
1428 = {
	name = "Irdis"
	dynasty = 1428
	religion = "thracian_faith"
	culture = "bryges"
	1283.7.10 = {
		birth = yes
	}
	1346.4.16 = {
		death = yes
	}
}
1429 = {
	name = "Kandes"
	dynasty = 1429
	religion = "thracian_faith"
	culture = "bryges"
	1274.1.14 = {
		birth = yes
	}
	1321.9.1 = {
		death = yes
	}
}
1430 = {
	name = "Katmaros"
	dynasty = 1430
	religion = "thracian_faith"
	culture = "bryges"
	1285.6.8 = {
		birth = yes
	}
	1329.7.11 = {
		death = yes
	}
}
1431 = {
	name = "Kosbylos"
	dynasty = 1431
	religion = "thracian_faith"
	culture = "bryges"
	1281.9.21 = {
		birth = yes
	}
	1332.11.10 = {
		death = yes
	}
}
1432 = {
	name = "Koulas"
	dynasty = 1432
	religion = "thracian_faith"
	culture = "bryges"
	1286.4.4 = {
		birth = yes
	}
	1349.1.25 = {
		death = yes
	}
}
1433 = {
	name = "Kouriles"
	dynasty = 1433
	religion = "thracian_faith"
	culture = "bryges"
	1271.9.7 = {
		birth = yes
	}
	1328.8.22 = {
		death = yes
	}
}
1434 = {
	name = "Koussou"
	dynasty = 1434
	religion = "thracian_faith"
	culture = "bryges"
	1281.10.24 = {
		birth = yes
	}
	1347.9.10 = {
		death = yes
	}
}
1435 = {
	name = "Krimeinos"
	dynasty = 1435
	religion = "thracian_faith"
	culture = "bryges"
	1271.7.6 = {
		birth = yes
	}
	1337.11.2 = {
		death = yes
	}
}
1436 = {
	name = "Krouleus"
	dynasty = 1436
	religion = "thracian_faith"
	culture = "bryges"
	1287.9.4 = {
		birth = yes
	}
	1356.4.5 = {
		death = yes
	}
}
1437 = {
	name = "Kodalos"
	dynasty = 1437
	religion = "thracian_faith"
	culture = "bryges"
	1291.3.17 = {
		birth = yes
	}
	1332.2.27 = {
		death = yes
	}
}
1441 = {
	name = "Lasamos"
	dynasty = 1441
	religion = "thracian_faith"
	culture = "bryges"
	1273.9.11 = {
		birth = yes
	}
	1316.1.8 = {
		death = yes
	}
}
1442 = {
	name = "Mamas"
	dynasty = 1442
	religion = "thracian_faith"
	culture = "bryges"
	1288.1.11 = {
		birth = yes
	}
	1331.8.15 = {
		death = yes
	}
}
1443 = {
	name = "Mamios"
	dynasty = 1443
	religion = "thracian_faith"
	culture = "bryges"
	1274.5.20 = {
		birth = yes
	}
	1341.5.11 = {
		death = yes
	}
}
1444 = {
	name = "Manis"
	dynasty = 1444
	religion = "thracian_faith"
	culture = "bryges"
	1277.6.12 = {
		birth = yes
	}
	1323.1.24 = {
		death = yes
	}
}