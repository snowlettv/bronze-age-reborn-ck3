﻿

RICE_tuareg_dream_location_holding_bonus_value = 10

RICE_tuareg_dream_chance_value = {
	value = 40
	if = {
		limit = {
			has_character_flag = RICE_tuareg_location_holding_bonus
		}
		add = RICE_tuareg_dream_location_holding_bonus_value
	}
	if = {
		limit = {
			has_character_flag = RICE_tuareg_dream_basic_location_bonus
		}
		add = RICE_tuareg_dream_location_holding_bonus_value
	}
	# Bonus for piousness/learnedness
	add = {
		value = piety_level
		multiply = 5
	}
	add = {
		value = num_virtuous_traits
		multiply = 5
	}
	add = {
		value = num_sinful_traits
		multiply = -5
	}
	if = {
		limit = {
			has_trait = zealous
		}
		add = 5
	}
	if = {
		limit = {
			has_trait = cynical
		}
		add = -5
	}
	if = {
		limit = {
			has_trait = patient
		}
		add = 5
	}
	if = {
		limit = {
			has_trait = impatient
		}
		add = -5
	}
	if = {
		limit = {
			has_trait = content
		}
		add = 5
	}
	if = {
		limit = {
			has_trait = fickle
		}
		add = -5
	}
	if = {
		limit = {
			has_trait = calm
		}
		add = 5
	}
	if = {
		limit = {
			has_trait = wrathful
		}
		add = -5
	}
	add = {
		value = learning
		multiply = 1.25
		max = 30
	}
	min = 20
	max = 90
}

RICE_tuareg_dream_incubation_cost = {
	value = 25
	multiply = {
		value = root.primary_title.tier
		subtract = 1
		min = 1
	}
	if = {
		limit = {
			root.primary_title.tier > tier_duchy
		}
		add = 25
	}
	if = {
		limit = {
			root= {
				exists = dynasty
				dynasty = {
					has_dynasty_perk = law_legacy_1
				}
			}
		}
		multiply = law_legacy_cost_reduction_mult
	}
	if = {
		limit = {
			root= {
				government_has_flag = government_is_tribal
			}
		}
		multiply = 0.5
	}
	# Make it a little easier for smaller realms
	if = {
		limit = {
			root= {
				sub_realm_size < 3
			}
		}
		multiply = 0.8
	}
	min = 25
}