﻿#Dynasty XII
12SenusretA = { #Father of Amenemhat I, related to Nomarch of Elephantine/Abu
	name = "Senusret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "upper_egyptian"
	875.1.1 = {
		birth = yes
	}
	876.1.1 = {
		add_spouse = 12NeferetA
	}
	931.1.1 = {
		death = yes
	}
}
12NeferetA = { #Mother of Amenemhat I, possibly Nubian
	name = "Neferet"
	religion = "egyptian_faith"
	culture = "wawatic"
	female = yes
	874.1.1 = {
		birth = yes
	}
	940.1.1 = {
		death = yes
	}
}
12Amenemhat1 = { #Amenemhat I (Ancient Egyptian: Ỉmn-m-h't meaning 'Amun is at the forefront'), also known as Amenemhet I, was a pharaoh of ancient Egypt and the first king of the Twelfth Dynasty of the Middle Kingdom.
	name = "Amenemhat"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	intrigue = 12
	trait = education_intrigue_2
	trait = ambitious
	trait = paranoid
	father = 12SenusretA
	mother = 12NeferetA
	892.1.1 = {
		birth = yes
	}
	908.1.1 = {
		add_spouse = 12Neferitatjenen
	}
	938.3.7 = {
		death = {
			death_reason = death_murder #Assassinated by bodyguards, as attested in the Instructions of Amenemhat and the Story of Sinube
		}
	}
}
12Neferitatjenen = { #Neferitatjanen was the wife of Amenemhat I, the king of the 12th Dynasty in ancient Egypt, and the mother of Senwosret I. The documentation of her relationships is preserved on a statuette depicting her son.
	name = "Neferitatjenen"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	892.1.1 = {
		birth = yes
	}
	940.1.1 = {
		death = yes
	}
}
#Issue of Amenemhat I
12Senusret1 = { #Senusret I (Middle Egyptian: z-n-wsrt) also anglicized as Sesostris I and Senwosret I, was the second pharaoh of the Twelfth Dynasty of Egypt. He ruled from 1971 BC to 1926 BC (1920 BC to 1875 BC), and was one of the most powerful kings of this Dynasty. He was the son of Amenemhat I. Senusret I was known by his prenomen, Kheperkare, which means "the Ka of Re is created." He expanded the territory of Egypt allowing him to rule over an age of prosperity.
	name = "Senusret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat1
	mother = 12Neferitatjenen
	martial = 14
	trait = education_martial_3
	trait = architect
	909.1.1 = {
		birth = yes
	}
	926.1.1 = {
		add_spouse = 12Neferu3
	}
	974.1.1 = {
		death = yes
	}
}
12Neferu3 = { #Neferu (English: Beauty) was an ancient Egyptian queen of the 12th Dynasty. She was a daughter of Amenemhat I (r. 1991–1962 BC), sister-wife of Senusret I (r. 1971–1926 BC) and the mother of Amenemhat II.
	name = "Neferu"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat1
	mother = 12Neferitatjenen
	female = yes
	910.1.1 = {
		birth = yes
	}
	974.1.1 = {
		death = yes
	}
}
12Neferusherit = { #Daughter of Amenemhat I
	name = "Neferusherit"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat1
	mother = 12Neferitatjenen
	female = yes
	trait = sickly
	911.1.1 = {
		birth = yes
	}
	914.1.1 = {
		death = {
			death_reason = death_sickly
		}
	}
}
12Kayet = { #Daughter of Amenemhat I
	name = "Kayet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat1
	mother = 12Neferitatjenen
	female = yes
	trait = ill
	912.1.1 = {
		birth = yes
	}
	917.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}
#Issue of Senusret I
12Amenemhat2 = { #Nubkaure Amenemhat II, also known as Amenemhet II, was the third pharaoh of the 12th Dynasty of ancient Egypt. Although he ruled for at least 35 years, his reign is rather obscure, as well as his family relationships.
	name = "Amenemhat"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	927.1.1 = {
		birth = yes
	}
	947.1.1 = {
		add_spouse = 12Senet
	}
	1003.1.1 = {
		death = yes
	}
}
12Amenemhatankh = { #Son of Senusret I
	name = "Amenemhat-ankh"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	trait = ill
	928.1.1 = {
		birth = yes
	}
	937.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}
12Itakayt = { #Daughter of Senusret I
	name = "Itakayt"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	female = yes
	929.1.1 = {
		birth = yes
	}
	985.1.1 = {
		death = yes
	}
}
12Sebat = { #Sebat was an ancient Egyptian king's daughter of the Twelfth Dynasty. Her only known title is king's daughter of his body. She is so far only attested on the back slab of a statue base found at Serabit el-Khadim on Sinai.
	name = "Sebat"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	female = yes
	930.1.1 = {
		birth = yes
	}
	989.1.1 = {
		death = yes
	}
}
12Neferusobek = { #Daughter of Senusret I
	name = "Neferusobek"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	female = yes
	trait = ill
	931.1.1 = {
		birth = yes
	}
	935.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}
12Neferuptah = { #Daughter of Senusret I
	name = "Neferuptah"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret1
	mother = 12Neferu3
	female = yes
	trait = sickly
	932.1.1 = {
		birth = yes
	}
	933.1.1 = {
		death = {
			death_reason = death_sickly
		}
	}
}
#Issue of Amenemhat II
12Amenemhatankh2 = { #Amenemhatankh (his name means “Amenemhat lives”) was an ancient Egyptian prince of the 12th Dynasty, son or brother of Amenemhat II.
	name = "Amenemhat-ankh"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	943.1.1 = {
		birth = yes
	}
	957.1.1 = {
		death = {
			death_reason = death_accident
		}
	}
}
12Itawaret = { #Itaweret (Ita-the elder) was an Ancient Egyptian king's daughter who lived in the 12th Dynasty around 1850 BC. She is known from her burial next to the pyramid of king Amenemhat II at Dahshur.
	name = "Itaweret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	female = yes
	944.1.1 = {
		birth = yes
	}
	1006.1.1 = {
		death = yes
	}
}
12Ita = { #Ita was an Ancient Egyptian king's daughter who lived in the 12th Dynasty around 1850 BC. She is known from the statue of a sphinx found in Qatna in modern Syria.
	name = "Ita"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	female = yes
	945.1.1 = {
		birth = yes
	}
	1009.1.1 = {
		death = yes
	}
}
12Khenmet = { #Khenmet was an ancient Egyptian king's daughter of the Twelfth Dynasty, around 1800 BC. She is mainly known from her unrobbed tomb containing a set of outstanding personal adornments.
	name = "Khenmet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	female = yes
	946.1.1 = {
		birth = yes
	}
	1007.1.1 = {
		death = yes
	}
}
12Senet = { #Senet was an Ancient Egyptian king's wife and king's mother, known from three statues, that date to the Middle Kingdom, perhaps to the 12th Dynasty.
	name = "Senet"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	931.1.1 = {
		birth = yes
	}
	994.1.1 = {
		death = yes
	}
}
12Senusret2 = { #Senusret II was the fourth pharaoh of the Twelfth Dynasty of Egypt. He ruled from 1897 BC to 1878 BC. His pyramid was constructed at El-Lahun. Senusret II took a great deal of interest in the Faiyum oasis region and began work on an extensive irrigation system from Bahr Yussef through to Lake Moeris through the construction of a dike at El-Lahun and the addition of a network of drainage canals. The purpose of his project was to increase the amount of cultivable land in that area. The importance of this project is emphasized by Senusret II's decision to move the royal necropolis from Dahshur to El-Lahun where he built his pyramid. This location would remain the political capital for the 12th and 13th Dynasties of Egypt. Senusret II was known by his prenomen Khakheperre, which means "The Ka of Re comes into being". The king also established the first known workers' quarter in the nearby town of Senusrethotep (Kahun).
	name = "Senusret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	mother = 12Senet
	stewardship = 10
	trait = education_stewardship_2
	trait = architect
	948.1.1 = {
		birth = yes
	}
	966.1.1 = {
		add_spouse = 12Itawaret
	}
	967.1.1 = {
		add_spouse = 12Khenmet
	}
	978.1.1 = {
		add_spouse = 12Nofret2
	}
	994.1.1 = {
		add_spouse = 12Khenemetneferhedjet1
	}
	1022.1.1 = {
		death = yes
	}
}
12Khenemetneferhedjet1 = { #Khenemetneferhedjet I Weret was an ancient Egyptian queen of the 12th Dynasty, a wife of Senusret II and the mother of Senusret III.
	name = "Khenemetneferhedjet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	female = yes
	968.1.1 = {
		birth = yes
	}
	1013.1.1 = {
		death = yes
	}
}
12Nofret2 = { #Nofret II (her name means Beautiful One) was an ancient Egyptian queen of the 12th dynasty. She was a daughter of Amenemhat II and wife of Senusret II.
	name = "Nofret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat2
	female = yes
	962.1.1 = {
		birth = yes
	}
	1027.1.1 = {
		death = yes
	}
}
#Issue of Senusret II
12Senusret3 = { #Khakaure Senusret III (also written as Senwosret III or the hellenised form, Sesostris III) was a pharaoh of Egypt. He ruled from 1878 BC to 1839 BC during a time of great power and prosperity, and was the fifth king of the Twelfth Dynasty of the Middle Kingdom. He was a great pharaoh of the Twelfth Dynasty and is considered to rule at the height of the Middle Kingdom. Consequently, he is regarded as one of the sources for the legend about Sesostris. His military campaigns gave rise to an era of peace and economic prosperity that reduced the power of regional rulers and led to a revival in craftwork, trade, and urban development. Senusret III was among the few Egyptian kings who were deified and honored with a cult during their own lifetime.
	name = "Senusret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret2
	mother = 12Khenemetneferhedjet1
	martial = 15
	trait = education_martial_3
	trait = overseer
	trait = saint
	995.1.1 = {
		birth = yes
	}
	1024.1.1 = {
		add_spouse = 12Itakayt2
	}
	1025.1.1 = {
		add_spouse = 12Neferthenut
	}
	1026.1.1 = {
		add_spouse = 12Khenemetneferhedjet2
	}
	1027.1.1 = {
		add_spouse = 12Meretseger
	}
	1061.1.1 = {
		death = yes
	}
}
12Neferet = { #Daughter of Senusret II
	name = "Neferet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret2
	mother = 12Khenemetneferhedjet1
	female = yes
	trait = ill
	1006.1.1 = {
		birth = yes
	}
	1012.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}
12Sithathoryunet = { #Sithathoriunet (her name means “daughter of Hathor of Dendera”) was an Ancient Egyptian king's daughter of the 12th Dynasty, mainly known from her burial at El-Lahun in which a treasure trove of jewellery was found. She was possibly a daughter of Senusret II since her burial site was found next to the pyramid of this king. If so, this would make her one of five known children and one of three daughters of Senusret II—the other children were Senusret III, Senusretseneb, Itakayt and Nofret.
	name = "Sithathoryunet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret2
	mother = 12Nofret2
	female = yes
	979.1.1 = {
		birth = yes
	}
	1062.1.1 = {
		death = yes
	}
}
12Senusret-sonbe = { #Son of Senusret II
	name = "Senusret-sonbe"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret2
	mother = 12Nofret2
	trait = sickly
	980.1.1 = {
		birth = yes
	}
	982.1.1 = {
		death = {
			death_reason = death_sickly
		}
	}
}
12Itakayt2 = { #Itakayt was an ancient Egyptian princess and queen of the 12th Dynasty, around 1800 BC. She is mainly known from her small pyramid next to the one of Senusret III at Dahshur. She had the titles king's daughter of his body, powerful, graceful and beloved.
	name = "Itakayt"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret2
	mother = 12Nofret2
	female = yes
	998.1.1 = {
		birth = yes
	}
	1067.1.1 = {
		death = yes
	}
}
#Issue of Senusret III
12Neferthenut = { #Neferthenut was an ancient Egyptian queen of the Twelfth Dynasty of Egypt. She was most likely the wife of Senusret III.
	name = "Neferthenut"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1009.1.1 = {
		birth = yes
	}
	1033.1.1 = {
		death = yes
	}
}
12Khenemetneferhedjet2 = { #Khenemetneferhedjet II (Weret) was an ancient Egyptian queen of the 12th Dynasty, a wife of Senusret III.
	name = "Khenemetneferhedjet"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1010.1.1 = {
		birth = yes
	}
	1072.1.1 = {
		death = yes
	}
}
12Meretseger = { #Meretseger ("She who Loves Silence") was an ancient Egyptian queen consort.
	name = "Meretseger"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1011.1.1 = {
		birth = yes
	}
	1078.1.1 = {
		death = yes
	}
}
12Amenemhat3 = { #Amenemhat III (Ancient Egyptian: Ỉmn-m-h't meaning 'Amun is at the forefront'), also known as Amenemhet III, was a pharaoh of ancient Egypt and the sixth king of the Twelfth Dynasty of the Middle Kingdom. He was elevated to throne as co-regent by his father Senusret III, with whom he shared the throne as the active king for twenty years. During his reign, Egypt attained its cultural and economic zenith of the Middle Kingdom.
	name = "Amenemhat"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	stewardship = 10
	trait = education_stewardship_2
	trait = architect
	1025.1.1 = {
		birth = yes
	}
	1043.1.1 = {
		add_spouse = 12Hetepi
	}
	1042.1.1 = {
		add_spouse = 12Aat
	}
	1043.1.1 = {
		add_spouse = 12Khenemetneferhedjet3
	}
	1086.1.1 = {
		death = yes
	}
}
12Khnemet = { #Daughter of Senusret III
	name = "Khnemet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	female = yes
	trait = sickly
	1026.1.1 = {
		birth = yes
	}
	1029.1.1 = {
		death = {
			death_reason = death_sickly
		}
	}
}
12Menet = { #Menet was an ancient Egyptian king's daughter living in the Twelfth Dynasty most likely under the kings Senusret III and Amenemhat III. Menet had the titles king's daughter and the one united with the white crown (Khenemetneferhedjet). She is only known from her sarcophagus and burial in a gallery tomb buried with other members of the royal family next to the pyramid of Senusret III at Dahshur. From the position of the tomb it seems likely that she was the daughter of the latter king.
	name = "Menet"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	female = yes
	1027.1.1 = {
		birth = yes
	}
	1086.1.1 = {
		death = yes
	}
}
12Mereret = { #Mereret (or Meret) was an Ancient Egyptian King's Daughter known from her burial next to the Pyramid of Pharaoh Senusret III (ruled about 1878 BC to 1839 BC) at Dahshur. On the north side of the king's pyramid was a row of four pyramids belonging to the king's wives. These pyramids were connected by an underground gallery. On the west side of the gallery were further burials arranged for women with the title king's daughter. They were buried in sarcophagi that were placed into niches. All burials were found looted. However, the robbers missed two boxes for jewellery. Both boxes contained an outstanding collection of jewellery filled with personal adornments found in 1894 by Jacques de Morgan. One of these boxes must have belonged to a king's daughter Sithathor, the other box to a king's daughter with the name Mereret or Meret.
	name = "Mereret"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	female = yes
	1028.1.1 = {
		birth = yes
	}
	1088.1.1 = {
		death = yes
	}
}
12Senetsenebtysy = { #Senetsenebtysy was an ancient Egyptian king's daughter of the Twelfth Dynasty, around 1800 BC. She was most likely a daughter of king Senusret III.
	name = "Senetsenebtysy"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	female = yes
	1029.1.1 = {
		birth = yes
	}
	1087.1.1 = {
		death = yes
	}
}
12Sithathor = { #Sithathor (daughter of Hathor) was an ancient Egyptian princess with the title king's daughter. She is only known from her burial at Dahshur.
	name = "Sithathor"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Senusret3
	female = yes
	1030.1.1 = {
		birth = yes
	}
	1089.1.1 = {
		death = yes
	}
}
#Issue of Amenemhat III
12Hetepi = { #Hetepti {Htp.tj} is a woman holding the title King's Mother and believed to be the mother of king Amenemhat IV, who ruled at the end of the Twelfth Dynasty of the Middle Kingdom in Ancient Egypt.
	name = "Hetepi"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1025.1.1 = {
		birth = yes
	}
	1089.1.1 = {
		death = yes
	}
}
12Aat = { #Aat (“The Great One”) was a queen of the ancient Egyptian 12th Dynasty. Of all the wives of Amenemhat III, only her name is known to modern archaeology with any certainty.
	name = "Aat"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1026.1.1 = {
		birth = yes
	}
	1088.1.1 = {
		death = yes
	}
}
12Khenemetneferhedjet3 = { #Khenemetneferhedjet III was an Egyptian queen. She was the wife of the Twelfth Dynasty ruler Amenemhet III and was buried in his pyramid at Dahshur. Her name is so far only known from one object, an alabaster vessel found in her burial. She had the titles king's wife, member of the elite and mistress of the two countries. She was buried in a decorated, but uninscribed sarcophagus.
	name = "Khenemetneferhedjet"
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	female = yes
	1027.1.1 = {
		birth = yes
	}
	1091.1.1 = {
		death = yes
	}
}
12Neferuptah2 = { #Neferuptah or Ptahneferu (“Beauty of Ptah”) was a daughter of the Egyptian king Amenemhat III (c. 1860 BC to 1814 BC) of the 12th Dynasty. Her sister was the Pharaoh Sobekneferu (“Beauty of Sobek”).
	name = "Neferuptah"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	female = yes
	1042.1.1 = {
		birth = yes
	}
	1096.1.1 = {
		death = yes
	}
}
12Amenemhat4 = { #Amenemhat IV (also known as Amenemhet IV) was the seventh and penultimate king of the late Twelfth Dynasty of Egypt during the late Middle Kingdom period. He arguably ruled around 1786–1777 BC for about nine regnal years.
	name = "Amenemhat"
	dynasty = dynn_amenemhat2 #This is to ensure the dynasty tree of the Thirteenth dynasty, who are probably descended from his sons is not broken
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	mother = 12Hetepi
	trait = disputed_heritage #There is a great deal of speculation surrounding Amenemhat IV parentage
	1043.1.1 = {
		birth = yes
	}
	1060.1.1= {
		add_spouse = 12Sobekneferu #Speculative
	}
	1095.1.1 = {
		death = yes
	}
}
12Sobekneferu = { #Sobekneferu or Neferusobek (Ancient Egyptian: Sbk-nfrw meaning 'Beauty of Sobek') was a pharaoh of ancient Egypt and the last ruler of the Twelfth Dynasty of the Middle Kingdom. She ascended to the throne following the death of Amenemhat IV, possibly her brother or husband, though their relationship is unproven. Instead, she asserted legitimacy through her father Amenemhat III. Her reign lasted 3 years, 10 months, and 24 days, according to the Turin King List.
	name = "Sobekneferu"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	female = yes
	1044.1.1 = {
		birth = yes
	}
	1098.1.1 = {
		death = yes
	}
}
12Hathorhotep = { #Hathorhotep was an ancient Egyptian king's daughter at the end of the Twelfth Dynasty during the Middle Kingdom.
	name = "Hathorhotep"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	female = yes
	1045.1.1 = {
		birth = yes
	}
	1097.1.1 = {
		death = yes
	}
}
12Nubhotep = { #Daughter of Amenemhat III
	name = "Nubhotep"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	female = yes
	trait = ill
	1046.1.1 = {
		birth = yes
	}
	1052.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}
12Sithathor2 = { #Daughter of Amenemhat III
	name = "Sithathor"
	dynasty = dynn_amenemhat1
	religion = "egyptian_faith"
	culture = "middle_egyptian"
	father = 12Amenemhat3
	female = yes
	trait = ill
	1047.1.1 = {
		birth = yes
	}
	1052.1.1 = {
		death = {
			death_reason = death_ill
		}
	}
}