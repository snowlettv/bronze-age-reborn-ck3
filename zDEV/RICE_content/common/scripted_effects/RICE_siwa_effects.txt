﻿RICE_siwa_oracle_of_amun_intent_bonuses_effect = {
	if = {
		limit = {
			has_activity_intent = RICE_siwa_oracle_of_amun_guidance_intent
		}
		if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_sufficient
					}
				}
			}
			add_piety = 50
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_plentiful
					}
				}
			}
			add_piety = 100
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_extravagant
					}
				}
			}
			add_piety = 150
		}
		stress_impact = {
			base = minor_stress_impact_loss
			zealous = minor_stress_impact_loss
			humble = minor_stress_impact_loss
			content = minor_stress_impact_loss
		}
	}
	else_if = {
		limit = {
			has_activity_intent = RICE_siwa_oracle_of_amun_devotion_intent
		}
		if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_sufficient
					}
				}
			}
			add_piety = 50
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_plentiful
					}
				}
			}
			add_piety = 100
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_extravagant
					}
				}
			}
			add_piety = 150
		}
	}
	else_if = {
		limit = {
			has_activity_intent = RICE_siwa_oracle_of_amun_legitimacy_intent
		}
		if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_sufficient
					}
				}
			}
			add_piety = 25
			add_prestige = 25
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_plentiful
					}
				}
			}
			add_piety = 50
			add_prestige = 50
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_extravagant
					}
				}
			}
			add_piety = 75
			add_prestige = 75
		}
	}
	else_if = {
		limit = {
			has_activity_intent = RICE_siwa_oracle_of_amun_legacy_intent
		}
		if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_sufficient
					}
				}
			}
			add_piety = 25
			dynasty = {
				add_dynasty_prestige = 15
			}
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_plentiful
					}
				}
			}
			add_piety = 50
			dynasty = {
				add_dynasty_prestige = 30
			}
		}
		else_if = {
			limit = {
				scope:activity = {
					has_activity_option = {
						category = RICE_siwa_oracle_of_amun_type
						option = RICE_siwa_oracle_of_amun_type_extravagant
					}
				}
			}
			add_piety = 75
			dynasty = {
				add_dynasty_prestige = 45
			}
		}
	}
}


RICE_siwa_oracle_of_amun_completed_log_entry_effect = {
	scope:activity = {
		add_activity_log_entry = {
			key = RICE_siwa_oracle_of_amun_completed_log
			tags = { completed }
			# this line below adds the entry to the Effects section of the conclusion UI
			show_in_conclusion = yes
			character = root
			scope:host = {				
				RICE_siwa_oracle_of_amun_intent_bonuses_effect = yes
			}
			show_as_tooltip = {
				scope:host = {
					if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_minor_advice_family
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_minor_advice_family
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_major_advice_family
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_major_advice_family
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_grand_advice_family
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_grand_advice_family
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_minor_advice_military
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_minor_advice_military
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_major_advice_military
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_major_advice_military
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_grand_advice_military
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_grand_advice_military
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_minor_advice_realm
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_minor_advice_realm
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_major_advice_realm
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_major_advice_realm
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_grand_advice_realm
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_grand_advice_realm
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_minor_advice_intrigue
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_minor_advice_intrigue
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_major_advice_intrigue
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_major_advice_intrigue
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_grand_advice_intrigue
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_grand_advice_intrigue
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_minor_advice_personal
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_minor_advice_personal
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_major_advice_personal
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_major_advice_personal
							years = 10
						}
					}
					else_if = {
						limit = {
							has_character_modifier = RICE_siwa_oracle_grand_advice_personal
						}
						add_character_modifier = {
							modifier = RICE_siwa_oracle_grand_advice_personal
							years = 10
						}
					}
				}				
			}
			every_attending_character = {
				limit = { NOT = { this = scope:host } }
				custom = EVERY_ACTIVITY_PARTICIPANT_EFFECT
				add_piety = 25
			}
		}
	}
}


RICE_siwa_visit_hibis_completed_log_entry_effect = {
	scope:activity = {
		add_activity_log_entry = {
			key = RICE_siwa_visit_hibis_pilgrimage_completed_log
			tags = { completed }
			# this line below adds the entry to the Effects section of the conclusion UI
			show_in_conclusion = yes
			character = root
			scope:host = {
				RICE_local_pilgrimage_generic_host_effect = yes
				if = {
					limit = {
						has_character_modifier = RICE_siwa_amun_major_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_amun_major_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_amun_minor_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_amun_minor_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_seth_major_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_seth_major_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_seth_minor_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_seth_minor_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_ha_major_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_ha_major_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_ha_minor_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_ha_minor_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_igai_major_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_igai_major_blessing
						years = 5
					}
				}
				else_if = {
					limit = {
						has_character_modifier = RICE_siwa_igai_minor_blessing
					}
					add_character_modifier = {
						modifier = RICE_siwa_igai_minor_blessing
						years = 5
					}
				}
			}
			every_attending_character = {
				limit = { NOT = { this = scope:host } }
				custom = EVERY_ACTIVITY_PARTICIPANT_EFFECT
				RICE_local_pilgrimage_generic_guest_effect = yes
			}	
		}
	}
}
