﻿#k_katmuhi
##d_katmuhi ###################################
###c_katmuhi
5916 = { #Abzu
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5903 = { #Bêtanîu
	holding = none
}
5768 = { #Zikrûtu
	holding = none
}
5770 = { #Qêpu
	holding = none
}
###c_kibaki
5917 = { #Kibaki
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5769 = { #Zupru
	holding = none
}
5771 = { #Kakikkum
	holding = none
}
##d_kasiyari
###c_shuru
5918 = { #Suru
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5776 = { #Mazaranu
	holding = none
}
5919 = { #Malku
	holding = none
}
###c_singisa
6448 = { #Singisa
	culture = shubrian
	religion = western_hurrian_faith
	holding = tribal_holding
}
5783 = { #Ir'emum
	holding = none
}
5920 = { #Sakanu
	holding = none
}
###c_mardiyane
5777 = { #Mardiyane
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
155 = { #Qinnu
	holding = none
}
5921 = { #Gasan
	holding = none
}
##d_simanum
###c_simanum
5912 = { #Simanum
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5913 = { #Apse
	holding = none
}
5909 = { #Imriqqu
	holding = none
}
###c_uda2
5781 = { #Uda
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5775 = { #Ru'ubtu
	holding = none
}
5910 = { #Kisellu
	holding = none
}
6446 = { #Sanumin
	holding = none
}
###c_matiati
5911 = { #Unqu
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5773 = { #Hiâlu
	holding = none
}
5774 = { #Ra'âmu
	holding = none
}
5772 = { #Matiati
	holding = none
}