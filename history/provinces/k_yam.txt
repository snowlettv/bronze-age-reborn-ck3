﻿#k_yam
##d_kerma ###################################
###c_kerma
1288 = { #Kerma
	culture = kushite
	religion = kushite_faith
	holding = castle_holding

	785.1.1 = {
		special_building_slot = temple_of_aman_01 #Temple of Aman
		duchy_capital_building = sub_saharan_imports_01
	}
}
1248 = { #Neferu
	holding = church_holding
}
2628 = { #Metut
	holding = castle_holding
}
1491 = { #Asartiu
	holding = city_holding
}
2245 = { #Tebu
	holding = castle_holding
}
1162 = { #Mehtiu
	holding = city_holding
	
	785.1.1 = {
		special_building_slot = gold_mine_01
		special_building = gold_mine_01
	}
}
###c_nauri
1338 = { #Reqau
	culture = kushite
	religion = kushite_faith
	holding = castle_holding
}
2249 = { #Setetu
	holding = city_holding
}
1392 = { #Chenem-Waset
	holding = none
}
###c_tumbus
1304 = { #Renp-ta
	culture = kushite
	religion = kushite_faith
	holding = castle_holding
}
1282 = { #Cheset
	holding = city_holding
}
1326 = { #Rertu-nefu
	holding = none
}
117 = { #Uta
	holding = none
}
##d_semna
###c_semna
2255 = { #Setupt
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
2414 = { #Perrites
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
2412 = { #Pertcha
	holding = church_holding
}
2343 = { #Nekut
	holding = none
}
###c_akin
1336 = { #Iken
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1459 = { #Teherau
	holding = none
}
1472 = { #Ashta
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
2423 = { #Hetut
	holding = none
}
###c_kummah
1521 = { #Akhser
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
2426 = { #Uhennu
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
2428 = { #Atur-ti
	holding = none
}
3111 = { #Perit
	holding = church_holding
	
	785.1.1 = {
		special_building_slot = gold_mine_01
	}

	1310.1.1 = {
		special_building = gold_mine_01
	}
}
##d_dal
###c_dal
2339 = { #Ustesh
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
2417 = { #Mesut
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
3131 = { #Perkh
	holding = none
}
2336 = { #Urtut
	holding = church_holding
}
3122 = { #Pertiu
	holding = none
}
###c_amarah
1197 = { #Nakeka
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
3205 = { #Pestu
	holding = none
}
2520 = { #Atur-res
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
1310 = { #Ermentu
	holding = none
}
###c_meska
1182 = { #Meska
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
2419 = { #Netchem
	holding = none
}
1186 = { #Naarik
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
##d_sye
###c_sye
2271 = { #Bessu
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding
	
	785.1.1 = {
		special_building_slot = gold_mine_01
		special_building = gold_mine_01
	}

	1310.1.1 = {
		holding = castle_holding
	}
}
1198 = { #Nub-heh
	holding = none
}
2256 = { #Qenat
	holding = none
}
2537 = { #Atum
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
###c_adere
2561 = { #Nubit
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1555 = { #Amt-tehen
	holding = none
}
13 = { #Usesh
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
###c_sesebi
1519 = { #Sehnen
	culture = kushite
	religion = kushite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1606 = { #Ukheb
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
1600 = { #Ashemu
	holding = none
}
1602 = { #Uhemu
	holding = none
}
1580 = { #Ahibit
	holding = none
}