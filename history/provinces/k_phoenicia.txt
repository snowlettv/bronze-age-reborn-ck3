﻿#k_phoenicia
##d_gubla ###################################
###c_gubla
3887 = { #Byblos
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
	#holy_site = omen_baalat_gebal
	
	785.1.1 = {
		special_building_slot = temple_of_baalat_gebal_01 #Temple of Baalat Gebal
		special_building = temple_of_baalat_gebal_01
	}
}
3889 = { #Halat
	holding = city_holding
}
3886 = { #Mechmech
	holding = church_holding
}
3882 = { #Faouqa
	holding = none
}
3883 = { #Qartaba
	holding = none
}
###c_hildua
3900 = { #Hildua
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3897 = { #Ain Dara
	holding = none
}
3898 = { #Baaqline
	holding = city_holding
}
3899 = { #Nîha
	holding = none
}
###c_beirut
3893 = { #Berot
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3891 = { #Bikfaïya
	holding = none
}
3895 = { #Hammâna
	holding = none
}
3890 = { #Jounieh
	holding = city_holding
}
###c_sannine
3892 = { #Sannine
	culture = phoenician
	religion = phoenician_faith
	holding = tribal_holding
}
3445 = { #Sua-llu
	holding = none
}
##d_tyre
###c_tyre
3982 = { #Tyre
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
	#holy_site = omen_melqart
	
	785.1.1 = {
		special_building_slot = temple_of_melqart_01 #Temple of Melqart
		duchy_capital_building = island_of_tyre_01
	}
	1310.1.1 = {
		special_building = temple_of_melqart_01
	}
}
3984 = { #Srifa
	holding = none
}
3989 = { #Jouaiyya
	holding = none
}
3992 = { #Usû
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
3983 = { #Maarakeh
	holding = church_holding
}
###c_baram
3991 = { #Bar'am
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3993 = { #Qânâ
	holding = none
}
3990 = { #Haris
	holding = city_holding
}
3987 = { #Tebnine
	holding = none
}
###c_acre
3997 = { #Akkô
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3996 = { #Akzîb
	holding = city_holding
}
3998 = { #Kêisam
	holding = church_holding
}
4187 = { #Kâbûl
	holding = city_holding
}
###c_hammon
3994 = { #Hammôn
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
4184 = { #Mi'ilyâ
	holding = city_holding
}
3995 = { #Ba'ali-râ'si
	holding = church_holding
}
4183 = { #Elon
	holding = city_holding
}
##d_halpa
###c_halpa
3867 = { #Irqata
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3865 = { #Halpa
	holding = city_holding
}
3863 = { #Fnaidek
	holding = none
}
3868 = { #Hrar
	holding = none
}
###c_munjez
3861 = { #Munjez
	culture = phoenician
	religion = phoenician_faith
	holding = tribal_holding

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3862 = { #Daoura
	holding = none
}
##d_sigata
###c_sigata
3877 = { #Batruna
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3876 = { #Sigata
	holding = city_holding
}
3885 = { #Zan
	holding = none
}
###c_kusbat
3874 = { #Kusbat
	culture = phoenician
	religion = phoenician_faith
	holding = tribal_holding
}
3879 = { #Cedar Forest
	holding = none
}
###c_ardata
3872 = { #Ardata
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3873 = { #Miziara
	holding = none
}
3871 = { #Miryata
	holding = city_holding
}
##d_sidon
###c_sidon
3905 = { #Sîdôn
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
	#holy_site = omen_eshmun
	
	785.1.1 = {
		special_building_slot = temple_of_eshmun_01 #Temple of Eshmun
	}
	1310.1.1 = {
		special_building = temple_of_eshmun_01
	}
}
3902 = { #Besri
	holding = church_holding
}
3903 = { #Gî'a
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
3906 = { #Habboûch
	holding = none
}
###c_sariptu
3908 = { #Sariptu
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
3911 = { #Insar
	holding = none
}
3910 = { #Ebba
	holding = city_holding
}