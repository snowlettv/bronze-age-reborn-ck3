#Orphaned Names - These names are not in any namelist or assigned to any character, but based on their position I think they are Lullubi.
#Immashkush
#Irib
#Darianam
#Ikki
#Ikkibshakhmat
#Hubaia
#Ianzu
#Musasina
#Ameka
#Arashtua
#Sabini

#List of Innovation replacements and Innovation triggers
#Courtly
#has_innovation = innovation_royal_prerogative #innovation_law_code #innovation_imperial_authority
#has_innovation = innovation_heraldry #innovation_kingship #innovation_empowered_aristocracy
#has_innovation = innovation_court_officials #innovation_distance_efficiency_4
#Glory Hound
#has_innovation = innovation_baliffs #innovation_law_code
#has_innovation = innovation_knighthood #innovation_charioteer_elite
#has_innovation = innovation_rightful_ownership #innovation_puppet_rulers
#Parochial
#has_innovation = innovation_manorialism #simple_agricultural_building_requirement_tier_1 = yes #innovation_economic_2
#has_innovation = innovation_land_grants #innovation_econ_3
#has_innovation = innovation_development_04 #innovation_econ_4
#Zealot
#has_innovation = innovation_chronicle_writing #innovation_conscription
#has_innovation = innovation_reconquista #Deleted
#has_innovation = innovation_divine_right #innovation_conscription #innovation_puppet_rulers #innovation_new_capitals
#has_innovation = innovation_primogeniture #innovation_designate_heir?

#innovation_bannus # innovation_conscription
#innovation_casus_belli #innovation_border_stones
#innovation_windmills #simple_agricultural_building_requirement_tier_2 = yes
#innovation_cranes #simple_agricultural_building_requirement_tier_3 = yes
#innovation_guilds #innovation_potters_wheel
#innovation_quilted_armor #innovation_swords
#innovation_arched_saddle #innovation_scale_armor
#innovation_development_02 #innovation_development_2
#innovation_battlements #innovation_earthworks
#innovation_noblesse_oblige #innovation_empowered_aristocracy
#innovation_men_at_arms #innovation_standing_armies
#innovation_royal_armory #innovation_barracks_4

#triple_cb_discount_trigger = {
#	culture = {
#		has_innovation  = innovation_border_stones
#		has_innovation  = innovation_conscription
#		has_innovation  = innovation_puppet_rulers
#	}
#}
#double_cb_discount_trigger = {
#	culture = {
#		OR = {
#			AND = {
#				has_innovation  = innovation_border_stones
#				has_innovation  = innovation_conscription
#			}
#			AND = {
#				has_innovation  = innovation_border_stones
#				has_innovation  = innovation_puppet_rulers
#			}
#			AND = {
#				has_innovation  = innovation_conscription
#				has_innovation  = innovation_puppet_rulers
#			}
#		}
#	}
#}
#single_cb_discount_trigger = {
#	culture = {
#		OR = {
#			has_innovation  = innovation_border_stones
#			has_innovation  = innovation_conscription
#			has_innovation  = innovation_puppet_rulers
#		}
#	}
#}
#unlocked_de_jure_cb_trigger = {
#	culture = {
#		has_innovation  = innovation_puppet_rulers
#	}
#}
#unlocked_raiding_cb_trigger = {
#	culture = {
#		has_innovation  = innovation_raiding_parties
#	}
#}
#single_county_de_jure_cb_trigger = {
#	always = yes
#}
#duchy_de_jure_cb_trigger = {
#	culture = {
#		has_innovation  = innovation_border_stones
#	}
#}
#tributary_cb_allowed_trigger = {
#	culture = {
#		has_innovation  = innovation_tributaries
#	}
#}
#multi_claim_own_allowed_trigger = {
#	culture = {
#		has_innovation  = innovation_conscription
#	}
#}
#multi_claim_other_allowed_trigger = {
#	culture = {
#		has_innovation  = innovation_puppet_rulers
#	}
#}
#governor_contract_trigger = {
#	culture = {
#		has_innovation = innovation_governors
#	}
#}
#has_all_tribal_era_innovations_trigger = {
#	has_innovation = innovation_earthworks
#	has_innovation = innovation_barracks
#	has_innovation = innovation_raiding_parties
#	has_innovation = innovation_mustering_grounds
#	has_innovation = innovation_tribal_vassals
#	has_innovation = innovation_bronze_socket_axe

#	has_innovation = innovation_city_planning
#	has_innovation = innovation_early_palaces
#	has_innovation = innovation_centralized_irrigation
#	has_innovation = innovation_potters_wheel
#	has_innovation = innovation_city_states
#	has_innovation = innovation_kingship
#	has_innovation = innovation_kings_justice
#	has_innovation = innovation_border_stones
#}
#innovation_crown_authority_1_trigger = { has_innovation = innovation_kings_justice } # 1 is the 2nd one in game, first is 0
#innovation_crown_authority_2_trigger = { has_innovation = innovation_law_code }
#innovation_crown_authority_3_trigger = { has_innovation = innovation_imperial_authority }
#innovation_distance_efficiency_1_trigger = { has_innovation = innovation_early_palaces }
#innovation_distance_efficiency_2_trigger = { has_innovation = innovation_palatial }
#innovation_distance_efficiency_3_trigger = { has_innovation = innovation_imperial_palaces }
#innovation_distance_efficiency_4_trigger = { has_innovation = innovation_distance_efficiency_4 }
#economic_building_requirement_tier_1 = { scope:holder.culture = { has_innovation = innovation_potters_wheel } }
#economic_building_requirement_tier_2 = { scope:holder.culture = { has_innovation = innovation_economic_2 } }
#economic_building_requirement_tier_3 = { scope:holder.culture = { has_innovation = innovation_econ_3 } }
#economic_building_requirement_tier_4 = { scope:holder.culture = { has_innovation = innovation_econ_4 } }
#
#agricultural_building_requirement_tier_1 = { scope:holder.culture = { has_innovation = innovation_centralized_irrigation } }
#agricultural_building_requirement_tier_2 = { scope:holder.culture = { has_innovation = innovation_artificial_resorvoirs } }
#agricultural_building_requirement_tier_3 = { scope:holder.culture = { has_innovation = innovation_agri_3 } }
#agricultural_building_requirement_tier_4 = { scope:holder.culture = { has_innovation = innovation_agri_4 } }
#
#military_building_requirement_tier_1 = { scope:holder.culture = { has_innovation = innovation_barracks } }
#military_building_requirement_tier_2 = { scope:holder.culture = { has_innovation = innovation_barracks_2 } }
#military_building_requirement_tier_3 = { scope:holder.culture = { has_innovation = innovation_barracks_3 } }
#military_building_requirement_tier_4 = { scope:holder.culture = { has_innovation = innovation_barracks_4 } }
#
#defensive_building_requirement_tier_1 = { scope:holder.culture = { has_innovation = innovation_earthworks } }
#defensive_building_requirement_tier_2 = { scope:holder.culture = { has_innovation = innovation_fortifications_2 } }
#defensive_building_requirement_tier_3 = { scope:holder.culture = { has_innovation = innovation_fortifications_3 } }
#defensive_building_requirement_tier_4 = { scope:holder.culture = { has_innovation = innovation_fortification_4 } }
#
#duchy_building_requirement_tier_1 = { culture = { has_innovation = innovation_early_palaces } }
#duchy_building_requirement_tier_2 = { culture = { has_innovation = innovation_palatial } }
#duchy_building_requirement_tier_3 = { culture = { has_innovation = innovation_imperial_palaces } }
#
#simple_economic_building_requirement_tier_1 = { has_innovation = innovation_potters_wheel }
#simple_economic_building_requirement_tier_2 = { has_innovation = innovation_economic_2 }
#simple_economic_building_requirement_tier_3 = { has_innovation = innovation_econ_3 }
#simple_economic_building_requirement_tier_4 = { has_innovation = innovation_econ_4 }
#
#simple_agricultural_building_requirement_tier_1 = { has_innovation = innovation_centralized_irrigation }
#simple_agricultural_building_requirement_tier_2 = { has_innovation = innovation_artificial_resorvoirs }
#simple_agricultural_building_requirement_tier_3 = { has_innovation = innovation_agri_3 }
#simple_agricultural_building_requirement_tier_4 = { has_innovation = innovation_agri_4 }
#
#simple_military_building_requirement_tier_1 = { has_innovation = innovation_barracks }
#simple_military_building_requirement_tier_2 = { has_innovation = innovation_barracks_2 }
#simple_military_building_requirement_tier_3 = { has_innovation = innovation_barracks_3 }
#simple_military_building_requirement_tier_4 = { has_innovation = innovation_barracks_4 }
#
#simple_defensive_building_requirement_tier_1 = { has_innovation = innovation_earthworks }
#simple_defensive_building_requirement_tier_2 = { has_innovation = innovation_fortifications_2 }
#simple_defensive_building_requirement_tier_3 = { has_innovation = innovation_fortifications_3 }
#simple_defensive_building_requirement_tier_4 = { has_innovation = innovation_fortification_4 }