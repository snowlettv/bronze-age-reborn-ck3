﻿language_placeholder = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_placeholder
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_placeholder }
			multiply = 10
		}
	}
	
	color = amorite #placeholder
}

language_amorite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_amorite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_amorite }
			multiply = 10
		}
	}
	
	color = amorite
}

language_eblaite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_eblaite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_eblaite }
			multiply = 10
		}
	}
	
	color = eblaite
}

language_aramean = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_aramean
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_aramean }
			multiply = 10
		}
	}
	
	color = aramean
}

language_canaanite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_canaanite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_canaanite }
			multiply = 10
		}
	}
	
	color = canaanite
}

language_hurrian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_hurrian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_hurrian }
			multiply = 10
		}
	}
	
	color = hurrian
}

language_akkadian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_akkadian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_akkadian }
			multiply = 10
		}
	}
	
	color = akkadian
}

language_kemetic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kemetic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kemetic }
			multiply = 10
		}
	}
	
	color = kemetic
}

language_elamite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_elamite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_elamite }
			multiply = 10
		}
	}
	
	color = elamite
}

language_gutian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_gutian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_gutian }
			multiply = 10
		}
	}
	
	color = gutian
}

language_lullubi = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_lullubi
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_lullubi }
			multiply = 10
		}
	}
	
	color = lullubi
}

language_kassite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kassite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kassite }
			multiply = 10
		}
	}
	
	color = kassite
}

language_proto_greek = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_proto_greek
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_proto_greek }
			multiply = 10
		}
	}
	
	color = proto_greek
}

language_greek = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_greek
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_greek }
			multiply = 10
		}
	}
	
	color = proto_greek
}

language_sumerian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_sumerian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_sumerian }
			multiply = 10
		}
	}
	
	color = sumerian
}

language_phrygian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_phrygian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_phrygian }
			multiply = 10
		}
	}
	
	color = phrygian
}

language_hattian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_hattian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_hattian }
			multiply = 10
		}
	}
	
	color = hattian
}

language_maganite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_maganite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_maganite }
			multiply = 10
		}
	}
	
	color = maganite
}

language_kushite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kushite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kushite }
			multiply = 10
		}
	}
	
	color = kushite
}

language_cypriot = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_cypriot
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_cypriot }
			multiply = 10
		}
	}
	
	color = cypriot
}

language_berber = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_berber
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_berber }
			multiply = 10
		}
	}
	
	color = libyan
}

language_hittite = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_hittite
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_hittite }
			multiply = 10
		}
	}
	
	color = hittite
}

language_luwian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_luwian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_luwian }
			multiply = 10
		}
	}
	
	color = luwian
}

language_minoan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_minoan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_minoan }
			multiply = 10
		}
	}
	
	color = minoan
}

language_tyrsenian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_tyrsenian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_tyrsenian }
			multiply = 10
		}
	}
	
	color = tyrsenian
}

language_lelegian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_lelegian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_lelegian }
			multiply = 10
		}
	}
	
	color = lelegian
}

language_helladic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_helladic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_helladic }
			multiply = 10
		}
	}
	
	color = helladic
}

language_thracian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_thracian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_thracian }
			multiply = 10
		}
	}
	
	color = thracian
}

language_indo_aryan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_indo_aryan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_indo_aryan }
			multiply = 10
		}
	}
	
	color = punjabi
}

language_proto_iranian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_proto_iranian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_proto_iranian }
			multiply = 10
		}
	}
	
	color = persian
}

language_kartvelian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_kartvelian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_kartvelian }
			multiply = 10
		}
	}
	
	color = mushki
}

language_jiroft = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_jiroft
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_jiroft }
			multiply = 10
		}
	}
	
	color = jiroft
}

language_harappan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_harappan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_harappan }
			multiply = 10
		}
	}
	
	color = harappan
}

language_proto_italic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_proto_italic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_proto_italic }
			multiply = 10
		}
	}
	
	color = proto_italic
}

language_palaic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_palaic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_palaic }
			multiply = 10
		}
	}
	
	color = palaic
}

language_lydian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_lydian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_lydian }
			multiply = 10
		}
	}
	
	color = lydian
}

language_central_arabian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_central_arabian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_central_arabian }
			multiply = 10
		}
	}
	
	color = central_arabian
}

language_south_arabian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_south_arabian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_south_arabian }
			multiply = 10
		}
	}
	
	color = south_arabian
}

language_medjay = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_medjay
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_medjay }
			multiply = 10
		}
	}
	
	color = medjay
}

language_puntic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_puntic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_puntic }
			multiply = 10
		}
	}
	
	color = puntic
}

language_nuragic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_nuragic
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_nuragic }
			multiply = 10
		}
	}
	
	color = sardinian
}

language_paleoiberian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_paleoiberian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_paleoiberian }
			multiply = 10
		}
	}
	
	color = basque
}

language_urartuan = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_urartuan
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_urartuan }
			multiply = 10
		}
	}
	
	color = urartuan
}

language_proto_armenian = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_proto_armenian
		}
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_proto_armenian }
			multiply = 10
		}
	}
	
	color = proto_armenian
}

