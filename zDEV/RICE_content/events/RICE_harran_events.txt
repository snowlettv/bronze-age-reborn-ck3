﻿###############################################################################
# 
# AACHEN: Charlemagne's Capital
# 
# harran.0000-harran.0019	Misc Events (including Decisions)
# harran.0080-harran.0099	Harran flavor events
# 
###############################################################################

namespace = harran

######################################################################################
# 
# MISCELLANEOUS EVENTS (including decisions)
# 
# harran.0000-harran.0019 reserved
# 
######################################################################################

# Pray to a Planet
harran.0002 = {
	type = character_event
	title = harran.0002.t
	desc = harran.0002.desc
	theme = RICE_theme_desert_night
	
	right_portrait = {
		character = root
		animation = personality_honorable
	}

	option = {
		name = harran.0002.a
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.sun.blessing
					add_character_modifier = {
						modifier = RICE_harran_sun_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.sun.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.b
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.moon.blessing
					add_character_modifier = {
						modifier = RICE_harran_moon_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.moon.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.c
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.mercury.blessing
					add_character_modifier = {
						modifier = RICE_harran_mercury_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.mercury.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.d
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.venus.blessing
					add_character_modifier = {
						modifier = RICE_harran_venus_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.venus.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.e
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.mars.blessing
					add_character_modifier = {
						modifier = RICE_harran_mars_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.mars.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.f
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.jupiter.blessing
					add_character_modifier = {
						modifier = RICE_harran_jupiter_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.jupiter.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	option = {
		name = harran.0002.g
		random_list = {
			40 = {	
				modifier = {
					add = 5
					num_virtuous_traits >= 1
				}
				modifier = {
					add = 5
					num_virtuous_traits >= 2
				}
				modifier = {
					add = 5
					learning >= decent_skill_rating
				}
				send_interface_toast = {
					type = event_toast_effect_good
					left_icon = ROOT
					title = harran.0002.saturn.blessing
					add_character_modifier = {
						modifier = RICE_harran_saturn_blessing
						years = 7
					}
				}			
			}
			40 = {
				modifier = {
					add = -5
					num_sinful_traits >= 1
				}
				modifier = {
					add = -5
					num_sinful_traits >= 2
				}
				modifier = {
					add = 5
					piety_level < 2
				}	
				send_interface_toast = {
					left_icon = ROOT
					title = harran.0002.saturn.nothing
					custom_tooltip = harran.0002.nothing.tooltip
				}			
			}
		}
	}
	after = {		
		RICE_remove_planned_activity_flag_effect = yes
	}
}

######################################################################################
# 
# HARRANIAN FLAVOR EVENTS
# 
# harran.0080-harran.0099 reserved
# 
######################################################################################

# Harran divination good
harran.0081 = {
	type = character_event
	title = harran.0081.t
	desc = harran.0081.desc
	theme = RICE_theme_desert_night
	
	left_portrait = root
	
	trigger = {
		NOT = {
			any_held_title = {
				tier = tier_county
				county = { has_county_modifier = RICE_harran_good_divination }
			}
		}
		is_mesopotamian_trigger = yes
	}

	weight_multiplier = {
		base = 1
		modifier = {
			add = 0.2
			has_trait = diligent
		}
		modifier = {
			add = -0.2
			has_trait = lazy
		}
		modifier = {
			add = 0.2
			has_trait = content
		}
		modifier = {
			add = -0.2
			has_trait = ambitious
		}
		modifier = {
			add = 0.2
			has_trait = just
		}
		modifier = {
			add = -0.2
			has_trait = arbitrary
		}
	}

	option = { # Ok
		name = harran.0081.a
		random_held_title = {
			save_scope_as = target_county
		}
		scope:target_county = {
			add_county_modifier = {
				modifier = RICE_harran_good_divination
				years = 10
			}
		}
	}
}


# Harran divination bad
harran.0082 = {
	type = character_event
	title = harran.0082.t
	desc = harran.0082.desc
	theme = RICE_theme_desert_night
	
	left_portrait = root
	
	trigger = {
		NOT = {
			any_held_title = {
				tier = tier_county
				county = { has_county_modifier = RICE_harran_bad_divination }
			}
		}
		is_mesopotamian_trigger = yes
	}

	weight_multiplier = {
		base = 1
		modifier = {
			add = 0.2
			has_trait = lazy
		}
		modifier = {
			add = -0.2
			has_trait = diligent
		}
		modifier = {
			add = 0.2
			has_trait = ambitious
		}
		modifier = {
			add = -0.2
			has_trait = content
		}
		modifier = {
			add = 0.2
			has_trait = arbitrary
		}
		modifier = {
			add = -0.2
			has_trait = just
		}
	}

	option = { # Ok
		name = harran.0082.a
		random_held_title = {
			save_scope_as = target_county
		}
		scope:target_county = {
			add_county_modifier = {
				modifier = RICE_harran_bad_divination
				years = 10
			}
		}
	}
}


# Visiting hive hosues in Harran
harran.0083 = {
	type = character_event
	title = harran.0083.t
	desc = harran.0083.desc
	theme = RICE_theme_harran_town
	
	left_portrait = {
		character = root
		animation = personality_compassionate
	}
	
	trigger = {
		has_title = title:c_harran
	}

	weight_multiplier = {
		base = 1
		modifier = {
			add = 0.2
			has_trait = gregarious
		}
		modifier = {
			add = -0.2
			has_trait = shy
		}
		modifier = {
			add = 0.2
			has_trait = diligent
		}
		modifier = {
			add = -0.2
			has_trait = lazy
		}
		modifier = {
			add = 0.2
			has_trait = patient
		}
		modifier = {
			add = -0.2
			has_trait = impatient
		}
	}

	option = { # Ok
		name = harran.0083.a
		add_piety = 20
	}
	option = { # Ok
		name = harran.0083.b
		add_prestige = 20
	}
}


# Tammuz festival
harran.0084 = {
	type = character_event
	title = harran.0084.t
	desc = harran.0084.desc
	theme = RICE_theme_harran_town
	override_icon = { reference = "gfx/interface/icons/event_types/type_faith.dds" }
	
	left_portrait = root
	
	trigger = {
		NOT = {
			any_held_title = {
				tier = tier_county
				county = { has_county_modifier = RICE_harran_tammuz_festival }
			}
		}
		is_mesopotamian_trigger = yes
	}

	weight_multiplier = {
		base = 1
		modifier = {
			add = 0.2
			has_trait = compassionate
		}
		modifier = {
			add = -0.2
			has_trait = callous
		}
		modifier = {
			add = 0.2
			has_trait = gregarious
		}
		modifier = {
			add = -0.2
			has_trait = shy
		}
		modifier = {
			add = 0.2
			has_trait = calm
		}
		modifier = {
			add = -0.2
			has_trait = wrathful
		}
	}
	
	immediate = {
		random_held_title = {
			save_scope_as = target_county
		}
		scope:target_county = {
			add_county_modifier = {
				modifier = RICE_harran_tammuz_festival
				years = 5
			}
		}	
	}

	option = { # Ok
		name = harran.0084.a
		add_prestige = 40
	}
}

