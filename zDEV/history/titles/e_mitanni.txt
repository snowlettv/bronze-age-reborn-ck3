﻿e_mitanni = {
	1310.1.1 = {
		effect = {
			set_capital_county = title:c_wassukanni
		}
	}
}
k_qattanun = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
####
d_qattanun = {
	785.1.1 = {
		holder = 335
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		de_jure_liege = k_mari
		holder = 976
	}
	1311.1.1 = {
		holder = 0
	}
}
c_qattanun = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 335
	}
	1310.1.1 = {
		holder = 976
	}
}
c_durkatlimmu = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 335
	}
	1310.1.1 = {
		holder = 976
	}
}
####
d_razanma = {
	1310.1.1 = {
		de_jure_liege = k_singara
	}
}
c_razanma = {
	785.1.1 = {
		holder = 337
	}
	1310.1.1 = {
		holder = 1269
	}
}
c_riksati = {
	785.1.1 = {
		holder = 337
	}
	1310.1.1 = {
		liege = "d_qattanun"
		holder = 1697
	}
	1311.1.1 = {
		liege = 0
	}
}
c_nabutu = {
	785.1.1 = {
		holder = 844
	}
	1310.1.1 = {
		liege = "d_qattanun"
		holder = 1698
	}
	1311.1.1 = {
		liege = 0
	}
}
c_qeberu = {
	785.1.1 = {
		holder = 336
	}
	1310.1.1 = {
		liege = "d_andarig"
		holder = 1699
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_rapsu = {
	1310.1.1 = {
		de_jure_liege = k_singara
	}
}
c_rapsu = {
	785.1.1 = {
		holder = 337
	}
	1310.1.1 = {
		holder = 1269
	}
}
c_nibu = {
	785.1.1 = {
		holder = 361
	}
	1310.1.1 = {
		holder = 977
	}
}
c_tiittu = {
	785.1.1 = {
		holder = 361
	}
	1310.1.1 = {
		holder = 1269
	}
}
####
d_habur = {
	785.1.1 = {
		holder = 360
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		de_jure_liege = k_khabur
		holder = 975
	}
	1311.1.1 = {
		holder = 0
	}
}
c_tabatum = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 360
	}
	1310.1.1 = {
		holder = 975
	}
}
c_sadikanni = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 335
	}
	1310.1.1 = {
		holder = 975
	}
}
c_sikiri = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_qattanun"
		holder = 845
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 976
	}
}
c_liddatu = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 360
	}
	1310.1.1 = {
		holder = 975
	}
}
####
k_dibar = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
}
####
d_dibar = {
	785.1.1 = {
		holder = 343
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		de_jure_liege = k_khabur
		holder = 974
	}
	1311.1.1 = {
		holder = 0
	}
}
c_haddu = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 343
	}
	1310.1.1 = {
		holder = 974
	}
}
c_diruu = {
	785.1.1 = {
		liege = "d_dibar"
		holder = 846
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_dibar"
		holder = 1700
	}
	1311.1.1 = {
		liege = 0
	}
}
c_susabinnu = {
	785.1.1 = {
		liege = "d_dibar"
		holder = 847
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_dibar"
		holder = 1701
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_saha = {
	1310.1.1 = {
		de_jure_liege = k_harran
	}
}
c_kunshu = {
	785.1.1 = {
		holder = 341
	}
	1310.1.1 = {
		holder = 973
	}
}
c_tublu = {
	785.1.1 = {
		holder = 342
	}
	1310.1.1 = {
		holder = 973
	}
}
c_ishtanu = {
	785.1.1 = {
		holder = 341
	}
	1310.1.1 = {
		liege = "d_arnabani"
		holder = 1702
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_arnabani = {
	1310.1.1 = {
		liege = "k_khabur"
		de_jure_liege = k_khabur
		holder = 972
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_arnabani = {
	785.1.1 = { change_development_level = 9 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 344
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 972
	}
	1311.1.1 = {
		liege = 0
	}
}
c_shernu = {
	1310.1.1 = { change_development_level = 17 }
	785.1.1 = {
		holder = 344
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 972
	}
	1311.1.1 = {
		liege = 0
	}
}
c_mangu = {
	785.1.1 = {
		holder = 342
	}
	1310.1.1 = {
		liege = "d_arnabani"
		holder = 1703
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_katmuhi = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
####
#d_katmuhi
c_katmuhi = {
	785.1.1 = {
		holder = 356
	}
	1310.1.1 = {
		holder = 1280
	}
}
c_kibaki = {
	785.1.1 = {
		holder = 356
	}
	1310.1.1 = {
		holder = 1280
	}
}
####
#d_kasiyari
c_shuru = {
	785.1.1 = {
		holder = 354
	}
	1310.1.1 = {
		holder = 1278
	}
}
c_singisa = {
	785.1.1 = {
		holder = 353
	}
	1310.1.1 = {
		liege = "d_tushan"
		holder = 1704
	}
	1311.1.1 = {
		liege = 0
	}
}
c_mardiyane = {
	785.1.1 = {
		holder = 353
	}
	1310.1.1 = {
		holder = 1278
	}
}
####
#d_simanum
c_simanum = {
	785.1.1 = {
		holder = 355
	}
	1310.1.1 = {
		holder = 1279
	}
}
c_uda2 = {
	785.1.1 = {
		holder = 354
	}
	1310.1.1 = {
		holder = 1278
	}
}
c_matiati = {
	785.1.1 = {
		holder = 355
	}
	1310.1.1 = {
		holder = 1279
	}
}
####
k_nairi = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
}
####
d_eluhat = {
	785.1.1 = {
		holder = 347
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1273
	}
	1311.1.1 = {
		holder = 0
	}
}
c_amidu = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 347
	}
	1310.1.1 = {
		holder = 1273
	}
}
c_mamitu = {
	785.1.1 = {
		holder = 367
	}
	1310.1.1 = {
		liege = "d_eluhat"
		holder = 1705
	}
	1311.1.1 = {
		liege = 0
	}
}
c_kamalu = {
	785.1.1 = {
		holder = 347
	}
	1310.1.1 = {
		holder = 1273
	}
}
####
#d_amadani
c_magulu = {
	785.1.1 = {
		holder = 347
	}
	1310.1.1 = {
		holder = 1273
	}
}
c_dunnunu = {
	785.1.1 = {
		holder = 370
	}
	1310.1.1 = {
		holder = 1706
	}
}
####
d_tushan = {
	1310.1.1 = {
		holder = 1276
	}
	1311.1.1 = {
		holder = 0
	}
}
c_tushan = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 351
	}
	1310.1.1 = {
		holder = 1276
	}
}
c_tela = {
	785.1.1 = {
		holder = 351
	}
	1310.1.1 = {
		holder = 1276
	}
}
c_adru = {
	785.1.1 = {
		holder = 351
	}
	1310.1.1 = {
		holder = 1276
	}
}
####
#d_bityahiri
c_maqatu = {
	785.1.1 = {
		holder = 352
	}
	1310.1.1 = {
		holder = 1277
	}
}
c_suharruru = {
	785.1.1 = {
		holder = 352
	}
	1310.1.1 = {
		holder = 1277
	}
}
####
k_shubria = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
}
####
d_upumu = {
	1310.1.1 = {
		holder = 1275
	}
	1311.1.1 = {
		holder = 0
	}
}
c_upumu = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 349
	}
	1310.1.1 = {
		holder = 1275
	}
}
c_timali = {
	785.1.1 = {
		holder = 349
	}
	1310.1.1 = {
		holder = 1275
	}
}
####
#d_habhutu
c_habhutu = {
	785.1.1 = {
		holder = 848
	}
	1310.1.1 = {
		holder = 1707
	}
}
c_ashibu = {
	785.1.1 = {
		holder = 849
	}
	1310.1.1 = {
		liege = "d_qana"
		holder = 1708
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_kulimmeri
c_kulimmeri = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 368
	}
	1310.1.1 = {
		holder = 1709
	}
}
c_darutu = {
	785.1.1 = {
		holder = 368
	}
	1310.1.1 = {
		liege = "d_upumu"
		holder = 1710
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_diglat
c_diglat = {
	785.1.1 = {
		holder = 350
	}
	1310.1.1 = {
		holder = 1275
	}
}
c_abbissa = {
	785.1.1 = {
		holder = 350
	}
	1310.1.1 = {
		holder = 1275
	}
}
####
d_qilissa = {
	1310.1.1 = {
		holder = 1274
	}
	1311.1.1 = {
		holder = 0
	}
}
c_qilissa = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 348
	}
	1310.1.1 = {
		holder = 1274
	}
}
c_hallut = {
	785.1.1 = {
		holder = 348
	}
	1310.1.1 = {
		holder = 1274
	}
}
####
#d_zamani
c_gammurti = {
	785.1.1 = {
		holder = 367
	}
	1310.1.1 = {
		liege = "d_qilissa"
		holder = 1711
	}
	1311.1.1 = {
		liege = 0
	}
}
c_sullumu = {
	785.1.1 = {
		holder = 348
	}
	1310.1.1 = {
		holder = 1274
	}
}
####
k_sumu = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
}
####
d_kaprabi = {
	1310.1.1 = {
		holder = 1272
	}
	1311.1.1 = {
		holder = 0
	}
}
c_kaprabi = {
	785.1.1 = {
		holder = 346
	}
	1310.1.1 = {
		holder = 1272
	}
}
c_sikkanu = {
	785.1.1 = {
		holder = 370
	}
	1310.1.1 = {
		liege = "d_kaprabi"
		holder = 1712
	}
	1311.1.1 = {
		liege = 0
	}
}
c_daku = {
	785.1.1 = {
		holder = 371
	}
	1310.1.1 = {
		holder = 1272
	}
}
c_naslamu = {
	785.1.1 = {
		holder = 371
	}
	1310.1.1 = {
		holder = 1272
	}
}
####
#d_habhu
c_tanihtu = {
	785.1.1 = {
		holder = 850
	}
	1310.1.1 = {
		holder = 1271
	}
}
c_takne = {
	785.1.1 = {
		holder = 369
	}
	1310.1.1 = {
		holder = 1271
	}
}
####
d_qipanu = {
	1310.1.1 = {
		holder = 1270
	}
	1311.1.1 = {
		holder = 0
	}
}
c_mashkanutu = {
	785.1.1 = {
		holder = 346
	}
	1310.1.1 = {
		holder = 1270
	}
}
c_tupullu = {
	785.1.1 = {
		holder = 369
	}
	1310.1.1 = {
		holder = 1270
	}
}
c_niputum = {
	785.1.1 = {
		liege = "d_harran"
		holder = 851
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1270
	}
}
####
k_kummuhi = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
}
####
d_kummuhi = {
	785.1.1 = {
		holder = 338
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1264
	}
	1311.1.1 = {
		holder = 0
	}
}
c_kummuhi = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 338
	}
	1310.1.1 = {
		holder = 1264
	}
}
c_shushu = {
	785.1.1 = {
		holder = 338
	}
	1310.1.1 = {
		holder = 1264
	}
}
c_hulalessar = {
	785.1.1 = {
		holder = 852
	}
	1310.1.1 = {
		holder = 1713
	}
}
####
d_halpi = {
	785.1.1 = {
		holder = 339
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1224
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_halpi = {
	785.1.1 = {
		holder = 339
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1224
	}
	1311.1.1 = {
		liege = 0
	}
}
c_sukziya = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = 339
	}
	1310.1.1 = {
		liege = "k_hattusa"
		holder = 1224
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_upperpurattu = {
	1310.1.1 = {
		holder = 1264
	}
	1311.1.1 = {
		holder = 0
	}
}
c_shigushu = {
	785.1.1 = {
		liege = "d_kummuhi"
		holder = 853
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1264
	}
}
c_meliu = {
	785.1.1 = {
		liege = "d_halpi"
		holder = 854
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_upperpurattu"
		holder = 1714
	}
	1311.1.1 = {
		liege = 0
	}
}
c_sheru = {
	785.1.1 = {
		liege = "d_kummuhi"
		holder = 855
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1264
	}
}
####
k_harran = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
}
####
d_hasamu = {
	1310.1.1 = {
		holder = 1263
	}
	1311.1.1 = {
		holder = 0
	}
}
c_kiru = {
	785.1.1 = {
		liege = "d_harran"
		holder = 856
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1263
	}
}
c_laqatu = {
	785.1.1 = {
		holder = 345
	}
	1310.1.1 = {
		holder = 1263
	}
}
c_sirimtu = {
	785.1.1 = {
		holder = 345
	}
	1310.1.1 = {
		holder = 1263
	}
}
c_qallu = {
	785.1.1 = {
		liege = "d_harran"
		holder = 857
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1263
	}
}
c_shihtu = {
	785.1.1 = {
		holder = 345
	}
	1310.1.1 = {
		liege = "d_wassukanni"
		holder = 1715
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_sahlala = {
	785.1.1 = {
		holder = 237
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1262
	}
	1311.1.1 = {
		holder = 0
	}
}
c_sahlala = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = 237
	}
	1310.1.1 = {
		holder = 1262
	}
}
c_zalpa = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = 237
	}
	1310.1.1 = {
		liege = "d_sahlala"
		holder = 1716
	}
	1311.1.1 = {
		liege = 0
	}
}
c_gerkinakku = {
	785.1.1 = {
		holder = 236
	}
	1310.1.1 = {
		liege = "d_sahlala"
		holder = 1717
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_harran = {
	785.1.1 = {
		holder = 235
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1262
	}
	1311.1.1 = {
		holder = 0
	}
}
c_harran = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = 235
	}
	1310.1.1 = {
		holder = 1262
	}
}
c_duru = {
	785.1.1 = { change_development_level = 10 }
	1310.1.1 = { change_development_level = 18 }
	785.1.1 = {
		holder = 235
	}
	1310.1.1 = {
		holder = 1262
	}
}
c_rugulutu = {
	785.1.1 = {
		holder = 340
	}
	1310.1.1 = {
		liege = "d_harran"
		holder = 1718
	}
	1311.1.1 = {
		liege = 0
	}
}
c_seru = {
	785.1.1 = {
		holder = 340
	}
	1310.1.1 = {
		liege = "d_harran"
		holder = 1719
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_khabur = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 16 }
	1310.1.1 = {
		holder = 2keret1
		name = MITANNI_NAME
		effect = {
			set_capital_county = title:c_wassukanni
		}
	}
	1311.1.1 = {
		holder = 0
	}
}
####
d_siimme = {
	785.1.1 = {
		holder = 403
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		de_jure_liege = k_singara
	}
}
c_siimme = {
	785.1.1 = {
		holder = 403
	}
	1310.1.1 = {
		holder = 1256
	}
}
c_idirtu = {
	785.1.1 = {
		holder = 403
	}
	1310.1.1 = {
		liege = "d_nagar"
		holder = 1720
	}
	1311.1.1 = {
		liege = 0
	}
}
c_dumamu = {
	785.1.1 = {
		liege = "d_siimme"
		holder = 858
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1256
	}
}
####
d_nagar = {
	785.1.1 = {
		holder = 334
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 977
	}
	1311.1.1 = {
		holder = 0
	}
}
c_nagar = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 334
	}
	1310.1.1 = {
		holder = 977
	}
}
c_shekhna = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_nagar"
		holder = 859
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 977
	}
}
c_saluppu = {
	785.1.1 = {
		holder = 334
	}
	1310.1.1 = {
		holder = 977
	}
}
####
#d_nawar
c_nawar = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		liege = "d_urkesh"
		holder = 860
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_nagar"
		holder = 1721
	}
	1311.1.1 = {
		liege = 0
	}
}
c_nasibina = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 332
	}
	1310.1.1 = {
		liege = "d_urkesh"
		holder = 1722
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_nabada = {
	1310.1.1 = {
		liege = "k_khabur"
		holder = 970
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_nabada = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 333
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 970
	}
	1311.1.1 = {
		liege = 0
	}
}
c_kurda = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 333
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 970
	}
	1311.1.1 = {
		liege = 0
	}
}
c_karsu = {
	785.1.1 = { change_development_level = 11 }
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 333
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 970
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_hurra = {
	1310.1.1 = {
		liege = "k_khabur"
		holder = 969
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_hurra = {
	785.1.1 = {
		liege = "d_urkesh"
		holder = 861
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 969
	}
	1311.1.1 = {
		liege = 0
	}
}
c_aridu = {
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 358
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 969
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_sudu = {
	785.1.1 = {
		holder = 359
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 2keret1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_sudu = {
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 359
	}
	1310.1.1 = {
		holder = 2keret1
	}
}
c_matnu = {
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 359
	}
	1310.1.1 = {
		holder = 2keret1
	}
}
c_kinutu = {
	785.1.1 = {
		holder = 358
	}
	1310.1.1 = {
		liege = "d_sudu"
		holder = 1723
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_wassukanni = {
	1310.1.1 = {
		holder = 2keret1
	}
	1311.1.1 = {
		holder = 0
	}
}
c_wassukanni = {
	1310.1.1 = { change_development_level = 25 }
	785.1.1 = {
		liege = "d_sudu"
		holder = 862
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 2keret1
	}
}
c_guzana = {
	1310.1.1 = { change_development_level = 20 }
	785.1.1 = {
		holder = 344
	}
	1310.1.1 = {
		holder = 2keret1
	}
}
c_marditu = {
	785.1.1 = {
		holder = 346
	}
	1310.1.1 = {
		holder = 2keret1
	}
}
c_pardesu = {
	785.1.1 = {
		holder = 863
	}
	1310.1.1 = {
		liege = "d_wassukanni"
		holder = 1724
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_urkesh = {
	785.1.1 = {
		holder = 332
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 971
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_urkesh = {
	785.1.1 = { change_development_level = 14 }
	1310.1.1 = { change_development_level = 23 }
	785.1.1 = {
		holder = 332
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 971
	}
	1311.1.1 = {
		liege = 0
	}
}
c_abashlu = {
	785.1.1 = {
		holder = 332
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 971
	}
	1311.1.1 = {
		liege = 0
	}
}
c_dintu = {
	785.1.1 = {
		liege = "d_urkesh"
		holder = 864
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_khabur"
		holder = 971
	}
	1311.1.1 = {
		liege = 0
	}
}
####