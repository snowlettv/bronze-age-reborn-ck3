﻿namespace = fp1_yearly

##################################################
# When I Grow Up
# by Ewan Cowhig Croft
# 0581 - 0590
##################################################

scripted_trigger fp1_0581_wrong_gender_child_wishes_to_fight_trigger = {
	NOT = { has_character_flag = fp1_dreams_of_shieldpersondom }
	trigger_if = { # Has the Royal Court and thus can modify pillars
		limit = { has_dlc_feature = diverge_culture }
		# For ease, they must share your faith or your culture.
		OR = {
			culture = root.culture
			faith = root.faith
		}
	}
	trigger_else = {
		# For ease, they must share your faith.
		faith = root.faith
	}
	# Have an appropriate matching personality.
	OR = {
		childhood_loud_child_trigger = yes
		childhood_mean_child_trigger = yes
	}
	# Be the wrong gender to fight.
	NOT = { can_be_combatant_based_on_gender_trigger = { ARMY_OWNER = root } }
	# Aaaand be available to play.
	child_suitable_to_play_with_character_one_sided = { CHARACTER = root }
}

scripted_trigger fp1_0581_valid_courtier_to_bully_trigger = {
	# No positive relationship with root.
	has_any_good_relationship_with_root_trigger = no
	# And they have a negative opinion of you; player only action, so we only care about making sure the player might dislike this character.
	opinion = {
		target = root
		value <= medium_negative_opinion
	}
}

#	Your wrong-gender child wants to fight and raid in a few years.
fp1_yearly.0581 = {
	type = character_event
	title = fp1_yearly.0581.t
	desc = {
		desc = fp1_yearly.0581.desc.intro
		# Currently causes too much risk of scrolling copy.
		triggered_desc = {
			trigger = { always = no }
			desc = fp1_yearly.0581.desc.victim
		}
		desc = fp1_yearly.0581.desc.outro
	}
	theme = martial
	left_portrait = {
		character = root
		animation = personality_rational
	}
	right_portrait = {
		character = scope:child
		animation = admiration
	}
	lower_right_portrait = scope:victim
	override_background = { reference = corridor_day }

	trigger = {
		# DLC check.
		has_fp1_dlc_trigger = yes
		# Standard checks.
		is_available_even_at_war_adult = yes
		NOT = { has_character_flag = had_event_fp1_yearly_0581 }
		# Filter to shieldmaiden-having culture.
		culture = { has_cultural_parameter = has_access_to_shieldmaidens }
		trigger_if = { # Has the Royal Court and thus can modify pillars
			limit = { has_dlc_feature = diverge_culture }
			culture = { NOT = { has_cultural_parameter = martial_custom_equal_combatant } }
		}
		trigger_else = {
			# Can't be following an equal faith.
			faith = { NOT = { has_doctrine_parameter = combatant_can_be_either_gender_if_no_roco } }
		}
		# Any child of yours might want to be a shieldperson when they grow up.
		any_child = { fp1_0581_wrong_gender_child_wishes_to_fight_trigger = yes }
	}

	weight_multiplier = {
		base = 1

		# Shieldmaidens are something of an inspiration, and thus
		modifier = {
			add = 1
			has_trait = shieldmaiden
		}

		# If you're of low prowess, then there's less to aspire to.
		modifier = {
			add = -0.5
			prowess <= medium_skill_rating
		}
		# But if you're of high prowess, then there's plenty.
		modifier = {
			add = 0.5
			prowess >= very_high_skill_rating
		}
	}

	immediate = {
		add_character_flag = {
			flag = had_event_fp1_yearly_0581
			days = 1825
		}
		# Nab an appropriate child.
		random_child = {
			limit = { fp1_0581_wrong_gender_child_wishes_to_fight_trigger = yes }
			# Weight up the most promising fighter amongst your valid children.
			weight = {
				base = 0
				modifier = {
					add = {
						value = prowess
						multiply = 100
					}
				}
			}
			save_scope_as = child
		}
		# See if there's someone at court you can set the child to bully.
		if = {
			limit = {
				# Keep this player-specific for ease of presentation.
				is_ai = no
				any_courtier_or_guest = { fp1_0581_valid_courtier_to_bully_trigger = yes }
			}
			random_courtier_or_guest = {
				# Try to pick rivals et al first.
				limit = {
					has_any_bad_relationship_with_root_trigger = yes
					fp1_0581_valid_courtier_to_bully_trigger = yes
				}
				# Otherwise just any random dick will do.
				alternative_limit = { fp1_0581_valid_courtier_to_bully_trigger = yes }
				save_scope_as = victim
			}
		}
	}

	# My child, you shall make the world itself *tremble*.
	option = {
		name = fp1_yearly.0581.a
		trigger = { has_trait = shieldmaiden }
		trait = shieldmaiden

		# Scope:child is significantly more likely to try to become a shieldmaiden.
		custom_tooltip = fp1_yearly.0581.a.tt
		scope:child = { add_character_flag = fp1_dreams_of_shieldpersondom }
		# Scope:child gains prowess.
		scope:child = { add_prowess_skill = massive_skill_bonus }
		# Scope:child gains opinion.
		reverse_add_opinion = {
			target = scope:child
			modifier = love_opinion
			opinion = 60
		}

		stress_impact = {
			shieldmaiden = major_stress_impact_loss
			brave = major_stress_impact_loss
			ambitious = major_stress_impact_loss
			compassionate = major_stress_impact_gain
			humble = major_stress_impact_gain
		}
		ai_chance = {
			base = 100
			ai_value_modifier = {
				ai_honor = 0.75
				ai_zeal = 0.5
				ai_energy = 0.25
			}
			modifier = {	# Weight down for stress.
				add = -30
				has_trait = compassionate
			}
			modifier = {	# Weight down for stress.
				add = -30
				has_trait = humble
			}
			# Raiders & adventurers want their prodigy to follow in their footsteps.
			modifier = {
				add = 20
				has_trait = viking
			}
			modifier = {
				add = 20
				has_trait = adventurer
			}
			modifier = {
				add = 20
				has_trait = brave
			}
			modifier = {
				add = 20
				has_trait = shieldmaiden
			}
			modifier = {
				add = 20
				has_trait = ambitious
			}
		}
	}

	# A noble goal! Have at thee, rogue!
	option = {
		name = fp1_yearly.0581.b
		trigger = {
			NOT = { has_trait = shieldmaiden }
		}

		# Scope:child is significantly more likely to try to become a shieldmaiden.
		custom_tooltip = fp1_yearly.0581.b.tt
		scope:child = { add_character_flag = fp1_dreams_of_shieldpersondom }
		# Scope:child gains prowess.
		scope:child = { add_prowess_skill = medium_skill_bonus }
		# Scope:child gains opinion.
		reverse_add_opinion = {
			target = scope:child
			modifier = love_opinion
			opinion = 30
		}

		stress_impact = {
			compassionate = minor_stress_impact_loss
			gregarious = minor_stress_impact_loss
		}
		ai_chance = {
			base = 100
			ai_value_modifier = {
				ai_compassion = 0.5
				ai_honor = 0.5
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = compassionate
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = gregarious
			}
			# Raiders & adventurers want their prodigy to follow in their footsteps.
			modifier = {
				add = 20
				has_trait = viking
			}
			modifier = {
				add = 20
				has_trait = adventurer
			}
			modifier = {
				add = 20
				has_trait = brave
			}
			modifier = {
				add = 20
				has_trait = shieldmaiden
			}
			modifier = {
				add = 20
				has_trait = ambitious
			}
		}
	}

	# Perhaps. You should practice by ambushing scope:victim.
	option = {
		name = fp1_yearly.0581.c
		trigger = { exists = scope:victim }

		# Scope:child gains prowess.
		scope:child = { add_prowess_skill = medium_skill_bonus }
		# Scope:victim gains wounded.
		scope:victim = {
			increase_wounds_no_death_effect = { REASON = duel }
		}
		# Scope:victim loses opinion of you.
		reverse_add_opinion = {
			target = scope:victim
			modifier = angry_opinion
			opinion = -20
		}
		# Scope:child adds scope:victim as a potential rival.
		hidden_effect = {
			scope:child = {
				progress_towards_rival_effect = {
					REASON = rival_hit_with_stick
					CHARACTER = scope:victim
					OPINION = -30
				}
			}
		}

		stress_impact = {
			vengeful = minor_stress_impact_loss
			sadistic = minor_stress_impact_loss
			forgiving = minor_stress_impact_gain
			compassionate = minor_stress_impact_gain
		}
		ai_chance = {
			base = 100
			ai_value_modifier = {
				ai_vengefulness = 0.5
				ai_boldness = 0.25
				ai_compassion = -0.25
				ai_honor = -0.5
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = vengeful
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = sadistic
			}
			modifier = {	# Weight down for stress.
				add = -10
				has_trait = forgiving
			}
			modifier = {	# Weight down for stress.
				add = -10
				has_trait = compassionate
			}
		}
	}

	# Stop troubling me with foolish dreams.
	option = {
		name = fp1_yearly.0581.d

		# Gain a minor beneficial modifier for having your child avoid you.
		add_character_modifier = {
			modifier = fp1_ignoring_annoyances_modifier
			years = 10
		}
		# Scope:child loses opinion of you.
		reverse_add_opinion = {
			target = scope:child
			modifier = disappointed_opinion
			opinion = -30
		}

		stress_impact = {
			callous = minor_stress_impact_loss
			sadistic = minor_stress_impact_loss
			impatient = minor_stress_impact_loss
			compassionate = minor_stress_impact_gain
			patient = minor_stress_impact_gain
			shieldmaiden = major_stress_impact_gain
		}
		ai_chance = {
			base = 100
			ai_value_modifier = {
				ai_sociability = -0.25
				ai_compassion = -0.5
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = callous
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = sadistic
			}
			modifier = {	# Weight up for stress.
				add = 10
				has_trait = impatient
			}
			modifier = {	# Weight down for stress.
				add = -10
				has_trait = compassionate
			}
			modifier = {	# Weight down for stress.
				add = -10
				has_trait = patient
			}
			modifier = {	# Weight down for stress.
				add = -50
				has_trait = shieldmaiden
			}
		}
	}

	# Hah! We shall see, little one.
	option = {
		name = fp1_yearly.0581.e

		reverse_add_opinion = {
			target = scope:child
			modifier = pleased_opinion
			opinion = 15
		}

		# Fairly mild option, so doesn't incur stress.
		ai_chance = {
			base = 100
			ai_value_modifier = {
				ai_energy = -0.25
				ai_boldness = -0.5
			}
		}
	}
}