﻿#culture
culture_exposed_to_writing = {
	global = culture_exposed_to_writing
	first = culture_exposed_to_writing
	third = culture_exposed_to_writing
}
culture_exposed_to_spoked_wheel = {
	global = culture_exposed_to_spoked_wheel
	first = culture_exposed_to_spoked_wheel
	third = culture_exposed_to_spoked_wheel
}
culture_exposed_to_composite_bows = {
	global = culture_exposed_to_composite_bows
	first = culture_exposed_to_composite_bows
	third = culture_exposed_to_composite_bows
}
marduk_statue_returned_tooltip = {
	global = marduk_statue_returned_tooltip
	first = marduk_statue_returned_tooltip
	third = marduk_statue_returned_tooltip
}
esagila_restored_tooltip = {
	global = esagila_restored_tooltip
	first = esagila_restored_tooltip
	third = esagila_restored_tooltip
}
#character
found_kingdom_decision_two_duchies_held = {
	global = HOLDS_TWO_OR_MORE_DUCHY_TITLES_TRIGGER
	first = I_HOLD_TWO_OR_MORE_DUCHY_TITLES_TRIGGER
	third = THEY_HOLD_TWO_OR_MORE_DUCHY_TITLES_TRIGGER
}
LIEGE_PRIMARY_TITLE_EMPEROR = {
	global = LIEGE_PRIMARY_TITLE_EMPEROR
	first = LIEGE_PRIMARY_TITLE_EMPEROR
	third = LIEGE_PRIMARY_TITLE_EMPEROR
}
CAPITAL_CANNOT_BE_IN_NIPPUR = {
	global = CAPITAL_CANNOT_BE_IN_NIPPUR
	first = CAPITAL_CANNOT_BE_IN_NIPPUR
	third = CAPITAL_CANNOT_BE_IN_NIPPUR
}
MUST_CONTROL_80_PERCENT_OF_BABYLON = {
	global = MUST_CONTROL_80_PERCENT_OF_BABYLON
	first = MUST_CONTROL_80_PERCENT_OF_BABYLON
	third = MUST_CONTROL_80_PERCENT_OF_BABYLON
}
capital_in_babylon_region_tooltip = {
	first = MY_CAPITAL_IS_IN_BABYLON
}
capital_in_egypt_region_tooltip = {
	first = MY_CAPITAL_IS_IN_EGYPT
}
capital_in_ebla_region_tooltip = {
	first = MY_CAPITAL_IS_IN_EBLA
}
elevate_cult_of_marduk_holy_sites = {
	first = MARDUK_ENOUGH_HOLY_SITES
	first_not = MARDUK_NOT_ENOUGH_HOLY_SITES
}
elevate_cult_of_ashur_holy_sites = {
	first = ASHUR_ENOUGH_HOLY_SITES
	first_not = ASHUR_NOT_ENOUGH_HOLY_SITES
}
at_least_10_sumerian_counties_tt = {
	first = at_least_10_sumerian_counties_tt
	first_not = at_least_10_sumerian_counties_tt
}
can_only_change_faith_once = {
	global_not = YOU_CAN_ONLY_CHANGE_FAITH_ONCE
}
is_egyptian_trigger = {
	first = is_egyptian_trigger_desc
}
is_mesopotamian_trigger = {
	first = is_mesopotamian_trigger_desc
}
#religion
incompatible_doctrine_infanticide_trigger = {
	first_not = incompatible_doctrine_infanticide_trigger
	third_not = incompatible_doctrine_infanticide_trigger
}
requires_no_syncretism_trigger = {
	first_not = requires_no_syncretism_trigger
	third_not = requires_no_syncretism_trigger
}
requires_minoan_matriarchy_trigger = {
	first_not = requires_minoan_matriarchy_trigger
	third_not = requires_minoan_matriarchy_trigger
}
doctrine_not_ancestor_worship_trigger = {
	first_not = doctrine_not_ancestor_worship_trigger
	third_not = doctrine_not_ancestor_worship_trigger
}
incompatible_tenet_astrology_trigger = {
	first_not = incompatible_tenet_astrology_trigger
	third_not = incompatible_tenet_astrology_trigger
}
requires_doctrine_pluralism_fundamentalist_trigger = {
	first_not = requires_doctrine_pluralism_fundamentalist_trigger
	third_not = requires_doctrine_pluralism_fundamentalist_trigger
}
requires_no_other_cult_trigger = {
	first_not = requires_no_other_cult_trigger
	third_not = requires_no_other_cult_trigger
}
doctrine_not_divine_marriage_trigger = {
	first_not = doctrine_not_divine_marriage_trigger
	third_not = doctrine_not_divine_marriage_trigger
}
incompatible_tenet_minoan_tranquility_trigger = {
	first_not = incompatible_tenet_minoan_tranquility_trigger
	third_not = incompatible_tenet_minoan_tranquility_trigger
}
incompatible_tenet_sumerian_mythology_trigger = {
	first_not = incompatible_tenet_sumerian_mythology_trigger
	third_not = incompatible_tenet_sumerian_mythology_trigger
}
#Syncretisms
requires_mesopotamian_syncretism_trigger = {
	first_not = requires_mesopotamian_syncretism_trigger
	third_not = requires_mesopotamian_syncretism_trigger
}
requires_egyptian_syncretism_trigger = {
	first_not = requires_egyptian_syncretism_trigger
	third_not = requires_egyptian_syncretism_trigger
}
requires_aegean_syncretism_trigger = {
	first_not = requires_aegean_syncretism_trigger
	third_not = requires_aegean_syncretism_trigger
}
requires_canaanite_syncretism_trigger = {
	first_not = requires_canaanite_syncretism_trigger
	third_not = requires_canaanite_syncretism_trigger
}
requires_elamite_syncretism_trigger = {
	first_not = requires_elamite_syncretism_trigger
	third_not = requires_elamite_syncretism_trigger
}
requires_hellenic_syncretism_trigger = {
	first_not = requires_hellenic_syncretism_trigger
	third_not = requires_hellenic_syncretism_trigger
}
requires_hurrian_syncretism_trigger = {
	first_not = requires_hurrian_syncretism_trigger
	third_not = requires_hurrian_syncretism_trigger
}
requires_kushite_syncretism_trigger = {
	first_not = requires_kushite_syncretism_trigger
	third_not = requires_kushite_syncretism_trigger
}
requires_hattian_syncretism_trigger = {
	first_not = requires_hattian_syncretism_trigger
	third_not = requires_hattian_syncretism_trigger
}
requires_luwian_syncretism_trigger = {
	first_not = requires_luwian_syncretism_trigger
	third_not = requires_luwian_syncretism_trigger
}
requires_zagrosian_syncretism_trigger = {
	first_not = requires_zagrosian_syncretism_trigger
	third_not = requires_zagrosian_syncretism_trigger
}