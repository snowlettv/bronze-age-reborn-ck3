﻿#Defined
2alashiya1 = {
	name = "Oileus"
	dynasty = dynn_2alashiya1
	dna = alashiya_1_dna
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1287.8.11 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1341.5.3 = {
		death = yes
	}
}
#Generated
137 = {
	name = "Nanas"
	dynasty = 137
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	760.3.1 = {
		birth = yes
	}
	809.9.24 = {
		death = yes
	}
}
138 = {
	name = "Amphiktyon"
	dynasty = 138
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	767.5.20 = {
		birth = yes
	}
	818.7.25 = {
		death = yes
	}
}
139 = {
	name = "Nireus"
	dynasty = 139
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	759.3.6 = {
		birth = yes
	}
	804.1.13 = {
		death = yes
	}
}
140 = {
	name = "Eurystheus"
	dynasty = 140
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	766.9.18 = {
		birth = yes
	}
	829.4.16 = {
		death = yes
	}
}
141 = {
	name = "Nykteus"
	dynasty = 141
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	766.7.15 = {
		birth = yes
	}
	814.6.4 = {
		death = yes
	}
}
801 = {
	name = "Katreus"
	dynasty = 801
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	748.6.15 = {
		birth = yes
	}
	793.4.13 = {
		death = yes
	}
}
802 = {
	name = "Bliarios"
	dynasty = 802
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	753.5.10 = {
		birth = yes
	}
	813.8.7 = {
		death = yes
	}
}
803 = {
	name = "Tydeus"
	dynasty = 803
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	757.3.2 = {
		birth = yes
	}
	821.5.8 = {
		death = yes
	}
}
804 = {
	name = "Ophioneus"
	dynasty = 804
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	756.1.14 = {
		birth = yes
	}
	809.2.17 = {
		death = yes
	}
}
805 = {
	name = "Titaresios"
	dynasty = 805
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	746.5.17 = {
		birth = yes
	}
	807.2.8 = {
		death = yes
	}
}
806 = {
	name = "Phthonos"
	dynasty = 806
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	751.8.12 = {
		birth = yes
	}
	813.6.18 = {
		death = yes
	}
}
1083 = {
	name = "Leukopeus"
	dynasty = 1083
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1272.2.22 = {
		birth = yes
	}
	1313.6.21 = {
		death = yes
	}
}
1085 = {
	name = "Ageleus"
	dynasty = 1085
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1285.1.11 = {
		birth = yes
	}
	1350.8.24 = {
		death = yes
	}
}
1086 = {
	name = "Pelasgos"
	dynasty = 1086
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1294.5.23 = {
		birth = yes
	}
	1347.11.25 = {
		death = yes
	}
}
1652 = {
	name = "Korythos"
	dynasty = 1652
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1280.1.16 = {
		birth = yes
	}
	1326.11.18 = {
		death = yes
	}
}
1653 = {
	name = "Peneus"
	dynasty = 1653
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1283.2.5 = {
		birth = yes
	}
	1324.9.15 = {
		death = yes
	}
}
1654 = {
	name = "Theseus"
	dynasty = 1654
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1285.6.9 = {
		birth = yes
	}
	1345.4.12 = {
		death = yes
	}
}
1655 = {
	name = "Bliarios"
	dynasty = 1655
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1293.3.9 = {
		birth = yes
	}
	1350.9.20 = {
		death = yes
	}
}
1656 = {
	name = "Guneus"
	dynasty = 1656
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1288.8.12 = {
		birth = yes
	}
	1340.8.6 = {
		death = yes
	}
}
1657 = {
	name = "Pandareus"
	dynasty = 1657
	religion = "cypriot_faith"
	culture = "ba_cypriot"
	1281.2.11 = {
		birth = yes
	}
	1341.2.12 = {
		death = yes
	}
}