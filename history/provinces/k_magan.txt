﻿#k_magan
##d_hili ###################################
###c_hili
6879 = { #Hili
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding
}
6878 = { #Al-Ain
	holding = church_holding
}
6877 = { #Nûni
	holding = city_holding

	785.1.1 = {
		special_building_slot = copper_mine_01
		special_building = copper_mine_01
	}
}
6876 = { #Ersuppu
	holding = none
}
6880 = { #Sillatu
	holding = none
}
###c_nunu
6874 = { #Nûnu
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}
6873 = { #Parâshu
	holding = none
}
6872 = { #Alittu
	holding = none
}
###c_enqetu
6875 = { #Enqêtu
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding
}
6869 = { #Urnu
	holding = none
}
6867 = { #Hulmâhu
	holding = city_holding

	785.1.1 = {
		special_building_slot = copper_mine_01
		special_building = copper_mine_01
	}
}
6868 = { #Shammanu
	holding = none
}
##d_ummannar
###c_ummannar
1799 = { #Umm an-Nar
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding

	785.1.1 = {
		special_building_slot = bitumen_mine_01
		special_building = bitumen_mine_01
	}
}
6850 = { #Errê
	holding = none
}
6848 = { #Misissa
	holding = city_holding
}
6847 = { #Summunu
	holding = none
}
###c_saratu
6851 = { #Sarâtu
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding
}
6852 = { #Shuplu
	holding = none
}
6871 = { #Kussimtu
	holding = none
}
6849 = { #Igibû
	holding = city_holding
}
###c_ubbubu
1807 = { #Ubbubu
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}
6846 = { #Nakmartu
	holding = none
}
6845 = { #Para'u
	holding = none
}
6844 = { #Ita
	holding = none
}
##d_tellabraq
###c_tellabraq
1653 = { #Tell Abraq
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding
}
6856 = { #Muttarrittum
	holding = none
}
6859 = { #Naiabtu
	holding = none
}
6855 = { #Rahû
	holding = church_holding
}
6861 = { #Halâbu
	holding = city_holding

	785.1.1 = {
		special_building_slot = major_quarry_01
		special_building = major_quarry_01
	}
}
###c_zazu
6854 = { #Zâzu
	culture = maganite
	religion = dilmunite_faith
	holding = castle_holding
}
6853 = { #Uldu
	holding = city_holding
}
6864 = { #Bashmu
	holding = none
}
6870 = { #Pashâlu
	holding = none
}
###c_bamtu
6862 = { #Bâmtu
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding

	785.1.1 = {
		special_building_slot = copper_mine_01
		special_building = copper_mine_01
	}
}
6863 = { #Nasku
	holding = none
}
6865 = { #Serru
	holding = none
}
6866 = { #Qubirtu
	holding = none
}
##d_gulfislands
###c_arakata
1809 = { #Arakata
	culture = dilmunite
	religion = dilmunite_faith
	holding = castle_holding
}
###c_sirri
1804 = { #Sirri
	culture = dilmunite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_forur
2074 = { #Forur
	culture = dilmunite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_moussa
2076 = { #Moussa
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_nuayr
2091 = { #Nu'ayr
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_zirkuh
193 = { #Zirkuh
	culture = dilmunite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_das
2141 = { #Das
	culture = dilmunite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_yas
2637 = { #Yas
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}
###c_delma
2204 = { #Delma
	culture = maganite
	religion = dilmunite_faith
	holding = tribal_holding
}