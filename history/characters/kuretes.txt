﻿#Generated
219 = {
	name = "Nanas"
	dynasty = 219
	religion = "aegean_faith"
	culture = "kuretes"
	769.2.10 = {
		birth = yes
	}
	826.4.17 = {
		death = yes
	}
}
453 = {
	name = "Amphiktyon"
	dynasty = 453
	religion = "aegean_faith"
	culture = "kuretes"
	749.11.19 = {
		birth = yes
	}
	806.2.10 = {
		death = yes
	}
}
454 = {
	name = "Nireus"
	dynasty = 454
	religion = "aegean_faith"
	culture = "kuretes"
	746.6.3 = {
		birth = yes
	}
	814.5.22 = {
		death = yes
	}
}
455 = {
	name = "Eurystheus"
	dynasty = 455
	religion = "aegean_faith"
	culture = "kuretes"
	764.7.17 = {
		birth = yes
	}
	832.7.7 = {
		death = yes
	}
}
456 = {
	name = "Nykteus"
	dynasty = 456
	religion = "aegean_faith"
	culture = "kuretes"
	747.1.23 = {
		birth = yes
	}
	795.9.3 = {
		death = yes
	}
}
457 = {
	name = "Katreus"
	dynasty = 457
	religion = "aegean_faith"
	culture = "kuretes"
	768.3.13 = {
		birth = yes
	}
	817.10.26 = {
		death = yes
	}
}
458 = {
	name = "Bliarios"
	dynasty = 458
	religion = "aegean_faith"
	culture = "kuretes"
	768.11.1 = {
		birth = yes
	}
	817.8.1 = {
		death = yes
	}
}
459 = {
	name = "Tydeus"
	dynasty = 459
	religion = "aegean_faith"
	culture = "kuretes"
	747.11.14 = {
		birth = yes
	}
	795.2.4 = {
		death = yes
	}
}
1322 = {
	name = "Ophioneus"
	dynasty = 1322
	religion = "aegean_faith"
	culture = "kuretes"
	1273.2.27 = {
		birth = yes
	}
	1319.8.20 = {
		death = yes
	}
}
1342 = {
	name = "Titaresios"
	dynasty = 1342
	religion = "aegean_faith"
	culture = "kuretes"
	1286.3.10 = {
		birth = yes
	}
	1335.9.18 = {
		death = yes
	}
}
1348 = {
	name = "Phthonos"
	dynasty = 1348
	religion = "aegean_faith"
	culture = "kuretes"
	1282.2.12 = {
		birth = yes
	}
	1351.3.27 = {
		death = yes
	}
}
1349 = {
	name = "Leukopeus"
	dynasty = 1349
	religion = "aegean_faith"
	culture = "kuretes"
	1290.2.12 = {
		birth = yes
	}
	1337.1.5 = {
		death = yes
	}
}
1350 = {
	name = "Theseus"
	dynasty = 1350
	religion = "aegean_faith"
	culture = "kuretes"
	1275.3.17 = {
		birth = yes
	}
	1344.9.19 = {
		death = yes
	}
}
1351 = {
	name = "Ageleus"
	dynasty = 1351
	religion = "aegean_faith"
	culture = "kuretes"
	1281.1.26 = {
		birth = yes
	}
	1345.11.8 = {
		death = yes
	}
}
1352 = {
	name = "Pelasgos"
	dynasty = 1352
	religion = "aegean_faith"
	culture = "kuretes"
	1273.6.21 = {
		birth = yes
	}
	1325.11.11 = {
		death = yes
	}
}