﻿785.1.1 = {
	discover_innovation = innovation_earthworks
	discover_innovation = innovation_barracks
	discover_innovation = innovation_mustering_grounds
	discover_innovation = innovation_siege_ladders
	#
	discover_innovation = innovation_city_states
	discover_innovation = innovation_potters_wheel
	#
	discover_innovation = innovation_aegean_raiders
	discover_innovation = innovation_aegean_maceman
	discover_innovation = innovation_permanent_settlements
}
1200.1.1 = {
	join_era = culture_era_early_medieval
}
1310.1.1 = {
	discover_innovation = innovation_raiding_parties
	discover_innovation = innovation_tribal_vassals
	discover_innovation = innovation_bronze_socket_axe
	discover_innovation = innovation_battering_rams
	#
	discover_innovation = innovation_early_palaces
	discover_innovation = innovation_kings_justice
	discover_innovation = innovation_centralized_irrigation
	discover_innovation = innovation_border_stones
	discover_innovation = innovation_kingship
	#
	discover_innovation = innovation_fortifications_2
	discover_innovation = innovation_barracks_2
	discover_innovation = innovation_swords
	#
	discover_innovation = innovation_development_2
	#
	discover_innovation = innovation_frescoes
	discover_innovation = innovation_early_hoplite
}