﻿#Generated
166 = {
	name = "Karruwa"
	dynasty = 166
	religion = "anatolian_faith"
	culture = "lydian"
	747.5.18 = {
		birth = yes
	}
	795.4.19 = {
		death = yes
	}
}
172 = {
	name = "Paihami"
	dynasty = 172
	religion = "anatolian_faith"
	culture = "lydian"
	748.7.27 = {
		birth = yes
	}
	808.5.14 = {
		death = yes
	}
}
173 = {
	name = "Irhamuwa"
	dynasty = 173
	religion = "anatolian_faith"
	culture = "lydian"
	748.3.6 = {
		birth = yes
	}
	809.4.26 = {
		death = yes
	}
}
615 = {
	name = "Yaratiwa"
	dynasty = 615
	religion = "anatolian_faith"
	culture = "lydian"
	752.2.19 = {
		birth = yes
	}
	796.6.13 = {
		death = yes
	}
}
617 = {
	name = "Larija"
	dynasty = 617
	religion = "anatolian_faith"
	culture = "lydian"
	763.5.20 = {
		birth = yes
	}
	809.3.25 = {
		death = yes
	}
}
645 = {
	name = "Kururi"
	dynasty = 645
	religion = "anatolian_faith"
	culture = "lydian"
	756.10.10 = {
		birth = yes
	}
	816.5.12 = {
		death = yes
	}
}
646 = {
	name = "Musu"
	dynasty = 646
	religion = "anatolian_faith"
	culture = "lydian"
	753.3.6 = {
		birth = yes
	}
	816.11.11 = {
		death = yes
	}
}
1116 = {
	name = "Marakkui"
	dynasty = 1116
	religion = "anatolian_faith"
	culture = "lydian"
	1280.4.8 = {
		birth = yes
	}
	1336.4.27 = {
		death = yes
	}
}
1122 = {
	name = "Wasupija"
	dynasty = 1122
	religion = "anatolian_faith"
	culture = "lydian"
	1275.1.9 = {
		birth = yes
	}
	1318.3.22 = {
		death = yes
	}
}
1123 = {
	name = "Kalawija"
	dynasty = 1123
	religion = "anatolian_faith"
	culture = "lydian"
	1290.9.5 = {
		birth = yes
	}
	1351.2.2 = {
		death = yes
	}
}
1482 = {
	name = "Suppiluliuma"
	dynasty = 1482
	religion = "anatolian_faith"
	culture = "lydian"
	1287.9.21 = {
		birth = yes
	}
	1354.11.20 = {
		death = yes
	}
}
1506 = {
	name = "Zipili"
	dynasty = 1506
	religion = "anatolian_faith"
	culture = "lydian"
	1278.5.14 = {
		birth = yes
	}
	1340.1.15 = {
		death = yes
	}
}
1507 = {
	name = "Kippuruwa"
	dynasty = 1507
	religion = "anatolian_faith"
	culture = "lydian"
	1275.4.23 = {
		birth = yes
	}
	1327.11.11 = {
		death = yes
	}
}