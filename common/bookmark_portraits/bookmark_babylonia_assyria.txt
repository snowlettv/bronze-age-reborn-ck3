# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:2assyria1
bookmark_babylonia_assyria={
	type=male
	id=1545
	age=0.240000
	genes={ 		hair_color={ 57 238 2 244 }
 		skin_color={ 146 144 149 144 }
 		eye_color={ 26 245 62 255 }
 		gene_chin_forward={ "chin_forward_neg" 103 "chin_forward_neg" 121 }
 		gene_chin_height={ "chin_height_pos" 135 "chin_height_pos" 133 }
 		gene_chin_width={ "chin_width_neg" 119 "chin_width_pos" 131 }
 		gene_eye_angle={ "eye_angle_pos" 137 "eye_angle_pos" 139 }
 		gene_eye_depth={ "eye_depth_neg" 122 "eye_depth_neg" 125 }
 		gene_eye_height={ "eye_height_neg" 118 "eye_height_pos" 136 }
 		gene_eye_distance={ "eye_distance_pos" 130 "eye_distance_neg" 116 }
 		gene_eye_shut={ "eye_shut_neg" 107 "eye_shut_neg" 123 }
 		gene_forehead_angle={ "forehead_angle_pos" 174 "forehead_angle_pos" 149 }
 		gene_forehead_brow_height={ "forehead_brow_height_neg" 124 "forehead_brow_height_neg" 120 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 127 "forehead_roundness_pos" 142 }
 		gene_forehead_width={ "forehead_width_neg" 116 "forehead_width_pos" 146 }
 		gene_forehead_height={ "forehead_height_neg" 63 "forehead_height_neg" 71 }
 		gene_head_height={ "head_height_neg" 108 "head_height_neg" 38 }
 		gene_head_width={ "head_width_neg" 107 "head_width_neg" 104 }
 		gene_head_profile={ "head_profile_pos" 169 "head_profile_pos" 148 }
 		gene_head_top_height={ "head_top_height_neg" 124 "head_top_height_pos" 138 }
 		gene_head_top_width={ "head_top_width_neg" 120 "head_top_width_neg" 117 }
 		gene_jaw_angle={ "jaw_angle_pos" 131 "jaw_angle_pos" 175 }
 		gene_jaw_forward={ "jaw_forward_neg" 123 "jaw_forward_pos" 143 }
 		gene_jaw_height={ "jaw_height_neg" 125 "jaw_height_neg" 124 }
 		gene_jaw_width={ "jaw_width_pos" 150 "jaw_width_neg" 89 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_pos" 132 "mouth_corner_depth_pos" 134 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 137 "mouth_corner_height_pos" 153 }
 		gene_mouth_forward={ "mouth_forward_neg" 98 "mouth_forward_neg" 115 }
 		gene_mouth_height={ "mouth_height_neg" 97 "mouth_height_neg" 121 }
 		gene_mouth_width={ "mouth_width_neg" 122 "mouth_width_neg" 123 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 168 "mouth_upper_lip_size_neg" 115 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 130 "mouth_lower_lip_size_pos" 153 }
 		gene_mouth_open={ "mouth_open_neg" 56 "mouth_open_neg" 96 }
 		gene_neck_length={ "neck_length_neg" 100 "neck_length_pos" 170 }
 		gene_neck_width={ "neck_width_pos" 140 "neck_width_neg" 123 }
 		gene_bs_cheek_forward={ "cheek_forward_pos" 40 "cheek_forward_pos" 81 }
 		gene_bs_cheek_height={ "cheek_height_neg" 39 "cheek_height_neg" 16 }
 		gene_bs_cheek_width={ "cheek_width_pos" 43 "cheek_width_neg" 34 }
 		gene_bs_ear_angle={ "ear_angle_neg" 15 "ear_angle_pos" 8 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 2 "ear_inner_shape_pos" 38 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 26 "ear_upper_bend_pos" 109 }
 		gene_bs_ear_outward={ "ear_outward_pos" 42 "ear_outward_neg" 21 }
 		gene_bs_ear_size={ "ear_size_neg" 20 "ear_size_neg" 36 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 54 "eye_corner_depth_neg" 38 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 137 "eye_fold_shape_pos" 42 }
 		gene_bs_eye_size={ "eye_size_pos" 152 "eye_size_pos" 169 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_pos" 159 "eye_upper_lid_size_pos" 130 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 27 "forehead_brow_curve_neg" 77 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_neg" 23 "forehead_brow_forward_pos" 25 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_pos" 88 "forehead_brow_inner_height_neg" 124 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 35 "forehead_brow_outer_height_neg" 29 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 37 "forehead_brow_width_pos" 78 }
 		gene_bs_jaw_def={ "jaw_def_pos" 8 "jaw_def_pos" 13 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 128 "mouth_lower_lip_def_pos" 34 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 15 "mouth_lower_lip_full_neg" 30 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_neg" 9 "mouth_lower_lip_pad_pos" 82 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 91 "mouth_lower_lip_width_neg" 10 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 22 "mouth_philtrum_def_pos" 25 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 21 "mouth_philtrum_shape_pos" 15 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_neg" 81 "mouth_philtrum_width_pos" 126 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 103 "mouth_upper_lip_def_pos" 127 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_neg" 182 "mouth_upper_lip_full_pos" 98 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 31 "mouth_upper_lip_profile_neg" 25 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 4 "mouth_upper_lip_width_neg" 65 }
 		gene_bs_nose_forward={ "nose_forward_pos" 22 "nose_forward_neg" 58 }
 		gene_bs_nose_height={ "nose_height_pos" 5 "nose_height_pos" 114 }
 		gene_bs_nose_length={ "nose_length_neg" 64 "nose_length_neg" 17 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_pos" 12 "nose_nostril_height_pos" 18 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_neg" 40 "nose_nostril_width_neg" 0 }
 		gene_bs_nose_profile={ "nose_profile_pos" 57 "nose_profile_pos" 118 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 32 "nose_ridge_angle_neg" 20 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 10 "nose_ridge_width_pos" 41 }
 		gene_bs_nose_size={ "nose_size_pos" 57 "nose_size_neg" 28 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_neg" 85 "nose_tip_angle_neg" 104 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 23 "nose_tip_forward_pos" 22 }
 		gene_bs_nose_tip_width={ "nose_tip_width_neg" 162 "nose_tip_width_pos" 51 }
 		face_detail_cheek_def={ "cheek_def_02" 81 "cheek_def_02" 18 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 204 "cheek_fat_04_pos" 195 }
 		face_detail_chin_cleft={ "chin_cleft" 20 "chin_dimple" 12 }
 		face_detail_chin_def={ "chin_def" 14 "chin_def" 9 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 128 "eye_lower_lid_def" 202 }
 		face_detail_eye_socket={ "eye_socket_01" 249 "eye_socket_02" 11 }
 		face_detail_nasolabial={ "nasolabial_01" 41 "nasolabial_01" 76 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_pos" 19 "nose_ridge_def_pos" 202 }
 		face_detail_nose_tip_def={ "nose_tip_def" 24 "nose_tip_def" 13 }
 		face_detail_temple_def={ "temple_def" 208 "temple_def" 223 }
 		expression_brow_wrinkles={ "brow_wrinkles_04" 0 "brow_wrinkles_04" 202 }
 		expression_eye_wrinkles={ "eye_wrinkles_03" 228 "eye_wrinkles_01" 48 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 60 "forehead_wrinkles_03" 222 }
 		expression_other={ "cheek_wrinkles_both_01" 51 "cheek_wrinkles_both_01" 0 }
 		complexion={ "complexion_2" 232 "complexion_4" 179 }
 		gene_height={ "normal_height" 137 "normal_height" 123 }
 		gene_bs_body_type={ "body_fat_head_fat_full" 125 "body_fat_head_fat_medium" 109 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 227 "body_shape_hourglass_half" 0 }
 		gene_bs_bust={ "bust_clothes" 104 "bust_shape_1_half" 91 }
 		gene_age={ "old_2" 73 "old_2" 251 }
 		gene_eyebrows_shape={ "avg_spacing_high_thickness" 232 "avg_spacing_avg_thickness" 250 }
 		gene_eyebrows_fullness={ "layer_2_low_thickness" 127 "layer_2_low_thickness" 138 }
 		gene_body_hair={ "body_hair_dense" 230 "body_hair_avg" 191 }
 		gene_hair_type={ "hair_curly" 137 "hair_curly" 144 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 188 "normal_eyes" 188 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "normal_eyelashes" 161 "normal_eyelashes" 161 }
 		pose={ "" 255 "" 0 }
 		beards={ "fp3_iranian_beards_curly" 0 "no_beard" 0 }
 		hairstyles={ "hair_akkadian_hairstyles" 30 "all_hairstyles" 0 }
 		legwear={ "mena_nobility_legwear" 102 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "fp3_iranian_high_nobility_clothes" 99 "most_clothes" 0 }
 		headgear={ "fp3_iranian_royalty" 209 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 807438772 807438772 }
	tags={ {
			hash=2125534279
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=2793867254
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 {
			hash=1485265044
			invert=no
		}
 }
}

