﻿#Defined
gilgamesh1 = {
	name = "Gilgamesh" #Gilgamesh was a hero in ancient Mesopotamian mythology and the protagonist of the Epic of Gilgamesh, an epic poem written in Akkadian during the late 2nd millennium BC. He was possibly a historical king of the Sumerian city-state of Uruk, who was posthumously deified. His rule probably would have taken place sometime in the beginning of the Early Dynastic Period, c. 2900 – 2350 BC, though he became a major figure in Sumerian legend during the Third Dynasty of Ur (c.2112 – c.2004 BC).
	dynasty = dynn_gilgamesh1
	religion = "sumerian_faith"
	culture = "sumerian"
	martial = 21
	diplomacy = 25
	stewardship = 18
	intrigue = 15
	learning = 15
	disallow_random_traits = yes
	trait = physique_good_3
	trait = education_martial_4
	trait = brave
	trait = impatient
	trait = arrogant
	trait = strong
	trait = saint
	trait = lifestyle_blademaster
	trait = lifestyle_traveler
	398.7.1 = {
		birth = yes
		effect = {
			add_trait_xp = {
				trait = lifestyle_blademaster
				value = {
					integer_range = {
						min = small_lifestyle_random_xp_low
						max = small_lifestyle_random_xp_high
					}
				}
			}
			add_trait_xp = {
				trait = lifestyle_traveler
				track = travel
				value = {
					integer_range = {
						min = small_lifestyle_random_xp_low
						max = small_lifestyle_random_xp_high
					}
				}
			}
			add_trait_xp = {
				trait = lifestyle_traveler
				track = danger
				value = {
					integer_range = {
						min = small_lifestyle_random_xp_low
						max = small_lifestyle_random_xp_high
					}
				}
			}
		}
	}
	433.6.1 = {
		death = yes
	}
}
der1 = {
	name = "Mesh-ki-ang-Nanna"
	dynasty = dynn_der1
	religion = "sumerian_faith"
	culture = "sumerian"
	758.11.11 = {
		birth = yes
	}
	817.9.2 = {
		death = yes
	}
}
uruk1 = {
	name = "Utu-Hengal"
	dynasty = dynn_uruk1
	dna = uruk_1_dna
	martial = 11
	diplomacy = 8
	intrigue = 5
	stewardship = 9
	religion = "sumerian_faith"
	culture = "sumerian"
	trait = education_martial_3
	trait = overseer
	#trait = lifestyle_blademaster
	trait = brave
	trait = ambitious
	trait = chaste #To decrease fertility further
	trait = impotent
	trait = lifestyle_blademaster #TODO - Fix Charioteer and Scribe traits
	733.4.25 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
			add_trait_xp = {
				trait = lifestyle_blademaster
				value = {
					integer_range = {
						min = small_lifestyle_random_xp_low
						max = small_lifestyle_random_xp_high
					}
				}
			}
		}
	}
	788.1.3 = {
		death = yes
	}
}
ur1 = {
	name = "Ur-Nammu"
	dynasty = dynn_ur1
	dna = ur_1_dna
	martial = 12
	diplomacy = 9
	intrigue = 6
	stewardship = 15
	religion = "sumerian_faith"
	culture = "sumerian"
	trait = education_martial_4
	trait = administrator
	trait = brave
	trait = diligent
	trait = ambitious
	trait = just
	758.1.3 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	780.1.3 = {
		add_spouse = ur2
	}
	806.1.3 = {
		death = yes
	}
}
ur2 = {
	name = "Mammetum"
	female = yes
	dynasty = dynn_uruk1
	religion = "sumerian_faith"
	culture = "sumerian"
	father = uruk1
	764.8.21 = {
		birth = yes
	}
	823.1.3 = {
		death = yes
	}
}
ur3 = {
	name = "Shulgi"
	dynasty = dynn_ur1
	religion = "sumerian_faith"
	culture = "sumerian"
	trait = physique_good_2
	father = ur1
	mother = ur2
	782.11.13 = {
		birth = yes
	}
	853.6.4 = {
		death = yes
	}
}
nippur1 = { #Not touched
	name = "Puzur-ili"
	dynasty = dynn_nippur1
	religion = "sumerian_faith"
	culture = "sumerian"
	746.1.3 = {
		birth = yes
	}
	789.1.3 = {
		death = yes
	}
}
umma1 = {
	name = "Nanniya"
	dynasty = dynn_umma1
	religion = "sumerian_faith"
	culture = "sumerian"
	762.2.10 = {
		birth = yes
	}
	828.4.22 = {
		death = yes
	}
}
lagash1 = {
	name = "Nam-mahani" #Son of Gudea (hopefully?), 2124-2110 Last after Gudea
	dynasty = dynn_lagash1
	dna = lagash_1_dna
	religion = "sumerian_faith"
	culture = "sumerian"
	father = lagash2
	mother = lagash6
	760.3.10 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	788.1.1 = {
		add_spouse = lagash5
	}
	815.10.3 = {
		death = yes
	}
}
lagash2 = {
	name = "Gudea" #Gudea, 2144-2124
	dynasty = dynn_lagash1
	martial = 9
	diplomacy = 11
	intrigue = 7
	stewardship = 18
	religion = "sumerian_faith"
	culture = "sumerian"
	trait = education_stewardship_4
	trait = administrator
	trait = just
	trait = diligent
	trait = compassionate
	718.6.12 = {
		birth = yes
	}
	743.1.1 = {
		add_spouse = lagash6
	}
	776.1.1 = {
		death = yes
	}
}
lagash3 = {
	name = "Ur-Ningirsu" #Son of Gudea (actually), 2124-x, 1st after Gudea
	dynasty = dynn_lagash1
	religion = "sumerian_faith"
	culture = "sumerian"
	father = lagash2
	mother = lagash6
	745.3.10 = {
		birth = yes
	}
	780.1.1 = {
		death = yes
	}
}
lagash4 = {
	name = "Ur-gar" #Son of Gudea (hopefully?), 2124-x, 2nd after Gudea
	dynasty = dynn_lagash1
	religion = "sumerian_faith"
	culture = "sumerian"
	father = lagash2
	mother = lagash6
	752.3.10 = {
		birth = yes
	}
	785.1.1 = {
		death = yes
	}
}
lagash5 = {
	name = "Ninkagina" #Wife of Nam-mahani
	#dynasty = dynn_lagash1
	religion = "sumerian_faith"
	culture = "sumerian"
	female = yes
	762.1.5 = {
		birth = yes
	}
	805.10.3 = {
		death = yes
	}
}
lagash6 = {
	name = "Ninalla"
	dynasty = dynn_lagash2
	religion = "sumerian_faith"
	culture = "sumerian"
	father = lagash7
	female = yes
	720.1.5 = {
		birth = yes
	}
	815.10.3 = {
		death = yes
	}
}
lagash7 = {
	name = "Ur-Baba" #Father in law of Gudea
	dynasty = dynn_lagash2
	religion = "sumerian_faith"
	culture = "sumerian"
	695.3.2 = {
		birth = yes
	}
	756.1.1 = {
		death = yes
	}
}
larak1 = {
	name = "Urukagina"
	dynasty = dynn_larak1
	religion = "sumerian_faith"
	culture = "sumerian"
	767.3.25 = {
		birth = yes
	}
	832.7.12 = {
		death = yes
	}
}
adab1 = {
	name = "En-sipad-zid-ana"
	dynasty = dynn_adab1
	religion = "sumerian_faith"
	culture = "sumerian"
	750.6.9 = {
		birth = yes
	}
	814.7.9 = {
		death = yes
	}
}
isin1 = {
	name = "Ur-Nanshe"
	dynasty = dynn_isin1
	religion = "sumerian_faith"
	culture = "sumerian"
	750.5.16 = {
		birth = yes
	}
	814.10.23 = {
		death = yes
	}
}
mashkan1 = {
	name = "Ur-Zababa"
	dynasty = dynn_mashkan1
	religion = "sumerian_faith"
	culture = "sumerian"
	759.8.11 = {
		birth = yes
	}
	823.1.25 = {
		death = yes
	}
}
bily1 = {
	name = "Bily"
	dynasty = dynn_bily1
	dna = patreon_bily
	martial = 12
	diplomacy = 8
	intrigue = 7
	stewardship = 14
	religion = "sumerian_faith"
	culture = "sumerian"
	disallow_random_traits = yes
	sexuality = homosexual
	trait = education_learning_2
	trait = beauty_good_2
	trait = hashishiyah
	trait = lustful
	trait = honest
	trait = impatient
	751.1.1 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	812.1.1 = {
		death = yes
	}
}
#Generated
277 = {
	name = "Ur-Mama"
	dynasty = 277
	religion = "sumerian_faith"
	culture = "sumerian"
	749.11.9 = {
		birth = yes
	}
	809.11.5 = {
		death = yes
	}
}
278 = {
	name = "Shu-Suen"
	dynasty = 278
	religion = "sumerian_faith"
	culture = "sumerian"
	766.2.16 = {
		birth = yes
	}
	808.4.14 = {
		death = yes
	}
}
279 = {
	name = "Unzi"
	dynasty = 279
	religion = "sumerian_faith"
	culture = "sumerian"
	761.6.21 = {
		birth = yes
	}
	802.8.17 = {
		death = yes
	}
}
280 = {
	name = "Ur-gar"
	dynasty = 280
	religion = "sumerian_faith"
	culture = "sumerian"
	749.11.16 = {
		birth = yes
	}
	808.2.12 = {
		death = yes
	}
}
740 = {
	name = "Utu-hengal"
	dynasty = 740
	religion = "sumerian_faith"
	culture = "sumerian"
	753.2.12 = {
		birth = yes
	}
	811.5.18 = {
		death = yes
	}
}
741 = {
	name = "Urukagina"
	dynasty = 741
	religion = "sumerian_faith"
	culture = "sumerian"
	769.10.8 = {
		birth = yes
	}
	823.7.25 = {
		death = yes
	}
}
742 = {
	name = "Dumuzi"
	dynasty = 742
	religion = "sumerian_faith"
	culture = "sumerian"
	747.7.11 = {
		birth = yes
	}
	797.11.9 = {
		death = yes
	}
}
743 = {
	name = "En-hegal"
	dynasty = 743
	religion = "sumerian_faith"
	culture = "sumerian"
	749.1.26 = {
		birth = yes
	}
	816.2.18 = {
		death = yes
	}
}
744 = {
	name = "Lugula"
	dynasty = 744
	religion = "sumerian_faith"
	culture = "sumerian"
	766.7.13 = {
		birth = yes
	}
	823.2.5 = {
		death = yes
	}
}
745 = {
	name = "Udul-klama"
	dynasty = 745
	religion = "sumerian_faith"
	culture = "sumerian"
	764.11.18 = {
		birth = yes
	}
	823.11.27 = {
		death = yes
	}
}
746 = {
	name = "Puzur-Suen"
	dynasty = 746
	religion = "sumerian_faith"
	culture = "sumerian"
	758.8.6 = {
		birth = yes
	}
	824.1.14 = {
		death = yes
	}
}
747 = {
	name = "Mesh-ki-ang-gasher"
	dynasty = 747
	religion = "sumerian_faith"
	culture = "sumerian"
	757.11.4 = {
		birth = yes
	}
	819.5.7 = {
		death = yes
	}
}
752 = {
	name = "Lugal-ure"
	dynasty = 752
	religion = "sumerian_faith"
	culture = "sumerian"
	747.4.26 = {
		birth = yes
	}
	800.5.7 = {
		death = yes
	}
}