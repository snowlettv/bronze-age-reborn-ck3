﻿#k_karkamisha
##d_sarugu ###################################
###c_sarugu
1543 = { #Sarugu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
160 = { #Hashdu
	holding = none
}
3919 = { #Shuburru
	holding = none
}
3914 = { #Shîpu
	holding = none
}
152 = { #Aligu
	holding = none
}
###c_shurqu
3141 = { #Shurqu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3158 = { #Sissu
	holding = none
}
3155 = { #Hadattu
	holding = none
}
###c_parrisu
3162 = { #Parrisu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3142 = { #Urkutu
	holding = none
}
3866 = { #Nêrubu
	holding = none
}
###c_irridu
3183 = { #Irridu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3189 = { #Isharish
	holding = none
}
3180 = { #Erinnu
	holding = none
}
2514 = { #Zamâru
	holding = none
}
182 = { #Esertu
	holding = none
}
##d_karkamisa
###c_karkamisa
4362 = { #Karkamisa
	culture = harrani
	religion = western_hurrian_faith
	holding = castle_holding
	#holy_site = omen_kubaba
	
	785.1.1 = {
		special_building_slot = temple_of_kubaba_01 #Temple of Kubaba
	}
	1310.1.1 = {
		special_building = temple_of_kubaba_01
	}
}
4363 = { #Undu-tti
	holding = city_holding
}
4364 = { #Te-u-kki
	holding = church_holding
}
4367 = { #Te-u-tali
	holding = castle_holding
}
4439 = { #Mazuwati
	holding = city_holding
}
4440 = { #Marîna
	holding = castle_holding
}
###c_tobeh
3722 = { #Tobeh
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = harrani
		religion = western_hurrian_faith
	}
}
3721 = { #Latma
	holding = city_holding
}
3755 = { #Qlayaat
	holding = church_holding
}
3723 = { #Bared
	holding = city_holding
}
###c_hassuwa
4371 = { #Hassuwa
	culture = harrani
	religion = western_hurrian_faith
	holding = castle_holding
}
4365 = { #Sini-tal-mi
	holding = city_holding
}
4369 = { #Uri-nui
	holding = church_holding
}
4370 = { #Si-a-tti
	holding = city_holding
}
4374 = { #Fur-a-tti
	holding = castle_holding
}
###c_tuba
4360 = { #Tûba
	culture = harrani
	religion = western_hurrian_faith
	holding = castle_holding
}
4366 = { #Tal-a-tti
	holding = city_holding
}
4443 = { #Kudu-nui
	holding = church_holding
}
4441 = { #Pal-itti
	holding = castle_holding
}
4442 = { #Mâ-uffa
	holding = castle_holding
}
4361 = { #Mani-alla
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
##d_urshu
###c_urshu
4378 = { #Ursu
	culture = harrani
	religion = western_hurrian_faith
	holding = castle_holding
}
4379 = { #Ese-siwe
	holding = city_holding
}
4377 = { #Pab-ni-yi
	holding = church_holding
}
4419 = { #Xas-ar
	holding = city_holding
}
4375 = { #Naxx-ar
	holding = castle_holding
}
###c_urumu
4359 = { #Sa-alla
	culture = harrani
	religion = western_hurrian_faith
	holding = castle_holding
}
4368 = { #Ase-payi
	holding = city_holding
}
4376 = { #Xill-nui
	holding = none
}
4431 = { #Sini-siye
	holding = church_holding
}
###c_awtiye
4420 = { #Aw-tiye
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
4422 = { #Asta-et-i
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
4421 = { #Xill-kudu
	holding = church_holding
}
4373 = { #Siya-nui
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}