﻿# Happens once a year for the culture. Arbitrary day, but the same day every year for that culture
# Root is the culture
yearly_culture_pulse = {
	on_actions = {
		yearly_culture_pulse_RICE
	}

}


yearly_culture_pulse_RICE = {
	effect = {
		if = {
			limit = {
				NOT = { culture_number_of_counties > 0 }
			}
			# Give innovations for cultures that aren't on the map
			random = {
				chance = 8
				add_random_innovation = yes
			}
		}
	}
}
