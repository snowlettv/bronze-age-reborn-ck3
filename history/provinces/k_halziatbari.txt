﻿#k_halziatbari
##d_haburatum ###################################
###c_haburatum
5257 = { #Haburatum
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_allani_01 #Temple of Allani
	}
}
5886 = { #Sabiresu
	holding = none
}
5758 = { #Lumnu
	holding = city_holding
}
5759 = { #Niziqtu
	holding = none
}
5762 = { #Shabbu
	holding = none
}
###c_kipshuna
6445 = { #Kipshuna
	culture = tukri
	religion = eastern_hurrian_faith
	holding = tribal_holding
}
5887 = { #Adappu
	holding = none
}
5888 = { #Ashbu
	holding = none
}
5890 = { #Ashtammu
	holding = none
}
5889 = { #Ashur-iqisha
	holding = none
}
5891 = { #Kultâru
	holding = none
}
##d_qana
###c_qana
5907 = { #Qana
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5765 = { #Tasmertu
	holding = city_holding
}
5763 = { #Rûbtu
	holding = none
}
5908 = { #Bashâmu
	holding = none
}
###c_ressubnat
5764 = { #Res-Subnar
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5767 = { #Tubbâti
	holding = none
}
5915 = { #Kalum
	holding = none
}
5766 = { #Takâlu
	holding = none
}
###c_ukku
5859 = { #Ukku
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5855 = { #Qa'âlu
	holding = city_holding
}
###c_masiktu
5229 = { #Masiktu
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5761 = { #Pirittu
	holding = none
}
5760 = { #Pardish
	holding = none
}
##d_tille
###c_tille
445 = { #Tille
	culture = haburi
	religion = western_hurrian_faith
	holding = castle_holding
}
420 = { #Rasmu
	holding = city_holding
}
5243 = { #Restu
	holding = none
}
5244 = { #Qarnu
	holding = none
}
5245 = { #Pethallu
	holding = none
}
1865 = { #Karâr
	holding = none
}
###c_sibirri
5255 = { #Sibirri
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5249 = { #Kugru
	holding = none
}
5223 = { #Hahhuratta
	holding = none
}
5267 = { #Istaruta
	holding = none
}
##d_halziatbari
###c_habatu
5224 = { #Habatu
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5261 = { #Hasum
	holding = none
}
5265 = { #Iltanu
	holding = city_holding
}
5260 = { #Gigunu
	holding = none
}
5389 = { #Kubshu
	holding = church_holding
}
###c_buaru
5256 = { #Bu'aru
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5258 = { #Danniti
	holding = none
}
5259 = { #Attina
	holding = city_holding
}
5266 = { #Sibtu
	holding = none
}
###c_talmusa
5264 = { #Talmusa
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5263 = { #Barari
	holding = city_holding
}
5262 = { #Mahar
	holding = none
}
5388 = { #Labaris
	holding = none
}