﻿scutage_contract_trigger = { always = no }
march_contract_trigger = {
	culture = { has_innovation = innovation_tribal_vassals }
}
palatinate_contract_trigger = { always = no }
fortification_rights_contract_trigger = {
	culture = { has_innovation = innovation_fortifications_2 }
}
triple_cb_discount_trigger = {
	culture = {
		has_innovation  = innovation_border_stones
		has_innovation  = innovation_conscription
		has_innovation  = innovation_puppet_rulers
	}
}
double_cb_discount_trigger = {
	culture = {
		OR = {
			AND = {
				has_innovation  = innovation_border_stones
				has_innovation  = innovation_conscription
			}
			AND = {
				has_innovation  = innovation_border_stones
				has_innovation  = innovation_puppet_rulers
			}
			AND = {
				has_innovation  = innovation_conscription
				has_innovation  = innovation_puppet_rulers
			}
		}
	}
}
single_cb_discount_trigger = {
	culture = {
		OR = {
			has_innovation  = innovation_border_stones
			has_innovation  = innovation_conscription
			has_innovation  = innovation_puppet_rulers
		}
	}
}
unlocked_de_jure_cb_trigger = {
	culture = {
		has_innovation  = innovation_puppet_rulers
	}
}
unlocked_raiding_cb_trigger = {
	culture = {
		has_innovation  = innovation_raiding_parties
	}
}
single_county_de_jure_cb_trigger = {
	always = yes
}
duchy_de_jure_cb_trigger = {
	culture = {
		has_innovation  = innovation_border_stones
	}
}
tributary_cb_allowed_trigger = {
	culture = {
		has_innovation  = innovation_tributaries
	}
}
multi_claim_own_allowed_trigger = {
	culture = {
		has_innovation  = innovation_conscription
	}
}
multi_claim_other_allowed_trigger = {
	culture = {
		has_innovation  = innovation_puppet_rulers
	}
}
governor_contract_trigger = {
	culture = {
		has_innovation = innovation_governors
	}
}
has_all_tribal_era_innovations_trigger = {
	has_innovation = innovation_earthworks
	has_innovation = innovation_barracks
	has_innovation = innovation_raiding_parties
	has_innovation = innovation_mustering_grounds
	has_innovation = innovation_tribal_vassals
	has_innovation = innovation_bronze_socket_axe
	has_innovation = innovation_siege_ladders

	has_innovation = innovation_city_planning
	has_innovation = innovation_early_palaces
	has_innovation = innovation_centralized_irrigation
	has_innovation = innovation_potters_wheel
	has_innovation = innovation_city_states
	has_innovation = innovation_kingship
	has_innovation = innovation_kings_justice
	has_innovation = innovation_border_stones
}
innovation_crown_authority_1_trigger = { has_innovation = innovation_kings_justice } # 1 is the 2nd one in game, first is 0
innovation_crown_authority_2_trigger = { has_innovation = innovation_law_code }
innovation_crown_authority_3_trigger = { has_innovation = innovation_imperial_authority }
innovation_distance_efficiency_1_trigger = { has_innovation = innovation_early_palaces }
innovation_distance_efficiency_2_trigger = { has_innovation = innovation_palatial }
innovation_distance_efficiency_3_trigger = { has_innovation = innovation_imperial_palaces }
innovation_distance_efficiency_4_trigger = { has_innovation = innovation_distance_efficiency_4 }