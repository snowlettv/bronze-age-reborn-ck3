﻿#k_tehenu
##d_ammonia ###################################
###c_ammonia
4466 = { #Ammonia
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4747 = { #Ayane
	holding = none
}
106 = { #Adrarre
	holding = none
}
###c_mareia
1487 = { #Mareia
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
1954 = { #Usekhtu
	holding = none
}
4630 = { #Manaswane
	holding = none
}
###c_antiphrai
2098 = { #Antiphrai
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4751 = { #Magharre
	holding = none
}
4749 = { #Itnine
	holding = none
}
###c_apis
1425 = { #Apis
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
1454 = { #Rata
	holding = none
}
4708 = { #Assaje
	holding = none
}
##d_aghilasse
###c_aghilasse
4712 = { #Aghilasse
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4709 = { #Aydy
	holding = none
}
4711 = { #Ahrelma
	holding = none
}
4710 = { #Washta
	holding = none
}
###c_iwarkate
4718 = { #Iwarkate
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4716 = { #Ifista
	holding = none
}
4717 = { #Jaje
	holding = none
}
4719 = { #Tassajate
	holding = none
}
###c_katisa
4804 = { #Katisa
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4933 = { #Doushbihe
	holding = none
}
4715 = { #Adlasse
	holding = none
}
4932 = { #Douke
	holding = none
}
##d_siwa
###c_siwa
4902 = { #Shot-jemet
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
	
	785.1.1 = {
		special_building_slot = temple_of_ammon_01 #Oracle of Ammon
	}
}
4952 = { #Inka
	holding = none
}
4953 = { #Ihla
	holding = none
}
4955 = { #Takattite
	holding = none
}
4954 = { #Dakdime
	holding = none
}
4956 = { #Ba-Tathenn
	holding = none
}
###c_khepen
4957 = { #Metut
	culture = tehenu
	religion = berber_faith
	holding = tribal_holding
}
4959 = { #Rebashaiu
	holding = none
}
4958 = { #Asiut
	holding = none
}
4791 = { #Khepen
	holding = none
}