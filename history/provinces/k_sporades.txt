﻿#k_sporades
##d_karpathos ###################################
###c_karpathos			
1148 = { #Potidaion
	culture = karpathan
	religion = aegean_faith
	holding = tribal_holding
}
1149 = { #Arkaseia
	holding = none
}
1150 = { #Karpa
	holding = none
}
1152 = { #Saros
	holding = none
}
###c_kasos
1147 = { #Kasos
	culture = karpathan
	religion = aegean_faith
	holding = tribal_holding
}
##d_kos
###c_kos
1188 = { #Kos
	culture = leleges
	religion = aegean_faith
	holding = castle_holding
}
1181 = { #Onia
	holding = city_holding
}
1184 = { #Halasarna
	holding = church_holding
}
1185 = { #Lido
	holding = city_holding
}
###c_symi
1548 = { #Ipranara
	culture = telchines
	religion = aegean_faith
	holding = tribal_holding
}
###c_knidos
1550 = { #Ahma
	culture = carian
	religion = anatolian_faith
	holding = castle_holding
}
1546 = { #Orna
	holding = city_holding
}
###c_astypalea
1104 = { #Astypalea
	culture = leleges
	religion = aegean_faith
	holding = tribal_holding
}
###c_nisyros
1180 = { #Nisyros
	culture = leleges
	religion = aegean_faith
	holding = tribal_holding
}
###c_telos
1179 = { #Telos
	culture = telchines
	religion = aegean_faith
	holding = tribal_holding
}
##d_sporades
###c_kalimnos
1190 = { #Kalimnos
	culture = leleges
	religion = aegean_faith
	holding = castle_holding
}
###c_patmos
1202 = { #Patmos
	culture = leleges
	religion = aegean_faith
	holding = tribal_holding
}
###c_lepsia
1200 = { #Lepsia
	culture = leleges
	religion = aegean_faith
	holding = tribal_holding
}
###c_leros
1199 = { #Leros
	culture = leleges
	religion = aegean_faith
	holding = castle_holding
}
##d_samos
###c_samos
1216 = { #Samos
	culture = leleges
	religion = aegean_faith
	holding = castle_holding
}
1209 = { #Kerkis
	holding = city_holding
}
1212 = { #Pefkos
	holding = church_holding
}
1213 = { #Pirgos
	holding = city_holding
}
1218 = { #Palaio
	holding = castle_holding
}
###c_ikaros
1204 = { #Drakanon
	culture = leleges
	religion = aegean_faith
	holding = castle_holding
} 
1208 = { #Pesi
	holding = city_holding
}
###c_korsiai
1203 = { #Korsiai
	culture = leleges
	religion = aegean_faith
	holding = tribal_holding
}