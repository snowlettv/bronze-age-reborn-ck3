﻿#k_sumu
##d_kaprabi ###################################
###c_kaprabi
3878 = { #Kaprabi
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3542 = { #Kaqqar
	holding = none
}
5868 = { #Zârutu
	holding = none
}
5866 = { #Sulummê
	holding = none
}
5934 = { #Pana
	holding = none
}
###c_sikkanu
5853 = { #Sikkanu
	culture = shubrian
	religion = western_hurrian_faith
	holding = tribal_holding
}
5852 = { #Magallum
	holding = none
}
5850 = { #Amu
	holding = none
}
###c_daku
5932 = { #Daku
	culture = haburi
	religion = western_hurrian_faith
	holding = tribal_holding
}
5865 = { #Tahapshu
	holding = none
}
5854 = { #Berkabtu
	holding = none
}
###c_naslamu
5867 = { #Naslamu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
5860 = { #Maqtu
	holding = none
}
5864 = { #Raksu
	holding = none
}
##d_habhu
###c_tanihtu
5869 = { #Tanîhtu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
5875 = { #Na'ru
	holding = none
}
5870 = { #Litûtu
	holding = none
}
5880 = { #Halâlu
	holding = none
}
5863 = { #Birtu
	holding = none
}
###c_takne
5878 = { #Taknê
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
5879 = { #Balangu
	holding = none
}
5877 = { #Pilaggu
	holding = none
}
5876 = { #Mazzû
	holding = none
}
##d_qipanu
###c_mashkanutu
5871 = { #Mashkânutu
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
5872 = { #Tigû
	holding = none
}
5881 = { #Ebbûbu
	holding = none
}
###c_tupullu
4110 = { #Tupullû
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3548 = { #Yânumish
	holding = none
}
2974 = { #Âmerânu
	holding = none
}
350 = { #Abarnium
	holding = none
}
###c_niputum
3909 = { #Nipûtum
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
3948 = { #Zûtu
	holding = none
}
5873 = { #Ningûtu
	holding = none
}
5874 = { #Buluggu
	holding = none
}