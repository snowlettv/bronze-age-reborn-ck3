﻿
local_pilgrimage = {
	texture = "gfx/interface/icons/traits/pilgrim.dds"
	parent = pilgrimage
	alias = { local_pilgrimages local_pilgrimage_short local_pilgrimages_short }
}

nile_flood = {
	texture = "gfx/interface/icons/terrain_types/floodplains.dds"
}

temples_of_aswan = {
	alias = { temples_of_aswan_city temple_in_aswan }
	parent = local_pilgrimage	
	texture = "gfx/interface/icons/activities/activity_RICE_upper_egypt_visit_aswan_temples.dds"
}

hot_springs = {
	texture = "gfx/interface/icons/activities/activity_RICE_hot_springs_visit.dds"
	alias = { hot_spring }
}
