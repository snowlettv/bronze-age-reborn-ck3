﻿#Generated
282 = {
	name = "Siwe-Palar-Hupak"
	dynasty = 282
	religion = "elamite_faith"
	culture = "anshanian"
	756.2.2 = {
		birth = yes
	}
	811.8.26 = {
		death = yes
	}
}
283 = {
	name = "Napi-Ilhush"
	dynasty = 283
	religion = "elamite_faith"
	culture = "anshanian"
	768.9.23 = {
		birth = yes
	}
	818.4.8 = {
		death = yes
	}
}
284 = {
	name = "Atta-Merra-Haiki"
	dynasty = 284
	religion = "elamite_faith"
	culture = "anshanian"
	757.5.7 = {
		birth = yes
	}
	821.4.26 = {
		death = yes
	}
}
285 = {
	name = "Tazitta"
	dynasty = 285
	religion = "elamite_faith"
	culture = "anshanian"
	754.11.14 = {
		birth = yes
	}
	815.5.26 = {
		death = yes
	}
}
287 = {
	name = "Lila-Irtash"
	dynasty = 287
	religion = "elamite_faith"
	culture = "anshanian"
	747.8.9 = {
		birth = yes
	}
	798.7.3 = {
		death = yes
	}
}
288 = {
	name = "Shilhaha"
	dynasty = 288
	religion = "elamite_faith"
	culture = "anshanian"
	756.3.23 = {
		birth = yes
	}
	820.1.25 = {
		death = yes
	}
}
315 = {
	name = "Kutik-Inshushinak"
	dynasty = 315
	religion = "elamite_faith"
	culture = "anshanian"
	763.10.23 = {
		birth = yes
	}
	806.4.16 = {
		death = yes
	}
}
718 = {
	name = "Martiya"
	dynasty = 718
	religion = "elamite_faith"
	culture = "anshanian"
	766.1.25 = {
		birth = yes
	}
	808.9.12 = {
		death = yes
	}
}
719 = {
	name = "Tan-Uli"
	dynasty = 719
	religion = "elamite_faith"
	culture = "anshanian"
	760.5.25 = {
		birth = yes
	}
	805.4.3 = {
		death = yes
	}
}
720 = {
	name = "Atamaita"
	dynasty = 720
	religion = "elamite_faith"
	culture = "anshanian"
	746.10.1 = {
		birth = yes
	}
	802.6.15 = {
		death = yes
	}
}
721 = {
	name = "Pahir-Ishshan"
	dynasty = 721
	religion = "elamite_faith"
	culture = "anshanian"
	749.9.5 = {
		birth = yes
	}
	815.9.4 = {
		death = yes
	}
}
722 = {
	name = "Kuk-Kirwash"
	dynasty = 722
	religion = "elamite_faith"
	culture = "anshanian"
	752.5.8 = {
		birth = yes
	}
	796.1.6 = {
		death = yes
	}
}
723 = {
	name = "Napirisha-Untash"
	dynasty = 723
	religion = "elamite_faith"
	culture = "anshanian"
	748.6.25 = {
		birth = yes
	}
	795.3.7 = {
		death = yes
	}
}
726 = {
	name = "Kur-Ishshak"
	dynasty = 726
	religion = "elamite_faith"
	culture = "anshanian"
	748.8.25 = {
		birth = yes
	}
	815.1.13 = {
		death = yes
	}
}
727 = {
	name = "Kidinu"
	dynasty = 727
	religion = "elamite_faith"
	culture = "anshanian"
	745.11.9 = {
		birth = yes
	}
	808.8.10 = {
		death = yes
	}
}
728 = {
	name = "Atta-hushu"
	dynasty = 728
	religion = "elamite_faith"
	culture = "anshanian"
	748.6.11 = {
		birth = yes
	}
	809.1.23 = {
		death = yes
	}
}
729 = {
	name = "Insushinak-Sunkir-Nappipir"
	dynasty = 729
	religion = "elamite_faith"
	culture = "anshanian"
	749.10.19 = {
		birth = yes
	}
	812.4.14 = {
		death = yes
	}
}
1128 = {
	name = "Humban-Untash"
	dynasty = 1128
	religion = "elamite_faith"
	culture = "anshanian"
	1270.5.1 = {
		birth = yes
	}
	1320.4.11 = {
		death = yes
	}
}
1129 = {
	name = "Kutik-Inshushinak"
	dynasty = 1129
	religion = "elamite_faith"
	culture = "anshanian"
	1280.2.19 = {
		birth = yes
	}
	1331.9.12 = {
		death = yes
	}
}
1130 = {
	name = "Eparti"
	dynasty = 1130
	religion = "elamite_faith"
	culture = "anshanian"
	1273.3.11 = {
		birth = yes
	}
	1326.2.14 = {
		death = yes
	}
}
1131 = {
	name = "Kutik-Matlat"
	dynasty = 1131
	religion = "elamite_faith"
	culture = "anshanian"
	1275.7.8 = {
		birth = yes
	}
	1338.6.23 = {
		death = yes
	}
}
1133 = {
	name = "Unpatar-Napirisha"
	dynasty = 1133
	religion = "elamite_faith"
	culture = "anshanian"
	1286.2.27 = {
		birth = yes
	}
	1333.2.22 = {
		death = yes
	}
}
1134 = {
	name = "Hita"
	dynasty = 1134
	religion = "elamite_faith"
	culture = "anshanian"
	1281.1.1 = {
		birth = yes
	}
	1344.4.17 = {
		death = yes
	}
}
1135 = {
	name = "Tazitta"
	dynasty = 1135
	religion = "elamite_faith"
	culture = "anshanian"
	1291.3.18 = {
		birth = yes
	}
	1354.4.22 = {
		death = yes
	}
}
1163 = {
	name = "Luh-Ishshan"
	dynasty = 1163
	religion = "elamite_faith"
	culture = "anshanian"
	1270.3.18 = {
		birth = yes
	}
	1312.6.17 = {
		death = yes
	}
}
1164 = {
	name = "Martiya"
	dynasty = 1164
	religion = "elamite_faith"
	culture = "anshanian"
	1289.8.24 = {
		birth = yes
	}
	1337.5.22 = {
		death = yes
	}
}
1570 = {
	name = "Attar-Kittah"
	dynasty = 1570
	religion = "elamite_faith"
	culture = "anshanian"
	1270.9.2 = {
		birth = yes
	}
	1327.11.14 = {
		death = yes
	}
}
1571 = {
	name = "Napi-Ilhush"
	dynasty = 1571
	religion = "elamite_faith"
	culture = "anshanian"
	1283.11.7 = {
		birth = yes
	}
	1333.11.6 = {
		death = yes
	}
}
1574 = {
	name = "Gir-Namme"
	dynasty = 1574
	religion = "elamite_faith"
	culture = "anshanian"
	1285.4.11 = {
		birth = yes
	}
	1329.10.1 = {
		death = yes
	}
}
1575 = {
	name = "Indattu-Napir"
	dynasty = 1575
	religion = "elamite_faith"
	culture = "anshanian"
	1271.4.10 = {
		birth = yes
	}
	1334.8.22 = {
		death = yes
	}
}