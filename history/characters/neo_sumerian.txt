﻿#Defined
2sumer1 = {
	name = "Ea-gamil"
	dynasty = dynn_2sumer1
	dna = sumer_1_dna
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	father = 2sumer2
	mother = 2sumer3
	1293.9.12 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1310.1.1 = {
		effect = {
			spawn_army = {
				name = event_troop_default_name
				men_at_arms = {
					type = marsh_raider
					stacks = 4
				}
				location = province:6002
				origin = province:6002
			}
		}
	}
	1353.10.22 = {
		death = yes
	}
}
2sumer2 = { #dates uncertain just wanted the king to have a king as father
	name = "Melamkurkurra"
	dynasty = dynn_2sumer1
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1253.11.4 = {
		birth = yes
	}
	1285.1.1 = {
		add_spouse = 2sumer3
	}
	1310.1.1 = {
		death = yes
	}
}
2sumer3 = { #dates uncertain just wanted the king to have a king as father
	name = "Enanatuma"
	#dynasty = dynn_2sumer1
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	female = yes
	1256.11.4 = {
		birth = yes
	}
	1320.10.22 = {
		death = yes
	}
}
#Generated
992 = {
	name = "Ur-Utu"
	dynasty = 992
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1289.7.3 = {
		birth = yes
	}
	1351.4.23 = {
		death = yes
	}
}
993 = {
	name = "Adad-nirari"
	dynasty = 993
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1293.3.16 = {
		birth = yes
	}
	1350.1.16 = {
		death = yes
	}
}
994 = {
	name = "En-me-barage-si"
	dynasty = 994
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1270.10.4 = {
		birth = yes
	}
	1337.4.2 = {
		death = yes
	}
}
996 = {
	name = "Shu-Durul"
	dynasty = 996
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1291.10.15 = {
		birth = yes
	}
	1343.1.7 = {
		death = yes
	}
}
1586 = {
	name = "Asharid-apal-Ekur"
	dynasty = 1586
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1288.1.5 = {
		birth = yes
	}
	1356.9.12 = {
		death = yes
	}
}
1587 = {
	name = "Puzur-Ashur"
	dynasty = 1587
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1280.10.9 = {
		birth = yes
	}
	1331.2.9 = {
		death = yes
	}
}
1588 = {
	name = "Shar-kali-sharri"
	dynasty = 1588
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1277.7.21 = {
		birth = yes
	}
	1336.6.25 = {
		death = yes
	}
}
1589 = {
	name = "Ur-gar"
	dynasty = 1589
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1291.2.17 = {
		birth = yes
	}
	1355.10.1 = {
		death = yes
	}
}
1592 = {
	name = "En-sipad-zid-ana"
	dynasty = 1592
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1274.1.8 = {
		birth = yes
	}
	1322.7.26 = {
		death = yes
	}
}
1593 = {
	name = "Ur-Baba"
	dynasty = 1593
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1270.1.2 = {
		birth = yes
	}
	1331.1.4 = {
		death = yes
	}
}
1594 = {
	name = "Usi-watar"
	dynasty = 1594
	religion = "sumerian_faith"
	culture = "neo_sumerian"
	1273.2.14 = {
		birth = yes
	}
	1322.3.16 = {
		death = yes
	}
}