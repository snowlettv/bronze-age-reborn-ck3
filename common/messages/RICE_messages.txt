﻿### Toast SFX
@default_toast_soundeffect = "event:/SFX/UI/Notifications/Toasts/sfx_ui_notification_toast_generic"
@toast_soundeffect_neutral = "event:/SFX/UI/Notifications/Toasts/sfx_ui_notifications_toast_neutral"
@toast_soundeffect_good = "event:/SFX/UI/Notifications/Toasts/sfx_ui_notifications_toast_positive"
@toast_soundeffect_bad = "event:/SFX/UI/Notifications/Toasts/sfx_ui_notifications_toast_negative"


### Message SFX
@default_msg_soundeffect = "event:/SFX/UI/Notifications/Messages/sfx_ui_message_theme_neutral"
@msg_neutral_soundeffect = "event:/SFX/UI/Notifications/Messages/sfx_ui_message_theme_neutral"
@msg_good_soundeffect = "event:/SFX/UI/Notifications/Messages/sfx_ui_message_theme_positive"
@msg_bad_soundeffect = "event:/SFX/UI/Notifications/Messages/sfx_ui_message_theme_negative"
@msg_diarchy_update_soundeffect = "event:/DLC/EP2/SFX/Events/Regencies/ep2_evn_regencies_update"

### Descriptions
# desc = event_message_effect
# desc = event_message_text
# desc = event_message_text_and_effect
# desc = event_message_effect_and_text

### RICE EVENT MESSAGES ###

egypt_nile_flood_event_bad = {
	icon = "egypt_nile_flood"
	title = event_message_title
	desc = event_message_effect
	style = bad
	soundeffect = @msg_bad_soundeffect
}

egypt_nile_flood_event_neutral = {
	icon = "egypt_nile_flood"
	title = event_message_title
	desc = event_message_effect
	style = neutral
	soundeffect = @msg_neutral_soundeffect
}

egypt_nile_flood_event_good = {
	icon = "egypt_nile_flood"
	title = event_message_title
	desc = event_message_effect
	style = good
	soundeffect = @msg_good_soundeffect
}