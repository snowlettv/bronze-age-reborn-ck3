﻿harappan = {
	color = harappan
	
	ethos = ethos_egalitarian
	heritage = heritage_harappan
	language = language_harappan
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_dryland_dwellers
		tradition_agrarian
		tradition_city_keepers
		tradition_maritime_mercantilism
	}
	
	name_list = name_list_harappan
	
	coa_gfx = { elamite_coa_gfx }
	building_gfx = { indian_building_gfx }
	clothing_gfx = { indian_clothing_gfx }
	unit_gfx = { indian_unit_gfx }
	
	ethnicities = {
		10 = elamite
	}
}