﻿set_liege_distance_pulse = { #Root is the vassal ruler
	effect = {
		if = {
			limit = { is_independent_ruler = no }
			liege = { save_scope_as = liege }
			set_variable = {
				name = liege_distance
				value = ba_get_vassal_distance_svalue
			}
		}
	}
}
ba_chariot_spread_pulse = {
	trigger = {	
		NOT = {
			side_primary_participant.culture = enemy_side.side_primary_participant.culture 
		}
		side_primary_participant.culture = {
			NOT = { has_variable = spoked_wheel_exposure }
		}
		enemy_side = {
			OR = {
				has_maa_of_type = light_chariot
				has_maa_of_type = heavy_chariot
				has_maa_of_type = hyksos_chariot
				has_maa_of_type = hittite_chariot
			}
		}
	}
	effect = {
		random = {
			chance = 10

			ba_culture_exposure_effect = {
				NAME = spoked_wheel
				CULTURE_TAKER = side_primary_participant.culture
				CULTURE_GIVER = enemy_side.side_primary_participant.culture
			}
		}
	}
}
ba_composite_bow_spread_pulse = {
	trigger = {	
		NOT = {
			side_primary_participant.culture = enemy_side.side_primary_participant.culture 
		}
		side_primary_participant.culture = {
			NOT = { has_variable = composite_bow_exposure }
		}
		enemy_side.side_primary_participant.culture = {
			has_innovation = innovation_composite_bow
		}
	}
	effect = {
		random = {
			chance = 10

			ba_culture_exposure_effect = {
				NAME = composite_bow
				CULTURE_TAKER = side_primary_participant.culture
				CULTURE_GIVER = enemy_side.side_primary_participant.culture
			}
		}
	}
}
ba_innovation_spread_pulse = { #root is no scope, starting scope is county
	trigger = { NOT = { culture = holder.culture } }
	effect = {
		#Spoked Wheel
		random_list = {
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = spoked_wheel_exposure } }
						NOT = { holder.culture = { has_variable = spoked_wheel_exposure } }
					}
				}
				ba_culture_exposure_effect = { #Holder gives
					NAME = spoked_wheel
					CULTURE_TAKER = culture
					CULTURE_GIVER = holder.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = spoked_wheel
					CULTURE_TAKER = holder.culture
					CULTURE_GIVER = culture
				}
			}
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = composite_bow_exposure } }
						NOT = { holder.culture = { has_variable = composite_bow_exposure } }
					}
				}
				#Composite Bow
				ba_culture_exposure_effect = { #Holder gives
					NAME = composite_bow
					CULTURE_TAKER = culture
					CULTURE_GIVER = holder.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = composite_bow
					CULTURE_TAKER = holder.culture
					CULTURE_GIVER = culture
				}
			}
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = writing_exposure } }
						NOT = { holder.culture = { has_variable = writing_exposure } }
					}
				}
				#Writing
				ba_culture_exposure_effect = { #Holder gives
					NAME = writing
					CULTURE_TAKER = culture
					CULTURE_GIVER = holder.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = writing
					CULTURE_TAKER = holder.culture
					CULTURE_GIVER = culture
				}
			}
		}
	}
}
ba_innovation_spread_marriage_pulse = { #root is the major party, think it is the man in normal marriages and women in matrilineal ones.
	effect = {
		#Spoked Wheel
		random_list = {
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = spoked_wheel_exposure } }
						NOT = { scope:spouse.culture = { has_variable = spoked_wheel_exposure } }
					}
				}
				ba_culture_exposure_effect = { #Holder gives
					NAME = spoked_wheel
					CULTURE_TAKER = culture
					CULTURE_GIVER = scope:spouse.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = spoked_wheel
					CULTURE_TAKER = scope:spouse.culture
					CULTURE_GIVER = culture
				}
			}
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = composite_bow_exposure } }
						NOT = { scope:spouse.culture = { has_variable = composite_bow_exposure } }
					}
				}
				#Composite Bow
				ba_culture_exposure_effect = { #Holder gives
					NAME = composite_bow
					CULTURE_TAKER = culture
					CULTURE_GIVER = scope:spouse.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = composite_bow
					CULTURE_TAKER = scope:spouse.culture
					CULTURE_GIVER = culture
				}
			}
			10 = {
				trigger = {
					OR = {
						NOT = { culture = { has_variable = writing_exposure } }
						NOT = { scope:spouse.culture = { has_variable = writing_exposure } }
					}
				}
				#Writing
				ba_culture_exposure_effect = { #Holder gives
					NAME = writing
					CULTURE_TAKER = culture
					CULTURE_GIVER = scope:spouse.culture
				}
				ba_culture_exposure_effect = { #Holder gets
					NAME = writing
					CULTURE_TAKER = scope:spouse.culture
					CULTURE_GIVER = culture
				}
			}
		}
	}
}