﻿#Defined
mari1 = {
	name = "Rim-Sin"
	dynasty = dynn_mari1
	religion = "amorite_faith"
	culture = "amoritic"
	743.1.3 = {
		birth = yes
	}
	789.1.3 = {
		death = yes
	}
}
2qatna1 = {
	name = "Apil-Sin"
	dynasty = dynn_2qatna1
	dna = qatna_1_dna
	religion = "amorite_faith"
	culture = "amoritic"
	1281.5.6 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1344.5.26 = {
		death = yes
	}
}
#Yamhadite rulers
2halab1 = {
	name = "Sarra-El" #Sarra-El, Early 16th, possibly son of Yarim-Lim III
	dynasty = dynn_2halab1
	dna = halab_1_dna
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab4
	1272.12.21 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1345.1.1 = {
		death = yes
	}
}
2halab2 = {
	name = "Abba-El" #Mid 16th, son of Sarra-El 
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab1
	1298.6.12 = {
		birth = yes
	}
	1365.1.1 = {
		death = yes
	}
}
2halab3 = {
	name = "Hammurabi" #1625-1600, son of Yarim-Lim III mod-1275-1300
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab4
	1238.8.12 = {
		birth = yes
	}
	1300.1.1 = {
		death = yes
	}
}
2halab4 = {
	name = "Yarim-Lim" #Mid 17th-1625, son of Niqmi-Epuh mod-?-1275
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab7
	1210.6.16 = {
		birth = yes
	}
	1275.1.1 = {
		death = yes
	}
}
2halab5 = {
	name = "Hammurabi" #Mid 17th, son of Niqmi-Epuh
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab7
	1198.3.6 = {
		birth = yes
	}
	1238.1.1 = {
		death = yes
	}
}
2halab6 = {
	name = "Irkabtum" #1675-17th, son of Niqmi-Epuh mod-1225-?
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab7
	1191.5.26 = {
		birth = yes
	}
	1229.1.1 = {
		death = yes
	}
}
2halab7 = {
	name = "Niqmi-Epuh" #1700-1675, son of Yarim-Lim II mod-1200-1225
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab8
	1174.7.12 = {
		birth = yes
	}
	1225.1.1 = {
		death = yes
	}
}
2halab8 = {
	name = "Yarim-Lim" #1720-1700, son of Abba-El I mod-1180-1200
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab9
	1157.5.21 = {
		birth = yes
	}
	1200.1.1 = {
		death = yes
	}
}
2halab9 = {
	name = "Abba-El" #1720-1700, son of Hammurabi I mod-1150-1180
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab10
	1140.7.14 = {
		birth = yes
	}
	1180.1.1 = {
		death = yes
	}
}
2halab10 = {
	name = "Hammurabi" #1720-1700, son of Yarim-Lim I mod-1136-1150
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab11
	1115.2.5 = {
		birth = yes
	}
	1150.1.1 = {
		death = yes
	}
}
2halab11 = {
	name = "Yarim-Lim" #1780-1764, son of Sumu-Epuh mod-1120-1136
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab12
	1082.3.11 = {
		birth = yes
	}
	1136.1.1 = {
		death = yes
	}
}
2halab12 = {
	name = "Sumu-Epuh" #1810-1780, founder? mod 1090-1120
	dynasty = dynn_2halab1
	religion = "amorite_faith"
	culture = "amoritic"
	1065.11.18 = {
		birth = yes
	}
	1120.1.1 = {
		death = yes
	}
}
#Alalakh-Yamhad Dynasty, mostly fictional
2alalakh1 = {
	name = "Yarim-Lim"
	dynasty_house = house_alalakh_yamhad
	dna = alalakh_1_dna
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2alalakh2
	1294.4.12 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1350.1.1 = {
		death = yes
	}
}
2alalakh2 = {
	name = "Yarim-Lim" #The grandson of founder Yarim-Lim I, hypothized to exist by some
	dynasty_house = house_alalakh_yamhad
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2alalakh3
	1246.4.12 = {
		birth = yes
	}
	1305.1.1 = {
		death = yes
	}
}
2alalakh3 = {
	name = "Ammitakum" #Son of Yarim-Lim I
	dynasty_house = house_alalakh_yamhad
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2alalakh4
	1202.8.5 = {
		birth = yes
	}
	1265.1.1 = {
		death = yes
	}
}
2alalakh4 = {
	name = "Yarim-Lim" #1735-? Founder of the house 1165-?
	dynasty_house = house_alalakh_yamhad
	religion = "amorite_faith"
	culture = "amoritic"
	father = 2halab10
	1157.5.11 = {
		birth = yes
	}
	1221.1.1 = {
		death = yes
	}
}
2khana1 = {
	name = "Ammi-ditana"
	dynasty = dynn_2khana1
	dna = khana_1_dna
	religion = "amorite_faith"
	culture = "amoritic"
	1277.9.22 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1340.8.24 = {
		death = yes
	}
}
#Generated
73 = {
	name = "Hammurabi"
	dynasty = 73
	religion = "amorite_faith"
	culture = "amoritic"
	754.10.18 = {
		birth = yes
	}
	815.9.26 = {
		death = yes
	}
}
74 = {
	name = "Samium"
	dynasty = 74
	religion = "amorite_faith"
	culture = "amoritic"
	745.4.6 = {
		birth = yes
	}
	808.3.20 = {
		death = yes
	}
}
86 = {
	name = "Samsu-iluna"
	dynasty = 86
	religion = "amorite_faith"
	culture = "amoritic"
	763.9.15 = {
		birth = yes
	}
	806.10.21 = {
		death = yes
	}
}
90 = {
	name = "Sumuel"
	dynasty = 90
	religion = "amorite_faith"
	culture = "amoritic"
	762.6.18 = {
		birth = yes
	}
	812.6.2 = {
		death = yes
	}
}
91 = {
	name = "Samsu-iluna"
	dynasty = 91
	religion = "amorite_faith"
	culture = "amoritic"
	760.6.7 = {
		birth = yes
	}
	808.4.3 = {
		death = yes
	}
}
96 = {
	name = "Sin-Eribam"
	dynasty = 96
	religion = "amorite_faith"
	culture = "amoritic"
	747.11.17 = {
		birth = yes
	}
	796.6.1 = {
		death = yes
	}
}
97 = {
	name = "Abisare"
	dynasty = 97
	religion = "amorite_faith"
	culture = "amoritic"
	759.8.19 = {
		birth = yes
	}
	800.1.13 = {
		death = yes
	}
}
98 = {
	name = "Ammi-ditana"
	dynasty = 98
	religion = "amorite_faith"
	culture = "amoritic"
	761.9.22 = {
		birth = yes
	}
	813.4.8 = {
		death = yes
	}
}
101 = {
	name = "Sumu-abum"
	dynasty = 101
	religion = "amorite_faith"
	culture = "amoritic"
	767.2.25 = {
		birth = yes
	}
	818.9.13 = {
		death = yes
	}
}
102 = {
	name = "Ammi-ditana"
	dynasty = 102
	religion = "amorite_faith"
	culture = "amoritic"
	755.9.24 = {
		birth = yes
	}
	813.4.13 = {
		death = yes
	}
}
103 = {
	name = "Apil-Sin"
	dynasty = 103
	religion = "amorite_faith"
	culture = "amoritic"
	760.8.17 = {
		birth = yes
	}
	802.7.19 = {
		death = yes
	}
}
105 = {
	name = "Warad-Sin"
	dynasty = 105
	religion = "amorite_faith"
	culture = "amoritic"
	745.9.9 = {
		birth = yes
	}
	807.9.3 = {
		death = yes
	}
}
106 = {
	name = "Samsu-Ditana"
	dynasty = 106
	religion = "amorite_faith"
	culture = "amoritic"
	769.8.14 = {
		birth = yes
	}
	830.2.25 = {
		death = yes
	}
}
107 = {
	name = "Naplanum"
	dynasty = 107
	religion = "amorite_faith"
	culture = "amoritic"
	759.4.1 = {
		birth = yes
	}
	809.11.15 = {
		death = yes
	}
}
108 = {
	name = "Su-abu"
	dynasty = 108
	religion = "amorite_faith"
	culture = "amoritic"
	755.1.15 = {
		birth = yes
	}
	820.8.1 = {
		death = yes
	}
}
233 = {
	name = "Sin-Iqisham"
	dynasty = 233
	religion = "amorite_faith"
	culture = "amoritic"
	750.11.1 = {
		birth = yes
	}
	817.4.15 = {
		death = yes
	}
}
234 = {
	name = "Sumu-la-El"
	dynasty = 234
	religion = "amorite_faith"
	culture = "amoritic"
	752.6.23 = {
		birth = yes
	}
	797.6.20 = {
		death = yes
	}
}
238 = {
	name = "Samsu-Ditana"
	dynasty = 238
	religion = "amorite_faith"
	culture = "amoritic"
	762.2.14 = {
		birth = yes
	}
	808.5.13 = {
		death = yes
	}
}
239 = {
	name = "Abisare"
	dynasty = 239
	religion = "amorite_faith"
	culture = "amoritic"
	767.8.8 = {
		birth = yes
	}
	832.2.8 = {
		death = yes
	}
}
241 = {
	name = "Sumu-abum"
	dynasty = 241
	religion = "amorite_faith"
	culture = "amoritic"
	758.9.14 = {
		birth = yes
	}
	813.2.26 = {
		death = yes
	}
}
242 = {
	name = "Abi-eshuh"
	dynasty = 242
	religion = "amorite_faith"
	culture = "amoritic"
	758.1.6 = {
		birth = yes
	}
	803.9.2 = {
		death = yes
	}
}
243 = {
	name = "Abisare"
	dynasty = 243
	religion = "amorite_faith"
	culture = "amoritic"
	769.5.27 = {
		birth = yes
	}
	815.5.10 = {
		death = yes
	}
}
244 = {
	name = "Sumuel"
	dynasty = 244
	religion = "amorite_faith"
	culture = "amoritic"
	749.8.2 = {
		birth = yes
	}
	794.3.2 = {
		death = yes
	}
}
245 = {
	name = "Zabaia"
	dynasty = 245
	religion = "amorite_faith"
	culture = "amoritic"
	755.9.4 = {
		birth = yes
	}
	807.7.4 = {
		death = yes
	}
}
342 = {
	name = "Ammi-ditana"
	dynasty = 342
	religion = "amorite_faith"
	culture = "amoritic"
	746.11.18 = {
		birth = yes
	}
	813.11.12 = {
		death = yes
	}
}
776 = {
	name = "Sabium"
	dynasty = 776
	religion = "amorite_faith"
	culture = "amoritic"
	769.2.13 = {
		birth = yes
	}
	811.8.7 = {
		death = yes
	}
}
777 = {
	name = "Samium"
	dynasty = 777
	religion = "amorite_faith"
	culture = "amoritic"
	754.8.22 = {
		birth = yes
	}
	820.9.12 = {
		death = yes
	}
}
813 = {
	name = "Abisare"
	dynasty = 813
	religion = "amorite_faith"
	culture = "amoritic"
	762.2.9 = {
		birth = yes
	}
	817.6.11 = {
		death = yes
	}
}
814 = {
	name = "Sin-Iddinam"
	dynasty = 814
	religion = "amorite_faith"
	culture = "amoritic"
	754.11.5 = {
		birth = yes
	}
	809.2.27 = {
		death = yes
	}
}
816 = {
	name = "Sin-Iqisham"
	dynasty = 816
	religion = "amorite_faith"
	culture = "amoritic"
	752.8.20 = {
		birth = yes
	}
	810.6.3 = {
		death = yes
	}
}
818 = {
	name = "Zabaia"
	dynasty = 818
	religion = "amorite_faith"
	culture = "amoritic"
	755.3.1 = {
		birth = yes
	}
	809.7.4 = {
		death = yes
	}
}
819 = {
	name = "Sin-Eribam"
	dynasty = 819
	religion = "amorite_faith"
	culture = "amoritic"
	751.9.6 = {
		birth = yes
	}
	820.7.5 = {
		death = yes
	}
}
820 = {
	name = "Hammurabi"
	dynasty = 820
	religion = "amorite_faith"
	culture = "amoritic"
	764.5.14 = {
		birth = yes
	}
	811.10.12 = {
		death = yes
	}
}
821 = {
	name = "Sin-Iddinam"
	dynasty = 821
	religion = "amorite_faith"
	culture = "amoritic"
	760.11.10 = {
		birth = yes
	}
	828.2.12 = {
		death = yes
	}
}
822 = {
	name = "Naplanum"
	dynasty = 822
	religion = "amorite_faith"
	culture = "amoritic"
	768.1.18 = {
		birth = yes
	}
	815.8.19 = {
		death = yes
	}
}
823 = {
	name = "Sin-Iqisham"
	dynasty = 823
	religion = "amorite_faith"
	culture = "amoritic"
	769.3.18 = {
		birth = yes
	}
	830.1.26 = {
		death = yes
	}
}
831 = {
	name = "Naplanum"
	dynasty = 831
	religion = "amorite_faith"
	culture = "amoritic"
	752.5.8 = {
		birth = yes
	}
	808.7.5 = {
		death = yes
	}
}
834 = {
	name = "Abisare"
	dynasty = 834
	religion = "amorite_faith"
	culture = "amoritic"
	760.1.10 = {
		birth = yes
	}
	822.1.21 = {
		death = yes
	}
}
835 = {
	name = "Silli-Adad"
	dynasty = 835
	religion = "amorite_faith"
	culture = "amoritic"
	764.1.9 = {
		birth = yes
	}
	830.11.6 = {
		death = yes
	}
}
836 = {
	name = "Abisare"
	dynasty = 836
	religion = "amorite_faith"
	culture = "amoritic"
	767.5.3 = {
		birth = yes
	}
	817.4.4 = {
		death = yes
	}
}
837 = {
	name = "Zabaia"
	dynasty = 837
	religion = "amorite_faith"
	culture = "amoritic"
	750.11.12 = {
		birth = yes
	}
	816.10.26 = {
		death = yes
	}
}
838 = {
	name = "Su-abu"
	dynasty = 838
	religion = "amorite_faith"
	culture = "amoritic"
	763.3.13 = {
		birth = yes
	}
	826.8.24 = {
		death = yes
	}
}
839 = {
	name = "Zabaia"
	dynasty = 839
	religion = "amorite_faith"
	culture = "amoritic"
	767.2.15 = {
		birth = yes
	}
	834.1.11 = {
		death = yes
	}
}
840 = {
	name = "Warad-Sin"
	dynasty = 840
	religion = "amorite_faith"
	culture = "amoritic"
	754.6.10 = {
		birth = yes
	}
	809.9.17 = {
		death = yes
	}
}
841 = {
	name = "Warad-Sin"
	dynasty = 841
	religion = "amorite_faith"
	culture = "amoritic"
	766.10.2 = {
		birth = yes
	}
	819.5.5 = {
		death = yes
	}
}
842 = {
	name = "Samium"
	dynasty = 842
	religion = "amorite_faith"
	culture = "amoritic"
	756.6.2 = {
		birth = yes
	}
	799.8.23 = {
		death = yes
	}
}
843 = {
	name = "Apil-Sin"
	dynasty = 843
	religion = "amorite_faith"
	culture = "amoritic"
	747.9.5 = {
		birth = yes
	}
	799.1.23 = {
		death = yes
	}
}
979 = {
	name = "Gungunum"
	dynasty = 979
	religion = "amorite_faith"
	culture = "amoritic"
	1288.1.6 = {
		birth = yes
	}
	1351.4.15 = {
		death = yes
	}
}
980 = {
	name = "Sin-Iddinam"
	dynasty = 980
	religion = "amorite_faith"
	culture = "amoritic"
	1274.11.25 = {
		birth = yes
	}
	1326.11.27 = {
		death = yes
	}
}
981 = {
	name = "Ammi-saduqa"
	dynasty = 981
	religion = "amorite_faith"
	culture = "amoritic"
	1280.4.8 = {
		birth = yes
	}
	1341.10.9 = {
		death = yes
	}
}
997 = {
	name = "Apil-Sin"
	dynasty = 997
	religion = "amorite_faith"
	culture = "amoritic"
	1280.1.20 = {
		birth = yes
	}
	1325.11.6 = {
		death = yes
	}
}
998 = {
	name = "Samsu-Ditana"
	dynasty = 998
	religion = "amorite_faith"
	culture = "amoritic"
	1283.10.15 = {
		birth = yes
	}
	1343.9.5 = {
		death = yes
	}
}
1046 = {
	name = "Zabaia"
	dynasty = 1046
	religion = "amorite_faith"
	culture = "amoritic"
	1275.7.19 = {
		birth = yes
	}
	1328.7.23 = {
		death = yes
	}
}
1047 = {
	name = "Nur-Adad"
	dynasty = 1047
	religion = "amorite_faith"
	culture = "amoritic"
	1292.7.11 = {
		birth = yes
	}
	1346.11.6 = {
		death = yes
	}
}
1051 = {
	name = "Naplanum"
	dynasty = 1051
	religion = "amorite_faith"
	culture = "amoritic"
	1279.7.9 = {
		birth = yes
	}
	1344.4.8 = {
		death = yes
	}
}
1209 = {
	name = "Sin-muballit"
	dynasty = 1209
	religion = "amorite_faith"
	culture = "amoritic"
	1293.5.11 = {
		birth = yes
	}
	1353.2.7 = {
		death = yes
	}
}
1210 = {
	name = "Sumuel"
	dynasty = 1210
	religion = "amorite_faith"
	culture = "amoritic"
	1289.11.10 = {
		birth = yes
	}
	1347.9.15 = {
		death = yes
	}
}
1213 = {
	name = "Sin-Eribam"
	dynasty = 1213
	religion = "amorite_faith"
	culture = "amoritic"
	1283.4.8 = {
		birth = yes
	}
	1342.9.14 = {
		death = yes
	}
}
1215 = {
	name = "Abisare"
	dynasty = 1215
	religion = "amorite_faith"
	culture = "amoritic"
	1290.9.27 = {
		birth = yes
	}
	1343.3.24 = {
		death = yes
	}
}
1218 = {
	name = "Sin-muballit"
	dynasty = 1218
	religion = "amorite_faith"
	culture = "amoritic"
	1272.9.16 = {
		birth = yes
	}
	1316.2.24 = {
		death = yes
	}
}
1219 = {
	name = "Zabaia"
	dynasty = 1219
	religion = "amorite_faith"
	culture = "amoritic"
	1293.2.6 = {
		birth = yes
	}
	1358.3.1 = {
		death = yes
	}
}
1220 = {
	name = "Ammi-saduqa"
	dynasty = 1220
	religion = "amorite_faith"
	culture = "amoritic"
	1270.10.15 = {
		birth = yes
	}
	1319.11.19 = {
		death = yes
	}
}
1221 = {
	name = "Su-abu"
	dynasty = 1221
	religion = "amorite_faith"
	culture = "amoritic"
	1292.4.24 = {
		birth = yes
	}
	1356.2.9 = {
		death = yes
	}
}
1222 = {
	name = "Sumu-la-El"
	dynasty = 1222
	religion = "amorite_faith"
	culture = "amoritic"
	1281.3.12 = {
		birth = yes
	}
	1326.3.21 = {
		death = yes
	}
}
1223 = {
	name = "Sumu-la-El"
	dynasty = 1223
	religion = "amorite_faith"
	culture = "amoritic"
	1289.2.25 = {
		birth = yes
	}
	1331.8.13 = {
		death = yes
	}
}
1259 = {
	name = "Emisum"
	dynasty = 1259
	religion = "amorite_faith"
	culture = "amoritic"
	1293.8.4 = {
		birth = yes
	}
	1340.7.24 = {
		death = yes
	}
}
1261 = {
	name = "Abisare"
	dynasty = 1261
	religion = "amorite_faith"
	culture = "amoritic"
	1292.5.21 = {
		birth = yes
	}
	1360.6.19 = {
		death = yes
	}
}
1266 = {
	name = "Gungunum"
	dynasty = 1266
	religion = "amorite_faith"
	culture = "amoritic"
	1271.11.17 = {
		birth = yes
	}
	1317.7.22 = {
		death = yes
	}
}
1267 = {
	name = "Sumu-la-El"
	dynasty = 1267
	religion = "amorite_faith"
	culture = "amoritic"
	1288.10.6 = {
		birth = yes
	}
	1330.5.5 = {
		death = yes
	}
}
1268 = {
	name = "Zabaia"
	dynasty = 1268
	religion = "amorite_faith"
	culture = "amoritic"
	1289.9.21 = {
		birth = yes
	}
	1339.2.18 = {
		death = yes
	}
}
1283 = {
	name = "Abi-eshuh"
	dynasty = 1283
	religion = "amorite_faith"
	culture = "amoritic"
	1280.4.3 = {
		birth = yes
	}
	1336.2.27 = {
		death = yes
	}
}
1284 = {
	name = "Su-abu"
	dynasty = 1284
	religion = "amorite_faith"
	culture = "amoritic"
	1283.8.7 = {
		birth = yes
	}
	1328.5.7 = {
		death = yes
	}
}
1534 = {
	name = "Sin-Eribam"
	dynasty = 1534
	religion = "amorite_faith"
	culture = "amoritic"
	1281.6.20 = {
		birth = yes
	}
	1329.9.25 = {
		death = yes
	}
}
1658 = {
	name = "Sin-Eribam"
	dynasty = 1658
	religion = "amorite_faith"
	culture = "amoritic"
	1283.3.21 = {
		birth = yes
	}
	1331.10.3 = {
		death = yes
	}
}
1659 = {
	name = "Ammi-saduqa"
	dynasty = 1659
	religion = "amorite_faith"
	culture = "amoritic"
	1293.4.1 = {
		birth = yes
	}
	1340.9.20 = {
		death = yes
	}
}
1660 = {
	name = "Sumu-abum"
	dynasty = 1660
	religion = "amorite_faith"
	culture = "amoritic"
	1286.2.4 = {
		birth = yes
	}
	1333.4.12 = {
		death = yes
	}
}
1661 = {
	name = "Naplanum"
	dynasty = 1661
	religion = "amorite_faith"
	culture = "amoritic"
	1294.10.7 = {
		birth = yes
	}
	1359.6.8 = {
		death = yes
	}
}
1662 = {
	name = "Sumu-la-El"
	dynasty = 1662
	religion = "amorite_faith"
	culture = "amoritic"
	1275.2.14 = {
		birth = yes
	}
	1343.7.18 = {
		death = yes
	}
}
1663 = {
	name = "Samsu-iluna"
	dynasty = 1663
	religion = "amorite_faith"
	culture = "amoritic"
	1273.11.11 = {
		birth = yes
	}
	1340.2.5 = {
		death = yes
	}
}
1665 = {
	name = "Hammurabi"
	dynasty = 1665
	religion = "amorite_faith"
	culture = "amoritic"
	1287.6.25 = {
		birth = yes
	}
	1349.9.15 = {
		death = yes
	}
}
1667 = {
	name = "Ammi-ditana"
	dynasty = 1667
	religion = "amorite_faith"
	culture = "amoritic"
	1274.8.20 = {
		birth = yes
	}
	1328.1.21 = {
		death = yes
	}
}
1683 = {
	name = "Sumuel"
	dynasty = 1683
	religion = "amorite_faith"
	culture = "amoritic"
	1281.4.22 = {
		birth = yes
	}
	1338.6.6 = {
		death = yes
	}
}
1684 = {
	name = "Naplanum"
	dynasty = 1684
	religion = "amorite_faith"
	culture = "amoritic"
	1294.1.17 = {
		birth = yes
	}
	1344.2.9 = {
		death = yes
	}
}
1685 = {
	name = "Rim-Sin"
	dynasty = 1685
	religion = "amorite_faith"
	culture = "amoritic"
	1292.4.10 = {
		birth = yes
	}
	1354.3.23 = {
		death = yes
	}
}
1686 = {
	name = "Su-abu"
	dynasty = 1686
	religion = "amorite_faith"
	culture = "amoritic"
	1281.1.6 = {
		birth = yes
	}
	1334.7.25 = {
		death = yes
	}
}
1689 = {
	name = "Samsu-iluna"
	dynasty = 1689
	religion = "amorite_faith"
	culture = "amoritic"
	1270.11.20 = {
		birth = yes
	}
	1337.4.10 = {
		death = yes
	}
}
1690 = {
	name = "Zabaia"
	dynasty = 1690
	religion = "amorite_faith"
	culture = "amoritic"
	1272.5.15 = {
		birth = yes
	}
	1314.5.21 = {
		death = yes
	}
}
1691 = {
	name = "Warad-Sin"
	dynasty = 1691
	religion = "amorite_faith"
	culture = "amoritic"
	1293.2.14 = {
		birth = yes
	}
	1342.7.5 = {
		death = yes
	}
}
1692 = {
	name = "Hammurabi"
	dynasty = 1692
	religion = "amorite_faith"
	culture = "amoritic"
	1291.1.20 = {
		birth = yes
	}
	1357.2.8 = {
		death = yes
	}
}
1693 = {
	name = "Sin-Eribam"
	dynasty = 1693
	religion = "amorite_faith"
	culture = "amoritic"
	1272.3.13 = {
		birth = yes
	}
	1335.2.5 = {
		death = yes
	}
}
1694 = {
	name = "Nur-Adad"
	dynasty = 1694
	religion = "amorite_faith"
	culture = "amoritic"
	1270.10.5 = {
		birth = yes
	}
	1330.8.7 = {
		death = yes
	}
}
1695 = {
	name = "Sin-Iddinam"
	dynasty = 1695
	religion = "amorite_faith"
	culture = "amoritic"
	1272.7.19 = {
		birth = yes
	}
	1337.4.7 = {
		death = yes
	}
}
1696 = {
	name = "Nur-Adad"
	dynasty = 1696
	religion = "amorite_faith"
	culture = "amoritic"
	1279.5.23 = {
		birth = yes
	}
	1329.9.12 = {
		death = yes
	}
}