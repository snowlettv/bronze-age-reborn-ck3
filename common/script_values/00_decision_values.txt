﻿
# Accuse the Krstjani of Heresy
#BA REMOVED

# Negotiate the Danelaw
#BA REMOVED							   
standard_commission_artifact_cooldown_time = {
	value = 1825
	#if = {
	#	limit = {
	#		root.culture = {
	#			has_cultural_parameter = more_frequent_hunts
	#		}
	#	}
	#	multiply = 0.5
	#}
}

found_witch_coven_member_count_value = {
	value = 4
}

found_witch_coven_member_percent_value = {
	value = 0.6
}

found_witch_coven_member_percent_display_value = {
	value = found_witch_coven_member_percent_value
	multiply = 100
}

invite_knights_decision_standard_value = {
	value = 12
	if = {
		limit = {
			root = {
				dynasty ?= { has_dynasty_perk = ep2_activities_legacy_5 }
				involved_activity ?= { has_activity_type = activity_tournament }
			}
		}
		add = 6
	}
}

invite_knights_decision_upper_value = {
	value = invite_knights_decision_standard_value
	add = 3
}
	
fp3_antagonize_character_interaction_cooldown = {
	value = 2
	if = {
		limit = { NOT = { any_character_struggle = { is_struggle_type = persian_struggle } } }
		add = 3
	}
}

invite_poets_decision_standard_value = {
	value = 14
}

invite_poets_decision_upper_value = {
	value = invite_poets_decision_standard_value
	add = 4
}