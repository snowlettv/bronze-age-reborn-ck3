﻿#k_zalwar
##d_zalwar ###################################
###c_zalwar
3589 = { #Zalwar
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
3586 = { #Al-Ra'i
	holding = none
}
3590 = { #Shamarin
	holding = city_holding
}
3588 = { #Zayzafin
	holding = none
}
3592 = { #Rojava
	holding = church_holding
}
###c_ziadiyah
3594 = { #Ziadiyah
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
3593 = { #Mregel
	holding = city_holding
}
3585 = { #Qabasin
	holding = church_holding
}
3583 = { #Arima
	holding = none
}
3584 = { #Bza'a
	holding = city_holding
}
###c_tsurgi
3486 = { #Tsurgi
	culture = zalwari
	religion = western_hurrian_faith
	holding = tribal_holding
}
3485 = { #Asxe
	holding = none
}

##d_samalla
###c_samalla
4342 = { #Sam'alla
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
3591 = { #Kafra
	holding = city_holding
}
4343 = { #Irde-nui
	holding = church_holding
}
4341 = { #Ase-siyi
	holding = city_holding
}
###c_herbel
3610 = { #Herbel
	culture = zalwari
	religion = western_hurrian_faith
	holding = tribal_holding
}
3609 = { #Malid
	holding = none
}
4425 = { #Kus-usi
	holding = none
}
###c_pabniri
4423 = { #Pab-ni-ri
	culture = zalwari
	religion = western_hurrian_faith
	holding = tribal_holding
}
4424 = { #Undu-xili
	holding = none
}
3611 = { #Ahras
	holding = none
}
##d_marqasa
###c_marqasa
4332 = { #Marqasa
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = silver_mine_01
		special_building = silver_mine_01
	}
}
4329 = { #Sa-fe-ni
	holding = none
}
4330 = { #Revivim
	holding = city_holding
}
4331 = { #Segev
	holding = none
}
4333 = { #Te-u-ni
	holding = church_holding
}
###c_suamani
4334 = { #Sua-mani
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
4335 = { #Inu-ab-i
	holding = city_holding
}
4336 = { #An-ni
	holding = church_holding
}
4337 = { #Niga-taye
	holding = city_holding
}
###c_eraditae
4339 = { #Eradi-tae
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
4344 = { #Shu-kudu
	holding = city_holding
}
4338 = { #Keri-mi
	holding = church_holding
}
4340 = { #Apsi-ra
	holding = city_holding
}
##d_bulbulhum
###c_bulbulhum
4345 = { #Bulbulhum
	culture = zalwari
	religion = western_hurrian_faith
	holding = castle_holding
}
4348 = { #Isena-siye
	holding = city_holding
}
4347 = { #Pal-migi
	holding = church_holding
}
4346 = { #Ul-xas
	holding = city_holding
}
###c_shutime
4426 = { #Shu-time
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
4427 = { #Siwe-xili
	holding = none
}
4355 = { #Tae-nariya
	holding = none
}
4354 = { #Ur-puxxi
	holding = none
}
###c_punyiti
4351 = { #Punyi-ti
	culture = harrani
	religion = western_hurrian_faith
	holding = tribal_holding
}
4352 = { #Tisa-ur-mi
	holding = none
}
4349 = { #Tari-xari
	holding = none
}
4350 = { #Pab-suye
	holding = none
}