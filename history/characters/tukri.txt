﻿#Defined
simurrum1 = {
	name = "Uzzu"
	dynasty = dynn_simurrum1
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	757.9.13 = {
		birth = yes
	}
	818.2.23 = {
		death = yes
	}
}
#Generated
357 = {
	name = "Nikir-sarri"
	dynasty = 357
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	764.7.19 = {
		birth = yes
	}
	833.9.5 = {
		death = yes
	}
}
363 = {
	name = "Anis-hurpe"
	dynasty = 363
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	752.10.22 = {
		birth = yes
	}
	807.11.20 = {
		death = yes
	}
}
364 = {
	name = "Paip-sarri"
	dynasty = 364
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	748.5.4 = {
		birth = yes
	}
	810.10.12 = {
		death = yes
	}
}
372 = {
	name = "Ela-paraluh"
	dynasty = 372
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	762.7.25 = {
		birth = yes
	}
	804.10.20 = {
		death = yes
	}
}
373 = {
	name = "Paku-zi"
	dynasty = 373
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	762.11.3 = {
		birth = yes
	}
	829.1.26 = {
		death = yes
	}
}
376 = {
	name = "Kabi-ata"
	dynasty = 376
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	761.7.10 = {
		birth = yes
	}
	820.4.5 = {
		death = yes
	}
}
379 = {
	name = "Zigil-tanu-m"
	dynasty = 379
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	747.11.6 = {
		birth = yes
	}
	792.9.16 = {
		death = yes
	}
}
380 = {
	name = "Puram-zi"
	dynasty = 380
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	766.11.5 = {
		birth = yes
	}
	812.9.11 = {
		death = yes
	}
}
381 = {
	name = "Uzu-na-n"
	dynasty = 381
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	748.10.22 = {
		birth = yes
	}
	793.5.7 = {
		death = yes
	}
}
382 = {
	name = "Tami"
	dynasty = 382
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	754.1.18 = {
		birth = yes
	}
	822.3.16 = {
		death = yes
	}
}
383 = {
	name = "Kutti"
	dynasty = 383
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	758.10.1 = {
		birth = yes
	}
	803.11.20 = {
		death = yes
	}
}
386 = {
	name = "Ustap-adal"
	dynasty = 386
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	769.6.20 = {
		birth = yes
	}
	810.4.11 = {
		death = yes
	}
}
389 = {
	name = "Kirta"
	dynasty = 389
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	759.8.3 = {
		birth = yes
	}
	813.3.16 = {
		death = yes
	}
}
400 = {
	name = "Sazu-e"
	dynasty = 400
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	769.6.13 = {
		birth = yes
	}
	815.1.13 = {
		death = yes
	}
}
782 = {
	name = "Kanzu"
	dynasty = 782
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	755.6.18 = {
		birth = yes
	}
	806.10.17 = {
		death = yes
	}
}
784 = {
	name = "Serum-naya"
	dynasty = 784
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	768.2.12 = {
		birth = yes
	}
	812.2.8 = {
		death = yes
	}
}
785 = {
	name = "Ustan-sarri"
	dynasty = 785
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	755.3.25 = {
		birth = yes
	}
	814.6.25 = {
		death = yes
	}
}
786 = {
	name = "Azzu-ka"
	dynasty = 786
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	763.1.15 = {
		birth = yes
	}
	817.2.25 = {
		death = yes
	}
}
787 = {
	name = "Eni-ya"
	dynasty = 787
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	745.3.4 = {
		birth = yes
	}
	808.8.10 = {
		death = yes
	}
}
788 = {
	name = "Samba-ri"
	dynasty = 788
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	765.2.13 = {
		birth = yes
	}
	811.3.15 = {
		death = yes
	}
}
789 = {
	name = "Unap-se"
	dynasty = 789
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	755.2.22 = {
		birth = yes
	}
	821.5.26 = {
		death = yes
	}
}
792 = {
	name = "Azzu-e"
	dynasty = 792
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	746.1.16 = {
		birth = yes
	}
	801.5.20 = {
		death = yes
	}
}
797 = {
	name = "Elan-za"
	dynasty = 797
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	754.1.14 = {
		birth = yes
	}
	813.2.6 = {
		death = yes
	}
}
800 = {
	name = "Aru-pal"
	dynasty = 800
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	769.6.16 = {
		birth = yes
	}
	830.8.21 = {
		death = yes
	}
}
1230 = {
	name = "Zika-n"
	dynasty = 1230
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1270.9.24 = {
		birth = yes
	}
	1333.2.26 = {
		death = yes
	}
}
1232 = {
	name = "Tai-ra"
	dynasty = 1232
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1288.9.24 = {
		birth = yes
	}
	1341.5.12 = {
		death = yes
	}
}
1233 = {
	name = "Ammin-na"
	dynasty = 1233
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1289.6.1 = {
		birth = yes
	}
	1351.8.19 = {
		death = yes
	}
}
1235 = {
	name = "Ehli"
	dynasty = 1235
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1290.6.17 = {
		birth = yes
	}
	1352.1.15 = {
		death = yes
	}
}
1236 = {
	name = "Illu-te"
	dynasty = 1236
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1284.9.5 = {
		birth = yes
	}
	1346.3.1 = {
		death = yes
	}
}
1237 = {
	name = "Kari-ta-n"
	dynasty = 1237
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1291.1.7 = {
		birth = yes
	}
	1359.8.8 = {
		death = yes
	}
}
1238 = {
	name = "Asa-kka"
	dynasty = 1238
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1274.8.15 = {
		birth = yes
	}
	1336.3.3 = {
		death = yes
	}
}
1239 = {
	name = "Kabi-pursa"
	dynasty = 1239
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1276.3.3 = {
		birth = yes
	}
	1322.8.26 = {
		death = yes
	}
}
1240 = {
	name = "Nupa-ta"
	dynasty = 1240
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1290.10.27 = {
		birth = yes
	}
	1354.7.1 = {
		death = yes
	}
}
1244 = {
	name = "Hazip-ulme"
	dynasty = 1244
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1287.1.8 = {
		birth = yes
	}
	1343.6.21 = {
		death = yes
	}
}
1248 = {
	name = "Shuttarna"
	dynasty = 1248
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1290.3.10 = {
		birth = yes
	}
	1343.9.13 = {
		death = yes
	}
}
1253 = {
	name = "Kannu-kka"
	dynasty = 1253
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1272.10.16 = {
		birth = yes
	}
	1314.8.8 = {
		death = yes
	}
}
1281 = {
	name = "Tis_ata"
	dynasty = 1281
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1271.11.17 = {
		birth = yes
	}
	1338.8.19 = {
		death = yes
	}
}
1282 = {
	name = "Azu-na-n"
	dynasty = 1282
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1284.1.20 = {
		birth = yes
	}
	1331.11.4 = {
		death = yes
	}
}
1632 = {
	name = "Kuppi-ya"
	dynasty = 1632
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1272.4.24 = {
		birth = yes
	}
	1320.8.2 = {
		death = yes
	}
}
1634 = {
	name = "Azzu-eli"
	dynasty = 1634
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1286.5.5 = {
		birth = yes
	}
	1330.1.17 = {
		death = yes
	}
}
1635 = {
	name = "Ari-ab"
	dynasty = 1635
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1289.2.20 = {
		birth = yes
	}
	1353.4.18 = {
		death = yes
	}
}
1636 = {
	name = "Sattu-ri"
	dynasty = 1636
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1282.1.23 = {
		birth = yes
	}
	1350.11.25 = {
		death = yes
	}
}
1637 = {
	name = "Arip-hurmis"
	dynasty = 1637
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1278.7.20 = {
		birth = yes
	}
	1335.9.3 = {
		death = yes
	}
}
1638 = {
	name = "Kutti"
	dynasty = 1638
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1291.2.1 = {
		birth = yes
	}
	1359.11.14 = {
		death = yes
	}
}
1640 = {
	name = "Kuza-ri-na"
	dynasty = 1640
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1289.2.9 = {
		birth = yes
	}
	1353.11.15 = {
		death = yes
	}
}
1642 = {
	name = "Anis-kipa-l"
	dynasty = 1642
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1276.10.3 = {
		birth = yes
	}
	1323.9.3 = {
		death = yes
	}
}
1644 = {
	name = "Taku-na"
	dynasty = 1644
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1284.10.5 = {
		birth = yes
	}
	1331.7.16 = {
		death = yes
	}
}
1647 = {
	name = "Zitu-ya"
	dynasty = 1647
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1272.7.11 = {
		birth = yes
	}
	1325.8.4 = {
		death = yes
	}
}
1649 = {
	name = "Awis-na"
	dynasty = 1649
	religion = "eastern_hurrian_faith"
	culture = "tukri"
	1279.8.25 = {
		birth = yes
	}
	1331.3.3 = {
		death = yes
	}
}