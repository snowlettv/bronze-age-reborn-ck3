﻿#k_wilusa
##d_wilion ###################################
###c_wilion
1477 = { #Wilion
	culture = wilusan
	religion = anatolian_faith
	holding = castle_holding
	#holy_site = omen_tiwad

	785.1.1 = {
		special_building_slot = temple_of_tiwad_01 #Temple of Tiwad
		duchy_capital_building = trojan_walls_01
	}
	1310.1.1 = {
		special_building = temple_of_tiwad_01
	}
}
1236 = { #Xios
	holding = none
}
1476 = { #Kobek
	holding = church_holding
}
1383 = { #Sigeion
	holding = city_holding
}
1257 = { #Wudruhu
	holding = city_holding
}
###c_tenedos
2488 = { #Tenedos
	culture = tyrsenian
	religion = aegean_faith
	holding = tribal_holding
}
###c_uxun
1170 = { #Uxun
	culture = wilusan
	religion = anatolian_faith
	holding = castle_holding
}
1286 = { #Zalra
	holding = city_holding
}
1482 = { #Ator
	holding = none
}
1278 = { #Skamandros
	holding = none
}
1303 = { #Wureha
	holding = none
}
###c_rakid
1335 = { #Rakid
	culture = wilusan
	religion = anatolian_faith
	holding = castle_holding
}
1329 = { #Ruwa
	holding = city_holding
}
1289 = { #Ahak
	holding = none
}
1276 = { #Assos
	holding = church_holding
}
###c_skamandros
1280 = { #Darzluza
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1291 = { #Malba
	holding = none
}
1247 = { #Azune
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
1575 = { #Opa
	holding = none
}
##d_dardanos
###c_dardanos
1437 = { #Dardanos
	culture = wilusan
	religion = anatolian_faith
	holding = castle_holding
}
1244 = { #Pratraha
	holding = city_holding
}
1657 = { #Prinun
	holding = church_holding
}
1312 = { #Kuqqu
	holding = none
}
###c_dazluza
1261 = { #Dazluza
	culture = wilusan
	religion = anatolian_faith
	holding = castle_holding
}
1284 = { #Arwu
	holding = city_holding
}
1264 = { #Perkote
	holding = church_holding
}
1268 = { #Urusi
	holding = none
}
###c_lampsakos
2449 = { #Lampsakos
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1645 = { #Ameae
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
2447 = { #Zawi
	holding = none
}
2443 = { #Asharsa
	holding = none
}
###c_awiti
2433 = { #Awiti
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
2441 = { #Atamanza
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
2436 = { #Halli
	holding = none
}
##d_granikos
###c_granikos
1395 = { #Sapri
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
1455 = { #Urle
	holding = none
}
1432 = { #Wezumti
	holding = none
}
###c_egta
1483 = { #Egta
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
1422 = { #Donab
	holding = none
}
1177 = { #Inuwa
	holding = none
}
1396 = { #Kajax
	holding = none
}
###c_arrayis
2413 = { #Arrayis
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
2415 = { #Zuwanis
	holding = none
}
2411 = { #Urazzas
	holding = none
}
##d_tawis
###c_tawis
2424 = { #Tawis
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
2489 = { #Piyai
	holding = none
}
2422 = { #Tappanis
	holding = none
}
###c_manati
2430 = { #Manati
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
2431 = { #Maliti
	holding = none
}
2427 = { #Utti
	holding = none
}
2491 = { #Armas
	holding = none
}
###c_zurni
2420 = { #Zurni
	culture = wilusan
	religion = anatolian_faith
	holding = tribal_holding
}
2432 = { #Ziyari
	holding = none
}
2418 = { #Alattarsa
	holding = none
}
2421 = { #Hassa
	holding = none
}