﻿@correct_culture_primary_score = 100
@better_than_the_alternatives_score = 50
@always_primary_score = 1000

e_doria = {
	color = { 255 23 12 }
	
	capital = c_khanoia
	
	ai_primary_priority = {
		if = {
			limit = {
				OR = {
					culture = culture:proto_greek
					culture = culture:dorian
				}
			}
			add = @correct_culture_primary_score
		}
		else_if = {
			limit = {
				OR = {
					culture = { has_cultural_pillar = heritage_hellenic }
					culture = { has_cultural_pillar = heritage_thracian }
				}
			}
			add = @better_than_the_alternatives_score
		}
	}
	
	k_epeiros = {
		color = { 254 142 94 }
		
		capital = c_torone
		
		ai_primary_priority = {
			if = {
				limit = {
					OR = {
						culture = culture:proto_greek
						culture = culture:dorian
					}
				}
				add = @better_than_the_alternatives_score
			}
		}
		
		d_molossis = {
			color = { 254 126 107 }
			
			capital = c_molossis
			
			c_molossis = {
				color = { 254 153 136 }
				
				b_1733 = { #Petsali
					province = 1733
					color = { 79 79 79 }
				} 
				b_1742 = { #Aristi
					province = 1742
					color = { 79 79 79 }
				}
				b_1734 = { #Zitsa
					province = 1734
					color = { 79 79 79 }
				}
				b_1740 = { #Vissani
					province = 1740
					color = { 79 79 79 }
				}
			}
			c_stratinista = {
				color = { 253 134 123 }
				
				b_1738 = { #Stratinista
					province = 1738
					color = { 79 79 79 }
				}
				b_1764 = { #Kakavia
					province = 1764
					color = { 79 79 79 }
				}
				b_1739 = { #Delvinaki
					province = 1739
					color = { 79 79 79 }
				}
			}
			c_raiko = {
				color = { 254 128 85 }
				
				b_1736 = { #Raiko
					province = 1736
					color = { 79 79 79 }
				}
				b_1735 = { #Raveni
					province = 1735
					color = { 79 79 79 }
				}
				b_1737 = { #Lista
					province = 1737
					color = { 79 79 79 }
				}
			}
		}
		d_ephyre = {
			color = { 254 112 22 }
			
			capital = c_ephyre
			
			c_ephyre = {
				color = { 253 124 1 }
				
				b_1752 = { #Ephyre
					province = 1752
					color = { 79 79 79 }
				}
				b_1753 = { #Euroia
					province = 1753
					color = { 79 79 79 }
				}
				b_1755 = { #Elaia
					province = 1755
					color = { 79 79 79 }
				}
			}
			c_nikopolis = {
				color = { 254 124 1 }
				
				b_1749 = { #Nikopolis
					province = 1749
					color = { 79 79 79 }
				}
				b_1750 = { #Kassope
					province = 1750
					color = { 79 79 79 }
				}
				b_1718 = { #Vrysoula
					province = 1718
					color = { 79 79 79 }
				}
			}
			c_elina = {
				color = { 254 111 1 }
				
				b_468 = { #Elina
					province = 468
					color = { 79 79 79 }
				}
				b_465 = { #Sybota
					province = 465
					color = { 79 79 79 }
				}
				b_1754 = { #Toryne
					province = 1754
					color = { 79 79 79 }
				}
			}
		}
		d_dodona = {
			color = { 254 152 133 }
			
			capital = c_dodona
			
			c_dodona = {
				color = { 254 170 134 }
				
				b_1721 = { #Dodona
					province = 1721
					color = { 79 79 79 }
				}
				b_1725 = { #Peratis
					province = 1725
					color = { 79 79 79 }
				}
				b_1724 = { #Sinka
					province = 1724
					color = { 79 79 79 }
				}
				b_1719 = { #Assos
					province = 1719
					color = { 79 79 79 }
				}
			}
			c_passaron = {
				color = { 254 177 160 }
				
				b_1732 = { #Passaron
					province = 1732
					color = { 79 79 79 }
				}
				b_457 = { #Ioannina
					province = 457
					color = { 79 79 79 }
				}
				b_1726 = { #Pedini
					province = 1726
					color = { 79 79 79 }
				}
				b_1727 = { #Avgo
					province = 1727
					color = { 79 79 79 }
				}
				b_1987 = { #Eurymenai
					province = 1987
					color = { 79 79 79 }
				}
			}
		}
		d_kerkyra = {
			color = { 254 140 56 }
			
			capital = c_kerkyra
			
			c_kerkyra = {
				color = { 254 157 95 }
				
				b_1891 = { #Kerkyra
					province = 1891
					color = { 79 79 79 }
				}
				b_1896 = { #Lefkimi
					province = 1896
					color = { 79 79 79 }
				}
				b_1892 = { #Grava
					province = 1892
					color = { 79 79 79 }
				}
			}
			c_kassiope = {
				color = { 254 151 64 }
				
				b_1885 = { #Kassiope
					province = 1885
					color = { 79 79 79 }
				}
				b_1887 = { #Afionas
					province = 1887
					color = { 79 79 79 }
				}
			}
		}
		d_epeiros = {
			color = { 254 189 152 }
			
			capital = c_torone
			
			c_torone = {
				color = { 254 214 167 }
				
				b_1759 = { #Torone
					province = 1759
					color = { 79 79 79 }
				}
				b_1758 = { #Pigadoulia
					province = 1758
					color = { 79 79 79 }
				}
				b_1757 = { #Neraida
					province = 1757
					color = { 79 79 79 }
				} 
			}
			c_bouthroton = {
				color = { 254 187 128 }
				
				b_1768 = { #Bouthroton
					province = 1768
					color = { 79 79 79 }
				}
				b_1766 = { #Vagalat
					province = 1766
					color = { 79 79 79 }
				} 
				b_1761 = { #Sagiada
					province = 1761
					color = { 79 79 79 }
				}
				b_1763 = { #Kerasochori
					province = 1763
					color = { 79 79 79 }
				}
			}
		}
		d_athamania = {
			color = { 230 135 124 }
			
			capital = c_theodoria
			
			c_theodoria = {
				color = { 254 150 148 }
				
				b_521 = { #Retsiana
					province = 521
					color = { 79 79 79 }
				}
				b_523 = { #Mesounta
					province = 523
					color = { 79 79 79 }
				}
				b_522 = { #Koryfi
					province = 522
					color = { 79 79 79 }
				}
				b_519 = { #Megkla
					province = 519
					color = { 79 79 79 }
				}
			}
			c_gomphoi = {
				color = { 254 129 114 }
				
				b_524 = { #Pindos
					province = 524
					color = { 79 79 79 }
				}
				b_525 = { #Elati
					province = 525
					color = { 79 79 79 }
				}
				b_526 = { #Spathes
					province = 526
					color = { 79 79 79 }
				}
			}
		}
		d_ambrakia = {
			color = { 254 142 59 }
			
			capital = c_ambrakia
			
			c_ambrakia = {
				color = { 220 153 88 }
				
				b_464 = { #Ambrakos
					province = 464
					color = { 79 79 79 }
				}
				b_163 = { #Sykies
					province = 163
					color = { 79 79 79 }
				}
				b_414 = { #Ambrakia
					province = 414
					color = { 79 79 79 }
				}
				b_466 = { #Rachi
					province = 466
					color = { 79 79 79 }
				}
				b_1728 = { #Anogeio
					province = 1728
					color = { 79 79 79 }
				}
			}
			c_idomene = {
				color = { 254 147 44 }
				
				b_515 = { #Idomene
					province = 515
					color = { 79 79 79 }
				}
				b_500 = { #Amphilokhia
					province = 500
					color = { 79 79 79 }
				}
				b_2522 = { #Praso
					province = 2522
					color = { 79 79 79 }
				}
				b_2515 = { #Limnaia
					province = 2515
					color = { 79 79 79 }
				}
			}
			c_peta = {
				color = { 254 172 86 }
				
				b_518 = { #Peta
					province = 518
					color = { 79 79 79 }
				}
				b_517 = { #Floriada
					province = 517
					color = { 79 79 79 }
				}
				b_520 = { #Irakleia
					province = 520
					color = { 79 79 79 }
				}
				b_516 = { #Thiamos
					province = 516
					color = { 79 79 79 }
				}
			}
		}
	}
	k_doria = {
		color = { 254 1 59 }
		
		capital = c_khanoia
		
		ai_primary_priority = {
			if = {
				limit = {
					OR = {
						culture = culture:proto_greek
						culture = culture:dorian
					}
				}
				add = @better_than_the_alternatives_score
			}
		}

		d_amyron = {
			color = { 197 84 78 }
			
			capital = c_saleia
			
			c_saleia = {
				color = { 199 89 74 }
				
				b_2636 = { #Saleia
					province = 2636
					color = { 79 79 79 }
				}
				b_1998 = { #Arnova
					province = 1998
					color = { 79 79 79 }
				}
				b_2638 = { #Olbaso
					province = 2638
					color = { 79 79 79 }
				}
				b_2635 = { #Tabai
					province = 2635
					color = { 79 79 79 }
				}
			}
			c_arsada = {
				color = { 190 62 53 }
				
				b_2644 = { #Arsada
					province = 2644
					color = { 79 79 79 }
				}
				b_2643 = { #Pinara
					province = 2643
					color = { 79 79 79 }
				}
				b_2646 = { #Boubon
					province = 2646
					color = { 79 79 79 }
				}
			}
			c_arneai = {
				color = { 168 71 73 }
				
				b_2640 = { #Arneai
					province = 2640
					color = { 79 79 79 }
				}
				b_2648 = { #Kandos
					province = 2648
					color = { 79 79 79 }
				}
				b_2647 = { #Gagai
					province = 2647
					color = { 79 79 79 }
				}
				b_2641 = { #Limyra
					province = 2641
					color = { 79 79 79 }
				}
			}
		}
		d_aoos = {
			color = { 254 62 39 }
			
			capital = c_aoos
			
			c_aoos = {
				color = { 254 1 16 }
				
				b_1985 = { #Khrastro
					province = 1985
					color = { 79 79 79 }
				}
				b_1793 = { #Kosine
					province = 1793
					color = { 79 79 79 }
				}
				b_1792 = { #Sokero
					province = 1792
					color = { 79 79 79 }
				}
				b_1794 = { #Piska
					province = 1794
					color = { 79 79 79 }
				}
				b_1992 = { #Khreporio
					province = 1992
					color = { 79 79 79 }
				}
			}
			c_iliorrachi = {
				color = { 223 107 72 }
				
				b_1743 = { #Iliorrachi
					province = 1743
					color = { 79 79 79 }
				}
				b_1746 = { #Pixaria
					province = 1746
					color = { 79 79 79 }
				}
				b_1991 = { #Meniko
					province = 1991
					color = { 79 79 79 }
				}
				b_1571 = { #Gorkala
					province = 1571
					color = { 79 79 79 }
				}
			}
		}
		d_parauaia = {
			color = { 254 43 62 }
			
			capital = c_parauaia
			
			c_parauaia = {
				color = { 254 1 41 }
				
				b_1994 = { #Erseki
					province = 1994
					color = { 79 79 79 }
				}
				b_1990 = { #Krekhovi
					province = 1990
					color = { 79 79 79 }
				}
				b_1989 = { #Voskori
					province = 1989
					color = { 79 79 79 }
				}
				b_1986 = { #Dardha
					province = 1986
					color = { 79 79 79 }
				}
			}
			c_phellos = {
				color = { 254 65 57 }
				
				b_2649 = { #Phellos
					province = 2649
					color = { 79 79 79 }
				}
				b_1997 = { #Kerano
					province = 1997
					color = { 79 79 79 }
				}
				b_2652 = { #Xystis
					province = 2652
					color = { 79 79 79 }
				}
				b_1996 = { #Phrasero
					province = 1996
					color = { 79 79 79 }
				}
			}
		}
		d_argyas = {
			color = { 224 1 27 }
			
			capital = c_aperlai
			
			c_aperlai = {
				color = { 225 1 1 }
				
				b_2650 = { #Aperlai
					province = 2650
					color = { 79 79 79 }
				}
				b_1786 = { #Poremo
					province = 1786
					color = { 79 79 79 }
				}
				b_2651 = { #Tabai
					province = 2651
					color = { 79 79 79 }
				}
			}
			c_orikon = {
				color = { 226 1 39 }
				
				b_1784 = { #Orikon
					province = 1784
					color = { 79 79 79 }
				}
				b_1783 = { #Aulon
					province = 1783
					color = { 79 79 79 }
				}
				b_1782 = { #Mekatso
					province = 1782
					color = { 79 79 79 }
				}
			}
			c_amantia = {
				color = { 196 6 33 }
				
				b_1780 = { #Amantia
					province = 1780
					color = { 79 79 79 }
				}
				b_1781 = { #Kote
					province = 1781
					color = { 79 79 79 }
				}
				b_1785 = { #Selenike
					province = 1785
					color = { 79 79 79 }
				}
			}
			c_khimaira = {
				color = { 254 1 5 }
				
				b_1776 = { #Khimaira
					province = 1776
					color = { 79 79 79 }
				}
				b_1778 = { #Mesaliko
					province = 1778
					color = { 79 79 79 }
				}
				b_1779 = { #Vermiko
					province = 1779
					color = { 79 79 79 }
				}
			}
		}
		d_khanoia = {
			color = { 247 75 38 }
			
			capital = c_khanoia
			
			c_khanoia = {
				color = { 227 65 22 }
				 
				b_1772 = { #Phoinike
					province = 1772
					color = { 79 79 79 }
				}
				b_1771 = { #Finiko
					province = 1771
					color = { 79 79 79 }
				}
				b_1775 = { #Borso
					province = 1775
					color = { 79 79 79 }
				}
				b_1774 = { #Zelato
					province = 1774
					color = { 79 79 79 }
				}
			}
			c_pikro = {
				color = { 254 1 25 }
				 
				b_1789 = { #Pikro
					province = 1789
					color = { 79 79 79 }
				}
				b_1791 = { #Lusato
					province = 1791
					color = { 79 79 79 }
				}
				b_1787 = { #Bregasi
					province = 1787
					color = { 79 79 79 }
				}
			}
		}
	}
	k_orestis = {
		color = { 191 96 105 }
		
		capital = c_orestis
		
		ai_primary_priority = {
			if = {
				limit = {
					OR = {
						culture = culture:proto_greek
						culture = culture:dorian
					}
				}
				add = @better_than_the_alternatives_score
			}
		}

		d_boion = {
			color = { 183 93 128 }
			
			capital = c_boion
			
			c_boion = {
				color = { 207 81 148 }
				
				b_1748 = { #Osna
					province = 1748
					color = { 79 79 79 }
				}
				b_1920 = { #Kotili
					province = 1920
					color = { 79 79 79 }
				}
				b_1747 = { #Molista
					province = 1747
					color = { 79 79 79 }
				}
			}
			c_zouzouli = {
				color = { 190 84 137 }
				
				b_1919 = { #Zouzouli
					province = 1919
					color = { 79 79 79 }
				}
				b_1918 = { #Dotsiko
					province = 1918
					color = { 79 79 79 }
				}
				b_1923 = { #Pentalophos
					province = 1923
					color = { 79 79 79 }
				}
			}
			c_kremasto = {
				color = { 180 112 129 }
				
				b_1969 = { #Kremasto
					province = 1969
					color = { 79 79 79 }
				}
				b_1922 = { #Nestorio
					province = 1922
					color = { 79 79 79 }
				}
				b_1924 = { #Zoni
					province = 1924
					color = { 79 79 79 }
				}
			}
		}
		d_orestis = {
			color = { 160 65 86 }
			
			capital = c_orestis
			
			c_orestis = {
				color = { 182 3 83 }
				
				b_1964 = { #Germas
					province = 1964
					color = { 79 79 79 }
				}
				b_1962 = { #Sisani
					province = 1962
					color = { 79 79 79 }
				}
				b_1967 = { #Militsa
					province = 1967
					color = { 79 79 79 }
				}
				b_1961 = { #Vasileiada
					province = 1961
					color = { 79 79 79 }
				}
			}
			c_keletron = {
				color = { 153 67 109 }
				
				b_1970 = { #Dispilio
					province = 1970
					color = { 79 79 79 }
				}
				b_1973 = { #Koromilia
					province = 1973
					color = { 79 79 79 }
				}
				b_1972 = { #Gavros
					province = 1972
					color = { 79 79 79 }
				}
				b_1890 = { #Antartiko
					province = 1890
					color = { 79 79 79 }
				}
			}
			c_molakha = {
				color = { 178 67 97 }
				
				b_1963 = { #Molakha
					province = 1963
					color = { 79 79 79 }
				}
				b_1966 = { #Simantro
					province = 1966
					color = { 79 79 79 }
				}
				b_1925 = { #Krimini
					province = 1925
					color = { 79 79 79 }
				}
				b_1926 = { #Tsotili
					province = 1926
					color = { 79 79 79 }
				}
			}
		}
		d_tymphaia = {
			color = { 194 107 145 }
			
			capital = c_eratyra
			
			c_eratyra = {
				color = { 194 97 156 }
				
				b_1927 = { #Eratyra
					province = 1927
					color = { 79 79 79 }
				}
				b_1915 = { #Kalokhi
					province = 1915
					color = { 79 79 79 }
				}
				b_1928 = { #Exarkhos
					province = 1928
					color = { 79 79 79 }
				}
			}
			c_lakmon = {
				color = { 196 118 176 }
				
				b_1731 = { #Prionia
					province = 1731
					color = { 79 79 79 }
				}
				b_1729 = { #Demati
					province = 1729
					color = { 79 79 79 }
				}
				b_1910 = { #Malakasi
					province = 1910
					color = { 79 79 79 }
				}
			}
			c_oxyneia = {
				color = { 203 107 172 }
				
				b_1906 = { #Oxyneia
					province = 1906
					color = { 79 79 79 }
				}
				b_1911 = { #Prionia
					province = 1911
					color = { 79 79 79 }
				}
				b_1909 = { #Kalliphea
					province = 1909
					color = { 79 79 79 }
				}
				b_1907 = { #Trikohkia
					province = 1907
					color = { 79 79 79 }
				}
			}
			c_pontini = {
				color = { 202 90 164 }
				
				b_1929 = { #Pontini
					province = 1929
					color = { 79 79 79 }
				}
				b_1914 = { #Kentro
					province = 1914
					color = { 79 79 79 }
				}
				b_1908 = { #Panagia
					province = 1908
					color = { 79 79 79 }
				}
			}
			c_elatos = {
				color = { 196 99 155 }
				
				b_1916 = { #Elatos
					province = 1916
					color = { 79 79 79 }
				}
				b_1912 = { #Lavdas
					province = 1912
					color = { 79 79 79 }
				}
				b_1913 = { #Zakas
					province = 1913
					color = { 79 79 79 }
				}
				b_1917 = { #Kalloni
					province = 1917
					color = { 79 79 79 }
				}
			}
		}
		d_prespa = {
			color = { 191 44 60 }
			
			capital = c_prespa
			
			c_prespa = {
				color = { 204 1 54 }
				
				b_2627 = { #Tyriaion
					province = 2627
					color = { 79 79 79 }
				}
				b_2626 = { #Lagrio
					province = 2626
					color = { 79 79 79 }
				}
				b_1893 = { #Maliko
					province = 1893
					color = { 79 79 79 }
				}
			}
			c_ohrid = {
				color = { 174 1 46 }
				
				b_2634 = { #Kidrama
					province = 2634
					color = { 79 79 79 }
				}
				b_2630 = { #Khoma
					province = 2630
					color = { 79 79 79 }
				}
				b_2631 = { #Tlos
					province = 2631
					color = { 79 79 79 }
				}
			}
			c_pharso = {
				color = { 209 67 87 }
				
				b_2203 = { #Pharso
					province = 2203
					color = { 79 79 79 }
				}
				b_1979 = { #Barbanoi
					province = 1979
					color = { 79 79 79 }
				}
				b_1978 = { #Miras
					province = 1978
					color = { 79 79 79 }
				}
				b_1977 = { #Tresteniko
					province = 1977
					color = { 79 79 79 }
				}
			}
		}
	}
	k_olympos = {
		color = { 230 45 1 }
		
		capital = c_herakleo
		
		ai_primary_priority = {
			if = {
				limit = {
					OR = {
						culture = culture:proto_greek
						culture = culture:aeolian
						culture = culture:bryges
					}
				}
				add = @better_than_the_alternatives_score
			}
		}

		d_herakleo = {
			color = { 181 92 46 }
			
			capital = c_herakleo
			
			c_herakleo = {
				color = { 174 76 52 }
				
				b_1797 = { #Herakleion
					province = 1797
					color = { 79 79 79 }
				}
				b_627 = { #Homolion
					province = 627
					color = { 79 79 79 }
				}
				b_628 = { #Eurymenai
					province = 628
					color = { 79 79 79 }
				}
			}
			c_aetolophos = {
				color = { 167 84 57 }
				
				b_595 = { #Aetolophos
					province = 595
					color = { 79 79 79 }
				}
				b_592 = { #Kasthaneia
					province = 592
					color = { 79 79 79 }
				}
				b_591 = { #Meliboia
					province = 591
					color = { 79 79 79 }
				}
			}
			c_dion = {
				color = { 190 112 53 }
				
				b_1798 = { #Dion
					province = 1798
					color = { 79 79 79 }
				}
				b_1800 = { #Paralia
					province = 1800
					color = { 79 79 79 }
				}
				b_1801 = { #Pydna
					province = 1801
					color = { 79 79 79 }
				}
				b_1802 = { #Alonia
					province = 1802
					color = { 79 79 79 }
				}
			}
		}
		d_perraibia = {
			color = { 170 83 37 }
			
			capital = c_pythion
			
			c_pythion = {
				color = { 186 55 1 }
				
				b_1899 = { #Pythion
					province = 1899
					color = { 79 79 79 }
				}
				b_1902 = { #Azoros
					province = 1902
					color = { 79 79 79 }
				}
				b_1898 = { #Malloia
					province = 1898
					color = { 79 79 79 }
				}
				b_1901 = { #Phylakai
					province = 1901
					color = { 79 79 79 }
				}
				b_568 = { #Oloosson
					province = 568
					color = { 79 79 79 }
				} 
			}
			c_mondaia = {
				color = { 153 92 34 }
				
				b_1903 = { #Mondaia
					province = 1903
					color = { 79 79 79 }
				}
				b_1905 = { #Deskati
					province = 1905
					color = { 79 79 79 }
				}
				b_1904 = { #Achelinada
					province = 1904
					color = { 79 79 79 }
				}
			}
		}
		d_elimeia = {
			color = { 224 1 1 }
			
			capital = c_elimeia
			
			c_elimeia = {
				color = { 253 1 1 }
				
				b_1999 = { #Elimeia
					province = 1999
					color = { 79 79 79 }
				}
				b_1796 = { #Polymylos
					province = 1796
					color = { 79 79 79 }
				}
				b_1936 = { #Neraida
					province = 1936
					color = { 79 79 79 }
				}
				b_1943 = { #Galani
					province = 1943
					color = { 79 79 79 }
				}
				b_1952 = { #Ermakia
					province = 1952
					color = { 79 79 79 }
				}
			}
			c_phylakai = {
				color = { 254 1 1 }
				
				b_1933 = { #Servia
					province = 1933
					color = { 79 79 79 }
				}
				b_1932 = { #Avles
					province = 1932
					color = { 79 79 79 }
				}
				b_1934 = { #Velventos
					province = 1934
					color = { 79 79 79 }
				}
			}
			c_aiane = {
				color = { 211 1 2 }
				
				b_1937 = { #Aiane
					province = 1937
					color = { 79 79 79 }
				}
				b_1930 = { #Phrourio
					province = 1930
					color = { 79 79 79 }
				}
				b_1945 = { #Xirolimni
					province = 1945
					color = { 79 79 79 }
				}
				b_1938 = { #Kharalampos
					province = 1938
					color = { 79 79 79 }
				}
			}
		}
	}
	k_eordaia = {
		color = { 254 1 92 }
		
		capital = c_bokeria
		
		ai_primary_priority = {
			if = {
				limit = {
					culture = culture:bryges
				}
				add = @better_than_the_alternatives_score
			}
		}

		d_eordaia = {
			color = { 254 27 142 }
			
			capital = c_bokeria
			
			c_bokeria = {
				color = { 254 1 109 }
				
				b_1955 = { #Philotas
					province = 1955
					color = { 79 79 79 }
				}
				b_1983 = { #Phaistino
					province = 1983
					color = { 79 79 79 }
				}
				b_1980 = { #Philnastro
					province = 1980
					color = { 79 79 79 }
				}
				b_1981 = { #Panagitsa
					province = 1981
					color = { 79 79 79 }
				}
			}
			c_ardassa = {
				color = { 219 57 137 }
				
				b_1948 = { #Ardassa
					province = 1948
					color = { 79 79 79 }
				}
				b_1944 = { #Koila
					province = 1944
					color = { 79 79 79 }
				}
				b_1950 = { #Drosero
					province = 1950
					color = { 79 79 79 }
				}
				b_1960 = { #Kleisoura
					province = 1960
					color = { 79 79 79 }
				}
			}
			c_arnissa = {
				color = { 254 79 153 }
				
				b_1958 = { #Anargyroi
					province = 1958
					color = { 79 79 79 }
				} 
				b_1959 = { #Aetos
					province = 1959
					color = { 79 79 79 }
				}
				b_1956 = { #Kellai
					province = 1956
					color = { 79 79 79 }
				}
				b_2597 = { #Satine
					province = 2597
					color = { 79 79 79 }
				}
			}
		}
		d_erigon_source = {
			color = { 214 88 110 }
			
			capital = c_erigon_source
			
			c_erigon_source = {
				color = { 226 78 129 }
				
				b_2618 = { #Azena
					province = 2618
					color = { 79 79 79 }
				}
				b_2623 = { #Edaes
					province = 2623
					color = { 79 79 79 }
				}
				b_2620 = { #Addaket
					province = 2620
					color = { 79 79 79 }
				}
				b_2616 = { #Matar
					province = 2616
					color = { 79 79 79 }
				}
			}
			c_germe = {
				color = { 205 64 91 }
				
				b_2603 = { #Germe
					province = 2603
					color = { 79 79 79 }
				}
				b_2612 = { #Dadon
					province = 2612
					color = { 79 79 79 }
				}
				b_2608 = { #Meka
					province = 2608
					color = { 79 79 79 }
				}
				b_2613 = { #Daos
					province = 2613
					color = { 79 79 79 }
				}
			}
			c_dahet = {
				color = { 218 107 114 }
				
				b_2604 = { #Dahet
					province = 2604
					color = { 79 79 79 }
				}
				b_2601 = { #Balaios
					province = 2601
					color = { 79 79 79 }
				}
				b_2600 = { #Adio
					province = 2600
					color = { 79 79 79 }
				}
				b_2606 = { #Kimeros
					province = 2606
					color = { 79 79 79 }
				}
			}
		}
	}
	k_makedon = {
		color = { 254 24 67 }
		
		capital = c_aigai
		
		ai_primary_priority = {
			if = {
				limit = {
					culture = culture:bryges
				}
				add = @better_than_the_alternatives_score
			}
		}

		d_almopia = {
			color = { 253 1 94 }
			
			capital = c_europos
			
			c_europos = {
				color = { 254 14 104 }
				
				b_1817 = { #Europos
					province = 1817
					color = { 79 79 79 }
				}
				b_1819 = { #Rizochori
					province = 1819
					color = { 79 79 79 }
				}
				b_1824 = { #Phoustani
					province = 1824
					color = { 79 79 79 }
				}
			}
			c_promachi = {
				color = { 217 88 112 }
				
				b_1821 = { #Promachi
					province = 1821
					color = { 79 79 79 }
				}
				b_1822 = { #Sarakinoi
					province = 1822
					color = { 79 79 79 }
				}
				b_1820 = { #Voreino
					province = 1820
					color = { 79 79 79 }
				}
			}
			c_apsalos = {
				color = { 226 1 67 }
				
				b_1818 = { #Apsalos
					province = 1818
					color = { 79 79 79 }
				}
				b_1982 = { #Aridaia
					province = 1982
					color = { 79 79 79 }
				}
				b_1823 = { #Agras
					province = 1823
					color = { 79 79 79 }
				}
				b_1984 = { #Karydia
					province = 1984
					color = { 79 79 79 }
				}
			}
		}
		d_amphaxitis = {
			color = { 201 80 57 }
			
			capital = c_moryllos
			
			c_moryllos = {
				color = { 245 57 53 }
				
				b_2063 = { #Karydia
					province = 2063
					color = { 79 79 79 }
				}
				b_2057 = { #Aspros
					province = 2057
					color = { 79 79 79 }
				}
				b_2062 = { #Mesaio
					province = 2062
					color = { 79 79 79 }
				}
				b_2059 = { #Euzonoi
					province = 2059
					color = { 79 79 79 }
				}
			}
			c_gephyra = {
				color = { 167 88 72 }
				
				b_2054 = { #Gephyra
					province = 2054
					color = { 79 79 79 }
				}
				b_1806 = { #Kymina
					province = 1806
					color = { 79 79 79 }
				}
				b_1828 = { #Kalokhori
					province = 1828
					color = { 79 79 79 }
				}
			}
			c_ikhnai = {
				color = { 238 48 46 }
				
				b_2053 = { #Ikhnai
					province = 2053
					color = { 79 79 79 }
				}
				b_2055 = { #Europos
					province = 2055
					color = { 79 79 79 }
				}
				b_2056 = { #Polykastro
					province = 2056
					color = { 79 79 79 }
				}
			}
		}
		d_dojran = {
			color = { 226 1 10 }
			
			capital = c_bragylai
			
			c_bragylai = {
				color = { 230 1 1 }
				
				b_2064 = { #Iliolousto
					province = 2064
					color = { 79 79 79 }
				}
				b_2060 = { #Eiriniko
					province = 2060
					color = { 79 79 79 }
				}
				b_2065 = { #Doirani
					province = 2065
					color = { 79 79 79 }
				}
				b_2202 = { #Mordi
					province = 2202
					color = { 79 79 79 }
				}
			}
			c_euippe = {
				color = { 231 1 32 }
				
				b_2655 = { #Euippe
					province = 2655
					color = { 79 79 79 }
				}
				b_2061 = { #Gortyna
					province = 2061
					color = { 79 79 79 }
				}
				b_2656 = { #Alabanda
					province = 2656
					color = { 79 79 79 }
				}
				b_2658 = { #Hyllarima
					province = 2658
					color = { 79 79 79 }
				}
				b_2654 = { #Hypaipa
					province = 2654
					color = { 79 79 79 }
				}
				b_2653 = { #Koloe
					province = 2653
					color = { 79 79 79 }
				}
			}
			c_orbelos = {
				color = { 236 1 1 }
				 
				b_2165 = { #Irakleia
					province = 2165
					color = { 79 79 79 }
				}
				b_2167 = { #Kerkini
					province = 2167
					color = { 79 79 79 }
				} 
				b_2169 = { #Mouries
					province = 2169
					color = { 79 79 79 }
				}
				b_2164 = { #Lithotopos
					province = 2164
					color = { 79 79 79 }
				}
			}
		}
		d_loudias = {
			color = { 223 1 43 }
			
			capital = c_methone
			
			c_methone = {
				color = { 252 1 38 }
				 
				b_1803 = { #Methone
					province = 1803
					color = { 79 79 79 }
				}
				b_1805 = { #Kleidi
					province = 1805
					color = { 79 79 79 }
				}
				b_1811 = { #Aloros
					province = 1811
					color = { 79 79 79 }
				}
				b_1974 = { #Vordori
					province = 1974
					color = { 79 79 79 }
				}
			}
			c_aigai = {
				color = { 219 1 28 }
				
				b_1975 = { #Ostrado
					province = 1975
					color = { 79 79 79 }
				}
				b_1816 = { #Naousa
					province = 1816
					color = { 79 79 79 }
				}
				b_1827 = { #Lefkopetra
					province = 1827
					color = { 79 79 79 }
				}
			}
			c_mieza = {
				color = { 254 1 64 }
				 
				b_1812 = { #Dovras
					province = 1812
					color = { 79 79 79 }
				}
				b_1814 = { #Kali
					province = 1814
					color = { 79 79 79 }
				}
				b_1813 = { #Mandalo
					province = 1813
					color = { 79 79 79 }
				}
				b_1534 = { #Onsitromo
					province = 1534
					color = { 79 79 79 }
				}
			}
			c_pella = {
				color = { 254 1 28 }
				 
				b_1808 = { #Pella
					province = 1808
					color = { 79 79 79 }
				}
				b_1427 = { #Sumze
					province = 1427
					color = { 79 79 79 }
				}
				b_1810 = { #Kyrros
					province = 1810
					color = { 79 79 79 }
				}
			}
		}
	}
}