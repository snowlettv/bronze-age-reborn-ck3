﻿#k_attika
##d_athens ###################################
###c_athens
257 = { #Athenai
	culture = kranaoi
	religion = aegean_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_athena_01 #Temple of Athena
	}
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
	#holy_site = omen_athene
}
256 = { #Pireas
	holding = city_holding
}
259 = { #Chalandri
	holding = none
}
261 = { #Kitsi
	holding = church_holding
}
###c_eleusis
254 = { #Eleusis
	culture = kranaoi
	religion = aegean_faith
	holding = castle_holding 
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
}
255 = { #Phyle
	holding = city_holding
}
258 = { #Axarnes
	holding = none
}
###c_koropi
263 = { #Koropi
	culture = kranaoi
	religion = aegean_faith
	holding = castle_holding 
	
	785.1.1 = {
		special_building_slot = major_quarry_01
		special_building = major_quarry_01
	}
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
}
262 = { #Lagonisi
	holding = none
}
268 = { #Mesoyaias
	holding = city_holding
}
269 = { #Rafina
	holding = none
}
###c_lavrion
266 = { #Lavrion
	culture = kranaoi
	religion = aegean_faith
	holding = castle_holding 
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
}
265 = { #Anavyssos
	holding = city_holding
}
267 = { #Thorikos
	holding = none
}
##d_oropos
###c_oropos
351 = { #Oinoi
	culture = hektenas
	religion = aegean_faith
	holding = castle_holding 
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
}
276 = { #Avlonas
	holding = none
}
349 = { #Dhrosia
	holding = city_holding
}
352 = { #Arma
	holding = none
}
###c_marathon
271 = { #Dikastika
	culture = kranaoi
	religion = aegean_faith
	holding = castle_holding 
	
	1310.1.1 = {
		culture = ionian
		religion = mycenean_faith
	}
}
270 = { #Marathon
	holding = city_holding
}
273 = { #Ramnous
	holding = none
}
274 = { #Malakasa
	holding = none
}