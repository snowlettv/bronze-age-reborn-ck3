﻿#### Local Types ####
@administration_1 = "gfx/interface/icons/culture_innovations/innovation_administration_01.dds"
@administration_2 = "gfx/interface/icons/culture_innovations/innovation_administration_02.dds"
@administration_3 = "gfx/interface/icons/culture_innovations/innovation_administration_03.dds"
@civil_construction_1 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_01.dds"
@civil_construction_2 = "gfx/interface/icons/culture_innovations/innovation_civil_construction_02.dds"
@leadership_1 = "gfx/interface/icons/culture_innovations/innovation_leadership_01.dds"
@leadership_2 = "gfx/interface/icons/culture_innovations/innovation_leadership_02.dds"
@raised_banner = "gfx/interface/icons/culture_innovations/innovation_raised_banner.dds"
@fortifications = "gfx/interface/icons/culture_innovations/innovation_fortifications.dds"
@siege_weapons = "gfx/interface/icons/culture_innovations/innovation_siege_weapons.dds"
@levy_building = "gfx/interface/icons/culture_innovations/innovation_levy_building.dds"
@maa_01 = "gfx/interface/icons/culture_innovations/innovation_maa_01.dds"
@maa_02 = "gfx/interface/icons/culture_innovations/innovation_maa_02.dds"
@weapons_and_armor_01 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_01.dds"
@weapons_and_armor_02 = "gfx/interface/icons/culture_innovations/innovation_weapons_and_armor_02.dds"
@knight = "gfx/interface/icons/culture_innovations/innovation_knight.dds"
@majesty_01 = "gfx/interface/icons/culture_innovations/innovation_majesty_01.dds"
@majesty_02 = "gfx/interface/icons/culture_innovations/innovation_majesty_02.dds"
@majesty_03 = "gfx/interface/icons/culture_innovations/innovation_majesty_03.dds"
@nobility_01 = "gfx/interface/icons/culture_innovations/innovation_nobility_01.dds"
@nobility_02 = "gfx/interface/icons/culture_innovations/innovation_nobility_02.dds"
@nobility_03 = "gfx/interface/icons/culture_innovations/innovation_nobility_03.dds"
@nobility_04 = "gfx/interface/icons/culture_innovations/innovation_nobility_04.dds"
@misc_inventions = "gfx/interface/icons/culture_innovations/innovation_misc_inventions.dds"

@camel = "gfx/interface/icons/culture_innovations/innovation_camel.dds"
@elephant = "gfx/interface/icons/culture_innovations/innovation_elephant.dds"
@special_maa_01 = "gfx/interface/icons/culture_innovations/innovation_special_maa_01.dds"
@special_maa_02 = "gfx/interface/icons/culture_innovations/innovation_special_maa_02.dds"

#Early Bronze Age
innovation_war_cart = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = "gfx/interface/icons/culture_innovations/innovation_war_cart.dds"

	potential = {
		has_cultural_pillar = heritage_mesopotamian
	}

	unlock_maa = war_cart

	flag = global_regional
	flag = tribal_era_regional
}
innovation_ta_seti_bowmen = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = @weapons_and_armor_02

	potential = {
		has_cultural_pillar = heritage_nubian
	}

	unlock_maa = kushite_bowmen

	flag = global_regional
	flag = tribal_era_regional
}
innovation_aegean_maceman = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = @weapons_and_armor_02

	potential = {
		has_cultural_pillar = heritage_aegean
	}

	unlock_maa = aegean_maceman

	flag = global_regional
	flag = tribal_era_regional
}

innovation_boar_tusk_warrior = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = @weapons_and_armor_02

	potential = {
		OR = {
			has_cultural_pillar = heritage_hellenic
			has_cultural_pillar = heritage_thracian
		}
	}

	unlock_maa = boar_tusk_warrior

	flag = global_regional
	flag = tribal_era_regional
}

innovation_axe_and_bow_warriors = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = @weapons_and_armor_02

	potential = {
		OR = {
			this = culture:tukri
			this = culture:lullubi
			any_parent_culture_or_above = {
				this = culture:tukri
			}
			any_parent_culture_or_above = {
				this = culture:lullubi
			}
		}
	}

	unlock_maa = axe_and_bow_warrior

	flag = global_regional
	flag = tribal_era_regional
}
innovation_gutian_raiders = {
	group = culture_group_regional
	culture_era = culture_era_tribal
	icon = @weapons_and_armor_02

	potential = {
		OR = {
			this = culture:gutian
			any_parent_culture_or_above = {
				this = culture:gutian
			}
		}
	}

	unlock_maa = gutian_raider

	flag = global_regional
	flag = tribal_era_regional
}
#Middle Bronze Age
innovation_khopesh = {
	group = culture_group_regional
	culture_era = culture_era_early_medieval
	region = world_egypt
	icon = @weapons_and_armor_02

	unlock_maa = khopesh_warrior

	flag = global_regional
	flag = early_medieval_era_regular
}

innovation_hyksos_charioteers = {
	group = culture_group_regional
	culture_era = culture_era_early_medieval
	icon = "gfx/interface/icons/culture_innovations/innovation_war_cart.dds"

	potential = {
		OR = {
			this = culture:hyksos
			any_parent_culture_or_above = {
				this = culture:hyksos
			}
		}
	}

	unlock_maa = hyksos_chariot

	flag = global_regional
	flag = early_medieval_era_regular
}

innovation_early_hoplite = {
	group = culture_group_regional
	culture_era = culture_era_early_medieval
	region = world_greekaegean
	icon = @weapons_and_armor_02

	unlock_maa = early_hoplite

	flag = global_regional
	flag = early_medieval_era_regular
}

innovation_marsh_raider = {
	group = culture_group_regional
	culture_era = culture_era_early_medieval
	region = world_southern_marshland
	icon = @weapons_and_armor_02

	unlock_maa = marsh_raider

	flag = global_regional
	flag = early_medieval_era_regular
}
#Late Bronze Age
innovation_haibrw_cavalry = {
	group = culture_group_regional
	culture_era = culture_era_high_medieval
	region = world_egypt
	icon = @special_maa_01

	unlock_maa = haibrw_cavalry

	flag = global_regional
	flag = high_medieval_era_regular
}

innovation_panoply_warrior = {
	group = culture_group_regional
	culture_era = culture_era_high_medieval
	region = world_greekaegean
	icon = @weapons_and_armor_02

	unlock_maa = panoply_warrior

	flag = global_regional
	flag = high_medieval_era_regular
}

innovation_hittite_chariot = {
	group = culture_group_regional
	culture_era = culture_era_early_medieval
	icon = "gfx/interface/icons/culture_innovations/innovation_war_cart.dds"

	potential = {
		OR = {
			this = culture:nesili
			this = culture:neohittite
			any_parent_culture_or_above = {
				this = culture:nesili
			}
			any_parent_culture_or_above = {
				this = culture:neohittite
			}
		}
	}

	unlock_maa = hittite_chariot

	flag = global_regional
	flag = early_medieval_era_regular
}