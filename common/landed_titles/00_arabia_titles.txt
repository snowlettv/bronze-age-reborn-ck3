﻿@correct_culture_primary_score = 100
@better_than_the_alternatives_score = 50
@always_primary_score = 1000

e_arabia = {
	color = { 88 161 87 }
		
	capital = c_dilmun
	
	ai_primary_priority = {
		if = {
			limit = {
				OR = {
					culture = culture:dilmunite
					culture = culture:maganite
				}
			}
			add = @correct_culture_primary_score
		}
		else_if = {
			limit = {
				OR = {
					culture = { has_cultural_pillar = heritage_mesopotamian }
					culture = { has_cultural_pillar = heritage_maganite }
				}
			}
			add = @better_than_the_alternatives_score
		}
	}

	k_magan = {
		color = { 7 211 140 }
		
		capital = c_hili

		ai_primary_priority = {
			if = {
				limit = {
					culture = culture:maganite
				}
				add = @correct_culture_primary_score
			}
			else_if = {
				limit = {
					culture = culture:dilmunite
				}
				add = @better_than_the_alternatives_score
			}
		}
		
		d_hili = {
			color = { 110 246 174 }
			
			capital = c_hili
			
			c_hili = {
				color = { 1 239 169 }
				
				b_6879 = { #Hili
					province = 6879
					color = { 79 79 79 }
				}
				b_6878 = { #Al-Ain
					province = 6878
					color = { 79 79 79 }
				}
				b_6877 = { #Nûni
					province = 6877
					color = { 79 79 79 }
				}
				b_6876 = { #Ersuppu
					province = 6876
					color = { 79 79 79 }
				}
				b_6880 = { #Sillatu
					province = 6880
					color = { 79 79 79 }
				}
			}
			c_nunu = {
				color = { 1 250 148 }
				
				b_6874 = { #Nûnu
					province = 6874
					color = { 79 79 79 }
				}
				b_6873 = { #Parâshu
					province = 6873
					color = { 79 79 79 }
				}
				b_6872 = { #Alittu
					province = 6872
					color = { 79 79 79 }
				}
			}
			c_enqetu = {
				color = { 117 254 217 }
				
				b_6875 = { #Enqêtu
					province = 6875
					color = { 79 79 79 }
				}
				b_6869 = { #Urnu
					province = 6869
					color = { 79 79 79 }
				}
				b_6867 = { #Hulmâhu
					province = 6867
					color = { 79 79 79 }
				}
				b_6868 = { #Shammanu
					province = 6868
					color = { 79 79 79 }
				}
			}
		}
		d_ummannar = {
			color = { 1 237 123 }
			
			capital = c_ummannar
			
			c_ummannar = {
				color = { 60 254 138 }
				
				b_1799 = { #Umm an-Nar
					province = 1799
					color = { 79 79 79 }
				}
				b_6850 = { #Errê
					province = 6850
					color = { 79 79 79 }
				}
				b_6848 = { #Misissa
					province = 6848
					color = { 79 79 79 }
				}
				b_6847 = { #Summunu
					province = 6847
					color = { 79 79 79 }
				}
			}
			c_saratu = {
				color = { 1 254 79 }
				
				b_6851 = { #Sarâtu
					province = 6851
					color = { 79 79 79 }
				}
				b_6852 = { #Shuplu
					province = 6852
					color = { 79 79 79 }
				}
				b_6871 = { #Kussimtu
					province = 6871
					color = { 79 79 79 }
				}
				b_6849 = { #Igibû
					province = 6849
					color = { 79 79 79 }
				}
			}
			c_ubbubu = {
				color = { 1 254 130 }
				
				b_1807 = { #Ubbubu
					province = 1807
					color = { 79 79 79 }
				}
				b_6846 = { #Nakmartu
					province = 6846
					color = { 79 79 79 }
				}
				b_6845 = { #Para'u
					province = 6845
					color = { 79 79 79 }
				}
				b_6844 = { #Ita
					province = 6844
					color = { 79 79 79 }
				}
			}
		}
		d_tellabraq = {
			color = { 70 233 201 }
			
			capital = c_tellabraq
			
			c_tellabraq = {
				color = { 1 223 195 }
				
				b_1653 = { #Tell Abraq
					province = 1653
					color = { 79 79 79 }
				}
				b_6856 = { #Muttarrittum
					province = 6856
					color = { 79 79 79 }
				}
				b_6859 = { #Naiabtu
					province = 6859
					color = { 79 79 79 }
				}
				b_6855 = { #Rahû
					province = 6855
					color = { 79 79 79 }
				}
				b_6861 = { #Halâbu
					province = 6861
					color = { 79 79 79 }
				}
			}
			c_zazu = {
				color = { 118 216 172 }
				
				b_6854 = { #Zâzu
					province = 6854
					color = { 79 79 79 }
				}
				b_6853 = { #Uldu
					province = 6853
					color = { 79 79 79 }
				}
				b_6864 = { #Bashmu
					province = 6864
					color = { 79 79 79 }
				}
				b_6870 = { #Pashâlu
					province = 6870
					color = { 79 79 79 }
				}
			}
			c_bamtu = {
				color = { 100 243 233 }
				
				b_6862 = { #Bâmtu
					province = 6862
					color = { 79 79 79 }
				}
				b_6863 = { #Nasku
					province = 6863
					color = { 79 79 79 }
				}
				b_6865 = { #Serru
					province = 6865
					color = { 79 79 79 }
				}
				b_6866 = { #Qubirtu
					province = 6866
					color = { 79 79 79 }
				}
			}
		}
		d_gulfislands = {
			color = { 1 181 86 }
			
			capital = c_arakata
			
			c_arakata = {
				color = { 1 196 49 }
				
				b_1809 = { #Arakata
					province = 1809
					color = { 79 79 79 }
				}
			}
			c_sirri = {
				color = { 1 203 111 }
				
				b_1804 = { #Sirri
					province = 1804
					color = { 79 79 79 }
				}
			}
			c_forur = {
				color = { 67 180 118 }
				
				b_2074 = { #Forur
					province = 2074
					color = { 79 79 79 }
				}
			}
			c_moussa = {
				color = { 12 167 94 }
				
				b_2076 = { #Moussa
					province = 2076
					color = { 79 79 79 }
				}
			}
			c_nuayr = {
				color = { 58 179 108 }
				
				b_2091 = { #Nu'ayr
					province = 2091
					color = { 79 79 79 }
				}
			}
			c_zirkuh = {
				color = { 1 185 93 }
				
				b_193 = { #Zirkuh
					province = 193
					color = { 79 79 79 }
				}
			}
			c_das = {
				color = { 1 193 75 }
				
				b_2141 = { #Das
					province = 2141
					color = { 79 79 79 }
				}
			}
			c_yas = {
				color = { 54 175 69 }
				
				b_2637 = { #Yas
					province = 2637
					color = { 79 79 79 }
				}
			}
			c_delma = {
				color = { 1 185 102 }
				
				b_2204 = { #Delma
					province = 2204
					color = { 79 79 79 }
				}
			}
		}
	}
	k_dilmun = {
		color = { 1 201 113 }
		
		capital = c_dilmun

		ai_primary_priority = {
			if = {
				limit = {
					culture = culture:dilmunite
				}
				add = @correct_culture_primary_score
			}
		}
		
		d_bayofbahrain = {
			color = { 138 224 142 }
			
			capital = c_dilmun
			
			c_dilmun = {
				color = { 108 215 95 }
				
				b_5666 = { #Dilmun
					province = 5666
					color = { 79 79 79 }
				}
				b_473 = { #Dashpu
					province = 473
					color = { 79 79 79 }
				}
				b_1826 = { #Shakâru
					province = 1826
					color = { 79 79 79 }
				}
				b_1453 = { #Nîru
					province = 1453
					color = { 79 79 79 }
				}
			}
			c_labianu = {
				color = { 144 234 108 }
				
				b_2159 = { #Labiânu
					province = 2159
					color = { 79 79 79 }
				}
				b_2082 = { #Huzzû
					province = 2082
					color = { 79 79 79 }
				}
				b_2111 = { #Qablu
					province = 2111
					color = { 79 79 79 }
				}
			}
			c_elamku = {
				color = { 148 198 131 }
				
				b_119 = { #Elamkû
					province = 119
					color = { 79 79 79 }
				}
				b_6831 = { #Hashûr
					province = 6831
					color = { 79 79 79 }
				}
				b_6832 = { #Harbutu
					province = 6832
					color = { 79 79 79 }
				}
				b_6833 = { #Kullu
					province = 6833
					color = { 79 79 79 }
				}
			}
		}
		d_failaka = {
			color = { 68 178 129 }
			
			capital = c_failaka
			
			c_failaka = {
				color = { 1 178 121 }
				
				b_1995 = { #Failaka
					province = 1995
					color = { 79 79 79 }
				}
				b_2429 = { #Pappât
					province = 2429
					color = { 79 79 79 }
				}
				b_1563 = { #Shummannu
					province = 1563
					color = { 79 79 79 }
				}
				b_2139 = { #Pêrtu
					province = 2139
					color = { 79 79 79 }
				}
				b_6889 = { #Harriru
					province = 6889
					color = { 79 79 79 }
				}
			}
			c_ahatutu = {
				color = { 1 158 99 }
				
				b_2490 = { #Targumu
					province = 2490
					color = { 79 79 79 }
				}
				b_2425 = { #Pêmu
					province = 2425
					color = { 79 79 79 }
				}
				b_2410 = { #Pûqu
					province = 2410
					color = { 79 79 79 }
				}
			}
		}
		d_tarut = {
			color = { 1 175 60 }
			
			capital = c_tarut
			
			c_tarut = {
				color = { 61 159 40 }
				
				b_6858 = { #Tarut
					province = 6858
					color = { 79 79 79 }
				}
				b_27 = { #Shaltu
					province = 27
					color = { 79 79 79 }
				}
				b_10 = { #Huliam
					province = 10
					color = { 79 79 79 }
				}
				b_213 = { #Hashadu
					province = 213
					color = { 79 79 79 }
				}
			}
			c_shaqati = {
				color = { 70 159 52 }
				
				b_1839 = { #Sha'qâti
					province = 1839
					color = { 79 79 79 }
				}
				b_1545 = { #Mîru
					province = 1545
					color = { 79 79 79 }
				}
				b_1851 = { #Napîshu
					province = 1851
					color = { 79 79 79 }
				}
				b_2092 = { #Napîshu
					province = 2092
					color = { 79 79 79 }
				}
			}
			c_pashi = {
				color = { 1 200 102 }
				
				b_52 = { #Pashi
					province = 52
					color = { 79 79 79 }
				}
				b_1730 = { #Adîrish
					province = 1730
					color = { 79 79 79 }
				}
				b_67 = { #Shilati
					province = 67
					color = { 79 79 79 }
				}
			}
		}
		d_khor = {
			color = { 1 175 64 }
			
			capital = c_kalkaltu
			
			c_kalkaltu = {
				color = { 25 162 54 }
				
				b_6836 = { #Kalkaltu
					province = 6836
					color = { 79 79 79 }
				}
				b_6834 = { #Nashpaku
					province = 6834
					color = { 79 79 79 }
				}
				b_6835 = { #Kurillu
					province = 6835
					color = { 79 79 79 }
				}
				b_6837 = { #Gissish
					province = 6837
					color = { 79 79 79 }
				}
			}
			c_aqqullu = {
				color = { 92 190 108 }
				
				b_6839 = { #Aqqullu
					province = 6839
					color = { 79 79 79 }
				}
				b_6838 = { #Dalû
					province = 6838
					color = { 79 79 79 }
				}
				b_6840 = { #Nassishu
					province = 6840
					color = { 79 79 79 }
				}
			}
			c_mishla = {
				color = { 1 159 75 }
				
				b_6842 = { #Mishlâ
					province = 6842
					color = { 79 79 79 }
				}
				b_6841 = { #Sikkatânu
					province = 6841
					color = { 79 79 79 }
				}
				b_6843 = { #Manât
					province = 6843
					color = { 79 79 79 }
				}
			}
		}
		d_alahsa = {
			color = { 1 179 38 }
			
			capital = c_alahsa
			
			c_alahsa = {
				color = { 1 156 1 }
				
				b_6884 = { #Uridimmu
					province = 6884
					color = { 79 79 79 }
				}
				b_6886 = { #Ya'alu
					province = 6886
					color = { 79 79 79 }
				}
				b_6885 = { #Urmahu
					province = 6885
					color = { 79 79 79 }
				}
				b_6883 = { #Yalu
					province = 6883
					color = { 79 79 79 }
				}
			}
			c_riqqu = {
				color = { 112 173 66 }
				
				b_511 = { #Riqqû
					province = 511
					color = { 79 79 79 }
				}
				b_6887 = { #Gerru
					province = 6887
					color = { 79 79 79 }
				}
				b_6888 = { #Arnabtu
					province = 6888
					color = { 79 79 79 }
				}
			}
			c_hutnu = {
				color = { 1 192 1 }
				
				b_145 = { #Hutnû
					province = 145
					color = { 79 79 79 }
				}
				b_2191 = { #Shaptu
					province = 2191
					color = { 79 79 79 }
				}
				b_6882 = { #Saiâdu
					province = 6882
					color = { 79 79 79 }
				}
				b_6881 = { #Isqillatu
					province = 6881
					color = { 79 79 79 }
				}
			}
		}
	}
}