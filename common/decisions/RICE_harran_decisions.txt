﻿RICE_harran_pray_to_a_planet = {
	picture = { reference = "gfx/interface/illustrations/decisions/decision_night_moon.dds" }
	ai_check_interval = 40
	
	cooldown = { days = 5475 } # 15 years

	desc = RICE_harran_pray_to_a_planet_desc
	selection_tooltip = RICE_harran_pray_to_a_planet_tooltip

	is_shown = {
		is_ruler = yes
		is_playable_character = yes
		is_mesopotamian_trigger = yes
	}
	
	is_valid = {
		custom_description = {
			text = is_mesopotamian_trigger
			is_mesopotamian_trigger = yes
		}
	}
	
	is_valid_showing_failures_only = {
		is_available_adult = yes
	}

	effect = {
		custom_tooltip = RICE_harran_pray_to_a_planet_effect_tooltip
		add_character_flag = {
			flag = planning_an_activity
			days = 1
		}
		trigger_event = harran.0002
	}

	cost = {
		gold = minor_gold_value # Will depend
		piety = medium_piety_gain
	}
	
	ai_potential = {
		short_term_gold > minor_gold_value
	}

	ai_will_do = {
		base = 20
		modifier = {
			add = -8
			has_trait = cynical
		}
		modifier = {
			add = 8
			has_trait = zealous
		}
	}
}