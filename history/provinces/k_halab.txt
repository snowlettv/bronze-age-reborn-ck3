﻿#k_halab
##d_hazazu ###################################
###c_hazazu
3632 = { #Hazazu
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding

	785.1.1 = {
		special_building_slot = copper_mine_01
		special_building = copper_mine_01
	}
	
	1310.1.1 = {
		holding = castle_holding
		culture = zalwari
		religion = western_hurrian_faith
	}
}
3612 = { #Baszkuj
	holding = none
}
3613 = { #Miskan
	holding = city_holding
}
3631 = { #Hraytan
	holding = none
}
3614 = { #Rifat
	holding = none
}
###c_sarmin
3656 = { #Sarmin
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = harrani
		religion = western_hurrian_faith
	}
}
3638 = { #Takad
	holding = none
}
3639 = { #A'ajel
	holding = none
}
###c_anadan
3635 = { #Anadan
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = zalwari
		religion = western_hurrian_faith
		holding = castle_holding
	}
}
3636 = { #A'wejel
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
3637 = { #Tadil
	holding = none
	
	1310.1.1 = {
		holding = church_holding
	}
}
3634 = { #Sajfat
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
##d_halab
###c_halab
3651 = { #Halab
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	#holy_site = omen_hadad

	785.1.1 = {
		special_building_slot = ruins_of_halab_01
		special_building = halab_01
	}

	1310.1.1 = {
		special_building = ruins_of_halab_01
	}
}
3629 = { #Shair
	holding = city_holding
}
3626 = { #At-Tamura
	holding = church_holding
}
3643 = { #Qanater
	holding = city_holding
}
3642 = { #Barfoum
	holding = castle_holding
}
###c_arpadda
3633 = { #Arpadda
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3630 = { #Hajlan
	holding = city_holding
}
3640 = { #Basratun
	holding = church_holding
}
3641 = { #Zerbeh
	holding = city_holding
}
###c_arada
3644 = { #Arada
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3646 = { #Birnah
	holding = city_holding
}
3645 = { #Zitan
	holding = none
}
###c_arne
3653 = { #Arnê
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3654 = { #Nerab
	holding = city_holding
}
3647 = { #Hadher
	holding = church_holding
}
3763 = { #Bêrl
	holding = none
}
3760 = { #Falame
	holding = none
}
###c_arsoun
3758 = { #Arsoun
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3655 = { #Afes
	holding = city_holding
}
3757 = { #Harf
	holding = church_holding
}
###c_amim
3652 = { #Amim
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3648 = { #Sahrij
	holding = none
}
3650 = { #Jallas
	holding = none
}
##d_qatuma
###c_qatuma
3615 = { #Qatuma
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = zalwari
		religion = western_hurrian_faith
	}
}
3607 = { #Qatmah
	holding = city_holding
}
3606 = { #Jalbul
	holding = church_holding
}
3605 = { #Aqîbah
	holding = city_holding
}
###c_pittina
3616 = { #Pittina
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3627 = { #Bayanoun
	holding = city_holding
}
3624 = { #Nubl
	holding = church_holding
}
3628 = { #Ratyan
	holding = city_holding
}
###c_eltiten
3720 = { #Eltiten
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = zalwari
		religion = western_hurrian_faith
	}
}
3608 = { #I'zâz
	holding = none
}
3719 = { #Elkorum
	holding = none
}