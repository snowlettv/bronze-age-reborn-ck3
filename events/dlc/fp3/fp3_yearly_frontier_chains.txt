﻿namespace = fp3_yearly

scripted_trigger not_steppe_nomad_trigger = {
	culture = { 
		NOR = {
			has_cultural_pillar = heritage_mongolic 
			has_cultural_pillar = heritage_turkic 
			has_cultural_tradition = tradition_horse_lords
		}
	}
}

scripted_trigger suitable_nomad_settling_province_trigger = {
	has_holding_type = castle_holding
	barony = {
		is_under_holy_order_lease = no
		#is_capital_barony = no
	}
}
