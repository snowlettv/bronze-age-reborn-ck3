﻿#Generated
159 = {
	name = "Amtnie"
	dynasty = 159
	religion = "aegean_faith"
	culture = "tyrsenian"
	759.6.19 = {
		birth = yes
	}
	812.6.15 = {
		death = yes
	}
}
218 = {
	name = "Cire"
	dynasty = 218
	religion = "aegean_faith"
	culture = "tyrsenian"
	752.9.6 = {
		birth = yes
	}
	793.5.25 = {
		death = yes
	}
}
521 = {
	name = "Carcuna"
	dynasty = 521
	religion = "aegean_faith"
	culture = "tyrsenian"
	759.8.24 = {
		birth = yes
	}
	809.5.23 = {
		death = yes
	}
}
522 = {
	name = "Licine"
	dynasty = 522
	religion = "aegean_faith"
	culture = "tyrsenian"
	748.11.8 = {
		birth = yes
	}
	814.2.25 = {
		death = yes
	}
}
569 = {
	name = "Pumpu"
	dynasty = 569
	religion = "aegean_faith"
	culture = "tyrsenian"
	768.3.8 = {
		birth = yes
	}
	828.9.9 = {
		death = yes
	}
}
570 = {
	name = "Tarchi"
	dynasty = 570
	religion = "aegean_faith"
	culture = "tyrsenian"
	760.11.12 = {
		birth = yes
	}
	804.6.14 = {
		death = yes
	}
}
571 = {
	name = "Vel"
	dynasty = 571
	religion = "aegean_faith"
	culture = "tyrsenian"
	762.1.10 = {
		birth = yes
	}
	819.2.13 = {
		death = yes
	}
}
578 = {
	name = "Amuni"
	dynasty = 578
	religion = "aegean_faith"
	culture = "tyrsenian"
	757.6.14 = {
		birth = yes
	}
	815.2.3 = {
		death = yes
	}
}
579 = {
	name = "Cnaive"
	dynasty = 579
	religion = "aegean_faith"
	culture = "tyrsenian"
	767.8.22 = {
		birth = yes
	}
	811.8.4 = {
		death = yes
	}
}
583 = {
	name = "Carthasie"
	dynasty = 583
	religion = "aegean_faith"
	culture = "tyrsenian"
	763.1.13 = {
		birth = yes
	}
	820.7.22 = {
		death = yes
	}
}
584 = {
	name = "Lucer"
	dynasty = 584
	religion = "aegean_faith"
	culture = "tyrsenian"
	762.8.3 = {
		birth = yes
	}
	829.10.27 = {
		death = yes
	}
}
585 = {
	name = "Puplie"
	dynasty = 585
	religion = "aegean_faith"
	culture = "tyrsenian"
	747.5.24 = {
		birth = yes
	}
	816.8.25 = {
		death = yes
	}
}
586 = {
	name = "Teitur"
	dynasty = 586
	religion = "aegean_faith"
	culture = "tyrsenian"
	752.9.3 = {
		birth = yes
	}
	813.1.24 = {
		death = yes
	}
}
587 = {
	name = "Velchaie"
	dynasty = 587
	religion = "aegean_faith"
	culture = "tyrsenian"
	760.7.17 = {
		birth = yes
	}
	810.1.19 = {
		death = yes
	}
}
591 = {
	name = "Anche"
	dynasty = 591
	religion = "aegean_faith"
	culture = "tyrsenian"
	752.7.23 = {
		birth = yes
	}
	800.1.8 = {
		death = yes
	}
}
1109 = {
	name = "Clate"
	dynasty = 1109
	religion = "aegean_faith"
	culture = "tyrsenian"
	1279.9.7 = {
		birth = yes
	}
	1336.11.14 = {
		death = yes
	}
}
1321 = {
	name = "Ceisie"
	dynasty = 1321
	religion = "aegean_faith"
	culture = "tyrsenian"
	1287.2.12 = {
		birth = yes
	}
	1341.8.26 = {
		death = yes
	}
}
1393 = {
	name = "Mamurce"
	dynasty = 1393
	religion = "aegean_faith"
	culture = "tyrsenian"
	1287.3.24 = {
		birth = yes
	}
	1338.4.19 = {
		death = yes
	}
}
1394 = {
	name = "Cupe"
	dynasty = 1394
	religion = "aegean_faith"
	culture = "tyrsenian"
	1292.7.8 = {
		birth = yes
	}
	1340.4.27 = {
		death = yes
	}
}
1438 = {
	name = "Thefri"
	dynasty = 1438
	religion = "aegean_faith"
	culture = "tyrsenian"
	1281.5.2 = {
		birth = yes
	}
	1339.2.1 = {
		death = yes
	}
}
1439 = {
	name = "Velthur"
	dynasty = 1439
	religion = "aegean_faith"
	culture = "tyrsenian"
	1289.1.22 = {
		birth = yes
	}
	1344.8.20 = {
		death = yes
	}
}
1440 = {
	name = "Anthai"
	dynasty = 1440
	religion = "aegean_faith"
	culture = "tyrsenian"
	1283.7.1 = {
		birth = yes
	}
	1335.4.12 = {
		death = yes
	}
}
1448 = {
	name = "Cruthni"
	dynasty = 1448
	religion = "aegean_faith"
	culture = "tyrsenian"
	1285.8.10 = {
		birth = yes
	}
	1333.4.6 = {
		death = yes
	}
}
1452 = {
	name = "Cavie"
	dynasty = 1452
	religion = "aegean_faith"
	culture = "tyrsenian"
	1285.1.17 = {
		birth = yes
	}
	1337.4.2 = {
		death = yes
	}
}
1453 = {
	name = "Marce"
	dynasty = 1453
	religion = "aegean_faith"
	culture = "tyrsenian"
	1271.7.17 = {
		birth = yes
	}
	1322.7.10 = {
		death = yes
	}
}
1454 = {
	name = "Ranazu"
	dynasty = 1454
	religion = "aegean_faith"
	culture = "tyrsenian"
	1282.11.25 = {
		birth = yes
	}
	1347.5.23 = {
		death = yes
	}
}
1455 = {
	name = "Thucer"
	dynasty = 1455
	religion = "aegean_faith"
	culture = "tyrsenian"
	1284.6.10 = {
		birth = yes
	}
	1326.2.24 = {
		death = yes
	}
}
1456 = {
	name = "Venel"
	dynasty = 1456
	religion = "aegean_faith"
	culture = "tyrsenian"
	1284.1.15 = {
		birth = yes
	}
	1338.6.14 = {
		death = yes
	}
}
1460 = {
	name = "Aninas"
	dynasty = 1460
	religion = "aegean_faith"
	culture = "tyrsenian"
	1293.5.26 = {
		birth = yes
	}
	1357.1.14 = {
		death = yes
	}
}