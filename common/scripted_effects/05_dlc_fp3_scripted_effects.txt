﻿##################################################
# Struggle & Struggle Decisions
##################################################

##### Struggle Ending #####
fp3_end_persian_struggle_effect = { #BA REMOVED
}

#### MISC STRUGGLE ENDING(s) STUFF ####
fp3_ending_effects_assertion = { #BA REMOVED
}

fp3_struggle_ending_shia_caliphate_effects = { #BA REMOVED
}

fp3_struggle_ending_vassalize_caliph_effects = { #BA REMOVED
}

#### REMOVE CONTRACT COOLDOWN ####
fp3_remove_vassal_contract_cooldown_for_tension_effect = {
	if = {
		limit = { is_struggle_phase = struggle_persia_phase_stabilisation }
		every_involved_ruler = {
			remove_interaction_cooldown = liege_modify_vassal_contract_interaction
			remove_interaction_cooldown = vassal_modify_vassal_contract_interaction
			remove_interaction_cooldown = ai_only_liege_modify_vassal_contract_interaction
			remove_interaction_cooldown = ai_only_vassal_modify_vassal_contract_interaction
		}
	}
}

fp3_struggle_apply_independent_vassalage_catalyst_effect = {
	$NEW_LIEGE$ = {
		# Involved characters vassalising an independent involved ruler.
		if = {
			limit = {
				any_character_struggle = {
					involvement = involved
					activate_struggle_catalyst_secondary_character_involvement_involved_trigger = {
						CATALYST = catalyst_vassalize_independent_ruler
						CHAR = $NEW_VASSAL$
					}
				}
			}
			every_character_struggle = {
				involvement = involved
				limit = {
					activate_struggle_catalyst_secondary_character_involvement_involved_trigger = {
						CATALYST = catalyst_vassalize_independent_ruler
						CHAR = $NEW_VASSAL$
					}
				}
				activate_struggle_catalyst = {
					catalyst = catalyst_vassalize_independent_ruler
					character = $NEW_LIEGE$
				}
				log_debug_variable_for_persian_struggle_effect = { VAR = stabil_catalyst_vassalize_independent_ruler }
			}
		}
	}
}

fp3_struggle_apply_supporter_detractor_war_won_catalysts_effect = {
	# During the Persian Struggle, a supporter wins any war in the region.
	## Unfair wars.
	if = {
		limit = {
			has_trait = fp3_struggle_supporter
			# Scope:attacker won?
			trigger_if = {
				limit = {
					scope:winner = scope:attacker
					exists = scope:war.var:struggle_defender_tier
				}
				highest_held_title_tier > scope:war.var:struggle_defender_tier
			}
			# Else, scope:defender won.
			trigger_if = {
				limit = {
					scope:winner = scope:defender
					exists = scope:war.var:struggle_attacker_tier
				}
				highest_held_title_tier > scope:war.var:struggle_attacker_tier
			}
			any_character_struggle = {
				involvement = involved
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_supporter_win_unfair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
		}
		every_character_struggle = {
			involvement = involved
			limit = {
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_supporter_win_unfair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
			activate_struggle_catalyst = {
				catalyst = catalyst_supporter_win_unfair_war_within_the_region
				character = scope:winner
			}
			scope:war = { set_variable = used_for_struggle_catalyst_on_war_ended }
			log_debug_variable_for_persian_struggle_effect = { VAR = stabil_catalyst_supporter_win_unfair_war_within_the_region }
		}
	}
	## Fair wars.
	else_if = {
		limit = {
			has_trait = fp3_struggle_supporter
			# Scope:attacker won?
			trigger_if = {
				limit = {
					scope:winner = scope:attacker
					exists = scope:war.var:struggle_defender_tier
				}
				highest_held_title_tier <= scope:war.var:struggle_defender_tier
			}
			# Else, scope:defender won.
			trigger_if = {
				limit = {
					scope:winner = scope:defender
					exists = scope:war.var:struggle_attacker_tier
				}
				highest_held_title_tier <= scope:war.var:struggle_attacker_tier
			}
			scope:war = {
				NOT = { has_variable = used_for_struggle_catalyst_on_war_ended }
			}
			any_character_struggle = {
				involvement = involved
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_supporter_win_fair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
		}
		every_character_struggle = {
			involvement = involved
			limit = {
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_supporter_win_fair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
			activate_struggle_catalyst = {
				catalyst = catalyst_supporter_win_fair_war_within_the_region
				character = scope:winner
			}
			scope:war = { set_variable = used_for_struggle_catalyst_on_war_ended }
			log_debug_variable_for_persian_struggle_effect = { VAR = stabil_catalyst_supporter_win_fair_war_within_the_region }
		}
	}
	# During the Persian Struggle, a detractor wins any war in the region.
	## Unfair wars.
	else_if = {
		limit = {
			has_trait = fp3_struggle_detractor
			# Scope:attacker won?
			trigger_if = {
				limit = {
					scope:winner = scope:attacker
					exists = scope:war.var:struggle_defender_tier
				}
				highest_held_title_tier > scope:war.var:struggle_defender_tier
			}
			# Else, scope:defender won.
			trigger_if = {
				limit = {
					scope:winner = scope:defender
					exists = scope:war.var:struggle_attacker_tier
				}
				highest_held_title_tier > scope:war.var:struggle_attacker_tier
			}
			scope:war = {
				NOT = { has_variable = used_for_struggle_catalyst_on_war_ended }
			}
			any_character_struggle = {
				involvement = involved
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_detractor_win_unfair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
		}
		every_character_struggle = {
			involvement = involved
			limit = {
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_detractor_win_unfair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
			activate_struggle_catalyst = {
				catalyst = catalyst_detractor_win_unfair_war_within_the_region
				character = scope:winner
			}
			scope:war = { set_variable = used_for_struggle_catalyst_on_war_ended }
			log_debug_variable_for_persian_struggle_effect = { VAR = unrest_catalyst_detractor_win_unfair_war_within_the_region }
		}
	}
	## Fair wars.
	else_if = {
		limit = {
			has_trait = fp3_struggle_detractor
			# Scope:attacker won?
			trigger_if = {
				limit = {
					scope:winner = scope:attacker
					exists = scope:war.var:struggle_defender_tier
				}
				highest_held_title_tier <= scope:war.var:struggle_defender_tier
			}
			# Else, scope:defender won.
			trigger_if = {
				limit = {
					scope:winner = scope:defender
					exists = scope:war.var:struggle_attacker_tier
				}
				highest_held_title_tier <= scope:war.var:struggle_attacker_tier
			}
			any_character_struggle = {
				involvement = involved
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_detractor_win_fair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
		}
		every_character_struggle = {
			involvement = involved
			limit = {
				activate_struggle_catalyst_warfare_check_attacker_involvement_trigger = {
					CATALYST = catalyst_detractor_win_fair_war_within_the_region
					VAR_SCOPE = scope:war
					INVOLVEMENT = involved
				}
			}
			activate_struggle_catalyst = {
				catalyst = catalyst_detractor_win_fair_war_within_the_region
				character = scope:winner
			}
			scope:war = { set_variable = used_for_struggle_catalyst_on_war_ended }
			log_debug_variable_for_persian_struggle_effect = { VAR = unrest_catalyst_detractor_win_fair_war_within_the_region }
		}
	}
}

###################################
# MISC Effects
###################################
remove_any_education_traits_effect = { # Looks ugly but performance is better than with an IF check
	remove_trait = education_martial_1
	remove_trait = education_martial_2
	remove_trait = education_martial_3
	remove_trait = education_martial_4
	remove_trait = education_martial_prowess_1
	remove_trait = education_martial_prowess_2
	remove_trait = education_martial_prowess_3
	remove_trait = education_martial_prowess_4
	remove_trait = education_learning_1
	remove_trait = education_learning_2
	remove_trait = education_learning_3
	remove_trait = education_learning_4
	remove_trait = education_intrigue_1
	remove_trait = education_intrigue_2
	remove_trait = education_intrigue_3
	remove_trait = education_intrigue_4
	remove_trait = education_stewardship_1
	remove_trait = education_stewardship_2
	remove_trait = education_stewardship_3
	remove_trait = education_stewardship_4
	remove_trait = education_diplomacy_1
	remove_trait = education_diplomacy_2
	remove_trait = education_diplomacy_3
	remove_trait = education_diplomacy_4
}

fp3_struggle_ending_concession_effects = { #BA REMOVED
}


	# Strong independent iranian ruler becomes persian emperor, frees all the iranian subjects and has an easier time vassalizing and or converting them
fp3_struggle_rekindle_iran_effects = { #BA REMOVED
}

fp3_sundered_caliphate_effects = { #BA REMOVED
}

fp3_struggle_catalysts_for_activities_effect = {
	scope:activity = {
		if = {
			limit = {
				any_special_guest = {
					always = yes
				}
			}
			random_special_guest = {
				save_scope_as = special_honorary_guest
			}
		}
	}
	if = {
		limit = {
			this = scope:host
			scope:special_honorary_guest ?= { is_alive = yes }
			scope:host = {
				is_alive = yes
				any_character_struggle = {
					involvement = involved
					activate_struggle_catalyst_secondary_character_involvement_involved_trigger = {
						CATALYST = catalyst_invite_involved_as_honorary_guests_to_feast_hunt
						CHAR = scope:special_honorary_guest
					}
				}
			}
		}
		scope:host = {
			every_character_struggle = {
				involvement = involved
				limit = { phase_has_catalyst = catalyst_invite_involved_as_honorary_guests_to_feast_hunt }
				activate_struggle_catalyst = {
					catalyst = catalyst_invite_involved_as_honorary_guests_to_feast_hunt
					character = scope:host
				}
				log_debug_variable_for_persian_struggle_effect = { VAR = stabil_catalyst_invite_involved_as_honorary_guests_to_feast_hunt }
			}
		}
	}
}

fp3_challenge_house_head_duel_challenger_win_prestige_effect = {
	scope:house_challenger = { add_prestige = medium_prestige_gain }
}

fp3_challenge_house_head_duel_challenger_win_house_effect = {
	scope:house_challenger.house = { set_house_head = scope:house_challenger }
	scope:house_challenger = {
		custom_tooltip = {
			text = fp3_challenge_house_head_recent_cooldown_tt
			add_character_flag = {
				flag = fp3_challenge_house_head_recent_flag
				years = 3
			}
		}
	}
}

fp3_challenge_house_head_duel_challenger_loss_effect = {
	scope:house_challenger = {
		if = {
			limit = { is_alive = yes }
			add_prestige = medium_prestige_loss
		}
	}
	scope:house_head = {
		add_prestige = medium_piety_value
		if = {
			limit = {
				scope:house_challenger = { is_alive = yes }
			}
			add_hook_no_toast = {
				type = trial_by_combat_hook
				target = scope:house_challenger
			}
		}
		custom_tooltip = {
			text = fp3_challenge_house_head_recent_cooldown_tt
			add_character_flag = {
				flag = fp3_challenge_house_head_recent_flag
				years = 3
			}
		}
	}
}

# Saves Sunni holder, or the last one
fp3_save_sunni_caliph_or_previous_effect = { #BA REMOVED
}

fp3_struggle_apply_catalyst_interloper_uninvolved_gain_struggle_titles = { #BA REMOVED
}
