﻿ziggurat_of_kiririsha_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_of_kiririsha_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_kiririsha_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_of_kiririsha_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_kiririsha_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}

ziggurat_of_humban_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_of_humban_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_humban_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_of_humban_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_humban_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}

ziggurat_of_ruhurater_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_of_ruhurater_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_ruhurater_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_of_ruhurater_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_ruhurater_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}

ziggurat_of_napirisha_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_of_napirisha_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_napirisha_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_of_napirisha_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_napirisha_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}

ziggurat_of_inshushinak_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_of_inshushinak_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_inshushinak_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_of_inshushinak_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_of_inshushinak_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						religion_tag = mesopotamian_religion
						has_doctrine = tenet_elamite_syncretism
						has_doctrine = tenet_mesopotamian_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}

ziggurat_choga_zunbil_01 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.05
		development_growth_factor = 0.05
		development_growth = 0.05
	}
	
	province_modifier = {
		monthly_income = 0.5
	}
	
	next_building = ziggurat_choga_zunbil_02

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_choga_zunbil_02 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1000

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.1
		development_growth_factor = 0.1
		development_growth = 0.1
	}
	
	province_modifier = {
		monthly_income = 1
	}
	
	next_building = ziggurat_choga_zunbil_03

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}
ziggurat_choga_zunbil_03 = {
	construction_time = very_slow_construction_time

	type_icon = "icon_building_ziggurats.dds"
	
	can_construct_potential = {
		building_requirement_tribal = no
	}
	
	can_construct = {
		scope:holder = {
			culture = {
				has_innovation = innovation_potters_wheel
			}
		}
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	is_enabled = {
		OR = {
			custom_description = {
				text = holy_site_building_trigger
				barony = {
					is_holy_site_of = scope:holder.faith
				}
			}
			scope:holder = {
				faith = {
					OR = {
						religion_tag = elamite_religion
						has_doctrine = tenet_elamite_syncretism
					}
				}
			}
		}
	}
	
	cost_gold = 1500

	character_modifier = {
		monthly_piety = 0.25
		monthly_dynasty_prestige_mult = 0.05
		men_at_arms_maintenance = -0.025
		short_reign_duration_mult = -0.1
		monthly_county_control_growth_factor = 0.05
	}
	
	county_modifier = {
		tax_mult = 0.15
		development_growth_factor = 0.15
		development_growth = 0.15
	}
	
	province_modifier = {
		monthly_income = 2
	}

	ai_value = {
		base = 100
		modifier = { # Fill all building slots before going for special buildings
			factor = 0
			free_building_slots > 0
		}
	} 

	type = special

	flag = travel_point_of_interest_religious
}