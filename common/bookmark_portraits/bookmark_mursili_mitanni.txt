# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:2keret1
bookmark_mursili_mitanni={
	type=male
	id=1193
	age=0.350000
	genes={ 		hair_color={ 101 239 101 239 }
 		skin_color={ 16 71 16 71 }
 		eye_color={ 236 178 236 178 }
 		gene_chin_forward={ "chin_forward_pos" 149 "chin_forward_pos" 149 }
 		gene_chin_height={ "chin_height_neg" 117 "chin_height_neg" 117 }
 		gene_chin_width={ "chin_width_pos" 133 "chin_width_pos" 133 }
 		gene_eye_angle={ "eye_angle_pos" 138 "eye_angle_pos" 138 }
 		gene_eye_depth={ "eye_depth_pos" 134 "eye_depth_pos" 134 }
 		gene_eye_height={ "eye_height_pos" 132 "eye_height_pos" 132 }
 		gene_eye_distance={ "eye_distance_pos" 132 "eye_distance_pos" 132 }
 		gene_eye_shut={ "eye_shut_neg" 107 "eye_shut_neg" 107 }
 		gene_forehead_angle={ "forehead_angle_neg" 122 "forehead_angle_neg" 122 }
 		gene_forehead_brow_height={ "forehead_brow_height_pos" 137 "forehead_brow_height_pos" 137 }
 		gene_forehead_roundness={ "forehead_roundness_pos" 158 "forehead_roundness_pos" 158 }
 		gene_forehead_width={ "forehead_width_neg" 127 "forehead_width_neg" 127 }
 		gene_forehead_height={ "forehead_height_neg" 73 "forehead_height_neg" 73 }
 		gene_head_height={ "head_height_neg" 95 "head_height_neg" 95 }
 		gene_head_width={ "head_width_pos" 144 "head_width_pos" 144 }
 		gene_head_profile={ "head_profile_neg" 110 "head_profile_neg" 110 }
 		gene_head_top_height={ "head_top_height_neg" 103 "head_top_height_neg" 103 }
 		gene_head_top_width={ "head_top_width_pos" 157 "head_top_width_pos" 157 }
 		gene_jaw_angle={ "jaw_angle_pos" 145 "jaw_angle_pos" 145 }
 		gene_jaw_forward={ "jaw_forward_neg" 118 "jaw_forward_neg" 118 }
 		gene_jaw_height={ "jaw_height_pos" 127 "jaw_height_pos" 127 }
 		gene_jaw_width={ "jaw_width_neg" 110 "jaw_width_neg" 110 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_pos" 140 "mouth_corner_depth_pos" 140 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 135 "mouth_corner_height_pos" 135 }
 		gene_mouth_forward={ "mouth_forward_pos" 132 "mouth_forward_pos" 132 }
 		gene_mouth_height={ "mouth_height_pos" 127 "mouth_height_pos" 127 }
 		gene_mouth_width={ "mouth_width_neg" 116 "mouth_width_neg" 116 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 127 "mouth_upper_lip_size_pos" 127 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_pos" 139 "mouth_lower_lip_size_pos" 139 }
 		gene_mouth_open={ "mouth_open_pos" 134 "mouth_open_pos" 134 }
 		gene_neck_length={ "neck_length_pos" 175 "neck_length_pos" 175 }
 		gene_neck_width={ "neck_width_neg" 117 "neck_width_neg" 117 }
 		gene_bs_cheek_forward={ "cheek_forward_pos" 19 "cheek_forward_pos" 19 }
 		gene_bs_cheek_height={ "cheek_height_neg" 5 "cheek_height_neg" 5 }
 		gene_bs_cheek_width={ "cheek_width_pos" 9 "cheek_width_pos" 9 }
 		gene_bs_ear_angle={ "ear_angle_neg" 34 "ear_angle_neg" 34 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 5 "ear_inner_shape_pos" 5 }
 		gene_bs_ear_bend={ "ear_upper_bend_pos" 174 "ear_upper_bend_pos" 174 }
 		gene_bs_ear_outward={ "ear_outward_neg" 121 "ear_outward_neg" 121 }
 		gene_bs_ear_size={ "ear_size_neg" 21 "ear_size_neg" 21 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_pos" 117 "eye_corner_depth_pos" 117 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_pos" 72 "eye_fold_shape_pos" 72 }
 		gene_bs_eye_size={ "eye_size_pos" 122 "eye_size_pos" 122 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 15 "eye_upper_lid_size_neg" 15 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_neg" 27 "forehead_brow_curve_neg" 27 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_pos" 30 "forehead_brow_forward_pos" 30 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 15 "forehead_brow_inner_height_neg" 15 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_pos" 2 "forehead_brow_outer_height_pos" 2 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_pos" 9 "forehead_brow_width_pos" 9 }
 		gene_bs_jaw_def={ "jaw_def_pos" 189 "jaw_def_pos" 189 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 202 "mouth_lower_lip_def_pos" 202 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 16 "mouth_lower_lip_full_pos" 16 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_neg" 105 "mouth_lower_lip_pad_neg" 105 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 3 "mouth_lower_lip_width_neg" 3 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 40 "mouth_philtrum_def_pos" 40 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 2 "mouth_philtrum_shape_neg" 2 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_neg" 25 "mouth_philtrum_width_neg" 25 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 12 "mouth_upper_lip_def_pos" 12 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_neg" 36 "mouth_upper_lip_full_neg" 36 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_pos" 127 "mouth_upper_lip_profile_pos" 127 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_pos" 96 "mouth_upper_lip_width_pos" 96 }
 		gene_bs_nose_forward={ "nose_forward_pos" 9 "nose_forward_pos" 9 }
 		gene_bs_nose_height={ "nose_height_neg" 17 "nose_height_neg" 17 }
 		gene_bs_nose_length={ "nose_length_neg" 33 "nose_length_neg" 33 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 33 "nose_nostril_height_neg" 33 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 11 "nose_nostril_width_pos" 11 }
 		gene_bs_nose_profile={ "nose_profile_hawk_pos" 38 "nose_profile_hawk_pos" 38 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 19 "nose_ridge_angle_pos" 19 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_pos" 86 "nose_ridge_width_pos" 86 }
 		gene_bs_nose_size={ "nose_size_pos" 54 "nose_size_pos" 54 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_pos" 13 "nose_tip_angle_pos" 13 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 9 "nose_tip_forward_neg" 9 }
 		gene_bs_nose_tip_width={ "nose_tip_width_pos" 16 "nose_tip_width_pos" 16 }
 		face_detail_cheek_def={ "cheek_def_01" 13 "cheek_def_01" 13 }
 		face_detail_cheek_fat={ "cheek_fat_04_pos" 175 "cheek_fat_04_pos" 175 }
 		face_detail_chin_cleft={ "chin_dimple" 14 "chin_dimple" 14 }
 		face_detail_chin_def={ "chin_def" 24 "chin_def" 24 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 227 "eye_lower_lid_def" 227 }
 		face_detail_eye_socket={ "eye_socket_02" 17 "eye_socket_02" 17 }
 		face_detail_nasolabial={ "nasolabial_03" 16 "nasolabial_03" 16 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 12 "nose_ridge_def_neg" 12 }
 		face_detail_nose_tip_def={ "nose_tip_def" 42 "nose_tip_def" 42 }
 		face_detail_temple_def={ "temple_def" 240 "temple_def" 240 }
 		expression_brow_wrinkles={ "brow_wrinkles_01" 203 "brow_wrinkles_01" 203 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 255 "eye_wrinkles_01" 185 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 10 "forehead_wrinkles_01" 10 }
 		expression_other={ "cheek_wrinkles_both_01" 0 "cheek_wrinkles_both_01" 0 }
 		complexion={ "complexion_2" 246 "complexion_2" 246 }
 		gene_height={ "normal_height" 129 "normal_height" 129 }
 		gene_bs_body_type={ "body_fat_head_fat_low" 112 "body_fat_head_fat_low" 120 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 255 "body_shape_pear_full" 0 }
 		gene_bs_bust={ "bust_clothes" 105 "bust_shape_2_half" 132 }
 		gene_age={ "old_4" 191 "old_4" 191 }
 		gene_eyebrows_shape={ "avg_spacing_lower_thickness" 166 "avg_spacing_lower_thickness" 166 }
 		gene_eyebrows_fullness={ "layer_2_high_thickness" 246 "layer_2_high_thickness" 246 }
 		gene_body_hair={ "body_hair_avg" 144 "body_hair_avg" 144 }
 		gene_hair_type={ "hair_straight" 196 "hair_straight" 196 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 185 "normal_eyes" 185 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "normal_eyelashes" 193 "normal_eyelashes" 193 }
 		pose={ "" 255 "" 0 }
 		beards={ "ep2_beards" 21 "no_beard" 0 }
 		hairstyles={ "fp3_hairstyles_iranian_straight_common" 168 "all_hairstyles" 0 }
 		legwear={ "mena_nobility_legwear" 58 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "fp3_iranian_high_nobility_clothes" 44 "most_clothes" 0 }
 		headgear={ "fp3_iranian_royalty" 168 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=2125534279
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=2793867254
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 {
			hash=1485265044
			invert=no
		}
 }
}

