 war = {
	name = "great_sumerian_revolt"	# the name can be left out, will be generated then
	start_date = 785.1.1
	end_date = 788.1.1
	targeted_titles={
		k_gutium
	}
	casus_belli = sumerian_revolt_cb
	attackers = { uruk1 ur1 push1 kutha1 mashkan1 kish1 isin1 adab1 larak1 lagash1 umma1 marad1 apak1 aplak1 babylon1 sippar1 rapiqu1 malgum1 der1 }
	defenders = { gutium1 }
}
