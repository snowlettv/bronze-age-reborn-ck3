﻿#k_hupishna
##d_ulma ###################################
###c_ulma
1386 = { #Ulma
	culture = nesili
	religion = hittite_faith
	holding = tribal_holding
}
2592 = { #Ligai
	holding = none
}
1328 = { #Ato
	holding = none
}
1305 = { #Aulza
	holding = none
}
1349 = { #Igin
	holding = none
}
###c_pabilili
1378 = { #Pabilili
	culture = palaic
	religion = anatolian_faith
	holding = tribal_holding
}
1659 = { #Uwamwa
	holding = none
}
6994 = { #Negna
	holding = none
}
1377 = { #Sakuwassar
	holding = none
}
###c_nenassa
1345 = { #Nenašša
	culture = nesili
	religion = hittite_faith
	holding = castle_holding
}
7008 = { #Harkanna
	holding = city_holding
}
1322 = { #Sesan
	holding = church_holding
}
##d_hupishna
###c_hupishna
6945 = { #Hupišna
	culture = tarhuntassan
	religion = anatolian_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_suwaliyat_01 #Temple of Šuwaliyat
	}
}
1615 = { #Azisso
	holding = city_holding
}
7045 = { #Dammesha
	holding = church_holding
}
###c_shinnuwanda2
1509 = { #Šinnuwanda Taurus
	culture = nesili
	religion = hittite_faith
	holding = tribal_holding
}
7044 = { #Hassik
	holding = none
}
1301 = { #Pappars
	holding = none
}
###c_tunna
3380 = { #Tunna
	culture = nesili
	religion = hittite_faith
	holding = tribal_holding
}
2388 = { #Suztatta
	holding = none
}
###c_tuwanuwa
7015 = { #Tuwanuwa
	culture = nesili
	religion = hittite_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_aruna_01 #Temple of Aruna
	}
}
7043 = { #Pessiya
	holding = city_holding
}
1400 = { #Prikanti
	holding = church_holding
}
##d_uda
###c_uda
1613 = { #Uda
	culture = tarhuntassan
	religion = anatolian_faith
	holding = castle_holding
}
59 = { #Mitassa
	holding = city_holding
}
1620 = { #Kluri
	holding = church_holding
}
1623 = { #Unuaku
	holding = city_holding
}
7080 = { #Laxo
	holding = castle_holding
}
###c_hurniya
1275 = { #Hurniya
	culture = tarhuntassan
	religion = anatolian_faith
	holding = castle_holding
}
1242 = { #Dalugasti
	holding = church_holding
}
1283 = { #Spurio
	holding = city_holding
}
7084 = { #Ulrinna
	holding = city_holding
}
###c_shuwatara
1254 = { #Shuwatara
	culture = tarhuntassan
	religion = anatolian_faith
	holding = castle_holding
}
333 = { #Mavrommatia
	holding = city_holding
}
1239 = { #Pityous
	holding = church_holding
}