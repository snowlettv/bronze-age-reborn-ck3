﻿#k_sallawassa
##d_brioula ###################################
###c_brioula
1644 = { #Eriahe
	culture = carian
	religion = anatolian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
} 
1652 = { #Erha
	holding = none
} 
1642 = { #Ukiuha
	holding = none
}
1639 = { #Ela
	holding = none
	
	1310.1.1 = {
		holding = city_holding 
	}
}
1634 = { #Enu
	holding = none
}
###c_orthosia
1651 = { #Awaruha
	culture = carian
	religion = anatolian_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
1656 = { #Ohkro
	holding = none
	
	1310.1.1 = {
		holding = city_holding 
	}
} 
1658 = { #Priyaram
	holding = church_holding
}
##d_harpasos
###c_harpasos
2624 = { #Wallarimma
	culture = carian
	religion = anatolian_faith
	holding = castle_holding
}
1622 = { #Aniane
	holding = city_holding
}
1621 = { #Iturwa
	holding = church_holding
} 
1617 = { #Ula
	holding = castle_holding
}
2434 = { #Warsa
	holding = city_holding
}
1616 = { #Awirli
	holding = castle_holding
}
###c_priku
1628 = { #Priku
	culture = carian
	religion = anatolian_faith
	holding = tribal_holding
}
1630 = { #Zilala
	holding = none
}
1605 = { #Arwo
	holding = none
}
###c_mosymos
1614 = { #Arak
	culture = carian
	religion = anatolian_faith
	holding = castle_holding
}
1612 = { #Iwauxa
	holding = city_holding
}
1607 = { #Wanaraha
	holding = none
}
1608 = { #Afaxra
	holding = none
} 
##d_sallawassa
###c_sallawassa
1599 = { #Sallawassa
	culture = carian
	religion = anatolian_faith
	holding = castle_holding
}
1598 = { #Ero
	holding = city_holding
}
1601 = { #Amauni
	holding = church_holding
}
1603 = { #Wanona
	holding = city_holding
}
###c_ute
1587 = { #Otan
	culture = carian
	religion = anatolian_faith
	holding = tribal_holding
}
1586 = { #Ekir
	holding = none
}
1583 = { #Urkri
	holding = none
}
1582 = { #Dranok
	holding = none
}
1590 = { #Moktriani
	holding = none
}
###c_ziknik
1592 = { #Ziknik
	culture = carian
	religion = anatolian_faith
	holding = tribal_holding
}
1594 = { #Irsikexa
	holding = none
}
1595 = { #Orsolone
	holding = none
}