﻿#k_tadmor
##d_tadmor_oasis ###################################
###c_tadmor_oasis
4008 = { #Tadmor
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	#holy_site = omen_bol
	
	785.1.1 = {
		special_building_slot = temple_of_bol_01 #Temple of Bol and Yarhibol
	}
}
4009 = { #Hiddri
	holding = church_holding
}
4007 = { #Melga
	holding = none
}
4010 = { #Midar
	holding = none
}
4006 = { #Berkane
	holding = none
}
###c_kamra
4016 = { #Kamra
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
4011 = { #Iferni
	holding = city_holding
}
4130 = { #Ouasli
	holding = none
}
###c_sushkallu
30 = { #Sushkallu
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
107 = { #Waspu
	holding = none
}
3515 = { #Kûsâiu
	holding = none
}
##d_amorite_homeland
###c_ekkemu
2642 = { #Ekkêmu
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3718 = { #Kimsu
	holding = none
}
602 = { #Pasru
	holding = none
}
2645 = { #Pagûtu
	holding = none
}
###c_mashiru
109 = { #Mashîru
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
584 = { #Himêtu
	holding = none
}
3546 = { #Ubârûtu
	holding = none
}
6660 = { #Natbâku
	holding = none
}
###c_sakkuku
444 = { #Sakkuku
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
5250 = { #Ingallu
	holding = none
}
3843 = { #Umâm
	holding = none
}
##d_nazala
###c_nazala
3859 = { #Hnaider
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding

	1310.1.1 = {
		holding = castle_holding
	}
}
3816 = { #Urle
	holding = none
}
4014 = { #Qurasiti
	holding = none
}
3817 = { #Akkiru
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
###c_sadad
3937 = { #Sadad
	culture = eblaite
	religion = eblaite_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
		holding = castle_holding
	}
}
3936 = { #Hafar
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
3860 = { #Arbeen
	holding = none
}
4023 = { #Hapis
	holding = none
}
###c_elelbawi
3815 = { #Elelbawi
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3825 = { #Eltut
	holding = none
}
4129 = { #Fasher
	holding = none
}
4004 = { #Rotarid
	holding = none
}
4005 = { #Mechra
	holding = none
}
##d_akash
###c_akash
3824 = { #Akash
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3852 = { #Abil
	holding = none
}
3822 = { #Sinan
	holding = none
}
3853 = { #Maskanah
	holding = none
}
3830 = { #Saiba
	holding = none
}
3823 = { #Rahya
	holding = none
}
###c_hilal
3813 = { #Hilal
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3821 = { #Mabouqa
	holding = none
}
4414 = { #Alonia
	holding = none
}
4013 = { #Traibb
	holding = none
}
4012 = { #Aaroui
	holding = none
}
3814 = { #Eliyeh
	holding = none
}