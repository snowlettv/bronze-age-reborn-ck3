﻿#On actions handling the Roman Restoration notification events.

on_action_roman_restoration_pulse = {
	trigger = {
		always = no
		NOT = { has_ep3_dlc_trigger = yes }
	}
	events = {
		#Restored Roman Provinces events.
	}
}

on_action_orthodox_pentarchy_pulse = {
	trigger = {
		NOT = { has_ep3_dlc_trigger = yes }
	}
	events = {
		#Byzantines restoring the Pentarchy.
	}
}