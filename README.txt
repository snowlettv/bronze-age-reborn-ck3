Installation Instructions

1. Download the mod from Gitlab
2. Make a new folder in your Documents\Paradox Interactive\Crusader Kings III\Mod folder named bronze_age_reborn
3. Extract all files into the new folder
4. Move the bronze_age_reborn.mod file to your base mod folder
5. Open the CK3 launcher and click on Playsets
6. Click on the Add More Mods button, look for Bronze Age and click the button next to it to add it to your current playset
7. Enable the mod and launch the game

#NOTE - This is an alpha version of the mod. There will be many bugs and missing or broken things. Play at your own risk!