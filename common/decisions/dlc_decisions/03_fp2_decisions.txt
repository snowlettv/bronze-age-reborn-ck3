﻿# No lists or indexes, only chaos. Keep scrolling down and thou shalt find them.

#############################################
# Hostility Ending					    	#
# by Joe Parkin								#
#############################################

struggle_iberia_ending_hostility_decision = {
	decision_group_type = major
	title = struggle_iberia_ending_hostility_decision
	picture = {
		reference = "gfx/interface/illustrations/decisions/fp2_decision_struggle_hostility.dds"
	}
	extra_picture = "gfx/interface/illustrations/struggle_decision_buttons/fp2_decision_hostility.dds"
	desc = struggle_iberia_ending_hostility_decision_desc
	selection_tooltip = struggle_iberia_ending_hostility_decision_tooltip
	confirm_click_sound = "event:/DLC/FP2/SFX/UI/fp2_struggle_ending_decision_confirm"
	is_invisible = yes
	sort_order = 80

	is_shown = {
		is_landless_adventurer = no
		has_fp2_dlc_trigger = yes
		always = no
	}

	is_valid = {
		is_independent_ruler = yes

		custom_tooltip = {
			text = struggle_ending_decision_correct_involvement_spain_tt
			any_character_struggle = {
				involvement = involved
				is_struggle_type = iberian_struggle
			}
		}
	}

	effect = {
		if = {
			limit = { has_dlc_feature = legends }
			legend_seed_struggle_ending_effect = {
				ENDER = root
				STRUGGLE = iberian_struggle
			}
		}
		
		##### Major Effects #####
		show_as_tooltip = {
			dynasty = { add_dynasty_prestige = 10000 }
			fp2_struggle_hostility_ender_effect = yes
		}
		custom_description_no_bullet = { text = fp2_struggle_house_tt }
		# Keep the Struggle Clash for your House
		custom_tooltip = fp2_struggle_can_keep_using_struggle_clash_tt
		# Boost to culture and faith conversion
		custom_tooltip = fp2_struggle_hostility_conversion_tt
		# Damage opinion with other culture/faiths
		custom_tooltip = fp2_struggle_hostility_opinion_tt
		custom_tooltip = fp2_struggle_hostility_opinion_negative_tt
		# Choose Holy War boost, Culture War boost, or both
		custom_description_no_bullet = { text = fp2_struggle_hostility_list_tt }
		#custom_tooltip = fp2_struggle_hostility_holy_cb_joint_tt
		#custom_tooltip = fp2_struggle_hostility_culture_cb_tt



		show_as_tooltip = {
			stress_impact = {
				humble = medium_stress_impact_gain
				cynical = medium_stress_impact_gain
			}
		}


		# Achievements
		add_achievement_global_variable_effect = {
			VARIABLE = fp2_iberian_hostilities_achievement_unlocked
			VALUE = yes
		}
		fp2_holiday_in_iberia_check = yes

		# Trigger a player facing event as a coda
	}

	cost = {}

	ai_check_interval = 120

	ai_potential = { always = no }

	ai_will_do = { base = 100 }
}

#############################################
# Eat a Cheese
# by Daniel "yes I know what exciting content is stop bullying me" Moore
###########################################################################
eat_cheese_decision = {
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_personal_religious.dds"
	}
	desc = eat_cheese_decision_desc

	is_shown = {
		always = no
	}

	effect = {

		custom_tooltip = eat_cheese_effect_tt

		trigger_event = fp2_yearly.1008

	}

	ai_check_interval = 60

	ai_potential = {

	}

	ai_will_do = {
		base = 100
	}
}

#############################################
# Develop a city
# by Maxence Voleau
#############################################
improve_city_province_decision = {
	title = improve_city_province_decision_name
	picture = {
		reference = "gfx/interface/illustrations/decisions/decision_misc.dds"
	}
	desc = improve_city_province_decision_desc
	selection_tooltip = improve_city_province_decision_tooltip
	cooldown = { years = 50 }
	sort_order = 90

	is_shown = {
		is_landless_adventurer = no
		exists = dynasty
		dynasty = {
			has_dynasty_perk = fp2_urbanism_legacy_5
		}
	}

	is_valid_showing_failures_only = {
		custom_tooltip = {
			text = improve_city_province_decision_at_least_one_city_tt
			any_sub_realm_county = {
				count > 0
				holder = root
				any_county_province = {
					building_slots <= 5
					has_holding_type = city_holding
				}
			}
		}
	}

	effect = {

		#Generate intelligible effect
		if = {
			limit = {
				any_sub_realm_county = {
					count > 0
					holder = root
					any_county_province = {
						building_slots <= 5
						has_holding_type = city_holding
					}
				}
			}
			every_sub_realm_county = {
				# only county directly own by the character
				limit = { holder = root }
				every_county_province = {
					limit = {
						has_holding_type = city_holding
						building_slots <= 5
					}
					save_scope_as = current_province
					prev.holder = {
						send_interface_toast = {
							type = event_toast_effect_good
							title = city_gained_building_slots
							left_icon = scope:current_province.barony

							scope:current_province = {
								add_province_modifier = extra_building_slot
							}
						}
					}

				}
			}
		}
		else = {
			custom_tooltip = improve_city_province_decision_decision_no_effect
		}
	}

	cost = {
		gold = {
			value = improve_city_province_decision_cost
			multiply = {
				value = 1
				every_sub_realm_county = {
					# only county directly own by the character
					limit = { holder = root }
					every_county_province = {
						limit = {
							building_slots <= 5
							has_holding_type = city_holding
						}
						add = 1
					}
				}
			}
		}
	}

	ai_check_interval = 120

	ai_potential = {
		is_at_war = no
		# Has enough gold.
		short_term_gold >= ai_war_chest_desired_gold_value
		NOR = {
			has_trait = lazy
			has_trait = callous
		}
	}

	ai_will_do = {
		base = 100
	}
}
