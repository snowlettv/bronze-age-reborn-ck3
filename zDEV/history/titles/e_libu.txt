﻿k_rebu = {
	1310.1.1 = { change_development_level = 3 }
}
####
d_timsahe = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 328
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 987
	}
	1311.1.1 = {
		holder = 0
	}
}
c_timsahe = {
	785.1.1 = {
		holder = 328
	}
	1310.1.1 = {
		holder = 987
	}
}
c_messolonghi = {
	785.1.1 = {
		holder = 328
	}
	1310.1.1 = {
		holder = 987
	}
}
c_dazirar = {
	785.1.1 = {
		liege = "d_timsahe"
		holder = 688
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_timsahe"
		holder = 1545
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_tablate
c_tablate = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 324
	}
	1310.1.1 = {
		holder = 983
	}
}
c_ourare = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 324
	}
	1310.1.1 = {
		holder = 983
	}
}
c_ighla = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 326
	}
	1310.1.1 = {
		holder = 985
	}
}
####
d_bitelma = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
}
c_bitelma = {
	785.1.1 = {
		holder = 325
	}
	1310.1.1 = {
		holder = 984
	}
}
c_yallise = {
	785.1.1 = {
		holder = 325
	}
	1310.1.1 = {
		holder = 984
	}
}
c_irtabe = {
	785.1.1 = {
		holder = 326
	}
	1310.1.1 = {
		holder = 985
	}
}
####
d_itbirane = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 327
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 986
	}
	1311.1.1 = {
		holder = 0
	}
}
c_itbirane = {
	785.1.1 = {
		holder = 327
	}
	1310.1.1 = {
		holder = 986
	}
}
c_imaklate = {
	785.1.1 = {
		liege = "d_itbirane"
		holder = 689
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_itbirane"
		holder = 1546
	}
	1311.1.1 = {
		liege = 0
	}
}
c_adale = {
	785.1.1 = {
		holder = 327
	}
	1310.1.1 = {
		holder = 986
	}
}
####
k_tehenu = {
	1310.1.1 = { change_development_level = 3 }
}
####
#d_ammonia
c_ammonia = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 331
	}
	1310.1.1 = {
		holder = 990
	}
}
c_mareia = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 331
	}
	1310.1.1 = {
		holder = 990
	}
}
c_antiphrai = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 331
	}
	1310.1.1 = {
		holder = 990
	}
}
c_apis = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 330
	}
	1310.1.1 = {
		holder = 989
	}
}
####
#d_aghilasse
c_aghilasse = {
	785.1.1 = {
		holder = 330
	}
	1310.1.1 = {
		holder = 989
	}
}
c_iwarkate = {
	785.1.1 = {
		holder = 329
	}
	1310.1.1 = {
		holder = 988
	}
}
c_katisa = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 329
	}
	1310.1.1 = {
		holder = 988
	}
}
####	
#d_siwa
c_siwa = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
	785.1.1 = {
		holder = 690
	}
	1310.1.1 = {
		holder = 1547
	}
}
c_khepen = {
	785.1.1 = {
		holder = 691
	}
	1310.1.1 = {
		holder = 1548
	}
}
####