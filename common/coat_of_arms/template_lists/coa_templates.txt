coat_of_arms_template_lists = {
	all = {
		# Simple catch all
		
		3 = basic_charge_01
		3 = basic_charge_01b
		3 = basic_ordinary_01
		3 = basic_ordinary_01_inverted
		
		#500 = basic_charge_02
		#50000 = basic_charge_03
		#1 = basic_charge_04
		#50000 = basic_charge_05
		#3 = basic_charge_06
		
		# Turned off
		#1 = basic_charge_13
		#1 = basic_charge_13_b		
		#1 = border_01
		#1 = border_01_metal
		#1 = basic_ordinary_02
		#1 = basic_ordinary_02_b		
		#1 = dynasty_godwin
		#1 = dynasty_godwin_02		


		
		#3 = basic_ordinary_division_01
		#1 = basic_ordinary_division_02
		#3 = basic_ordinary_chief_01
		#1 = basic_ordinary_chief_02
		
		
		# Pagan catch-all
		special_selection = {
			trigger = {				
			}
			5000 = basic_charge_01
			#50000 = basic_charge_02
		}
		#Ba new
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = egyptian_coa_gfx }
			}
			# Weights are temps
			2000 = basic_african_template
			2000 = basic_african_template_inverted	
			2000 = circled_african_template
			2000 = circled_african_template_inverted
			2500 = geometrical_african_template
			2500 = geometrical_african_template_inverted
			2000 = basic_charge_01 #Until there are more ce's
			10000 = basic_egyptian_template
			10000 = basic_egyptian_template_inverted
		}
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = mesopotamian_coa_gfx }
			}
			# Weights are temps
			16000 = basic_mesopotamian_template
			16000 = basic_mesopotamian_template_inverted
		}
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = helladic_coa_gfx }
			}
			# Weights are temps
			16000 = basic_helladic_template
			16000 = basic_helladic_template_inverted
		}
		#End of BA new
		# European Paganism - Single, Circle, Senester
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = anatolian_coa_gfx } # Replace later
			}
			# Weights are temps
			16000 = basic_pagan_template
			16000 = basic_pagan_template_inverted			
			8000 = basic_pagan_template_senester
			8000 = basic_pagan_template_senester_inverted
			3000 = geometrical_pagan_template
			4000 = geometrical_pagan_template_inverted				
			3000 = geometrical_pagan_template_senester	
			2000 = circled_pagan_template
			2000 = circled_pagan_template_inverted
			2000 = circled_pagan_template_offset	
			2000 = circled_pagan_template_offset_inverted
		}				
		# Germanic Paganism - No circle, more geometrical designs
		special_selection = {
			trigger = {
				always = no
			}
			5000 = basic_pagan_template
			5000 = basic_pagan_template_inverted			
			2500 = basic_pagan_template_senester
			2500 = basic_pagan_template_senester_inverted	
			5000 = geometrical_pagan_template
			5000 = geometrical_pagan_template_inverted				
			2500 = geometrical_pagan_template_senester	
		}	

		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = elamite_coa_gfx }
			}	
			50 = basic_charge_01
			50 = basic_charge_01b
			50 = basic_ordinary_01
			50 = basic_ordinary_01_inverted
			100 = indian_geometrical_template
			100 = indian_geometrical_template_inverted
			50 = indian_single_border_template
			50 = indian_single_border_template_inverted	
			150 = circled_indian_template
			150 = circled_indian_template_inverted
			60  = circled_indian_template_offset
			60  = circled_indian_template_offset_inverted				
		}
		# Tamgha users - Circle/Roundel
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = zagrosian_coa_gfx }
			}
			5000 = basic_tamgha_template
			5000 = basic_tamgha_template_inverted	
			2500 = circled_tamgha_template
			2500 = circled_tamgha_template_inverted		
			2500 = roundel_tamgha_template
			2500 = roundel_tamgha_template_inverted					
		}	
		# African Paganism - Single charge, geometrical patterns
		special_selection = {
			trigger = {
				scope:culture = { has_coa_gfx = african_coa_gfx }
			}
			2000 = basic_african_template
			2000 = basic_african_template_inverted	
			2000 = circled_african_template
			2000 = circled_african_template_inverted
			2500 = geometrical_african_template
			2500 = geometrical_african_template_inverted					
		}		
		# Middle East
		# Islam - catch all - Persian, Turkic & others will use these only
		special_selection = {	
			trigger = {
				scope:culture = { has_coa_gfx = maganite_coa_gfx }
			}			
			2000 = circled_mena_template
			2000 = circled_mena_template_inverted
			2000 = roundel_mena_template
			2000 = roundel_mena_template_inverted
			4000 = mena_geometrical_template
			4000 = mena_geometrical_template_inverted
		}		
		# MENA - Saracen/Mamluk heraldry - Additionnal layer centered on arabic & berber cultures
		special_selection = {
			trigger = {
				always = no
			}
			1000 = mena_fess_01
			1000 = mena_fess_01_barrulets
			1000 = mena_fess_02
			1000 = mena_fess_02_barrulets
			1000 = mena_fess_03
			1000 = mena_fess_03_barrulets
			1000 = mena_fess_04
			1000 = mena_fess_04_barrulets # 100000
			1000 = mena_fess_05
			1000 = mena_fess_05_barrulets
			1000 = mena_fess_06
			1000 = mena_fess_06_barrulets
			1000 = mena_chief_01
			1000 = mena_chief_01_b # 50000
			1000 = mena_chief_03
			1000 = mena_chief_04
			1000 = mena_bend_01
			1000 = mena_bend_02
			500 = mena_fess_chief
			500 = mena_lowered_fess
			500 = mena_fess_chief_inverted
			500 = mena_lowered_fess_inverted			
			# larger bend for larger charge, those could be used for arabic calligraphy (ie Nasrid)
		}
	}

	religious_title = {
		100 = religious_icon_01
		100 = religious_icon_01_metal
	}

	factions = {
		100 = faction_01
	}
}