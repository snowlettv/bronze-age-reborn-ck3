﻿#k_ugarit
##d_ugarit ###################################
###c_ugarit
3524 = { #Ugarit
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	#holy_site = omen_el
	
	785.1.1 = {
		special_building_slot = temple_of_el_01 #Temple of El
		special_building = temple_of_el_01
	}
}
3523 = { #Latakia
	holding = city_holding
}
3522 = { #Hursubu'i
	holding = church_holding
}
3525 = { #Kirsana
	holding = castle_holding
}
3526 = { #Dumatu
	holding = city_holding
}
3527 = { #Ra's al-Basît
	holding = castle_holding
}
###c_gibala
3539 = { #Gib'ala
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3528 = { #Pidi
	holding = city_holding
}
3529 = { #Mazar
	holding = none
}
3531 = { #Haffah
	holding = none
}
###c_kesab
3519 = { #Kesab
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_mount_zaphon_01 #Mount Zaphon Sanctuary
	}
}
3520 = { #Rabia
	holding = city_holding
}
3535 = { #Kinsabba
	holding = none
}
3518 = { #Hamri
	holding = none
}
3517 = { #Chettia
	holding = none
}
3521 = { #Ghmam
	holding = none
}
###c_suhurre
3536 = { #Suhurre
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3533 = { #Aramo
	holding = none
}
3537 = { #Dorien
	holding = none
}
3530 = { #Terjano
	holding = city_holding
}
##d_suksi
###c_suksi
3540 = { #Suksi
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3541 = { #Helbakko
	holding = none
}
3543 = { #Sharqiyah
	holding = none
}
3544 = { #Baniyas
	holding = city_holding
}
###c_bayda
3547 = { #Bayda
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3555 = { #Hammam
	holding = none
}
3549 = { #Qadmus
	holding = none
}
3550 = { #Srijes
	holding = none
}
##d_arwada
###c_sumura
3562 = { #Sumura
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3559 = { #Kafroun
	holding = none
}
3561 = { #Albarkieh
	holding = none
}
3567 = { #Haba
	holding = none
}
3563 = { #Aarida
	holding = city_holding
}
###c_arwada
3553 = { #Arwada
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = island_of_arwad_01
		special_building = island_of_arwad_01
	}

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3554 = { #As Soda
	holding = none
}
3552 = { #Marqueh
	holding = city_holding
}
3556 = { #Khirbet
	holding = none
}
###c_akkari
3571 = { #Akkari
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding

	1310.1.1 = {
		culture = amoritic
		religion = amorite_faith
	}
}
3572 = { #Almishtaya
	holding = city_holding
}
3568 = { #Bahzina
	holding = none
}