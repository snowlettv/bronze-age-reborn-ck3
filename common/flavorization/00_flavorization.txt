﻿#Princes (children of Kings/Emperors, sorted by culture)

### Roman ###

### Iberian ###

### Iranian ###

### Dravidian ### ### Indo-Aryan ###

#countling = {
#	gender = male
#	special = ruler_child
#	tier = county
#	governments = { feudal_government }
#}

#duckling = {
#	gender = male
#	special = ruler_child
#	tier = duchy
#	governments = { feudal_government }
#}

#empress_mutter = {
#	gender = female
#	special = queen_mother
#	tier = empire
#	
#	governments = { feudal_government }
#	heritages = { heritage_central_germanic }
#}
empress_mother = {
	type = character
	gender = female
	special = queen_mother
	tier = empire
	governments = { feudal_government palatial_government tributary_government }
	priority = 75
	flavourization_rules = { 
		top_liege = no
	}
}
queen_mother = {
	type = character
	gender = female
	special = queen_mother
	tier = kingdom
	governments = { feudal_government palatial_government tributary_government }
	priority = 42
	flavourization_rules = { 
		top_liege = no
	}
}

#Boring Fallbacks, keep at bottom of file!

prince = {
	type = character
	gender = male
	special = ruler_child
	tier = kingdom
	governments = { feudal_government clan_government palatial_government tributary_government }
	priority = 35
	flavourization_rules = { 
		top_liege = no
	}
}

princess = {
	type = character
	gender = female
	special = ruler_child
	tier = kingdom
	governments = { feudal_government clan_government palatial_government tributary_government }
	priority = 35
	flavourization_rules = { 
		top_liege = no
	}
}

prince_empire = {
	type = character
	gender = male
	special = ruler_child
	tier = empire
	governments = { feudal_government clan_government palatial_government tributary_government }
	priority = 35
	flavourization_rules = { 
		top_liege = no
	}
}

princess_empire = {
	type = character
	gender = female
	special = ruler_child
	tier = empire
	governments = { feudal_government clan_government palatial_government tributary_government }
	priority = 35
	flavourization_rules = { 
		top_liege = no
	}
}

# Administrative

duchy_administrative = {
	type = title
	tier = duchy
	priority = 50
	governments = { administrative_government }
	flavourization_rules = {
		top_liege = no
	}
}
kingdom_administrative = {
	type = title
	tier = kingdom
	priority = 73
	governments = { administrative_government }
	flavourization_rules = {
		top_liege = no
		only_vassals = yes
	}
}
kingdom_administrative_independent = {
	type = title
	tier = kingdom
	priority = 106
	governments = { administrative_government }
	flavourization_rules = {
		only_independent = yes
	}
}
 
# Landless Adventurer Camps

duchy_landless_adventurer_camp = {
	type = title
	tier = duchy
	priority = 30
	governments = { landless_adventurer_government }
}

king_egyptian_dynastic_male = { #Pharaoh
	type = character
	gender = male
	special = holder
	tier = kingdom
	priority = 50
	flavourization_rules = { 
		top_liege = no
		only_holder = yes
	}
	governments = { feudal_government palatial_government tributary_government administrative_government }
	titles = {
		k_sixth_dynasty
		k_eighth_dynasty
		k_ninth_dynasty
		k_tenth_dynasty
		k_eleventh_dynasty
		k_twelfth_dynasty
	}
}

king_egyptian_dynastic_female = { #Hemet-Nesut-Weret
	type = character
	gender = female
	special = holder
	tier = kingdom
	priority = 50
	flavourization_rules = { 
		top_liege = no
		only_holder = yes
	}
	governments = { feudal_government palatial_government tributary_government administrative_government }
	titles = {
		k_sixth_dynasty
		k_eighth_dynasty
		k_ninth_dynasty
		k_tenth_dynasty
		k_eleventh_dynasty
		k_twelfth_dynasty
	}
}
