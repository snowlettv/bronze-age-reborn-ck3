﻿# RED SEA

RICE_red_sea_event_trigger = {
	OR = {
		has_title = title:c_quseir
		has_title = title:c_maautu
		has_title = title:c_berenike
		has_title = title:c_aqher
		has_title = title:c_ahai
		has_title = title:c_rekhit
		has_title = title:c_kherta
		has_title = title:c_utchet
		has_title = title:c_gabalalzayt
		has_title = title:c_uamemti
		has_title = title:c_manutut
		has_title = title:c_kherta
		has_title = title:c_ashep
		has_title = title:c_magarah
		has_title = title:c_nubnub
		has_title = title:c_mestpekh
		has_title = title:c_merrt
		has_title = title:c_elath
		has_title = title:c_ubti
		has_title = title:c_herui
		has_title = title:c_amseth
		has_title = title:c_sekhekh
		has_title = title:c_tuatta
		has_title = title:c_merti
		has_title = title:c_uatchit
		has_title = title:c_kenemti
		has_title = title:c_neshni
		has_title = title:c_tebhet
		has_title = title:c_heru
		has_title = title:c_sekhekh
		has_title = title:c_khadem
		has_title = title:c_ketuit
		has_title = title:c_makhait
		has_title = title:c_ubekh
	}
}


RICE_red_sea_eastern_desert_event_trigger = {
	OR = {
		has_title = title:c_tuatta
		has_title = title:c_merti
		has_title = title:c_uatchit
		has_title = title:c_kenemti
		has_title = title:c_neshni
		has_title = title:c_tebhet
		has_title = title:c_heru
		has_title = title:c_sekhekh
		has_title = title:c_khadem
		has_title = title:c_ketuit
		has_title = title:c_makhait
		has_title = title:c_ubekh
	}
}


RICE_red_sea_coastal_event_trigger = {
	OR = {
		has_title = title:c_quseir
		has_title = title:c_maautu
		has_title = title:c_berenike
		has_title = title:c_aqher
		has_title = title:c_ahai
		has_title = title:c_rekhit
		has_title = title:c_kherta
		has_title = title:c_utchet
		has_title = title:c_gabalalzayt
		has_title = title:c_uamemti
		has_title = title:c_manutut
		has_title = title:c_kherta
		has_title = title:c_ashep
		has_title = title:c_magarah
		has_title = title:c_nubnub
		has_title = title:c_mestpekh
		has_title = title:c_merrt
		has_title = title:c_elath
		has_title = title:c_ubti
		has_title = title:c_herui
		has_title = title:c_amseth
	}
}