﻿#######################
# CULTURAL TRADITIONS #
#######################

######################
# Regional Traditions #
######################
tradition_RICE_children_of_the_nile = {
	category = regional

	layers = {
		0 = diplo
		1 = mediterranean
		4 = RICE_nile.dds
	}
	
	is_shown = {
		any_culture_county = {
			#count >= 4
			any_county_province = {
				geographical_region = RICE_nile_river_valley
			}
		}
	}
	can_pick = {
		custom_tooltip = {
			text = RICE_culture_along_the_nile_desc
			any_culture_county = {
				count >= 4
				any_county_province = {
					geographical_region = RICE_nile_river_valley
				}
			}
		}
	}
	
	parameters = {
		unlock_rice_decision_nile_flood_results = yes
		unlock_rice_decision_build_nilometers = yes
	}

	county_modifier = {
		floodplains_development_growth_factor = 0.05
		floodplains_construction_gold_cost = -0.05
		oasis_development_growth_factor = 0.05
		oasis_construction_gold_cost = -0.05
		desert_development_growth_factor = 0.05
		desert_construction_gold_cost = -0.05
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						culture_pillar:ethos_spiritual = { is_in_list = traits }
						culture_pillar:ethos_bureaucratic = { is_in_list = traits }
						culture_pillar:ethos_communal = { is_in_list = traits }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bureaucratic_spiritual_or_communal_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	ai_will_do = { value = 100 }
}
