﻿#Defined

#First Dynasty of Babylon (Amorite Dynasty)

1bSumuabum = { #Sumu-Abum (also Su-abu) was an Amorite, and the first King of the First Dynasty of Babylon (the Amorite Dynasty). He reigned between 1830 and 1817 BC (short chronology) or between 1894 and 1881 BC (middle chronology). He freed a small area of land previously ruled by the fellow Amorite city state of Kazallu which included Babylon, then a minor administrative center in southern Mesopotamia.
	name = "Sumu-abum"
	dynasty = dynn_sumuabum1
	religion = "amorite_faith"
	culture = "amoritic"
	958.1.1 = {
		birth = yes
	}
	1019.1.1 = {
		death = yes
	}
}
1bSumulael = { #Sumu-la-El (also Sumulael or Sumu-la-ilu) was a King in the First Dynasty of Babylon. He reigned c. 1817-1781 BC. He subjugated and conquered nearby cities like Kish and built a string of fortresses around his territory.
	name = "Sumu-la-El"
	dynasty = dynn_sumuabum1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 1bSumuabum
	995.1.1 = {
		birth = yes
	}
	1055.1.1 = {
		death = yes
	}
}
1bSabium = { #Sabium (also Sabum) was an Amorite King in the First Dynasty of Babylon, the Amorite Dynasty. He reigned c. 1781 BC – 1767 BC (short chronology), and ruled what was at the time a recently created, small and minor Amorite kingdom which included the town of Babylon.
	name = "Sabium"
	dynasty = dynn_sumuabum1
	religion = "amorite_faith"
	culture = "amoritic"
	father = 1bSumulael
	1012.1.1 = {
		birth = yes
	}
	1069.1.1 = {
		death = yes
	}
}
1bApilsin = { #Apil-Sin was an Amorite King of the First Dynasty of Babylon (the Amorite Dynasty). He possibly reigned between c. 1767 to 1749 BC.
	name = "Apil-Sin"
	dynasty = dynn_sumuabum1
	religion = "akkadian_faith"
	culture = "amoritic"
	father = 1bSabium
	1031.1.1 = {
		birth = yes
	}
	1087.1.1 = {
		death = yes
	}
}
1bSinmuballit = { #Sin-Muballit was the father of Hammurabi and the fifth Amorite king of the first dynasty (the Amorite Dynasty) of Babylonia, reigning c. 1813-1793 or 1749-1729 BC (see Chronology of the Ancient Near East). He ruled over a relatively new and minor kingdom; however, he was the first ruler of Babylon to actually declare himself king of the city, and the first to expand the territory ruled by the city, and his son greatly expanded the Babylonian kingdom into the short lived Babylonian Empire.
	name = "Sin-muballit"
	dynasty = dynn_sumuabum1
	religion = "akkadian_faith"
	culture = "amoritic"
	father = 1bApilsin
	1048.1.1 = {
		birth = yes
	}
	1107.1.1 = {
		death = yes
	}
}
1bHammurabi = { #Hammurabi (/ˌxæmʊˈrɑːbi/; Akkadian: Ḫâmmurapi;[a] c.1810 – c.1750 BC), also spelled Hammurapi, was the sixth Amorite king of the Old Babylonian Empire, reigning from c.1792 to c.1750 BC. He was preceded by his father, Sin-Muballit, who abdicated due to failing health. During his reign, he conquered the city-states of Larsa, Eshnunna, and Mari. He ousted Ishme-Dagan I, the king of Assyria, and forced his son Mut-Ashkur to pay tribute, bringing almost all of Mesopotamia under Babylonian rule.
	name = "Hammurabi"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bSinmuballit
	martial = 12
	learning = 12
	trait = education_martial_4
	trait = saint
	trait = just
	trait = callous
	trait = overseer
	1090.1.1 = {
		birth = yes
	}
	1150.1.1 = {
		death = yes
	}
}
1bSamsuiluna = { #Samsu-iluna (Amorite: Shamshu) was the seventh king of the founding Amorite dynasty of Babylon, ruling from 1750 BC to 1712 BC (middle chronology), or from 1686 to 1648 BC (short chronology). He was the son and successor of Hammurabi by an unknown mother. His reign was marked by the violent uprisings of areas conquered by his father and the abandonment of several important cities (primarily in Sumer).
	name = "Samsu-iluna"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bHammurabi
	1120.1.1 = {
		birth = yes
	}
	1188.1.1 = {
		death = yes
	}
}
1bAbieshuh = { #Abī-Ešuh (variants: ma-bi-ši, "Abiši", mE-bi-šum, "Ebišum") was the 8th king of the 1st Dynasty of Babylon and reigned for 28 years from c. 1648–1620 BC (short chronology) or 1712–1684 BC (middle chronology). He was preceded by Samsu-iluna, who was his father.
	name = "Abi-eshuh"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bSamsuiluna
	1158.1.1 = {
		birth = yes
	}
	1216.1.1 = {
		death = yes
	}
}
1bAmmiditana = { #Ammi-Ditana was a king of Babylon who reigned from 1620–1583 BC. He was preceded by Abi-Eshuh. Year-names survive for the first 37 years of his reign, plus fragments for a few possible additional years. His reign was a largely peaceful one; he was primarily engaged in enriching and enlarging the temples, and a few other building projects, although in his 37th regnal year he recorded having destroyed the city wall of Der, built earlier by Damiq-ilishu of Isin.
	name = "Ammi-ditana"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bAbieshuh
	1196.1.1 = {
		birth = yes
	}
	1253.1.1 = {
		death = yes
	}
}
1bAmmisaduqa = { #Ammi-Saduqa (or Ammisaduqa, Ammizaduga) was a king of the First Dynasty of Babylon, dating to around c. 1647–1626 BC (Middle Chronology) or c. 1583–1562 BC (Short Chronology).
	name = "Ammi-saduqa"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bAmmiditana
	1223.1.1 = {
		birth = yes
	}
	1274.1.1 = {
		death = yes
	}
}
1bSamsuditana = { #Samsu-ditāna, inscribed phonetically in cuneiform sa-am-su-di-ta-na in the seals of his servants, the 11th and last king of the Amorite or First Dynasty of Babylon, reigned for 31 years, 1626 – 1595 BC (Middle Chronology) or 1562 – 1531 BC (Short Chronology). His reign is best known for its demise with the sudden fall of Babylon at the hands of the Hittites.
	name = "Samsu-Ditana"
	dynasty = dynn_sumuabum1
	religion = "babylonian_faith"
	culture = "babylonian"
	father = 1bAmmisaduqa
	1264.1.1 = {
		birth = yes
	}
	1305.1.1 = {
		death = {
			death_reason = death_battle
			killer = 2hattusa7 #Mursili I
		}
	}
}

#Generated
999 = {
	name = "Ashur-nirari"
	dynasty = 999
	religion = "babylonian_faith"
	culture = "babylonian"
	1287.11.19 = {
		birth = yes
	}
	1350.2.4 = {
		death = yes
	}
}
1000 = {
	name = "Arwium"
	dynasty = 1000
	religion = "babylonian_faith"
	culture = "babylonian"
	1288.10.16 = {
		birth = yes
	}
	1341.10.19 = {
		death = yes
	}
}
1001 = {
	name = "Tuge"
	dynasty = 1001
	religion = "babylonian_faith"
	culture = "babylonian"
	1277.4.23 = {
		birth = yes
	}
	1323.10.14 = {
		death = yes
	}
}
1002 = {
	name = "Shar-kali-sharri"
	dynasty = 1002
	religion = "babylonian_faith"
	culture = "babylonian"
	1277.6.3 = {
		birth = yes
	}
	1327.2.27 = {
		death = yes
	}
}
1003 = {
	name = "Yangi"
	dynasty = 1003
	religion = "babylonian_faith"
	culture = "babylonian"
	1291.10.26 = {
		birth = yes
	}
	1359.5.12 = {
		death = yes
	}
}
1004 = {
	name = "Tizqar"
	dynasty = 1004
	religion = "babylonian_faith"
	culture = "babylonian"
	1282.2.7 = {
		birth = yes
	}
	1336.8.22 = {
		death = yes
	}
}
1005 = {
	name = "Dudu"
	dynasty = 1005
	religion = "babylonian_faith"
	culture = "babylonian"
	1274.1.20 = {
		birth = yes
	}
	1326.8.7 = {
		death = yes
	}
}
1006 = {
	name = "Yakmesi"
	dynasty = 1006
	religion = "babylonian_faith"
	culture = "babylonian"
	1286.1.4 = {
		birth = yes
	}
	1328.10.17 = {
		death = yes
	}
}
1007 = {
	name = "Manishtusu"
	dynasty = 1007
	religion = "babylonian_faith"
	culture = "babylonian"
	1278.4.27 = {
		birth = yes
	}
	1320.5.6 = {
		death = yes
	}
}
1008 = {
	name = "En-tarah-ana"
	dynasty = 1008
	religion = "babylonian_faith"
	culture = "babylonian"
	1272.5.23 = {
		birth = yes
	}
	1333.11.7 = {
		death = yes
	}
}
1009 = {
	name = "Adamu"
	dynasty = 1009
	religion = "babylonian_faith"
	culture = "babylonian"
	1274.9.13 = {
		birth = yes
	}
	1319.11.21 = {
		death = yes
	}
}
1010 = {
	name = "Babum"
	dynasty = 1010
	religion = "babylonian_faith"
	culture = "babylonian"
	1280.10.18 = {
		birth = yes
	}
	1322.11.23 = {
		death = yes
	}
}
1011 = {
	name = "Dadasig"
	dynasty = 1011
	religion = "babylonian_faith"
	culture = "babylonian"
	1289.10.25 = {
		birth = yes
	}
	1354.8.4 = {
		death = yes
	}
}
1161 = {
	name = "Didanu"
	dynasty = 1161
	religion = "babylonian_faith"
	culture = "babylonian"
	1288.7.10 = {
		birth = yes
	}
	1344.11.19 = {
		death = yes
	}
}
1162 = {
	name = "Mamagal"
	dynasty = 1162
	religion = "babylonian_faith"
	culture = "babylonian"
	1293.2.4 = {
		birth = yes
	}
	1343.1.24 = {
		death = yes
	}
}
1234 = {
	name = "Barsal-nuna"
	dynasty = 1234
	religion = "babylonian_faith"
	culture = "babylonian"
	1273.3.4 = {
		birth = yes
	}
	1320.8.7 = {
		death = yes
	}
}
1250 = {
	name = "Zuabu"
	dynasty = 1250
	religion = "babylonian_faith"
	culture = "babylonian"
	1288.10.14 = {
		birth = yes
	}
	1357.10.13 = {
		death = yes
	}
}
1251 = {
	name = "Etana"
	dynasty = 1251
	religion = "babylonian_faith"
	culture = "babylonian"
	1282.1.4 = {
		birth = yes
	}
	1348.1.24 = {
		death = yes
	}
}
1582 = {
	name = "Susuda"
	dynasty = 1582
	religion = "babylonian_faith"
	culture = "babylonian"
	1287.11.20 = {
		birth = yes
	}
	1341.1.10 = {
		death = yes
	}
}
1583 = {
	name = "Shar-kali-sharri"
	dynasty = 1583
	religion = "babylonian_faith"
	culture = "babylonian"
	1294.1.19 = {
		birth = yes
	}
	1362.10.9 = {
		death = yes
	}
}
1585 = {
	name = "Ashur-etil-ilani"
	dynasty = 1585
	religion = "babylonian_faith"
	culture = "babylonian"
	1290.11.6 = {
		birth = yes
	}
	1333.8.8 = {
		death = yes
	}
}
1590 = {
	name = "Ashur-shaduni"
	dynasty = 1590
	religion = "babylonian_faith"
	culture = "babylonian"
	1286.6.14 = {
		birth = yes
	}
	1331.7.7 = {
		death = yes
	}
}
1591 = {
	name = "Ninurta-apal-Ekur"
	dynasty = 1591
	religion = "babylonian_faith"
	culture = "babylonian"
	1271.7.6 = {
		birth = yes
	}
	1324.9.27 = {
		death = yes
	}
}
1595 = {
	name = "Tizqar"
	dynasty = 1595
	religion = "babylonian_faith"
	culture = "babylonian"
	1286.11.6 = {
		birth = yes
	}
	1331.3.6 = {
		death = yes
	}
}
1596 = {
	name = "Sharrum-Iter"
	dynasty = 1596
	religion = "babylonian_faith"
	culture = "babylonian"
	1293.3.9 = {
		birth = yes
	}
	1336.4.16 = {
		death = yes
	}
}
1597 = {
	name = "Nangishlishma"
	dynasty = 1597
	religion = "babylonian_faith"
	culture = "babylonian"
	1272.10.23 = {
		birth = yes
	}
	1335.6.13 = {
		death = yes
	}
}
1598 = {
	name = "Ashurnasirpal"
	dynasty = 1598
	religion = "babylonian_faith"
	culture = "babylonian"
	1286.7.16 = {
		birth = yes
	}
	1346.9.23 = {
		death = yes
	}
}
1599 = {
	name = "Ashur-uballit"
	dynasty = 1599
	religion = "babylonian_faith"
	culture = "babylonian"
	1293.11.13 = {
		birth = yes
	}
	1344.1.11 = {
		death = yes
	}
}
1600 = {
	name = "Bazi"
	dynasty = 1600
	religion = "babylonian_faith"
	culture = "babylonian"
	1288.9.11 = {
		birth = yes
	}
	1347.9.23 = {
		death = yes
	}
}
1601 = {
	name = "Dudu"
	dynasty = 1601
	religion = "babylonian_faith"
	culture = "babylonian"
	1289.1.21 = {
		birth = yes
	}
	1331.4.9 = {
		death = yes
	}
}
1603 = {
	name = "Men-nuna"
	dynasty = 1603
	religion = "babylonian_faith"
	culture = "babylonian"
	1276.5.19 = {
		birth = yes
	}
	1318.8.17 = {
		death = yes
	}
}
1604 = {
	name = "Arik-den-ili"
	dynasty = 1604
	religion = "babylonian_faith"
	culture = "babylonian"
	1270.11.4 = {
		birth = yes
	}
	1321.3.9 = {
		death = yes
	}
}
1605 = {
	name = "En-me-nuna"
	dynasty = 1605
	religion = "babylonian_faith"
	culture = "babylonian"
	1288.7.7 = {
		birth = yes
	}
	1355.4.15 = {
		death = yes
	}
}
1606 = {
	name = "Ashur-etil-ilani"
	dynasty = 1606
	religion = "babylonian_faith"
	culture = "babylonian"
	1275.6.2 = {
		birth = yes
	}
	1316.11.13 = {
		death = yes
	}
}
1607 = {
	name = "Sin-nami"
	dynasty = 1607
	religion = "babylonian_faith"
	culture = "babylonian"
	1287.7.9 = {
		birth = yes
	}
	1335.1.23 = {
		death = yes
	}
}
1608 = {
	name = "Adad-salulu"
	dynasty = 1608
	religion = "babylonian_faith"
	culture = "babylonian"
	1272.5.10 = {
		birth = yes
	}
	1320.2.17 = {
		death = yes
	}
}
1609 = {
	name = "Adasi"
	dynasty = 1609
	religion = "babylonian_faith"
	culture = "babylonian"
	1287.8.4 = {
		birth = yes
	}
	1350.7.23 = {
		death = yes
	}
}
1610 = {
	name = "Bel-bani"
	dynasty = 1610
	religion = "babylonian_faith"
	culture = "babylonian"
	1286.5.22 = {
		birth = yes
	}
	1339.4.21 = {
		death = yes
	}
}
1628 = {
	name = "Enbi-Ishtar"
	dynasty = 1628
	religion = "babylonian_faith"
	culture = "babylonian"
	1278.5.9 = {
		birth = yes
	}
	1344.5.19 = {
		death = yes
	}
}