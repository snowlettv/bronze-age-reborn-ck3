# Auto generated file, do not edit manually. Created using console command dump_bookmark_portraits
# History database id:2zidanta1
bookmark_mursili_zidanta={
	type=male
	id=1475
	age=0.260000
	genes={ 		hair_color={ 50 244 50 244 }
 		skin_color={ 119 75 119 75 }
 		eye_color={ 75 163 75 163 }
 		gene_chin_forward={ "chin_forward_neg" 122 "chin_forward_neg" 122 }
 		gene_chin_height={ "chin_height_neg" 126 "chin_height_neg" 126 }
 		gene_chin_width={ "chin_width_neg" 123 "chin_width_neg" 123 }
 		gene_eye_angle={ "eye_angle_neg" 124 "eye_angle_neg" 124 }
 		gene_eye_depth={ "eye_depth_pos" 132 "eye_depth_pos" 132 }
 		gene_eye_height={ "eye_height_pos" 135 "eye_height_pos" 135 }
 		gene_eye_distance={ "eye_distance_neg" 117 "eye_distance_neg" 117 }
 		gene_eye_shut={ "eye_shut_neg" 104 "eye_shut_neg" 104 }
 		gene_forehead_angle={ "forehead_angle_neg" 118 "forehead_angle_neg" 118 }
 		gene_forehead_brow_height={ "forehead_brow_height_pos" 135 "forehead_brow_height_pos" 135 }
 		gene_forehead_roundness={ "forehead_roundness_neg" 106 "forehead_roundness_neg" 106 }
 		gene_forehead_width={ "forehead_width_neg" 118 "forehead_width_neg" 118 }
 		gene_forehead_height={ "forehead_height_neg" 109 "forehead_height_neg" 109 }
 		gene_head_height={ "head_height_neg" 126 "head_height_neg" 126 }
 		gene_head_width={ "head_width_pos" 163 "head_width_pos" 163 }
 		gene_head_profile={ "head_profile_neg" 106 "head_profile_neg" 106 }
 		gene_head_top_height={ "head_top_height_neg" 126 "head_top_height_neg" 126 }
 		gene_head_top_width={ "head_top_width_pos" 133 "head_top_width_pos" 133 }
 		gene_jaw_angle={ "jaw_angle_pos" 148 "jaw_angle_pos" 148 }
 		gene_jaw_forward={ "jaw_forward_neg" 124 "jaw_forward_neg" 124 }
 		gene_jaw_height={ "jaw_height_pos" 127 "jaw_height_pos" 127 }
 		gene_jaw_width={ "jaw_width_neg" 88 "jaw_width_neg" 88 }
 		gene_mouth_corner_depth={ "mouth_corner_depth_neg" 116 "mouth_corner_depth_neg" 116 }
 		gene_mouth_corner_height={ "mouth_corner_height_pos" 132 "mouth_corner_height_pos" 132 }
 		gene_mouth_forward={ "mouth_forward_pos" 140 "mouth_forward_pos" 140 }
 		gene_mouth_height={ "mouth_height_neg" 114 "mouth_height_neg" 114 }
 		gene_mouth_width={ "mouth_width_neg" 124 "mouth_width_neg" 124 }
 		gene_mouth_upper_lip_size={ "mouth_upper_lip_size_pos" 151 "mouth_upper_lip_size_pos" 151 }
 		gene_mouth_lower_lip_size={ "mouth_lower_lip_size_neg" 58 "mouth_lower_lip_size_neg" 58 }
 		gene_mouth_open={ "mouth_open_neg" 123 "mouth_open_neg" 123 }
 		gene_neck_length={ "neck_length_neg" 105 "neck_length_neg" 105 }
 		gene_neck_width={ "neck_width_pos" 160 "neck_width_pos" 160 }
 		gene_bs_cheek_forward={ "cheek_forward_pos" 34 "cheek_forward_pos" 34 }
 		gene_bs_cheek_height={ "cheek_height_neg" 16 "cheek_height_neg" 16 }
 		gene_bs_cheek_width={ "cheek_width_neg" 38 "cheek_width_neg" 38 }
 		gene_bs_ear_angle={ "ear_angle_pos" 6 "ear_angle_pos" 6 }
 		gene_bs_ear_inner_shape={ "ear_inner_shape_pos" 157 "ear_inner_shape_pos" 157 }
 		gene_bs_ear_bend={ "ear_both_bend_pos" 6 "ear_both_bend_pos" 6 }
 		gene_bs_ear_outward={ "ear_outward_pos" 14 "ear_outward_pos" 14 }
 		gene_bs_ear_size={ "ear_size_neg" 18 "ear_size_neg" 18 }
 		gene_bs_eye_corner_depth={ "eye_corner_depth_neg" 1 "eye_corner_depth_neg" 1 }
 		gene_bs_eye_fold_shape={ "eye_fold_shape_neg" 111 "eye_fold_shape_neg" 111 }
 		gene_bs_eye_size={ "eye_size_neg" 85 "eye_size_neg" 85 }
 		gene_bs_eye_upper_lid_size={ "eye_upper_lid_size_neg" 4 "eye_upper_lid_size_neg" 4 }
 		gene_bs_forehead_brow_curve={ "forehead_brow_curve_pos" 197 "forehead_brow_curve_pos" 197 }
 		gene_bs_forehead_brow_forward={ "forehead_brow_forward_pos" 111 "forehead_brow_forward_pos" 111 }
 		gene_bs_forehead_brow_inner_height={ "forehead_brow_inner_height_neg" 45 "forehead_brow_inner_height_neg" 45 }
 		gene_bs_forehead_brow_outer_height={ "forehead_brow_outer_height_neg" 35 "forehead_brow_outer_height_neg" 35 }
 		gene_bs_forehead_brow_width={ "forehead_brow_width_neg" 43 "forehead_brow_width_neg" 43 }
 		gene_bs_jaw_def={ "jaw_def_neg" 32 "jaw_def_neg" 32 }
 		gene_bs_mouth_lower_lip_def={ "mouth_lower_lip_def_pos" 34 "mouth_lower_lip_def_pos" 34 }
 		gene_bs_mouth_lower_lip_full={ "mouth_lower_lip_full_pos" 63 "mouth_lower_lip_full_pos" 63 }
 		gene_bs_mouth_lower_lip_pad={ "mouth_lower_lip_pad_pos" 45 "mouth_lower_lip_pad_pos" 45 }
 		gene_bs_mouth_lower_lip_width={ "mouth_lower_lip_width_neg" 68 "mouth_lower_lip_width_neg" 68 }
 		gene_bs_mouth_philtrum_def={ "mouth_philtrum_def_pos" 28 "mouth_philtrum_def_pos" 28 }
 		gene_bs_mouth_philtrum_shape={ "mouth_philtrum_shape_neg" 0 "mouth_philtrum_shape_neg" 0 }
 		gene_bs_mouth_philtrum_width={ "mouth_philtrum_width_neg" 105 "mouth_philtrum_width_neg" 105 }
 		gene_bs_mouth_upper_lip_def={ "mouth_upper_lip_def_pos" 45 "mouth_upper_lip_def_pos" 45 }
 		gene_bs_mouth_upper_lip_full={ "mouth_upper_lip_full_pos" 61 "mouth_upper_lip_full_pos" 61 }
 		gene_bs_mouth_upper_lip_profile={ "mouth_upper_lip_profile_neg" 71 "mouth_upper_lip_profile_neg" 71 }
 		gene_bs_mouth_upper_lip_width={ "mouth_upper_lip_width_neg" 9 "mouth_upper_lip_width_neg" 9 }
 		gene_bs_nose_forward={ "nose_forward_neg" 12 "nose_forward_neg" 12 }
 		gene_bs_nose_height={ "nose_height_pos" 52 "nose_height_pos" 52 }
 		gene_bs_nose_length={ "nose_length_pos" 16 "nose_length_pos" 16 }
 		gene_bs_nose_nostril_height={ "nose_nostril_height_neg" 70 "nose_nostril_height_neg" 70 }
 		gene_bs_nose_nostril_width={ "nose_nostril_width_pos" 26 "nose_nostril_width_pos" 26 }
 		gene_bs_nose_profile={ "nose_profile_neg" 29 "nose_profile_neg" 29 }
 		gene_bs_nose_ridge_angle={ "nose_ridge_angle_pos" 17 "nose_ridge_angle_pos" 17 }
 		gene_bs_nose_ridge_width={ "nose_ridge_width_neg" 0 "nose_ridge_width_neg" 0 }
 		gene_bs_nose_size={ "nose_size_neg" 0 "nose_size_neg" 0 }
 		gene_bs_nose_tip_angle={ "nose_tip_angle_pos" 100 "nose_tip_angle_pos" 100 }
 		gene_bs_nose_tip_forward={ "nose_tip_forward_neg" 92 "nose_tip_forward_neg" 92 }
 		gene_bs_nose_tip_width={ "nose_tip_width_pos" 98 "nose_tip_width_pos" 98 }
 		face_detail_cheek_def={ "cheek_def_02" 24 "cheek_def_02" 24 }
 		face_detail_cheek_fat={ "cheek_fat_01_pos" 140 "cheek_fat_01_pos" 140 }
 		face_detail_chin_cleft={ "chin_cleft" 6 "chin_cleft" 6 }
 		face_detail_chin_def={ "chin_def" 250 "chin_def" 250 }
 		face_detail_eye_lower_lid_def={ "eye_lower_lid_def" 252 "eye_lower_lid_def" 252 }
 		face_detail_eye_socket={ "eye_socket_02" 58 "eye_socket_02" 58 }
 		face_detail_nasolabial={ "nasolabial_01" 70 "nasolabial_01" 70 }
 		face_detail_nose_ridge_def={ "nose_ridge_def_neg" 169 "nose_ridge_def_neg" 169 }
 		face_detail_nose_tip_def={ "nose_tip_def" 73 "nose_tip_def" 73 }
 		face_detail_temple_def={ "temple_def" 130 "temple_def" 130 }
 		expression_brow_wrinkles={ "brow_wrinkles_02" 22 "brow_wrinkles_02" 22 }
 		expression_eye_wrinkles={ "eye_wrinkles_01" 203 "eye_wrinkles_01" 50 }
 		expression_forehead_wrinkles={ "forehead_wrinkles_01" 41 "forehead_wrinkles_01" 41 }
 		expression_other={ "cheek_wrinkles_both_01" 0 "cheek_wrinkles_both_01" 0 }
 		complexion={ "complexion_6" 90 "complexion_6" 90 }
 		gene_height={ "normal_height" 115 "normal_height" 115 }
 		gene_bs_body_type={ "body_fat_head_fat_medium" 73 "body_fat_head_fat_medium" 119 }
 		gene_bs_body_shape={ "body_shape_average_clothed" 183 "body_shape_triangle_half" 0 }
 		gene_bs_bust={ "bust_clothes" 65 "bust_shape_1_full" 82 }
 		gene_age={ "old_3" 117 "old_3" 117 }
 		gene_eyebrows_shape={ "far_spacing_avg_thickness" 219 "far_spacing_avg_thickness" 219 }
 		gene_eyebrows_fullness={ "layer_2_lower_thickness" 156 "layer_2_lower_thickness" 156 }
 		gene_body_hair={ "body_hair_sparse" 104 "body_hair_sparse" 104 }
 		gene_hair_type={ "hair_straight" 5 "hair_straight" 5 }
 		gene_baldness={ "no_baldness" 127 "no_baldness" 127 }
 		eye_accessory={ "normal_eyes" 151 "normal_eyes" 151 }
 		teeth_accessory={ "normal_teeth" 0 "normal_teeth" 0 }
 		eyelashes_accessory={ "normal_eyelashes" 118 "normal_eyelashes" 118 }
 		pose={ "" 255 "" 0 }
 		beards={ "fp3_iranian_beards_straight" 205 "no_beard" 0 }
 		hairstyles={ "hair_luwian_hairstyles" 128 "all_hairstyles" 0 }
 		legwear={ "mena_nobility_legwear" 217 "all_legwear" 0 }
 		gene_shrink_body={ "shrink_all" 255 "" 0 }
 		gene_bs_additive_headgears={ "additive_headgears" 255 "" 0 }
 		gene_bs_cloak_offset={ "cloak_offset" 255 "" 0 }
 		gene_bs_long_beard={ "long_beard" 255 "" 0 }
 		clothes={ "fp3_iranian_high_nobility_clothes" 68 "most_clothes" 0 }
 		headgear={ "fp3_iranian_nobility" 215 "no_headgear" 0 }
 		gene_balding_hair_effect={ "no_baldness" 255 "no_baldness" 0 }
 }
	entity={ 979141817 979141817 }
	tags={ {
			hash=2125534279
			invert=no
		}
 {
			hash=681013727
			invert=no
		}
 {
			hash=2793867254
			invert=no
		}
 {
			hash=3672171019
			invert=no
		}
 {
			hash=2670398845
			invert=no
		}
 {
			hash=1500963499
			invert=no
		}
 {
			hash=1563190100
			invert=no
		}
 }
}

