﻿weapon_inspiration_average_skill_value = {
	value = martial
	add = {
		value = prowess
		multiply = 2
	}
	divide = 2
}
armor_inspiration_average_skill_value = {
	value = martial
	add = {
		value = prowess
		multiply = 2
	}
	divide = 2
}
book_inspiration_average_skill_value = {
	value = learning
	
	if = {
		limit = {
			has_variable = legendary_author
		}
		add = 1000
	}
}
weaver_inspiration_average_skill_value = {
	value = diplomacy
}
adventure_inspiration_average_skill_value = {
	value = prowess
	multiply = 3
	add = {
		value = diplomacy
		multiply = 1.5
	}
	add = {
		value = learning
		multiply = 1.5
	}
	divide = 4
}
artisan_inspiration_average_skill_value = {
	value = stewardship
}
smith_inspiration_average_skill_value = {
	value = stewardship
	multiply = 2
	add = {
		value = prowess
		multiply = 2
	}
	add = diplomacy
	divide = 4
}

alchemy_inspiration_average_skill_value = {
	value = learning
}

low_inspiration_skill = 8
medium_inspiration_skill = 12
high_inspiration_skill = 16

#This is how much var:artifact_quality needs to have been improved in events to correspond to high or medium skill.
medium_adventurer_epic_quality_level = 6
high_adventurer_epic_quality_level = 8

more_expensive_inspirations_multiplier_value = 0.5

artifact_brooch_value = {
	value = 0
	# Base score of up to 50 based on brooch quality.
	add = {
		value = scope:quality
		multiply = 0.5
	}

	# Brooches made out of certain materials gain bonus points.
	if = {
		limit = { has_artifact_feature = decoration_material_wire_gold }
		add = 25
	}
	else_if = {
		limit = { has_artifact_feature = decoration_material_wire_electrum }
		add = 15
	}
	else_if = {
		limit = { has_artifact_feature = decoration_material_wire_silver }
		add = 10
	}

	if = {
		# brooches set with gems gain up to 25 points based on the wealth/rarity of the gemstones.
		limit = { has_artifact_feature = brooch_decoration_centerpiece_and_adornment }
		add = {
			value = scope:wealth
			multiply = 0.25
		}
	}
	else_if = {
		# brooches decorating with filigree gain up to 20 points based on quality, or the artisan's skill.
		limit = { has_artifact_feature = brooch_decoration_centerpiece }
		add = {
			value = scope:quality
			multiply = 0.20
		}
	}
	else_if = {
		# brooches decorating with just an adornment gain up to 15 points based on wealth/rarity of the adornment
		limit = { has_artifact_feature = brooch_decoration_adornment }
		add = {
			value = scope:wealth
			multiply = 0.15
		}
	}
	else = {
		# Assume brooch value is just based on wealth
		add = {
			value = scope:wealth
			multiply = 0.10
		}
	}
}

artifact_wealth_quality_average_value = {
	if = {
		limit = {
			exists = var:wealth
			exists = var:quality
		}
		value = var:wealth
		add = var:quality
		multiply = 0.5
	}
	else_if = {
		limit = {
			exists = scope:wealth
			exists = scope:quality
		}
		value = scope:wealth
		add = scope:quality
		multiply = 0.5
	}
	else_if = {
		limit = {
			exists = var:quality
		}
		value = var:quality
	}
	else_if = {
		limit = {
			exists = scope:quality
		}
		value = scope:quality
	}
	else_if = {
		limit = {
			exists = var:wealth
		}
		value = var:wealth
	}
	else_if = {
		limit = {
			exists = scope:wealth
		}
		value = scope:wealth
	}
	else = {
		value = 20
	}
}

artifact_wealth_quality_combined_value = {
	value = 0
	if = {
		limit = { exists = scope:wealth }
		add = scope:wealth
	}
	if = {
		limit = { exists = scope:quality }
		add = scope:quality
	}
}


# Modifiers to decide what armor type someone wants to make/is created
artifact_armor_type_mail_weight_value = {
	value = 0 #BA CHANGED
}
artifact_armor_type_plate_weight_value = {
	value = 0
	if = {
		limit = {
			culture = { has_innovation = innovation_panoply_warrior } #BA CHANGED
		}
		add = 25
	}
}

artifact_armor_type_scale_weight_value = {
	value = 0
	 if = {
		limit = {
			culture = { has_innovation = innovation_scale_armor } #BA CHANGED
		}
		add = 25
	}  
}
artifact_armor_type_lamellar_weight_value = {
	value = 0 #BA CHANGED
}
artifact_armor_type_laminar_weight_value = {
	value = 0 #BA CHANGED
}
artifact_armor_type_brigandine_weight_value = {
	value = 0 #BA CHANGED
}

# Used to set quality and wealth for banners found after a battle
conquered_banner_value = {
	value = 10
	if = {
		limit = {
			scope:epic_loser = { highest_held_title_tier >= tier_empire }
		}
		add = 90
	}
	if = {
		limit = {
			scope:epic_loser = { highest_held_title_tier = tier_kingdom }
		}
		add = 60
	}
	if = {
		limit = {
			scope:epic_loser = { highest_held_title_tier = tier_duchy }
		}
		add = 20
	}
}

artifact_durability_percent = {
	value = artifact_durability
	divide = artifact_max_durability	
}
