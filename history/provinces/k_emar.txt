﻿#k_emar
##d_emar ###################################
###c_emar
3730 = { #Emar
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_attart_01 #Temple of 'Attart and Ba'al Hadad
	}
}
3742 = { #Meleh
	holding = church_holding
}
3731 = { #Dimo
	holding = city_holding
}
3737 = { #Kernaz
	holding = city_holding
}
3736 = { #Jalmeh
	holding = castle_holding
}
3738 = { #Laqbah
	holding = castle_holding
}
###c_yaharissa
3732 = { #Yaharissa
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3780 = { #Iktaba
	holding = city_holding
}
3776 = { #Sal'it
	holding = church_holding
}
3775 = { #Hefetz
	holding = castle_holding
}
3733 = { #Sekkim
	holding = city_holding
}
3774 = { #Anabta
	holding = castle_holding
}
###c_rawdah
3734 = { #Rawdah
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3773 = { #Labad
	holding = none
}
3772 = { #Ramin
	holding = none
}
3735 = { #Urayn
	holding = none
}
###c_ekalte
3778 = { #Ekalte
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3777 = { #Jamal
	holding = city_holding
}
3779 = { #Abush
	holding = church_holding
}
3729 = { #Araziqa
	holding = city_holding
}
##d_hermesh
###c_hermesh
3788 = { #Hermesh
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3781 = { #Ghusun
	holding = none
}
3782 = { #Zemer
	holding = none
}
3786 = { #Illar
	holding = none
}
###c_arraba
3790 = { #Arraba
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3787 = { #Seida
	holding = none
}
3791 = { #Mirka
	holding = none
}
3792 = { #Anza
	holding = none
}
###c_malal
3770 = { #Malal
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
4430 = { #Apamu
	holding = none
}
3739 = { #Anbura
	holding = none
}
3771 = { #Safarin
	holding = none
}
###c_zoran
3767 = { #Zoran
	culture = amoritic
	religion = amorite_faith
	holding = tribal_holding
}
3741 = { #Asilah
	holding = none
}
4001 = { #Temara
	holding = none
}
3769 = { #Adanim
	holding = none
}
3740 = { #Mahrusah
	holding = none
}
##d_dub
###c_dub
3744 = { #Tuba
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3749 = { #Marjaba
	holding = city_holding
}
3743 = { #Tremseh
	holding = none
}
3754 = { #Sfayleh
	holding = none
}
3750 = { #Jouar
	holding = none
}
###c_houz
3756 = { #Houz
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3752 = { #Qach
	holding = city_holding
}
3753 = { #Hemlaya
	holding = church_holding
}
3751 = { #Mrouj
	holding = city_holding
}
###c_eliyahu
3761 = { #Eliyahu
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3764 = { #Tirah
	holding = city_holding
}
3762 = { #Qalqiya
	holding = church_holding
}
3759 = { #Ya'ir
	holding = city_holding
}
3766 = { #Vered
	holding = castle_holding
}
##d_azu
###c_azu
3728 = { #Azû
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
}
3746 = { #Kfartayeh
	holding = none
}
3727 = { #Qren
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
4434 = { #Reham
	holding = church_holding
}
4435 = { #Jabria
	holding = city_holding
}
###c_basiru
4436 = { #Basîru
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = harrani
		religion = western_hurrian_faith
	}
}
3116 = { #Sherqu
	holding = none
}
3185 = { #Kattû
	holding = none
}
4437 = { #Surun
	holding = city_holding
}
4438 = { #Natkina
	holding = church_holding
}
3726 = { #Kanfo
	holding = city_holding
}
###c_pituru
3725 = { #Pitûru
	culture = amoritic
	religion = amorite_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = harrani
		religion = western_hurrian_faith
	}
}
3748 = { #Mizan
	holding = city_holding
}
3747 = { #Qabou
	holding = church_holding
}
3724 = { #Eljem
	holding = city_holding
}
3745 = { #Chbouq
	holding = castle_holding
}