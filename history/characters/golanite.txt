﻿#Generated
66 = {
	name = "Ittoba_al"
	dynasty = 66
	religion = "canaanite_faith"
	culture = "golanite"
	756.6.5 = {
		birth = yes
	}
	811.5.17 = {
		death = yes
	}
}
76 = {
	name = "Carthalo"
	dynasty = 76
	religion = "canaanite_faith"
	culture = "golanite"
	757.6.9 = {
		birth = yes
	}
	813.2.16 = {
		death = yes
	}
}
81 = {
	name = "Baltoros"
	dynasty = 81
	religion = "canaanite_faith"
	culture = "golanite"
	760.11.4 = {
		birth = yes
	}
	815.5.5 = {
		death = yes
	}
}
82 = {
	name = "Bomical"
	dynasty = 82
	religion = "canaanite_faith"
	culture = "golanite"
	758.10.22 = {
		birth = yes
	}
	801.10.25 = {
		death = yes
	}
}
686 = {
	name = "Zimrida"
	dynasty = 686
	religion = "canaanite_faith"
	culture = "golanite"
	764.3.14 = {
		birth = yes
	}
	805.3.25 = {
		death = yes
	}
}
687 = {
	name = "Gerimilki"
	dynasty = 687
	religion = "canaanite_faith"
	culture = "golanite"
	751.7.3 = {
		birth = yes
	}
	812.6.11 = {
		death = yes
	}
}
1042 = {
	name = "Milkiram"
	dynasty = 1042
	religion = "canaanite_faith"
	culture = "golanite"
	1274.3.27 = {
		birth = yes
	}
	1330.10.9 = {
		death = yes
	}
}
1048 = {
	name = "Yanud-Lim"
	dynasty = 1048
	religion = "canaanite_faith"
	culture = "golanite"
	1285.8.27 = {
		birth = yes
	}
	1348.6.25 = {
		death = yes
	}
}
1052 = {
	name = "Yehimlk"
	dynasty = 1052
	religion = "canaanite_faith"
	culture = "golanite"
	1285.9.23 = {
		birth = yes
	}
	1327.5.21 = {
		death = yes
	}
}
1053 = {
	name = "Silli-Dagan"
	dynasty = 1053
	religion = "canaanite_faith"
	culture = "golanite"
	1270.10.23 = {
		birth = yes
	}
	1335.7.18 = {
		death = yes
	}
}
1543 = {
	name = "Sikarbaal"
	dynasty = 1543
	religion = "canaanite_faith"
	culture = "golanite"
	1270.3.10 = {
		birth = yes
	}
	1327.11.21 = {
		death = yes
	}
}
1544 = {
	name = "Hiempsal"
	dynasty = 1544
	religion = "canaanite_faith"
	culture = "golanite"
	1291.6.24 = {
		birth = yes
	}
	1332.7.18 = {
		death = yes
	}
}