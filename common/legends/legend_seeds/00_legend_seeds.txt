﻿# All start dates
king_arthur = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = king_arthur_legend
		}
		always = no
	}
	is_valid = {
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

carolingian = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = carolingian_legend
		}
		always = no
	}
	is_valid = {
		always = no
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

sons_of_david = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = sons_of_david_legend
		}
		always = no
	}
	is_valid = {
		always = no
		prestige_level >= medium_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:shasu
		title = title:k_lower_egypt
		original_region = geographical_region:world_middle_east_jerusalem
	}
}

the_wheelwright = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = the_wheelwright_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

vercingetorix = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = vercingetorix_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory_not_in_history
	chronicle_properties = {
		ancestor_flag = flag:vercingetorix
		title = title:k_lower_egypt
	}
}

saman_khudah = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = saman_khudah_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		prestige_level >= very_high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

bahram_gur = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = bahram_gur_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		prestige_level >= very_high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

cadell = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = cadell_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

descendants_of_brahman = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = descendants_of_brahman_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		piety_level >= very_high_piety_level
	}

	chronicle = godly_descent
	chronicle_properties = {
		god = flag:hinduism_high_god_name
		title = title:k_lower_egypt
	}
}

gothic_kings = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = gothic_kings_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:proto_iranian
		title = title:k_lower_egypt
		original_region = geographical_region:world_steppe_west
	}
}

sons_of_rurik = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = sons_of_rurik_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= medium_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

holy_warrior = { # Generico Crusader legend
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		faith = {
			OR = {
				has_doctrine_parameter = great_holy_wars_active
				has_doctrine_parameter = great_holy_wars_active_if_reformed
			}
		}
		has_trait = crusader_king
	}
	is_valid = {
		has_trait = crusader_king
	}

	chronicle = holy_warrior
	chronicle_properties = {
		ancestor = root
		religion = root.religion
	}
}

holy_site = { # Holy Site
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		is_landed = yes
		faith = {
			any_holy_site = {
				OR = {
					county.holder = root
					county.holder.top_liege = root
				}
			}
		}
	}
	is_valid = {
		piety_level >= very_high_piety_level
	}

	chronicle = saintly_location
	chronicle_properties = {
		location = root.location
		religion = root.religion
		faith = root.faith
	}
}

# 867 start date
el_cid = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = el_cid_legend
		}
		always = no
	}
	is_valid = {
		always = no
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

alfred_of_wessex = {
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = alfred_of_wessex_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		piety_level >= high_piety_level
		always = no
	}

	chronicle = saintly_deed
	chronicle_properties = {
		ancestor = character:5Unas
		religion = religion:judaism_religion
	}
}

peasant_emperor = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = peasant_emperor_legend
		}
		always = no
	}
	is_valid = {
		always = no
		prestige_level >= medium_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}


# 1066 start date
william_gellones = {
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = william_gellones_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_duchy
		custom_tooltip = {
			text = theologian_or_high_piety_tt
			OR = {
				has_trait = theologian
				piety_level >= high_piety_level
			}
		}
	}

	chronicle = saintly_deed
	chronicle_properties = {
		ancestor = character:5Unas
		religion = religion:judaism_religion
	}
}

edward_the_martyr = {
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = edward_the_martyr_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_duchy
		custom_tooltip = {
			text = theologian_or_high_piety_tt
			OR = {
				has_trait = theologian
				piety_level >= high_piety_level
			}
		}
	}

	chronicle = saintly_deed
	chronicle_properties = {
		ancestor = character:5Unas
		religion = religion:judaism_religion
	}
}

yazdagird_iii = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = yazdagird_iii_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

afrasiyab = {
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = afrasiyab_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory_not_in_history
	chronicle_properties = {
		ancestor_flag = flag:afrasiyab
		title = title:k_lower_egypt
	}
}

sceafa = { # Claims to be descended from Noah
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = sceafa_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		piety_level >= high_piety_level
	}

	chronicle = saintly_descent
	chronicle_properties = {
		ancestor_flag = flag:noah # this felt very weird to type out
		religion = root.religion
	}
}

new_troy_london = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = new_troy_london_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:wilusan
		title = title:k_lower_egypt
		original_region = geographical_region:troy_region
	}
}

corineus = {
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = corineus_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		prestige_level >= very_high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:wilusan
		title = title:k_lower_egypt
		original_region = geographical_region:troy_region
	}
}

scota = { # Scots claiming ancestry from Ancient Egypt
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = scota_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		prestige_level >= high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:upper_egyptian
		title = title:k_lower_egypt
		original_region = geographical_region:ghw_region_egypt_et_al
	}
}

pyusawhti = { # Burmese legend claiming ancestry from a dragon goddess
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = pyusawhti_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = mythical_descent
	chronicle_properties = {
		beast = flag:dragon
	}
}

menelik_i = { # Supposed son of Solomon and the Queen of Sheba
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = menelik_i_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		piety_level >= very_high_piety_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:shasu
		title = title:k_lower_egypt
		original_region = geographical_region:ghw_region_jerusalem
	}
}

kings_of_semien = { # Ancestry to the Danites of Israel, and mythical kings of Semien
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = kings_of_semien_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		piety_level >= very_high_piety_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:shasu
		title = title:k_lower_egypt
		original_region = geographical_region:israel_region
	}
}

roman_heritage = { # Erm, my family are *actually* Roman nobles erm
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = roman_heritage_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:sumerian
		title = title:e_akkad
		original_region = geographical_region:custom_roman_full_borders
	}
}

hunnic_heritage = { # MY nomadic people *totally* are the Huns guys
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = hunnic_heritage_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancient_people
	chronicle_properties = {
		culture = culture:indo_aryan
		title = title:e_akkad
		original_region = geographical_region:world_steppe_west
	}
}

premysl = { # Descent from Premysl of Bohemia
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = premysl_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_duchy
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

borjigin = { # Alan Gua was impregnated by a ray of light
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = borjigin_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		highest_held_title_tier >= tier_duchy
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

naga_descent = { # Kashmiri kings claim descent from Naga (divine snake people or "sneople" for short)
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = naga_descent_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		piety_level >= high_piety_level
	}

	chronicle = mythical_descent
	chronicle_properties = {
		beast = flag:naga
	}
}

hieros_gamos_skirnismal = { # Fjolnir Yngling descended from Freyr and Gerdr
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = hieros_gamos_skirnismal_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		prestige_level >= high_prestige_level
	}

	chronicle = godly_descent
	chronicle_properties = {
		god = flag:germanic_high_god_name
		title = title:k_lower_egypt
	}
}

shibi_chakravarti = { # Chola claiming descent from the legendary king Shibi
	type = holy
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = shibi_chakravarti_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		piety_level >= very_high_piety_level
	}

	chronicle = saintly_descent
	chronicle_properties = {
		ancestor_flag = flag:shibi_chakravarti
		religion = root.religion
	}
}

the_red_hand = { # He who first lays hand on Ulster shall be its King
	type = legitimizing
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = the_red_hand_legend
		}
		always = no
	}
	is_valid = {
		is_playable_character = yes
		always = no
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

ragnarr = { # Ol Raggy S
	type = heroic
	quality = famed
	is_shown = {
		has_dlc_feature = legends
		NOT = {
			has_global_variable = ragnarr_legend
		}
		always = no
	}
	is_valid = {
		is_landed = yes
		highest_held_title_tier >= tier_kingdom
		prestige_level >= high_prestige_level
	}

	chronicle = ancestral_glory
	chronicle_properties = {
		ancestor = character:5Unas
		title = title:k_lower_egypt
	}
}

#Dynasty perk
ce1_heroic_legacy_1 = {
	type = legitimizing
	quality = famed
	is_shown = {
		dynasty = {
			has_dynasty_perk = ce1_heroic_legacy_1
			NOT = {
				exists = var:dynasty_legend_used
			}
		}
	}
	is_valid = {
		is_landed = yes
		OR = {
			is_ai = no
			# Prevent the AI from sniping the seed from the player
			AND = {
				is_ai = yes
				NOT = {
					dynasty = { any_dynasty_member = { is_ai = no } }
				}
			}
		}
	}
	chronicle = great_deed_dynasty
	chronicle_properties = {
		founder = house.house_founder
		dynasty = dynasty
		title = primary_title
	}
}
