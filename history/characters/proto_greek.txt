﻿#Generated
180 = {
	name = "Kosouphos"
	dynasty = 180
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	767.3.16 = {
		birth = yes
	}
	822.5.10 = {
		death = yes
	}
}
183 = {
	name = "Alektruon"
	dynasty = 183
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	757.7.21 = {
		birth = yes
	}
	802.11.27 = {
		death = yes
	}
}
184 = {
	name = "Kutheros"
	dynasty = 184
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	750.1.10 = {
		birth = yes
	}
	807.2.11 = {
		death = yes
	}
}
185 = {
	name = "Dexelawos"
	dynasty = 185
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	769.7.17 = {
		birth = yes
	}
	818.11.21 = {
		death = yes
	}
}
186 = {
	name = "Lakadanor"
	dynasty = 186
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	758.7.6 = {
		birth = yes
	}
	801.3.11 = {
		death = yes
	}
}
189 = {
	name = "Eunawos"
	dynasty = 189
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	757.8.8 = {
		birth = yes
	}
	815.3.9 = {
		death = yes
	}
}
191 = {
	name = "Atukhos"
	dynasty = 191
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	756.8.7 = {
		birth = yes
	}
	810.7.1 = {
		death = yes
	}
}
194 = {
	name = "Wadus"
	dynasty = 194
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	754.3.2 = {
		birth = yes
	}
	808.9.8 = {
		death = yes
	}
}
195 = {
	name = "Lukios"
	dynasty = 195
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	756.11.9 = {
		birth = yes
	}
	821.1.22 = {
		death = yes
	}
}
196 = {
	name = "Tylisios"
	dynasty = 196
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	749.3.6 = {
		birth = yes
	}
	795.2.13 = {
		death = yes
	}
}
197 = {
	name = "Ortinawos"
	dynasty = 197
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	753.2.27 = {
		birth = yes
	}
	822.3.4 = {
		death = yes
	}
}
201 = {
	name = "Iwasos"
	dynasty = 201
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	762.8.9 = {
		birth = yes
	}
	818.3.14 = {
		death = yes
	}
}
215 = {
	name = "Tomarkos"
	dynasty = 215
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	763.5.19 = {
		birth = yes
	}
	822.6.9 = {
		death = yes
	}
}
216 = {
	name = "Aetas"
	dynasty = 216
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	753.4.1 = {
		birth = yes
	}
	803.5.21 = {
		death = yes
	}
}
217 = {
	name = "Metanor"
	dynasty = 217
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	762.8.26 = {
		birth = yes
	}
	822.11.18 = {
		death = yes
	}
}
220 = {
	name = "Eurydamos"
	dynasty = 220
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	766.2.15 = {
		birth = yes
	}
	827.8.15 = {
		death = yes
	}
}
221 = {
	name = "Naputios"
	dynasty = 221
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	754.1.23 = {
		birth = yes
	}
	817.2.21 = {
		death = yes
	}
}
222 = {
	name = "Souphos"
	dynasty = 222
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	752.5.12 = {
		birth = yes
	}
	816.1.25 = {
		death = yes
	}
}
225 = {
	name = "Atukhos"
	dynasty = 225
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	769.9.1 = {
		birth = yes
	}
	822.6.7 = {
		death = yes
	}
}
228 = {
	name = "Dolikhanor"
	dynasty = 228
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	756.2.1 = {
		birth = yes
	}
	809.11.24 = {
		death = yes
	}
}
525 = {
	name = "Makhatas"
	dynasty = 525
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	749.6.4 = {
		birth = yes
	}
	814.2.2 = {
		death = yes
	}
}
526 = {
	name = "Pontios"
	dynasty = 526
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	769.8.9 = {
		birth = yes
	}
	815.10.23 = {
		death = yes
	}
}
527 = {
	name = "Arnumenos"
	dynasty = 527
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	748.11.1 = {
		birth = yes
	}
	804.8.5 = {
		death = yes
	}
}
528 = {
	name = "Dektos"
	dynasty = 528
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	769.1.19 = {
		birth = yes
	}
	818.5.5 = {
		death = yes
	}
}
529 = {
	name = "Amphiarewos"
	dynasty = 529
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	767.1.14 = {
		birth = yes
	}
	816.5.7 = {
		death = yes
	}
}
530 = {
	name = "Wadus"
	dynasty = 530
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	754.8.19 = {
		birth = yes
	}
	810.2.25 = {
		death = yes
	}
}
531 = {
	name = "Opheltas"
	dynasty = 531
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	762.4.22 = {
		birth = yes
	}
	809.10.12 = {
		death = yes
	}
}
532 = {
	name = "Alektruon"
	dynasty = 532
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	759.2.22 = {
		birth = yes
	}
	815.4.9 = {
		death = yes
	}
}
533 = {
	name = "Aupnos"
	dynasty = 533
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	749.9.17 = {
		birth = yes
	}
	797.11.27 = {
		death = yes
	}
}
534 = {
	name = "Eugoros"
	dynasty = 534
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	762.10.3 = {
		birth = yes
	}
	830.11.2 = {
		death = yes
	}
}
535 = {
	name = "Eurydamos"
	dynasty = 535
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	758.8.9 = {
		birth = yes
	}
	801.10.5 = {
		death = yes
	}
}
536 = {
	name = "Amphidoros"
	dynasty = 536
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	748.7.22 = {
		birth = yes
	}
	792.10.10 = {
		death = yes
	}
}
537 = {
	name = "Eupharos"
	dynasty = 537
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	754.9.5 = {
		birth = yes
	}
	802.8.10 = {
		death = yes
	}
}
538 = {
	name = "Kullanos"
	dynasty = 538
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	754.4.20 = {
		birth = yes
	}
	801.2.19 = {
		death = yes
	}
}
539 = {
	name = "Etewoklewes"
	dynasty = 539
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	760.1.21 = {
		birth = yes
	}
	822.10.9 = {
		death = yes
	}
}
540 = {
	name = "Agroquolos"
	dynasty = 540
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	765.9.10 = {
		birth = yes
	}
	827.8.5 = {
		death = yes
	}
}
541 = {
	name = "Euporos"
	dynasty = 541
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	747.6.8 = {
		birth = yes
	}
	806.5.20 = {
		death = yes
	}
}
542 = {
	name = "Agroquolos"
	dynasty = 542
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	758.1.25 = {
		birth = yes
	}
	805.11.6 = {
		death = yes
	}
}
543 = {
	name = "Arnumenos"
	dynasty = 543
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	765.5.4 = {
		birth = yes
	}
	827.7.17 = {
		death = yes
	}
}
544 = {
	name = "Iskhuodotos"
	dynasty = 544
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	753.4.16 = {
		birth = yes
	}
	812.5.20 = {
		death = yes
	}
}
545 = {
	name = "Atukhos"
	dynasty = 545
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	755.5.22 = {
		birth = yes
	}
	809.4.3 = {
		death = yes
	}
}
546 = {
	name = "Amphialos"
	dynasty = 546
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	745.7.2 = {
		birth = yes
	}
	814.2.7 = {
		death = yes
	}
}
547 = {
	name = "Metanor"
	dynasty = 547
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	746.9.26 = {
		birth = yes
	}
	787.8.2 = {
		death = yes
	}
}
548 = {
	name = "Amphialawos"
	dynasty = 548
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	764.5.5 = {
		birth = yes
	}
	821.11.12 = {
		death = yes
	}
}
549 = {
	name = "Iwasos"
	dynasty = 549
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	749.1.5 = {
		birth = yes
	}
	810.5.10 = {
		death = yes
	}
}
550 = {
	name = "Kasos"
	dynasty = 550
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	759.6.2 = {
		birth = yes
	}
	816.3.1 = {
		death = yes
	}
}
551 = {
	name = "Alxanor"
	dynasty = 551
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	767.1.21 = {
		birth = yes
	}
	831.3.8 = {
		death = yes
	}
}
552 = {
	name = "Opilimnios"
	dynasty = 552
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	748.10.8 = {
		birth = yes
	}
	815.8.22 = {
		death = yes
	}
}
553 = {
	name = "Widwoios"
	dynasty = 553
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	761.4.18 = {
		birth = yes
	}
	816.10.2 = {
		death = yes
	}
}
554 = {
	name = "Arnumenos"
	dynasty = 554
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	758.10.13 = {
		birth = yes
	}
	810.4.6 = {
		death = yes
	}
}
555 = {
	name = "Eugoros"
	dynasty = 555
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	768.4.6 = {
		birth = yes
	}
	829.5.19 = {
		death = yes
	}
}
556 = {
	name = "Widwoios"
	dynasty = 556
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	749.6.15 = {
		birth = yes
	}
	816.2.18 = {
		death = yes
	}
}
557 = {
	name = "Ortinawos"
	dynasty = 557
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	752.11.6 = {
		birth = yes
	}
	818.5.3 = {
		death = yes
	}
}
558 = {
	name = "Simos"
	dynasty = 558
	religion = "proto_hellenic_faith"
	culture = "proto_greek"
	748.3.11 = {
		birth = yes
	}
	799.1.3 = {
		death = yes
	}
}