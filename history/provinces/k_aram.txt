﻿#k_aram
##d_damascus_oasis ###################################
###c_damascus
4030 = { #Dammâseq
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
	
	785.1.1 = {
		special_building_slot = temple_of_hadad_01 #Temple of Hadad-Ramman
	}
}
4025 = { #Ein Al-Fijeh
	holding = none
}
4026 = { #Qudssaya
	holding = none
}
4031 = { #Maarba
	holding = city_holding
}
4033 = { #Duma
	holding = church_holding
}
4034 = { #'Irbîn
	holding = none
}
###c_erneh
4029 = { #Erneh
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
	
	785.1.1 = {
		special_building_slot = temple_mount_hermon_01 #Mount Hermon Sanctuary
	}
}
4027 = { #Jandal
	holding = none
}
4028 = { #Beqaseem
	holding = none
}
###c_fijeh_spring
3944 = { #Barada Spring
	culture = aramean
	religion = aramean_faith
	holding = castle_holding

	785.1.1 = {
		special_building_slot = barada_spring_01
		special_building = barada_spring_01
	}
}
3945 = { #Rawda
	holding = city_holding
}
3946 = { #Hâmeh
	holding = church_holding
}
3947 = { #Dar Kanon
	holding = city_holding
}
3942 = { #Serghaya
	holding = none
}
###c_hadara
4032 = { #Hadara
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
}
4044 = { #Rakniika
	holding = none
}
4042 = { #Adra
	holding = city_holding
}
###c_nashabiyah
4037 = { #Nashabiyah
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
}
4325 = { #Tulahit
	holding = none
}
4036 = { #Bilaliyah
	holding = none
}
###c_zabdean
4035 = { #Zabdean
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
}
4075 = { #Kiswah
	holding = none
}
4076 = { #Adleyeh
	holding = none
}
4038 = { #Dârayyâ
	holding = city_holding
}
4039 = { #Artooz
	holding = none
}
##d_yabrudu
###c_yabrudu
4019 = { #Yabrûdu
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
}
4021 = { #An Nabk
	holding = none
}
4022 = { #Qarah
	holding = none
}
4020 = { #Alward
	holding = none
}
###c_qutayfah
4043 = { #Qutayfah
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
}
4018 = { #Ma'loula
	holding = none
}
4017 = { #Sednayah
	holding = none
}
4015 = { #Qurasiti
	holding = none
}
##d_orontes_source
###c_orontes_source
3922 = { #Hazi
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
}
3925 = { #Younine
	holding = none
}
3921 = { #Safra
	holding = none
}
3923 = { #Nahleh
	holding = none
}
###c_hermel
3917 = { #Hermel
	culture = aramean
	religion = aramean_faith
	holding = tribal_holding
}
3916 = { #Charbine
	holding = none
}
3918 = { #Wadi Faara
	holding = none
}
3929 = { #Fakeha
	holding = none
}
3927 = { #Laboueh
	holding = none
}
##d_hazabu
###c_hazabu
3950 = { #Hasabu
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
}
3941 = { #Baalbek
	holding = city_holding
	
	785.1.1 = {
		special_building_slot = temple_of_baal_baalbek_01 #Temple of Ba'al of Baalbek
	}
}
3939 = { #Bodai
	holding = none
}
###c_kumidu
3968 = { #Kumidu
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
}
3953 = { #Sôbâh
	holding = none
}
3951 = { #Chamsine
	holding = city_holding
}
3954 = { #Tebah
	holding = none
} 
3960 = { #Kûn
	holding = none
}
###c_huzaza
3961 = { #Huzaza
	culture = aramean
	religion = aramean_faith
	holding = castle_holding
}
3956 = { #Kherbet
	holding = none
}
3967 = { #Bakka
	holding = none
}
3966 = { #Danabu
	holding = none
}
3963 = { #Yohmor
	holding = city_holding
}