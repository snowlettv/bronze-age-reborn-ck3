﻿k_yehudah = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
####
d_gaza = {
	1310.1.1 = {
		holder = 1028
	}
	1311.1.1 = {
		holder = 0
	}
}
c_gaza = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 50
	}
	1310.1.1 = {
		holder = 1028
	}
}
c_rapihu = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 50
	}
	1310.1.1 = {
		holder = 1028
	}
}
c_nerbut = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		holder = 652
	}
	1310.1.1 = {
		liege = "d_gaza"
		holder = 1511
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_ashkalon
c_ashkalon = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 50
	}
	1310.1.1 = {
		holder = 1028
	}
}
c_ashdod = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 52
	}
	1310.1.1 = {
		holder = 1030
	}
}
####
#d_jerusalem
c_jerusalem = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 653
	}
	1310.1.1 = {
		liege = "d_lakish"
		holder = 1512
	}
	1311.1.1 = {
		liege = 0
	}
}
c_western_dead_sea = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 654
	}
	1310.1.1 = {
		holder = 1056
	}
}
c_gezer = {
	785.1.1 = {
		liege = "d_yapo"
		holder = 655
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_yapo"
		holder = 1513
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_lakish = {
	1310.1.1 = {
		holder = 1040
	}
	1311.1.1 = {
		holder = 0
	}
}
c_lakish = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 63
	}
	1310.1.1 = {
		holder = 1040
	}
}
c_hebron = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 64
	}
	1310.1.1 = {
		holder = 1040
	}
}
c_tidhar = {
	785.1.1 = {
		holder = 63
	}
	1310.1.1 = {
		holder = 1040
	}
}
####
d_simon = {
	1310.1.1 = {
		holder = 1041
	}
	1311.1.1 = {
		holder = 0
	}
}
c_simon = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 65
	}
	1310.1.1 = {
		holder = 1041
	}
}
c_ramotnegeb = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 65
	}
	1310.1.1 = {
		holder = 1041
	}
}
c_amaleq = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 656
	}
	1310.1.1 = {
		liege = "d_simon"
		holder = 1514
	}
	1311.1.1 = {
		liege = 0
	}
}
c_arad = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 64
	}
	1310.1.1 = {
		holder = 1056
	}
}
####
k_samaria = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
####
d_yapo = {
	785.1.1 = {
		holder = 52
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1030
	}
	1311.1.1 = {
		holder = 0
	}
}
c_yapo = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 52
	}
	1310.1.1 = {
		holder = 1030
	}
}
c_sokoh = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 51
	}
	1310.1.1 = {
		holder = 1029
	}
}
####
d_jericho = {
	1310.1.1 = {
		holder = 1031
	}
	1311.1.1 = {
		holder = 0
	}
}
c_jericho = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 53
	}
	1310.1.1 = {
		holder = 1031
	}
}
c_saratan = {
	785.1.1 = {
		holder = 53
	}
	1310.1.1 = {
		holder = 1031
	}
}
c_jordan = {
	785.1.1 = {
		holder = 657
	}
	1310.1.1 = {
		holder = 1515
	}
}
####
d_megiddo = {
	1310.1.1 = {
		holder = 1027
	}
	1311.1.1 = {
		holder = 0
	}
}
c_megiddo = {
	785.1.1 = {
		holder = 48
	}
	1310.1.1 = {
		holder = 1027
	}
}
c_yizrael = {
	785.1.1 = {
		holder = 77
	}
	1310.1.1 = {
		liege = "d_megiddo"
		holder = 1516
	}
	1311.1.1 = {
		liege = 0
	}
}
c_taanak = {
	785.1.1 = {
		holder = 48
	}
	1310.1.1 = {
		holder = 1027
	}
}
####
#d_eprayim
c_siloh = {
	785.1.1 = {
		holder = 62
	}
	1310.1.1 = {
		holder = 1038
	}
}
c_rimmon = {
	785.1.1 = {
		holder = 62
	}
	1310.1.1 = {
		liege = "d_jericho"
		holder = 1517
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ebenezer = {
	785.1.1 = {
		liege = "d_yapo"
		holder = 658
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_samaria"
		holder = 1518
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_dor
c_dor = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 51
	}
	1310.1.1 = {
		holder = 1029
	}
}
c_menasseh = {
	785.1.1 = {
		holder = 51
	}
	1310.1.1 = {
		holder = 1029
	}
}
####
d_samaria = {
	785.1.1 = {
		holder = 60
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1038
	}
	1311.1.1 = {
		holder = 0
	}
}
c_samaria = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 60
	}
	1310.1.1 = {
		holder = 1038
	}
}
c_sekem = {
	785.1.1 = {
		holder = 60
	}
	1310.1.1 = {
		holder = 1038
	}
}
####
k_ammon = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
####
d_qamon = {
	1310.1.1 = {
		holder = 1036
	}
	1311.1.1 = {
		holder = 0
	}
}
c_qamon = {
	785.1.1 = {
		holder = 58
	}
	1310.1.1 = {
		holder = 1036
	}
}
c_abelmaholah = {
	785.1.1 = {
		holder = 58
	}
	1310.1.1 = {
		holder = 1036
	}
}
c_sama = {
	785.1.1 = {
		holder = 659
	}
	1310.1.1 = {
		liege = "d_qamon"
		holder = 1519
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_gilad = {
	785.1.1 = {
		holder = 57
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1035
	}
	1311.1.1 = {
		holder = 0
	}
}
c_gilad = {
	785.1.1 = {
		holder = 57
	}
	1310.1.1 = {
		holder = 1035
	}
}
c_edrei = {
	785.1.1 = {
		holder = 57
	}
	1310.1.1 = {
		holder = 1035
	}
}
c_metuna = {
	785.1.1 = {
		holder = 57
	}
	1310.1.1 = {
		holder = 1035
	}
}
####
d_mahanayim = {
	785.1.1 = {
		holder = 56
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1034
	}
	1311.1.1 = {
		holder = 0
	}
}
c_mahanayim = {
	785.1.1 = {
		holder = 56
	}
	1310.1.1 = {
		holder = 1034
	}
}
c_sukkot = {
	785.1.1 = {
		holder = 56
	}
	1310.1.1 = {
		holder = 1034
	}
}
####
d_rabbatammon = {
	785.1.1 = {
		holder = 54
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1032
	}
	1311.1.1 = {
		holder = 0
	}
}
c_rabbatammon = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 54
	}
	1310.1.1 = {
		holder = 1032
	}
}
c_sittim = {
	785.1.1 = {
		holder = 660
	}
	1310.1.1 = {
		liege = "d_mahanayim"
		holder = 1520
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hesbon = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 54
	}
	1310.1.1 = {
		holder = 1032
	}
}
c_abelkaramim = {
	785.1.1 = {
		holder = 54
	}
	1310.1.1 = {
		holder = 1032
	}
}
####
k_moab = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
}
####
d_dibon = {
	785.1.1 = {
		holder = 55
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1033
	}
	1311.1.1 = {
		holder = 0
	}
}
c_dibon = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 55
	}
	1310.1.1 = {
		holder = 1033
	}
}
c_atarot = {
	785.1.1 = {
		holder = 55
	}
	1310.1.1 = {
		holder = 1033
	}
}
c_meypaat = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		liege = "d_dibon"
		holder = 661
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_dibon"
		holder = 1521
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_ar = {
	785.1.1 = {
		holder = 45
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1024
	}
	1311.1.1 = {
		holder = 0
	}
}
c_ar = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 45
	}
	1310.1.1 = {
		holder = 1024
	}
}
c_eglat = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_ar"
		holder = 662
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_ar"
		holder = 1522
	}
	1311.1.1 = {
		liege = 0
	}
}
c_balu = {
	785.1.1 = {
		holder = 45
	}
	1310.1.1 = {
		holder = 1024
	}
}
####
#d_horonayim
c_horonayim = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 664
	}
	1310.1.1 = {
		holder = 1524
	}
}
c_qatranan = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		holder = 663
	}
	1310.1.1 = {
		liege = "d_ar"
		holder = 1523
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_edom = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
}
####
#d_soar
c_soar = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 46
	}
	1310.1.1 = {
		holder = 1025
	}
}
c_luhit = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 46
	}
	1310.1.1 = {
		holder = 1025
	}
}
c_ibi = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
	785.1.1 = {
		holder = 665
	}
	1310.1.1 = {
		holder = 1525
	}
}
####
d_nabatea = {
	785.1.1 = { change_development_level = 1 }
	1310.1.1 = { change_development_level = 5 }
}
c_herui = {
	785.1.1 = {
		holder = 61
	}
	1310.1.1 = {
		holder = 1039
	}
}
c_ubti = {
	785.1.1 = {
		holder = 666
	}
	1310.1.1 = {
		holder = 1526
	}
}
c_resit = {
	785.1.1 = {
		holder = 667
	}
	1310.1.1 = {
		holder = 1527
	}
}
c_amseth = {
	785.1.1 = {
		holder = 61
	}
	1310.1.1 = {
		holder = 1039
	}
}
####
#d_petra
c_petra = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 668
	}
	1310.1.1 = {
		holder = 1528
	}
}
c_maan = {
	785.1.1 = {
		holder = 669
	}
	1310.1.1 = {
		holder = 1529
	}
}
####
d_elath = {
	785.1.1 = {
		holder = 84
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1055
	}
	1311.1.1 = {
		holder = 0
	}
}
c_elath = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 84
	}
	1310.1.1 = {
		holder = 1055
	}
}
b_4396 = {
	1310.1.1 = {
		holder = 1055
	}
}
c_wadirum = {
	785.1.1 = {
		liege = "d_elath"
		holder = 670
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_elath"
		holder = 1530
	}
	1311.1.1 = {
		liege = 0
	}
}
c_shitim = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_elath"
		holder = 671
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		holder = 1055
	}
}
####
k_sinai = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
}
#d_land_of_turquoise
c_khadem = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 42
	}
	1310.1.1 = {
		holder = 1020
	}
}
c_magarah = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 43
	}
	1310.1.1 = {
		holder = 1021
	}
}
c_ashep = {
	785.1.1 = {
		holder = 42
	}
	1310.1.1 = {
		holder = 1020
	}
}
####
#d_south_sinai
c_mestpekh = {
	785.1.1 = {
		holder = 44
	}
	1310.1.1 = {
		holder = 1022
	}
}
c_nubnub = {
	785.1.1 = {
		holder = 43
	}
	1310.1.1 = {
		holder = 1021
	}
}
c_makhait = {
	785.1.1 = {
		holder = 44
	}
	1310.1.1 = {
		holder = 1022
	}
}
####
#d_east_sinai
c_merrt = {
	785.1.1 = {
		holder = 672
	}
	1310.1.1 = {
		holder = 1531
	}
}
c_ubekh = {
	785.1.1 = {
		holder = 673
	}
	1310.1.1 = {
		holder = 1023
	}
}
c_ketuit = {
	785.1.1 = {
		holder = 674
	}
	1310.1.1 = {
		holder = 1023
	}
}
####
k_phoenicia = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
}
d_gubla = {
	785.1.1 = {
		holder = 316
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1166
	}
	1311.1.1 = {
		holder = 0
	}
}
c_gubla = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 316
	}
	1310.1.1 = {
		holder = 1166
	}
}
c_hildua = {
	785.1.1 = {
		holder = 67
	}
	1310.1.1 = {
		holder = 1043
	}
}
c_beirut = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 316
	}
	1310.1.1 = {
		holder = 1166
	}
}
c_sannine = {
	785.1.1 = {
		holder = 675
	}
	1310.1.1 = {
		liege = "d_gubla"
		holder = 1532
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_tyre = {
	785.1.1 = {
		holder = 47
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1026
	}
	1311.1.1 = {
		holder = 0
	}
}
c_tyre = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 47
	}
	1310.1.1 = {
		holder = 1026
	}
}
c_baram = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 47
	}
	1310.1.1 = {
		holder = 1026
	}
}
c_acre = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 49
	}
	1310.1.1 = {
		liege = "d_megiddo"
		holder = 1533
	}
	1311.1.1 = {
		liege = 0
	}
}
c_hammon = {
	785.1.1 = {
		holder = 47
	}
	1310.1.1 = {
		holder = 1026
	}
}
####
d_halpa = {
	1310.1.1 = {
		liege = "k_amurru"
		de_jure_liege = k_amurru
		holder = 1051
	}
	1311.1.1 = {
		liege = 0
		holder = 0
	}
}
c_halpa = {
	785.1.1 = {
		holder = 80
	}
	1310.1.1 = {
		liege = "k_amurru"
		holder = 1051
	}
	1311.1.1 = {
		liege = 0
	}
}
c_munjez = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 676
	}
	1310.1.1 = {
		liege = "d_halpa"
		holder = 1534
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_sigata
c_sigata = {
	785.1.1 = {
		holder = 316
	}
	1310.1.1 = {
		holder = 1166
	}
}
c_kusbat = {
	785.1.1 = {
		liege = "d_gubla"
		holder = 677
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_gubla"
		holder = 1535
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ardata = {
	785.1.1 = {
		liege = "d_gubla"
		holder = 678
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "k_amurru"
		holder = 1051
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_sidon = {
	1310.1.1 = {
		holder = 1043
	}
	1311.1.1 = {
		holder = 0
	}
}
c_sidon = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 67
	}
	1310.1.1 = {
		holder = 1043
	}
}
c_sariptu = {
	785.1.1 = {
		liege = "d_tyre"
		holder = 679
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_sidon"
		holder = 1536
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_hazor = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
}
####
#d_qarnayim
c_qarnayim = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 82
	}
	1310.1.1 = {
		holder = 1053
	}
}
c_apeq = {
	785.1.1 = {
		holder = 76
	}
	1310.1.1 = {
		liege = "d_hazor"
		holder = 1048
	}
	1311.1.1 = {
		liege = 0
	}
}
c_ortal = {
	785.1.1 = {
		holder = 76
	}
	1310.1.1 = {
		liege = "d_hazor"
		holder = 1048
	}
	1311.1.1 = {
		liege = 0
	}
}
####
#d_raqqat
c_raqqat = {
	785.1.1 = {
		holder = 77
	}
	1310.1.1 = {
		liege = "d_hazor"
		holder = 1537
	}
	1311.1.1 = {
		liege = 0
	}
}
c_rehob = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 680
	}
	1310.1.1 = {
		liege = "d_tyre"
		holder = 1538
	}
	1311.1.1 = {
		liege = 0
	}
}
c_zebulun = {
	785.1.1 = {
		holder = 49
	}
	1310.1.1 = {
		holder = 1027
	}
}
####
d_hazor = {
	785.1.1 = {
		holder = 59
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1037
	}
	1311.1.1 = {
		holder = 0
	}
}
c_hazor = {
	785.1.1 = { change_development_level = 8 }
	1310.1.1 = { change_development_level = 15 }
	785.1.1 = {
		holder = 59
	}
	1310.1.1 = {
		holder = 1037
	}
}
c_yanoah = {
	785.1.1 = {
		holder = 59
	}
	1310.1.1 = {
		holder = 1037
	}
}
c_danabu = {
	785.1.1 = {
		liege = "d_hazor"
		holder = 681
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hazor"
		holder = 1539
	}
	1311.1.1 = {
		liege = 0
	}
}
c_qedes = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 59
	}
	1310.1.1 = {
		holder = 1037
	}
}
####
#k_aram
####
d_damascus_oasis = {
	1310.1.1 = {
		holder = 1044
	}
	1311.1.1 = {
		holder = 0
	}
}
c_damascus = {
	785.1.1 = { change_development_level = 6 }
	1310.1.1 = { change_development_level = 12 }
	785.1.1 = {
		holder = 68
	}
	1310.1.1 = {
		holder = 1044
	}
}
c_erneh = {
	785.1.1 = {
		holder = 83
	}
	1310.1.1 = {
		liege = "d_damascus_oasis"
		holder = 1054
	}
	1311.1.1 = {
		liege = 0
	}
}
c_fijeh_spring = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 683
	}
	1310.1.1 = {
		holder = 1044
	}
}
c_hadara = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		liege = "d_damascus_oasis"
		holder = 684
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_damascus_oasis"
		holder = 1541
	}
	1311.1.1 = {
		liege = 0
	}
}
c_nashabiyah = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		liege = "d_damascus_oasis"
		holder = 685
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_damascus_oasis"
		holder = 1542
	}
	1311.1.1 = {
		liege = 0
	}
}
c_zabdean = {
	785.1.1 = { change_development_level = 4 }
	1310.1.1 = { change_development_level = 9 }
	785.1.1 = {
		holder = 68
	}
	1310.1.1 = {
		holder = 1044
	}
}
####
#d_yabrudu
c_yabrudu = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 69
	}
	1310.1.1 = {
		liege = "d_nazala"
		holder = 1045
	}
	1311.1.1 = {
		liege = 0
	}
}
c_qutayfah = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 69
	}
	1310.1.1 = {
		liege = "d_nazala"
		holder = 1045
	}
	1311.1.1 = {
		liege = 0
	}
}
####
d_orontes_source = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
}
c_orontes_source = {
	785.1.1 = {
		holder = 78
	}
	1310.1.1 = {
		holder = 1049
	}
}
c_hermel = {
	785.1.1 = {
		holder = 78
	}
	1310.1.1 = {
		holder = 1049
	}
}
####
d_hazabu = {
	785.1.1 = { change_development_level = 7 }
	1310.1.1 = { change_development_level = 14 }
	785.1.1 = {
		holder = 79
	}
	786.1.1 = {
		holder = 0
	}
	1310.1.1 = {
		holder = 1050
	}
	1311.1.1 = {
		holder = 0
	}
}
c_hazabu = {
	785.1.1 = {
		holder = 79
	}
	1310.1.1 = {
		holder = 1050
	}
}
c_kumidu = {
	785.1.1 = {
		holder = 79
	}
	1310.1.1 = {
		holder = 1050
	}
}
c_huzaza = {
	785.1.1 = {
		liege = "d_hazabu"
		holder = 682
	}
	786.1.1 = {
		liege = 0
	}
	1310.1.1 = {
		liege = "d_hazabu"
		holder = 1540
	}
	1311.1.1 = {
		liege = 0
	}
}
####
k_basan = {
	785.1.1 = { change_development_level = 3 }
	1310.1.1 = { change_development_level = 8 }
}
####
#d_kanatha
c_kanatha = {
	785.1.1 = {
		holder = 66
	}
	1310.1.1 = {
		holder = 1042
	}
}
c_salkhad = {
	785.1.1 = {
		holder = 66
	}
	1310.1.1 = {
		holder = 1042
	}
}
c_hwayya = {
	785.1.1 = {
		holder = 686
	}
	1310.1.1 = {
		holder = 1543
	}
}
c_taraba = {
	785.1.1 = {
		holder = 687
	}
	1310.1.1 = {
		holder = 1544
	}
}
####
#d_sirbasani
c_sirbasani = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 82
	}
	1310.1.1 = {
		holder = 1053
	}
}
c_hulhulitu = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 81
	}
	1310.1.1 = {
		holder = 1052
	}
}
####
#d_saaru
c_saaru = {
	785.1.1 = { change_development_level = 5 }
	1310.1.1 = { change_development_level = 11 }
	785.1.1 = {
		holder = 81
	}
	1310.1.1 = {
		holder = 1052
	}
}
c_beitjinn = {
	785.1.1 = { change_development_level = 2 }
	1310.1.1 = { change_development_level = 6 }
	785.1.1 = {
		holder = 83
	}
	1310.1.1 = {
		liege = "d_damascus_oasis"
		holder = 1054
	}
	1311.1.1 = {
		liege = 0
	}
}
####