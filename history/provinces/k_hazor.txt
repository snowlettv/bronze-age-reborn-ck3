﻿#k_hazor
##d_qarnayim ###################################
###c_qarnayim
4051 = { #Qarnayim
	culture = golanite
	religion = canaanite_faith
	holding = castle_holding
}
4052 = { #'Astarôt
	holding = city_holding
}
4050 = { #Naf'ah
	holding = church_holding
}
4049 = { #Ain Thakar
	holding = city_holding
}
4045 = { #Rafeed
	holding = castle_holding
}
###c_apeq
3981 = { #Âpêq
	culture = golanite
	religion = canaanite_faith
	holding = castle_holding
}
4099 = { #Kazir
	holding = none
}
3980 = { #Geter
	holding = city_holding
}
4053 = { #Eliad
	holding = none
}
4054 = { #Haspin
	holding = none
}
###c_ortal
4047 = { #Ortal
	culture = golanite
	religion = canaanite_faith
	holding = castle_holding
}
4078 = { #Mas'ada
	holding = none
}
4048 = { #Ani'am
	holding = city_holding
}
4046 = { #Alonei
	holding = none
}
##d_raqqat
###c_raqqat
4327 = { #Raqqat
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4177 = { #Kinnâret
	holding = city_holding
}
4188 = { #Kadarim
	holding = church_holding
}
4190 = { #Rimmôn
	holding = castle_holding
}
4178 = { #Gat-hêpêr
	holding = city_holding
}
4101 = { #Bêyt-Semes
	holding = castle_holding
}
###c_rehob
4185 = { #Rehôb
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
}
4182 = { #Netu'a
	holding = none
}
4181 = { #Meron
	holding = city_holding
}
4186 = { #Karmi'el
	holding = none
}
###c_zebulun
4196 = { #Zebûlun
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
4191 = { #Hannâtôn
	holding = none
}
4189 = { #Arraba
	holding = city_holding
}
##d_hazor
###c_hazor
3975 = { #Hâzôr
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
3976 = { #Naptâlî
	holding = church_holding
}
3979 = { #Lesem
	holding = none

	1310.1.1 = {
		holding = city_holding
	}
}
3974 = { #Yesud
	holding = castle_holding
}
3978 = { #Qatsrin
	holding = city_holding
}
###c_yanoah
3972 = { #Yânôah
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
3977 = { #Gonên
	holding = none
}
3970 = { #'Abêl-Bêyt-Ma'âkâh
	holding = city_holding
}
3973 = { #Odem
	holding = none
}
###c_danabu
3971 = { #Dân
	culture = phoenician
	religion = phoenician_faith
	holding = castle_holding
	#holy_site = omen_lotan
	
	785.1.1 = {
		special_building_slot = temple_of_lotan_01 #Temple of Lotan
	}
}
3913 = { #Qlayaa
	holding = none
}
3985 = { #Markaba
	holding = none
}
3965 = { #Hâsbaïya
	holding = church_holding
}
3964 = { #'Iyyôn
	holding = none
}
###c_qedes
4179 = { #Qedes
	culture = canaanite
	religion = canaanite_faith
	holding = castle_holding
}
3986 = { #Chaqra
	holding = city_holding
}
3988 = { #Malkiya
	holding = none
}
4180 = { #Ami'ad
	holding = none
}