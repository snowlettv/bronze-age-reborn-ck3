﻿#Defined
kerma1 = {
	name = "Lhleye"
	dynasty = dynn_kerma1
	dna = kerma_1_dna
	religion = "kushite_faith"
	culture = "kushite"
	749.4.20 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	803.4.27 = {
		death = yes
	}
}
2kush1 = {
	name = "Xwili"
	dynasty = dynn_2kush1
	dna = kush_1_dna
	religion = "kushite_faith"
	culture = "kushite"
	1274.2.26 = {
		birth = yes
		effect = {
			add_character_flag = has_scripted_appearance
		}
	}
	1321.6.24 = {
		death = yes
	}
}
#Generated
318 = {
	name = "Tarekeniwal"
	dynasty = 318
	religion = "kushite_faith"
	culture = "kushite"
	746.3.1 = {
		birth = yes
	}
	811.3.17 = {
		death = yes
	}
}
319 = {
	name = "Ltlememe"
	dynasty = 319
	religion = "kushite_faith"
	culture = "kushite"
	755.9.15 = {
		birth = yes
	}
	803.3.24 = {
		death = yes
	}
}
320 = {
	name = "Kelqeli"
	dynasty = 320
	religion = "kushite_faith"
	culture = "kushite"
	768.10.21 = {
		birth = yes
	}
	825.4.5 = {
		death = yes
	}
}
700 = {
	name = "Teritedakhatey"
	dynasty = 700
	religion = "kushite_faith"
	culture = "kushite"
	756.9.3 = {
		birth = yes
	}
	822.8.18 = {
		death = yes
	}
}
959 = {
	name = "Teritnide"
	dynasty = 959
	religion = "kushite_faith"
	culture = "kushite"
	1272.9.25 = {
		birth = yes
	}
	1336.6.3 = {
		death = yes
	}
}
961 = {
	name = "Adeqatali"
	dynasty = 961
	religion = "kushite_faith"
	culture = "kushite"
	1283.4.16 = {
		birth = yes
	}
	1351.1.5 = {
		death = yes
	}
}
962 = {
	name = "Nastasen"
	dynasty = 962
	religion = "kushite_faith"
	culture = "kushite"
	1277.4.6 = {
		birth = yes
	}
	1320.11.11 = {
		death = yes
	}
}