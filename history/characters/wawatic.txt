﻿#Generated
23 = {
	name = "Xwili"
	dynasty = 23
	religion = "egyptian_faith"
	culture = "wawatic"
	758.8.12 = {
		birth = yes
	}
	800.2.10 = {
		death = yes
	}
}
41 = {
	name = "Taharqa"
	dynasty = 41
	religion = "kushite_faith"
	culture = "wawatic"
	755.7.7 = {
		birth = yes
	}
	814.9.21 = {
		death = yes
	}
}
321 = {
	name = "Mhes"
	dynasty = 321
	religion = "kushite_faith"
	culture = "wawatic"
	766.5.2 = {
		birth = yes
	}
	833.1.2 = {
		death = yes
	}
}
322 = {
	name = "Meqelli"
	dynasty = 322
	religion = "kushite_faith"
	culture = "wawatic"
	755.11.16 = {
		birth = yes
	}
	820.10.9 = {
		death = yes
	}
}
697 = {
	name = "Takideamani"
	dynasty = 697
	religion = "kushite_faith"
	culture = "wawatic"
	763.4.4 = {
		birth = yes
	}
	811.5.16 = {
		death = yes
	}
}
698 = {
	name = "Xrmdeye"
	dynasty = 698
	religion = "kushite_faith"
	culture = "wawatic"
	752.11.8 = {
		birth = yes
	}
	817.5.9 = {
		death = yes
	}
}
699 = {
	name = "Amanislo"
	dynasty = 699
	religion = "kushite_faith"
	culture = "wawatic"
	745.6.7 = {
		birth = yes
	}
	808.11.27 = {
		death = yes
	}
}
941 = {
	name = "Htpiye"
	dynasty = 941
	religion = "egyptian_faith"
	culture = "wawatic"
	1291.11.2 = {
		birth = yes
	}
	1335.4.8 = {
		death = yes
	}
}
963 = {
	name = "Amnitnide"
	dynasty = 963
	religion = "kushite_faith"
	culture = "wawatic"
	1272.8.11 = {
		birth = yes
	}
	1317.8.8 = {
		death = yes
	}
}
964 = {
	name = "Aramatleqo"
	dynasty = 964
	religion = "kushite_faith"
	culture = "wawatic"
	1294.8.12 = {
		birth = yes
	}
	1341.4.15 = {
		death = yes
	}
}
966 = {
	name = "Arqamani"
	dynasty = 966
	religion = "kushite_faith"
	culture = "wawatic"
	1280.3.10 = {
		birth = yes
	}
	1338.2.22 = {
		death = yes
	}
}
967 = {
	name = "Apile"
	dynasty = 967
	religion = "kushite_faith"
	culture = "wawatic"
	1293.6.22 = {
		birth = yes
	}
	1357.9.2 = {
		death = yes
	}
}