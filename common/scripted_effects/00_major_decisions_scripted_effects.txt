﻿create_roman_empire_scripted_effect = { #BA REMOVED
}
create_roman_empire_holy_scripted_effect = { #BA REMOVED
}
create_roman_empire_italy_scripted_effect = { #BA REMOVED
}
mend_great_schism_scripted_effect = { #BA REMOVED
}

restore_papacy_scripted_effect = { #BA REMOVED
}

destroy_papacy_scripted_effect = { #BA REMOVED
}

form_switzerland_scripted_effect = { #BA REMOVED
}

form_austria_scripted_effect = { #BA REMOVED
}

form_carolingian_empire_scripted_effect = { #BA REMOVED
}

unite_burgundies_scripted_effect = { #BA REMOVED
}

form_outremer_scripted_effect = { #BA REMOVED
}

restore_sunni_caliphate_scripted_effect = { #BA REMOVED
}
create_israel_scripted_effect = { #BA REMOVED
}
create_rum_scripted_effect = { #BA REMOVED
}

declare_bloodline_holy_decision_effect = {
	save_scope_as = progenitor_holy_blood
	if = {
		limit = {
			NOT = {
				has_character_flag = con_blood_leg
			}
		}
		add_character_flag = con_blood_leg
		hidden_effect = {
			legend_seed_great_deed_dynasty_effect = yes
		}
	}

	faith = {
		set_variable = {
			name = variable_savior_found
			value = yes
		}
		change_fervor = {
			value = 25
			desc = fervor_gain_holy_bloodline
		}
	}
	if = { #Temporal.
		limit = {
			root.faith.religious_head = root
		}
		add_trait = savior
		root.dynasty = {
			add_dynasty_prestige = 1000
			add_dynasty_prestige_level = 1
		}
	}
	else = { #Spiritual.
		add_trait = paragon
		root.dynasty = {
			add_dynasty_prestige = 500
		}
	}

	every_child = {
		even_if_dead = yes
		trigger_event = major_decisions.0101
	
		every_child = {
			even_if_dead = yes
			limit = { 
				OR = {
					is_grandchild_of = scope:progenitor_holy_blood 
					is_great_grandchild_of = scope:progenitor_holy_blood  
				}
			}
			trigger_event = major_decisions.0101

			every_child = {
				even_if_dead = yes
				limit = { 
					OR = {
						is_grandchild_of = scope:progenitor_holy_blood 
						is_great_grandchild_of = scope:progenitor_holy_blood  
					}
				}
				trigger_event = major_decisions.0101
			}
		}
	}
}

sicilian_parliament_building_scripted_effect = { #BA REMOVED
}
sicilian_parliament_kingdom_split_scripted_effect = { #If the decision is taken when the ruler doesn't hold the entirety of k_sicily, it results in the Naples/Trinacria split. (this is the ruler) #BA REMOVED
}

empower_sicilian_parliament_decision_scripted_effect = { #BA REMOVED
}

promote_gothic_innovations_decision_scripted_effect = { #BA REMOVED
}

promote_hungarian_settlement_decision_scripted_effect = { #BA REMOVED
}

revive_magyar_paganism_decision_scripted_effect = { #BA REMOVED
}

restore_dumnonia_decision_scripted_effect = { #BA REMOVED
}

revive_armenian_empire_decision_scripted_effect = { #BA REMOVED
}

restore_holy_roman_empire_decision_scripted_effect = { #BA REMOVED
}

found_kingdom_of_bosnia_decision_scripted_effect = { #BA REMOVED
}

found_kingdom_of_livonia_decision_scripted_effect = { #BA REMOVED
}

unite_bene_israel_effect = { #BA REMOVED
}

hre_elector_list_save_effect = { #BA REMOVED
}

favour_the_countryside_basques_decision_generic_effects_scripted_effect = { #BA REMOVED
}

favour_the_countryside_basques_decision_fundamentalist_path_scripted_effect = { #BA REMOVED
}

favour_the_countryside_basques_decision_righteous_path_scripted_effect = { #BA REMOVED
}

favour_the_countryside_basques_decision_pluralist_path_scripted_effect = { #BA REMOVED
}

favour_the_countryside_basques_decision_default_path_scripted_effect = { #BA REMOVED
}

create_kingdom_of_saxony_effect = { #BA REMOVED
}

restore_old_vasconia_decision_scripted_effect = { #BA REMOVED
}

restore_old_vasconia_decision_tooltip_scripted_effect = { #BA REMOVED
}
