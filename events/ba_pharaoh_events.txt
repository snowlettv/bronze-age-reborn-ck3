﻿namespace = ba_pharaoh_initialization

ba_pharaoh_initialization.0001 = {
	hidden = yes

	trigger = {
		scope:title = title:k_pharaoh
		exists = title:k_pharaoh.holder
		exists = title:k_pharaoh.previous_holder
	}

	immediate = {
		title:k_pharaoh.previous_holder = { save_scope_as = previous_holder }
		if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twelfth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWELFTH_DYNASTY
				set_variable = twelfth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = thirteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = THIRTEENTH_DYNASTY
				set_variable = thirteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = fourteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = FOURTEENTH_DYNASTY
				set_variable = fourteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = fifteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = FIFTEENTH_DYNASTY
				set_variable = fifteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = sixteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = SIXTEENTH_DYNASTY
				set_variable = sixteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = seventeenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = SEVENTEENTH_DYNASTY
				set_variable = seventeenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = eighteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = EIGHTEENTH_DYNASTY
				set_variable = eighteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = nineteenth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = NINETEENTH_DYNASTY
				set_variable = nineteenth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twentieth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTIETH_DYNASTY
				set_variable = twentieth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_first_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_FIRST_DYNASTY
				set_variable = twenty_first_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_second_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_SECOND_DYNASTY
				set_variable = twenty_second_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_third_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_THIRD_DYNASTY
				set_variable = twenty_third_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_fourth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_FOURTH_DYNASTY
				set_variable = twenty_fourth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_fifth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_FIFTH_DYNASTY
				set_variable = twenty_fifth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_sixth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_SIXTH_DYNASTY
				set_variable = twenty_sixth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_seventh_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_SEVENTH_DYNASTY
				set_variable = twenty_seventh_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_eighth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_EIGHTH_DYNASTY
				set_variable = twenty_eighth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = twenty_ninth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = TWENTY_NINTH_DYNASTY
				set_variable = twenty_ninth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = thirtieth_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = THIRTIETH_DYNASTY
				set_variable = thirtieth_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = thirty_first_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = THIRTY_FIRST_DYNASTY
				set_variable = thirty_first_dynasty
			}
		}
		else_if = {
			limit = {
				scope:previous_holder = {
					NOT = {
						dynasty = scope:title.holder.dynasty
					}
				}
				title:k_pharaoh = {
					NOT = { has_variable = thirty_second_dynasty }
				}
			}
			title:k_pharaoh = {
				set_title_name = THIRTY_SECOND_DYNASTY
				set_variable = thirty_second_dynasty
			}
		}
		else = {
		}
	}
}