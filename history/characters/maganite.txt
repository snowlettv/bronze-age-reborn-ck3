﻿#Generated
271 = {
	name = "Ilulu"
	dynasty = 271
	religion = "dilmunite_faith"
	culture = "maganite"
	746.11.20 = {
		birth = yes
	}
	803.11.27 = {
		death = yes
	}
}
272 = {
	name = "Kikkla"
	dynasty = 272
	religion = "dilmunite_faith"
	culture = "maganite"
	762.6.9 = {
		birth = yes
	}
	825.5.22 = {
		death = yes
	}
}
273 = {
	name = "Irgigi"
	dynasty = 273
	religion = "dilmunite_faith"
	culture = "maganite"
	763.10.10 = {
		birth = yes
	}
	829.11.8 = {
		death = yes
	}
}
274 = {
	name = "Ashurnasirpal"
	dynasty = 274
	religion = "dilmunite_faith"
	culture = "maganite"
	755.10.14 = {
		birth = yes
	}
	800.4.3 = {
		death = yes
	}
}
760 = {
	name = "Harsu"
	dynasty = 760
	religion = "dilmunite_faith"
	culture = "maganite"
	754.3.10 = {
		birth = yes
	}
	801.5.1 = {
		death = yes
	}
}
761 = {
	name = "Mandaru"
	dynasty = 761
	religion = "dilmunite_faith"
	culture = "maganite"
	753.9.16 = {
		birth = yes
	}
	802.8.27 = {
		death = yes
	}
}
765 = {
	name = "Samani"
	dynasty = 765
	religion = "dilmunite_faith"
	culture = "maganite"
	753.3.17 = {
		birth = yes
	}
	808.2.7 = {
		death = yes
	}
}
766 = {
	name = "Nuabu"
	dynasty = 766
	religion = "dilmunite_faith"
	culture = "maganite"
	760.4.18 = {
		birth = yes
	}
	804.3.1 = {
		death = yes
	}
}
1014 = {
	name = "Ashur-bel-kala"
	dynasty = 1014
	religion = "dilmunite_faith"
	culture = "maganite"
	1294.6.5 = {
		birth = yes
	}
	1348.3.23 = {
		death = yes
	}
}
1015 = {
	name = "Imi"
	dynasty = 1015
	religion = "dilmunite_faith"
	culture = "maganite"
	1289.6.3 = {
		birth = yes
	}
	1340.7.1 = {
		death = yes
	}
}
1016 = {
	name = "Nur-ili"
	dynasty = 1016
	religion = "dilmunite_faith"
	culture = "maganite"
	1293.4.10 = {
		birth = yes
	}
	1338.3.14 = {
		death = yes
	}
}
1017 = {
	name = "Ashur-etil-ilani"
	dynasty = 1017
	religion = "dilmunite_faith"
	culture = "maganite"
	1277.3.20 = {
		birth = yes
	}
	1337.10.4 = {
		death = yes
	}
}
1611 = {
	name = "Adamu"
	dynasty = 1611
	religion = "dilmunite_faith"
	culture = "maganite"
	1271.8.17 = {
		birth = yes
	}
	1317.4.4 = {
		death = yes
	}
}
1612 = {
	name = "Ninurta-tukulti-Ashur"
	dynasty = 1612
	religion = "dilmunite_faith"
	culture = "maganite"
	1274.3.12 = {
		birth = yes
	}
	1321.4.2 = {
		death = yes
	}
}
1616 = {
	name = "Ashur-bel-nisheshu"
	dynasty = 1616
	religion = "dilmunite_faith"
	culture = "maganite"
	1286.3.8 = {
		birth = yes
	}
	1343.3.10 = {
		death = yes
	}
}
1617 = {
	name = "Tiglath-Pileser"
	dynasty = 1617
	religion = "dilmunite_faith"
	culture = "maganite"
	1276.11.4 = {
		birth = yes
	}
	1322.1.24 = {
		death = yes
	}
}