##reload posteffectvolumes
#PostEffectVolumes.Enabled
#PostEffectVolumes.ForceUpdate
#Draw.DebugPostEffectVolumes

posteffect_values = {
	name = standard
	lut = "gfx/map/post_effects/colorcorrection_neutral.tga"
	saturation_scale = 1.10
}

posteffect_values = {
	name = cold
	inherit = standard
	saturation_scale = 0.8
	colorbalance = { 0.9 0.95 1.15 }
	fog_begin = 150
	fog_end = 400
}

posteffect_values = {
	name = hot
	inherit = standard
	saturation_scale = 1
	value_scale = 1.15
	colorbalance = { 1.1 1 0.9 }
}

posteffect_values = {
	name = ana
	inherit = standard
	value_scale = 1.1
	saturation_scale = 1
	colorbalance = { 1.05 0.95 1 }
}

posteffect_values = {
	name = aeg
	inherit = standard
	value_scale = 1
	saturation_scale = 1.15
	colorbalance = { 1.05 1 0.9 }
}

posteffect_values = {
	name = default
	inherit = standard
}

posteffect_values = {
	name = zoom_step_00
	inherit = standard
}
posteffect_values = {
	name = zoom_step_01
	inherit = zoom_step_00
	fog_begin = 2000
	fog_end = 5000
}
posteffect_values = {
	name = flatmap
	inherit = zoom_step_01
	exposure = 1.0
	fog_max = 0
	saturation_scale = 1
}

#################################

############ VOLUMES ############
############ HEIGHTS ############

#################################

posteffect_volumes = {

	posteffect_height_volume = {
		name = "zoom_step_00"

		posteffect_values_day = zoom_step_00
		posteffect_values_night = zoom_step_00

		height = 50
		fade_distance = 0
	}

	posteffect_height_volume = {
		name = "zoom_step_01"
	
		posteffect_values_day = zoom_step_01
		posteffect_values_night = zoom_step_01
	
		height = 100
		fade_distance = 800
	}

	posteffect_height_volume = {
		name = "flatmap"
	
		posteffect_values_day = flatmap
		posteffect_values_night = flatmap
	
		height = 1218
		fade_distance = 142
	}
}

################################# 

############ VOLUMES ############ 
############ BOXES	 ############ 

#################################

	
posteffect_volume = {
	name = "south1"
	
	posteffect_values_day = hot
	posteffect_values_night = hot
	
	position = { 4000 0 0}
	size = { 10000 1850 5100}
	fade_distance = 1000
}

posteffect_volume = {
	name = "south1"
	
	posteffect_values_day = hot
	posteffect_values_night = hot
	
	position = { 5000 0 2000}
	size = { 6000 1850 2000}
	fade_distance =  1000
}

posteffect_volume = {
	name = "InnerAnatolia"
	
	posteffect_values_day = ana
	posteffect_values_night = ana
	
	position = { 2500 0 3900}
	size = { 1500 1750 1500}
	fade_distance =  1000
}

posteffect_volume = {
	name = "InnerAnatolia"
	
	posteffect_values_day = ana
	posteffect_values_night = ana
	
	position = { 2500 0 3800}
	size = { 700 1750 1500}
	fade_distance =  1000
}

posteffect_volume = {
	name = "aegean"
	
	posteffect_values_day = aeg
	posteffect_values_night = aeg
	
	position = { 150 0 3900}
	size = { 2500 1850 2500}
	fade_distance =  1000
}
	