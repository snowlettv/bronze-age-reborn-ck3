﻿helladic_opinion = {
	decimals = 0
}
cycladic_opinion = {
	decimals = 0
}
minoan_opinion = {
	decimals = 0
}
telchines_opinion = {
	decimals = 0
}
karpathan_opinion = {
	decimals = 0
}
tyrsenian_opinion = {
	decimals = 0
}
leleges_opinion = {
	decimals = 0
}
hektenas_opinion = {
	decimals = 0
}
kranaoi_opinion = {
	decimals = 0
}
athamanes_opinion = {
	decimals = 0
}
hyades_opinion = {
	decimals = 0
}
dryopes_opinion = {
	decimals = 0
}
kuretes_opinion = {
	decimals = 0
}
 #Luwian/Anatolian
wilusan_opinion = {
	decimals = 0
}
sehan_opinion = {
	decimals = 0
}
miran_opinion = {
	decimals = 0
}
arzawan_opinion = {
	decimals = 0
}
lukkan_opinion = {
	decimals = 0
}
tarhuntassan_opinion = {
	decimals = 0
}
hapallan_opinion = {
	decimals = 0
}
carian_opinion = {
	decimals = 0
}
lydian_opinion = {
	decimals = 0
}
palaic_opinion = {
	decimals = 0
}
nesili_opinion = {
	decimals = 0
}
neohittite_opinion = {
	decimals = 0
}
kalasman_opinion = {
	decimals = 0
}
tummana_opinion = {
	decimals = 0
}
mysan_opinion = {
	decimals = 0
}
 #Maganite/Arabic
maganite_opinion = {
	decimals = 0
}
arabian_opinion = {
	decimals = 0
}
south_arabian_opinion = {
	decimals = 0
}
 #Levantine/Canaanite
amoritic_opinion = {
	decimals = 0
}
phoenician_opinion = {
	decimals = 0
}
sinaian_opinion = {
	decimals = 0
}
eblaite_opinion = {
	decimals = 0
}
canaanite_opinion = {
	decimals = 0
}
bedu_opinion = {
	decimals = 0
}
ammonite_opinion = {
	decimals = 0
}
moabite_opinion = {
	decimals = 0
}
edomite_opinion = {
	decimals = 0
}
golanite_opinion = {
	decimals = 0
}
aramean_opinion = {
	decimals = 0
}
hyksos_opinion = {
	decimals = 0
}
shasu_opinion = {
	decimals = 0
}
 #Caucasian
mushki_opinion = {
	decimals = 0
}
 #Cypriot
ba_cypriot_opinion = {
	decimals = 0
}
 #Elamite
ellipian_opinion = {
	decimals = 0
}
susanian_opinion = {
	decimals = 0
}
anshanian_opinion = {
	decimals = 0
}
liyan_opinion = {
	decimals = 0
}
jiroft_opinion = {
	decimals = 0
}
 #Harappan
harappan_opinion = {
	decimals = 0
}
 #Hattian
hattian_opinion = {
	decimals = 0
}
kaskian_opinion = {
	decimals = 0
}
 #Hellenic
proto_greek_opinion = {
	decimals = 0
}
achaian_opinion = {
	decimals = 0
}
aeolian_opinion = {
	decimals = 0
}
dorian_opinion = {
	decimals = 0
}
ionian_opinion = {
	decimals = 0
}
 #Hurrian
tukri_opinion = {
	decimals = 0
}
shubrian_opinion = {
	decimals = 0
}
haburi_opinion = {
	decimals = 0
}
harrani_opinion = {
	decimals = 0
}
kizzuni_opinion = {
	decimals = 0
}
zalwari_opinion = {
	decimals = 0
}
ishuwan_opinion = {
	decimals = 0
}
mitanni_opinion = {
	decimals = 0
}
 #Iberian
paleoiberian_opinion = {
	decimals = 0
}
 #Indo-Iranian
indo_aryan_opinion = {
	decimals = 0
}
proto_iranian_opinion = {
	decimals = 0
}
 #Italic
etruscan_opinion = {
	decimals = 0
}
proto_italic_opinion = {
	decimals = 0
}
 #Kemetic
lower_egyptian_opinion = {
	decimals = 0
}
middle_egyptian_opinion = {
	decimals = 0
}
upper_egyptian_opinion = {
	decimals = 0
}
 #Nubian
kushite_opinion = {
	decimals = 0
}
wawatic_opinion = {
	decimals = 0
}
iunetic_opinion = {
	decimals = 0
}
medjay_opinion = {
	decimals = 0
}
puntic_opinion = {
	decimals = 0
}
 #Nuragic
shekelesh_opinion = {
	decimals = 0
}
sherden_opinion = {
	decimals = 0
}
 #Libyan
rebu_opinion = {
	decimals = 0
}
tehenu_opinion = {
	decimals = 0
}
temehu_opinion = {
	decimals = 0
}
ifri_opinion = {
	decimals = 0
}
 #Mesopotamian
akkadian_opinion = {
	decimals = 0
}
assyrian_opinion = {
	decimals = 0
}
babylonian_opinion = {
	decimals = 0
}
neo_sumerian_opinion = {
	decimals = 0
}
dilmunite_opinion = {
	decimals = 0
}
sumerian_opinion = {
	decimals = 0
}
 #Thracian
bryges_opinion = {
	decimals = 0
}
proto_thracian_opinion = {
	decimals = 0
}
 #Urartuan
nairi_opinion = {
	decimals = 0
}
mannaean_opinion = {
	decimals = 0
}
urumu_opinion = {
	decimals = 0
}
 #Zagrosian
gutian_opinion = {
	decimals = 0
}
lullubi_opinion = {
	decimals = 0
}
kassite_opinion = {
	decimals = 0
}