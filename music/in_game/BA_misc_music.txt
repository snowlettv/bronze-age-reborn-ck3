﻿
## Egyptian Cue Tracks

# Sources:
# Children of the Nile Soundtrack (Note: Old website no longer available, must use internet archives to download)
# https://web.archive.org/web/20170228011704/http://immortalcities.com/cotn/downloads/music.php
# 

mx_BA_egypt_win = {
	music = "file:/music/cuetracks/win.ogg"
	pause_factor = 30
}

mx_BA_egypt_lose = {
	music = "file:/music/cuetracks/lose.ogg"
	pause_factor = 30
}

## Greek Cue Tracks

# Sources:
# Zeus Soundtrack: http://sierrachest.com/index.php?a=games&id=81&title=master-of-olympus&fld=music
#
mx_BA_greek_start = {
	music = "file:/music/cuetracks/mission_intro.ogg"
	pause_factor = 30
}

mx_BA_greek_major_win = {
	music = "file:/music/cuetracks/campaign_victory.ogg"
	pause_factor = 30
}

mx_BA_greek_minor_win = {
	music = "file:/music/cuetracks/mission_victory.ogg"
	pause_factor = 30
}