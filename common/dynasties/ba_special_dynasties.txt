﻿dynn_mereruka1 = {
	name = "dynn_Mereruka"
	culture = "lower_egyptian"
}
dynn_Khui1 = {
	name = "dynn_Khui"
	culture = "upper_egyptian"
}
dynn_userkaf1 = {
	name = "dynn_Userkaf"
	culture = "lower_egyptian"
}
dynn_Ibi1 = {
	name = "dynn_Ibi"
	culture = "lower_egyptian"
}
dynn_Meribre1 = {
	name = "dynn_Meryre"
	culture = "lower_egyptian"
}
dynn_teti1 = {
	name = "dynn_Teti"
	culture = "lower_egyptian"
}
dynn_waset1 = {
	name = "dynn_Khui-Amun"
	culture = "upper_egyptian"
}
dynn_hnn1 = {
	name = "dynn_Khety"
	culture = "middle_egyptian"
}
dynn_ankhtifi1 = {
	name = "dynn_Thety-Horus"
	culture = "upper_egyptian"
}
dynn_gebtu1 = {
	name = "dynn_Narmer-Min"
	culture = "upper_egyptian"
}
dynn_ebla1 = {
	name = "dynn_Ba_aleser-Shapash"
	culture = "eblaite"
}
dynn_mari1 = {
	name = "dynn_Shakkanakku"
	culture = "amoritic"
}
dynn_ur1 = {
	name = "dynn_Gar-Nanna"
	culture = "sumerian"
}
dynn_uruk1 = {
	name = "dynn_Ishu-Inanna"
	culture = "sumerian"
}
dynn_nippur1 = {
	name = "dynn_Kakug-Ninlil"
	culture = "sumerian"
}
dynn_lagash1 = {
	name = "dynn_Gudea-Lagash"
	culture = "sumerian"
}
dynn_lagash2 = {
	name = "dynn_Baba-Lagash"
	culture = "sumerian"
}
dynn_gutium1 = {
	name = "dynn_Jushur-Sin"
	culture = "gutian"
}
dynn_susa1 = {
	name = "dynn_Shimashki"
	culture = "susanian"
}
dynn_kerma1 = {
	name = "dynn_Dekrer-Hpesli"
	culture = "kushite"
}
dynn_lullubi1 = {
	name = "dynn_Hasharba-Kassu"
	culture = "lullubi"
}
dynn_simurrum1 = {
	name = "dynn_Philosirid"
	culture = "tukri"
}
dynn_assyria1 = {
	name = "dynn_Iltasadum-Nabu"
	culture = "assyrian"
}
dynn_hattusa1 = {
	name = "dynn_Taruntiya"
	culture = "hattian"
}
dynn_kanesh1 = {
	name = "dynn_Taruntiya"
	culture = "nesili"
}
dynn_rapiqu1 = {
	name = "dynn_Samani-Enlil"
	culture = "akkadian"
}
dynn_sippar1 = {
	name = "dynn_Sargon-Ea"
	culture = "akkadian"
}
dynn_babylon1 = {
	name = "dynn_Jushur-Sin"
	culture = "akkadian"
}
dynn_aplak1 = {
	name = "dynn_Rimush-Tishpak"
	culture = "akkadian"
}
dynn_apak1 = {
	name = "dynn_Lullaya-Shamash"
	culture = "akkadian"
}
dynn_marad1 = {
	name = "dynn_Samani"
	culture = "akkadian"
}
dynn_umma1 = {
	name = "dynn_Nanniya-Meskilak"
	culture = "sumerian"
}
dynn_larak1 = {
	name = "dynn_Sabit-Nanshe"
	culture = "sumerian"
}
dynn_adab1 = {
	name = "dynn_Ishu-Inanna"
	culture = "sumerian"
}
dynn_isin1 = {
	name = "dynn_Udul-Utu"
	culture = "sumerian"
}
dynn_kish1 = {
	name = "dynn_Iltasadum-Nabu"
	culture = "akkadian"
}
dynn_mashkan1 = {
	name = "dynn_Lugula-Ereshkigal"
	culture = "sumerian"
}
dynn_kutha1 = {
	name = "dynn_Akiya-Mulliltu"
	culture = "akkadian"
}
dynn_push1 = {
	name = "dynn_Sargon-Ea"
	culture = "akkadian"
}
dynn_malgum1 = {
	name = "dynn_Suhlamu-Inzak"
	culture = "akkadian"
}
dynn_der1 = {
	name = "dynn_Enmerkar-Ninazu"
	culture = "sumerian"
}
dynn_2waset1 = {
	name = "dynn_Djedhor-Nemty"
	culture = "upper_egyptian"
}
dynn_2hyksos1 = {
	name = "dynn_Yada_milk-Yam"
	culture = "hyksos"
}
dynn_2kush1 = {
	name = "dynn_Mhewitr-Axdeye"
	culture = "kushite"
}
dynn_2keret1 = {
	name = "dynn_Keret-Mitanni"
	culture = "mitanni"
}
dynn_2qatna1 = {
	name = "dynn_Mahra-Damu-Hermon"
	culture = "amoritic"
}
dynn_2kassite1 = {
	name = "dynn_Karduniash"
	culture = "kassite"
}
dynn_2sumer1 = {
	name = "dynn_Sealand"
	culture = "neo_sumerian"
}
dynn_2dilmun1 = {
	name = "dynn_Iltasadum-Nabu"
	culture = "dilmunite"
}
dynn_2kizzuwatna1 = {
	name = "dynn_Kikkula-Shaushka"
	culture = "kizzuni"
}
dynn_2alashiya1 = {
	name = "dynn_Kogamathe"
	culture = "ba_cypriot"
}
dynn_2knossos1 = {
	name = "dynn_Aziottenathe"
	culture = "minoan"
}
dynn_2susa1 = {
	name = "dynn_Sukkalmah"
	culture = "susanian"
}
dynn_2hattusa1 = {
	name = "dynn_Labarna"
	culture = "nesili"
}
dynn_2kanesh1 = {
	name = "dynn_Anawatta"
	culture = "nesili"
}
dynn_2ishuwa1 = {
	name = "dynn_Eugorid"
	culture = "ishuwan"
}
dynn_2halab1 = {
	name = "dynn_Yamhadite"
	culture = "amoritic"
}
dynn_2assyria1 = {
	name = "dynn_Adaside"
	culture = "assyrian"
}
dynn_2assyria2 = { #Usurper dynasty just for history
	name = "dynn_Lullayid"
	culture = "assyrian"
}
dynn_2khana1 = {
	name = "dynn_Yehimlk-Gebal"
	culture = "amoritic"
}
dynn_2iolkos1 = {
	name = "dynn_Sostratid"
	culture = "aeolian"
}
dynn_2mykene1 = {
	name = "dynn_Danaios"
	culture = "helladic"
}
dynn_2thebes1 = {
	name = "dynn_Karsid"
	culture = "achaian"
}
dynn_2orchomenos1 = {
	name = "dynn_Leonidid"
	culture = "achaian"
}
dynn_2troy1 = {
	name = "dynn_Tupanasalli"
	culture = "wilusan"
}
dynn_bily1 = {
	name = "dynn_Anunekit"
	culture = "sumerian"
}
dynn_gilgamesh1 = {
	name = "dynn_of_Uruk"
	culture = "sumerian"
}
dynn_kubaba1 = {
	name = "dynn_of_Kish"
	culture = "akkadian"
}
dynn_sargon1 = {
	name = "dynn_of_Akkad"
	culture = "akkadian"
}
dynn_imhotep1 = {
	name = "Djoser-Thoth"
	culture = "lower_egyptian"
}
dynn_sumuabum1 = {
	name = "dynn_Sumu-abum"
	culture = "amoritic"
}
#dynn_sneferu1 = { #Uncomment when adding Dynasty IV
#	name = "Sneferu-nabma_at"
#	culture = "lower_egyptian"
#}
dynn_amenemhat1 = {
	name = "Amenemhat-khepri"
	culture = "middle_egyptian"
}
dynn_amenemhat2 = {
	name = "Sobekhotep-ptah"
	culture = "middle_egyptian"
}