﻿#Generated
332 = {
	name = "Nikir-sarri"
	dynasty = 332
	religion = "western_hurrian_faith"
	culture = "haburi"
	746.10.7 = {
		birth = yes
	}
	802.6.8 = {
		death = yes
	}
}
333 = {
	name = "Anis-hurpe"
	dynasty = 333
	religion = "western_hurrian_faith"
	culture = "haburi"
	766.5.14 = {
		birth = yes
	}
	817.2.4 = {
		death = yes
	}
}
334 = {
	name = "Paip-sarri"
	dynasty = 334
	religion = "western_hurrian_faith"
	culture = "haburi"
	760.9.3 = {
		birth = yes
	}
	816.10.8 = {
		death = yes
	}
}
335 = {
	name = "Ela-paraluh"
	dynasty = 335
	religion = "western_hurrian_faith"
	culture = "haburi"
	764.9.5 = {
		birth = yes
	}
	829.2.2 = {
		death = yes
	}
}
336 = {
	name = "Paku-zi"
	dynasty = 336
	religion = "western_hurrian_faith"
	culture = "haburi"
	767.1.6 = {
		birth = yes
	}
	835.9.16 = {
		death = yes
	}
}
337 = {
	name = "Kabi-ata"
	dynasty = 337
	religion = "western_hurrian_faith"
	culture = "haburi"
	754.8.5 = {
		birth = yes
	}
	805.3.23 = {
		death = yes
	}
}
343 = {
	name = "Azzu-ka"
	dynasty = 343
	religion = "western_hurrian_faith"
	culture = "haburi"
	769.3.17 = {
		birth = yes
	}
	817.6.18 = {
		death = yes
	}
}
344 = {
	name = "Zigil-tanu-m"
	dynasty = 344
	religion = "western_hurrian_faith"
	culture = "haburi"
	769.8.27 = {
		birth = yes
	}
	821.11.1 = {
		death = yes
	}
}
352 = {
	name = "Puram-zi"
	dynasty = 352
	religion = "western_hurrian_faith"
	culture = "haburi"
	751.5.7 = {
		birth = yes
	}
	809.2.16 = {
		death = yes
	}
}
354 = {
	name = "Uzu-na-n"
	dynasty = 354
	religion = "western_hurrian_faith"
	culture = "haburi"
	766.7.19 = {
		birth = yes
	}
	818.7.26 = {
		death = yes
	}
}
355 = {
	name = "Tami"
	dynasty = 355
	religion = "western_hurrian_faith"
	culture = "haburi"
	768.4.6 = {
		birth = yes
	}
	816.11.1 = {
		death = yes
	}
}
356 = {
	name = "Kutti"
	dynasty = 356
	religion = "western_hurrian_faith"
	culture = "haburi"
	763.6.8 = {
		birth = yes
	}
	808.5.4 = {
		death = yes
	}
}
358 = {
	name = "Ustap-adal"
	dynasty = 358
	religion = "western_hurrian_faith"
	culture = "haburi"
	756.10.22 = {
		birth = yes
	}
	813.2.12 = {
		death = yes
	}
}
359 = {
	name = "Kirta"
	dynasty = 359
	religion = "western_hurrian_faith"
	culture = "haburi"
	754.6.20 = {
		birth = yes
	}
	802.9.2 = {
		death = yes
	}
}
360 = {
	name = "Sazu-e"
	dynasty = 360
	religion = "western_hurrian_faith"
	culture = "haburi"
	762.11.10 = {
		birth = yes
	}
	807.7.27 = {
		death = yes
	}
}
361 = {
	name = "Kanzu"
	dynasty = 361
	religion = "western_hurrian_faith"
	culture = "haburi"
	769.11.14 = {
		birth = yes
	}
	816.10.17 = {
		death = yes
	}
}
362 = {
	name = "Serum-naya"
	dynasty = 362
	religion = "western_hurrian_faith"
	culture = "haburi"
	752.11.20 = {
		birth = yes
	}
	804.4.19 = {
		death = yes
	}
}
371 = {
	name = "Ustan-sarri"
	dynasty = 371
	religion = "western_hurrian_faith"
	culture = "haburi"
	766.2.14 = {
		birth = yes
	}
	813.7.20 = {
		death = yes
	}
}
403 = {
	name = "Azzu-ka"
	dynasty = 403
	religion = "western_hurrian_faith"
	culture = "haburi"
	755.1.5 = {
		birth = yes
	}
	821.7.5 = {
		death = yes
	}
}
404 = {
	name = "Eni-ya"
	dynasty = 404
	religion = "western_hurrian_faith"
	culture = "haburi"
	769.4.25 = {
		birth = yes
	}
	838.10.12 = {
		death = yes
	}
}
793 = {
	name = "Samba-ri"
	dynasty = 793
	religion = "western_hurrian_faith"
	culture = "haburi"
	757.2.14 = {
		birth = yes
	}
	799.4.1 = {
		death = yes
	}
}
796 = {
	name = "Unap-se"
	dynasty = 796
	religion = "western_hurrian_faith"
	culture = "haburi"
	748.10.11 = {
		birth = yes
	}
	792.10.24 = {
		death = yes
	}
}
798 = {
	name = "Azzu-e"
	dynasty = 798
	religion = "western_hurrian_faith"
	culture = "haburi"
	768.11.22 = {
		birth = yes
	}
	810.5.7 = {
		death = yes
	}
}
799 = {
	name = "Elan-za"
	dynasty = 799
	religion = "western_hurrian_faith"
	culture = "haburi"
	755.10.10 = {
		birth = yes
	}
	817.4.19 = {
		death = yes
	}
}
844 = {
	name = "Aru-pal"
	dynasty = 844
	religion = "western_hurrian_faith"
	culture = "haburi"
	752.1.9 = {
		birth = yes
	}
	805.11.23 = {
		death = yes
	}
}
845 = {
	name = "Zika-n"
	dynasty = 845
	religion = "western_hurrian_faith"
	culture = "haburi"
	761.11.12 = {
		birth = yes
	}
	823.11.22 = {
		death = yes
	}
}
846 = {
	name = "Tai-ra"
	dynasty = 846
	religion = "western_hurrian_faith"
	culture = "haburi"
	764.9.7 = {
		birth = yes
	}
	828.1.1 = {
		death = yes
	}
}
847 = {
	name = "Ammin-na"
	dynasty = 847
	religion = "western_hurrian_faith"
	culture = "haburi"
	767.4.21 = {
		birth = yes
	}
	817.4.5 = {
		death = yes
	}
}
858 = {
	name = "Ehli"
	dynasty = 858
	religion = "western_hurrian_faith"
	culture = "haburi"
	760.1.13 = {
		birth = yes
	}
	825.5.7 = {
		death = yes
	}
}
859 = {
	name = "Illu-te"
	dynasty = 859
	religion = "western_hurrian_faith"
	culture = "haburi"
	769.3.22 = {
		birth = yes
	}
	830.10.22 = {
		death = yes
	}
}
860 = {
	name = "Kari-ta-n"
	dynasty = 860
	religion = "western_hurrian_faith"
	culture = "haburi"
	752.7.6 = {
		birth = yes
	}
	813.1.1 = {
		death = yes
	}
}
861 = {
	name = "Asa-kka"
	dynasty = 861
	religion = "western_hurrian_faith"
	culture = "haburi"
	755.1.23 = {
		birth = yes
	}
	798.11.23 = {
		death = yes
	}
}
862 = {
	name = "Kabi-pursa"
	dynasty = 862
	religion = "western_hurrian_faith"
	culture = "haburi"
	748.9.11 = {
		birth = yes
	}
	792.7.4 = {
		death = yes
	}
}
863 = {
	name = "Nupa-ta"
	dynasty = 863
	religion = "western_hurrian_faith"
	culture = "haburi"
	760.10.24 = {
		birth = yes
	}
	820.6.13 = {
		death = yes
	}
}
864 = {
	name = "Hazip-ulme"
	dynasty = 864
	religion = "western_hurrian_faith"
	culture = "haburi"
	756.5.24 = {
		birth = yes
	}
	805.6.8 = {
		death = yes
	}
}
969 = {
	name = "Kannu-kka"
	dynasty = 969
	religion = "western_hurrian_faith"
	culture = "haburi"
	1287.5.4 = {
		birth = yes
	}
	1348.10.18 = {
		death = yes
	}
}
970 = {
	name = "Tis_ata"
	dynasty = 970
	religion = "western_hurrian_faith"
	culture = "haburi"
	1280.4.2 = {
		birth = yes
	}
	1322.11.20 = {
		death = yes
	}
}
971 = {
	name = "Azu-na-n"
	dynasty = 971
	religion = "western_hurrian_faith"
	culture = "haburi"
	1285.3.18 = {
		birth = yes
	}
	1340.8.22 = {
		death = yes
	}
}
972 = {
	name = "Kuppi-ya"
	dynasty = 972
	religion = "western_hurrian_faith"
	culture = "haburi"
	1270.6.15 = {
		birth = yes
	}
	1327.3.7 = {
		death = yes
	}
}
974 = {
	name = "Azzu-eli"
	dynasty = 974
	religion = "western_hurrian_faith"
	culture = "haburi"
	1287.5.15 = {
		birth = yes
	}
	1348.4.7 = {
		death = yes
	}
}
975 = {
	name = "Ari-ab"
	dynasty = 975
	religion = "western_hurrian_faith"
	culture = "haburi"
	1283.8.11 = {
		birth = yes
	}
	1344.3.23 = {
		death = yes
	}
}
976 = {
	name = "Sattu-ri"
	dynasty = 976
	religion = "western_hurrian_faith"
	culture = "haburi"
	1288.10.13 = {
		birth = yes
	}
	1331.3.16 = {
		death = yes
	}
}
977 = {
	name = "Arip-hurmis"
	dynasty = 977
	religion = "western_hurrian_faith"
	culture = "haburi"
	1280.7.9 = {
		birth = yes
	}
	1321.6.7 = {
		death = yes
	}
}
1227 = {
	name = "Kutti"
	dynasty = 1227
	religion = "western_hurrian_faith"
	culture = "haburi"
	1284.4.19 = {
		birth = yes
	}
	1332.8.25 = {
		death = yes
	}
}
1229 = {
	name = "Kuza-ri-na"
	dynasty = 1229
	religion = "western_hurrian_faith"
	culture = "haburi"
	1273.11.5 = {
		birth = yes
	}
	1315.7.8 = {
		death = yes
	}
}
1256 = {
	name = "Anis-kipa-l"
	dynasty = 1256
	religion = "western_hurrian_faith"
	culture = "haburi"
	1276.7.27 = {
		birth = yes
	}
	1327.5.12 = {
		death = yes
	}
}
1269 = {
	name = "Taku-na"
	dynasty = 1269
	religion = "western_hurrian_faith"
	culture = "haburi"
	1271.8.26 = {
		birth = yes
	}
	1321.11.11 = {
		death = yes
	}
}
1277 = {
	name = "Zitu-ya"
	dynasty = 1277
	religion = "western_hurrian_faith"
	culture = "haburi"
	1293.10.19 = {
		birth = yes
	}
	1355.2.8 = {
		death = yes
	}
}
1278 = {
	name = "Awis-na"
	dynasty = 1278
	religion = "western_hurrian_faith"
	culture = "haburi"
	1277.1.9 = {
		birth = yes
	}
	1344.6.22 = {
		death = yes
	}
}
1279 = {
	name = "Iku-za"
	dynasty = 1279
	religion = "western_hurrian_faith"
	culture = "haburi"
	1291.1.6 = {
		birth = yes
	}
	1342.11.4 = {
		death = yes
	}
}
1280 = {
	name = "Ziza-pa-n"
	dynasty = 1280
	religion = "western_hurrian_faith"
	culture = "haburi"
	1286.3.3 = {
		birth = yes
	}
	1337.4.10 = {
		death = yes
	}
}
1645 = {
	name = "Tamar-zi"
	dynasty = 1645
	religion = "western_hurrian_faith"
	culture = "haburi"
	1270.9.22 = {
		birth = yes
	}
	1332.2.23 = {
		death = yes
	}
}
1646 = {
	name = "Unus-kiyazi"
	dynasty = 1646
	religion = "western_hurrian_faith"
	culture = "haburi"
	1284.6.18 = {
		birth = yes
	}
	1330.6.13 = {
		death = yes
	}
}
1650 = {
	name = "Asar-musni"
	dynasty = 1650
	religion = "western_hurrian_faith"
	culture = "haburi"
	1279.9.21 = {
		birth = yes
	}
	1331.3.27 = {
		death = yes
	}
}
1651 = {
	name = "Hari-pan"
	dynasty = 1651
	religion = "western_hurrian_faith"
	culture = "haburi"
	1289.6.19 = {
		birth = yes
	}
	1337.8.8 = {
		death = yes
	}
}
1697 = {
	name = "Sattawaza"
	dynasty = 1697
	religion = "western_hurrian_faith"
	culture = "haburi"
	1278.10.10 = {
		birth = yes
	}
	1323.3.6 = {
		death = yes
	}
}
1698 = {
	name = "Zaza-naya"
	dynasty = 1698
	religion = "western_hurrian_faith"
	culture = "haburi"
	1293.2.24 = {
		birth = yes
	}
	1346.9.3 = {
		death = yes
	}
}
1699 = {
	name = "Tuli-s"
	dynasty = 1699
	religion = "western_hurrian_faith"
	culture = "haburi"
	1292.4.8 = {
		birth = yes
	}
	1343.1.16 = {
		death = yes
	}
}
1700 = {
	name = "Ewe-ni"
	dynasty = 1700
	religion = "western_hurrian_faith"
	culture = "haburi"
	1285.3.13 = {
		birth = yes
	}
	1332.3.2 = {
		death = yes
	}
}
1701 = {
	name = "Kabi-ata"
	dynasty = 1701
	religion = "western_hurrian_faith"
	culture = "haburi"
	1281.11.19 = {
		birth = yes
	}
	1336.6.18 = {
		death = yes
	}
}
1703 = {
	name = "Elan-kiyazi"
	dynasty = 1703
	religion = "western_hurrian_faith"
	culture = "haburi"
	1291.10.18 = {
		birth = yes
	}
	1360.8.16 = {
		death = yes
	}
}
1720 = {
	name = "Tuza-na"
	dynasty = 1720
	religion = "western_hurrian_faith"
	culture = "haburi"
	1294.11.21 = {
		birth = yes
	}
	1363.11.27 = {
		death = yes
	}
}
1721 = {
	name = "Arim-adal"
	dynasty = 1721
	religion = "western_hurrian_faith"
	culture = "haburi"
	1270.4.17 = {
		birth = yes
	}
	1321.4.18 = {
		death = yes
	}
}
1722 = {
	name = "Ziza-pa-n"
	dynasty = 1722
	religion = "western_hurrian_faith"
	culture = "haburi"
	1279.9.6 = {
		birth = yes
	}
	1343.10.6 = {
		death = yes
	}
}
1723 = {
	name = "Senip-sarri"
	dynasty = 1723
	religion = "western_hurrian_faith"
	culture = "haburi"
	1274.6.12 = {
		birth = yes
	}
	1331.4.26 = {
		death = yes
	}
}
1724 = {
	name = "Sewa-ni"
	dynasty = 1724
	religion = "western_hurrian_faith"
	culture = "haburi"
	1293.8.3 = {
		birth = yes
	}
	1353.4.12 = {
		death = yes
	}
}