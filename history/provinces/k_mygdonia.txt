﻿#k_mygdonia
##d_volvi ###################################
###c_volvi
1871 = { #Apollonia
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2090 = { #Ardameri
	holding = none
}
2087 = { #Stivos
	holding = none
}
###c_kalindoia
1870 = { #Riza
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1868 = { #Stanos
	holding = none
}
###c_arethousa
2096 = { #Arethousa
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2097 = { #Asprovalta
	holding = none
}
2095 = { #Stephanina
	holding = none
}
###c_limaneia
2094 = { #Limaneia
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2103 = { #Lagkadi
	holding = none
}
2086 = { #Askos
	holding = none
}
##d_koroneia
###c_koroneia
2083 = { #Ossa
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2078 = { #Karteres
	holding = none
}
2081 = { #Rizana
	holding = none
}
2085 = { #Areti
	holding = none
}
###c_pedino
2072 = { #Pedino
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2068 = { #Vathi
	holding = none
}
2069 = { #Eptalophos
	holding = none
}
###c_lete
2077 = { #Lete
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2073 = { #Gallikos
	holding = none
}
2088 = { #Vasileios
	holding = none
}
##d_bisaltia
###c_serrai
2161 = { #Serrai
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2160 = { #Khryso
	holding = none
}
2109 = { #Skoutari
	holding = none
}
###c_arrolos
2105 = { #Arrolos
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2104 = { #Berge
	holding = none
}
2107 = { #Anagennisi
	holding = none
}
###c_skotoussa
2163 = { #Skotoussa
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2170 = { #Thermopigi
	holding = none
}
2162 = { #Lephkonas
	holding = none
}