﻿#k_makedon
##d_almopia ###################################
###c_europos
1817 = { #Europos
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1819 = { #Rizochori
	holding = none
}
1824 = { #Phoustani
	holding = none
}
###c_promachi
1821 = { #Promachi
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1822 = { #Sarakinoi
	holding = none
}
1820 = { #Voreino
	holding = none
}
###c_apsalos
1818 = { #Apsalos
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1982 = { #Aridaia
	holding = none
}
1823 = { #Agras
	holding = none
}
1984 = { #Karydia
	holding = none
}
##d_amphaxitis
###c_moryllos
2063 = { #Moryllos
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2057 = { #Aspros
	holding = none
}
2062 = { #Mesaio
	holding = none
}
2059 = { #Euzonoi
	holding = none
}
###c_gephyra
2054 = { #Gephyra
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1806 = { #Kymina
	holding = none
}
1828 = { #Kalokhori
	holding = none
}
###c_ikhnai
2053 = { #Ikhnai
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2055 = { #Europos
	holding = none
}
2056 = { #Polykastro
	holding = none
}
##d_dojran
###c_bragylai
2064 = { #Iliolousto
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2060 = { #Eiriniko
	holding = none
}
2065 = { #Doirani
	holding = none
}
2202 = { #Mordi
	holding = none
}
###c_euippe
2655 = { #Euippe
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2061 = { #Gortyna
	holding = none
}
2656 = { #Alabanda
	holding = none
}
2658 = { #Hyllarima
	holding = none
}
2654 = { #Hypaipa
	holding = none
}
2653 = { #Koloe
	holding = none
}
###c_orbelos
2165 = { #Irakleia
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
2167 = { #Kerkini
	holding = none
} 
2169 = { #Mouries
	holding = none
}
2164 = { #Lithotopos
	holding = none
}
##d_loudias
###c_methone
1803 = { #Methone
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1805 = { #Kleidi
	holding = none
}
1811 = { #Aloros
	holding = none
}
1974 = { #Vordori
	holding = none
}
###c_aigai
1975 = { #Ostrado
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1816 = { #Naousa
	holding = none
}
1827 = { #Lefkopetra
	holding = none
}
###c_mieza
1812 = { #Dovras
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1814 = { #Kali
	holding = none
}
1813 = { #Mandalo
	holding = none
}
1534 = { #Onsitromo
	holding = none
}
###c_pella
1808 = { #Pella
	culture = bryges
	religion = thracian_faith
	holding = tribal_holding
}
1427 = { #Sumze
	holding = none
}
1810 = { #Kyrros
	holding = none
}