﻿#k_lullubum
##d_kakmum ###################################
###c_kakmum
5500 = { #Dabibu
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5541 = { #Admûmu
	holding = none
}
5542 = { #Dalqu
	holding = none
}
5493 = { #Hutennu
	holding = none
}
5499 = { #Erishu
	holding = city_holding
}
5538 = { #Itati
	holding = none
}
###c_laglagu
5492 = { #Shulmi
	culture = lullubi
	religion = lullubi_faith
	holding = castle_holding
}
5496 = { #Humut
	holding = none
}
5498 = { #Gazalu
	holding = none
}
5497 = { #Kakmum
	holding = none
}
5495 = { #Ibilu
	holding = city_holding
}
##d_zamua
###c_arragdi
5634 = { #Arragdi
	culture = lullubi
	religion = lullubi_faith
	holding = castle_holding
}
5635 = { #Azari
	holding = none
}
5633 = { #Gilâmu
	holding = city_holding
}
5631 = { #Dastu
	holding = none
}
6453 = { #Mudi
	holding = none
}
5636 = { #Bâsish
	holding = none
}
###c_arzuhina
5484 = { #Arzuhina
	culture = tukri
	religion = eastern_hurrian_faith
	holding = castle_holding
}
5469 = { #Shugitu
	holding = none
}
5483 = { #Labbunu
	holding = city_holding
}
5632 = { #Shêtu
	holding = none
}
5485 = { #Dur-Atanate
	holding = none
}
5630 = { #Egru
	holding = none
}
###c_babiti
5489 = { #Babiti
	culture = lullubi
	religion = lullubi_faith
	holding = castle_holding
}
6452 = { #Ekishnugal
	holding = none
}
5494 = { #Laglagu
	holding = none
}
5490 = { #Karrishtum
	holding = none
}
5491 = { #Ishku
	holding = city_holding
}
##d_kanara
###c_kanara
5638 = { #Kanara
	culture = lullubi
	religion = lullubi_faith
	holding = castle_holding
}
5637 = { #Elmêshu
	holding = none
}
5639 = { #Atlila
	holding = city_holding
}
5640 = { #Kupru
	holding = none
}
5641 = { #Pîlânish
	holding = none
}
###c_luluban
6201 = { #Luluban
	culture = lullubi
	religion = lullubi_faith
	holding = castle_holding
}
5650 = { #Pettu
	holding = city_holding
}
5642 = { #Rîbu
	holding = church_holding
}
5649 = { #Dibiru
	holding = castle_holding
}
5647 = { #Adbaru
	holding = city_holding
}
5646 = { #Billu
	holding = castle_holding
}
###c_dushu
5644 = { #Dushû
	culture = lullubi
	religion = lullubi_faith
	holding = tribal_holding
}
5643 = { #Sâpinu
	holding = none
}
5645 = { #Didallu
	holding = none
}