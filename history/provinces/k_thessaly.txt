﻿#k_thessaly
##d_lamia ###################################
###c_lamia
376 = { #Lamia
	culture = dryopes
	religion = aegean_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
	}
}
378 = { #Anthili
	holding = city_holding
}
377 = { #Kostalexis
	holding = church_holding
}
634 = { #Phalara
	holding = city_holding
}
534 = { #Stirfaka
	holding = castle_holding
}
###c_ainis
532 = { #Ptelea
	culture = dryopes
	religion = aegean_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
	}
}
530 = { #Rovoliari
	holding = none
}
531 = { #Levkas
	holding = none
}
533 = { #Fteri
	holding = city_holding
}
###c_peleos
548 = { #Peleos
	culture = athamanes
	religion = aegean_faith
	holding = tribal_holding
	
}
381 = { #Ekhinos
	holding = none
}
547 = { #Nies
	holding = none
}
##d_iolkos
###c_iolkos
555 = { #Iolkos
	culture = athamanes
	religion = aegean_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
	}
}
619 = { #Agria
	holding = city_holding
}
565 = { #Sesklo
	holding = church_holding
}
554 = { #Pagasai
	holding = city_holding
}
###c_glaphyrai
564 = { #Glaphyrai
	culture = athamanes
	religion = aegean_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
		holding = castle_holding
	}
}
573 = { #Bouthoe
	holding = none
}
574 = { #Agnanteri
	holding = none
}
589 = { #Melia
	holding = none
}
593 = { #Kileler
	holding = none
	
	1310.1.1 = {
		holding = city_holding
	}
}
##d_magnesia
###c_magnesia
557 = { #Korope
	culture = athamanes
	religion = aegean_faith
	holding = tribal_holding
}
560 = { #Trikeri
	holding = none
}
561 = { #Anri
	holding = none
}
###c_dolopes
1043 = { #Preparethos
	culture = tyrsenian
	religion = aegean_faith
	holding = tribal_holding
}
1042 = { #Skiathos
	holding = none
}
1044 = { #Dolopes
	holding = none
}
###c_skyros
1045 = { #Skyros
	culture = tyrsenian
	religion = aegean_faith
	holding = tribal_holding
}
1046 = { #Kresion
	holding = none
}
##d_alos
###c_alos
551 = { #Pyrasos
	culture = athamanes
	religion = aegean_faith
	holding = castle_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
	}
}
543 = { #Neraida
	holding = city_holding
}
553 = { #Filaki
	holding = none
}
541 = { #Anavra
	holding = none
}
###c_xyniada
536 = { #Xyniada
	culture = athamanes
	religion = aegean_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
		religion = mycenean_faith
	}
}
535 = { #Perivoli
	holding = none
}
539 = { #Palamas
	holding = none
}
537 = { #Metallia
	holding = none
}
538 = { #Vouzion
	holding = none
}
###c_halos
546 = { #Halos
	culture = athamanes
	religion = aegean_faith
	holding = tribal_holding
	
}
545 = { #Almirou
	holding = none
}
##d_pharsalos
###c_pharsalos
586 = { #Pharsalos
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
587 = { #Thetideion
	holding = none
}
578 = { #Thaumakoi
	holding = none
}
576 = { #Asprogia
	holding = none
}
###c_iperia
601 = { #Iperia
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
603 = { #Fyllo
	holding = none
}
585 = { #Euydrion
	holding = none
}
###c_ortha
528 = { #Lampero
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
529 = { #Smoukovou
	holding = none
}
580 = { #Leontari
	holding = none
}
583 = { #Filia
	holding = none
}
##d_larissa
###c_larissa
618 = { #Larissa
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
622 = { #Ampelona
	holding = none
}
617 = { #Amygdalea
	holding = none
}
621 = { #Gyrtone
	holding = none
}
###c_anargiri
615 = { #Anargiri
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
616 = { #Argoura
	holding = none
}
600 = { #Krannon
	holding = none
}
599 = { #Zappio
	holding = none
}
###c_kondaia
623 = { #Deleria
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
624 = { #Argyro
	holding = none
}
625 = { #Gonnoi
	holding = none
}
###c_gyrtone
620 = { #Elateia
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
597 = { #Eleftherio
	holding = none
}
##d_hestiaiotis
###c_limnaion
604 = { #Limnaion
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
632 = { #Pedino
	holding = none
}
613 = { #Potamia
	holding = none
}
612 = { #Piniada
	holding = none
}
###c_pialeia
609 = { #Pialeia
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
639 = { #Phortosi
	holding = none
}
610 = { #Trikke
	holding = none
}
608 = { #Dimos
	holding = none
}
###c_ithome
607 = { #Ithome
	culture = proto_greek
	religion = proto_hellenic_faith
	holding = tribal_holding
	
	1310.1.1 = {
		culture = aeolian
	}
}
527 = { #Kalyvia
	holding = none
}
606 = { #Proastio
	holding = none
}
582 = { #Kedros
	holding = none
}